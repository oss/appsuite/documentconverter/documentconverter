###################################################################################################
# documentconverter build stage for project and project specific JRE
###################################################################################################
FROM        registry-proxy.sre.cloud.oxoe.io/library/eclipse-temurin:21 AS builder-stage

# Copy relevant project sources
COPY        build.gradle ./
COPY        settings.gradle ./
COPY        gradlew ./
COPY        gradle ./gradle
COPY        src ./src
COPY        lib ./lib
COPY        nativeLib ./nativeLib

# Build project
# TODO runtime results in segv in docker image - no idea why!
# RUN         ./gradlew --parallel --max-workers 4 runtime

RUN         ./gradlew --parallel --max-workers 4 bootJar
# TODO NEXT Are the following lines required? Kai?
RUN         useradd -ms /bin/bash nonroot
USER        nonroot

###################################################################################################
# XRechnung
###################################################################################################
FROM        registry.open-xchange.com/appsuite-core-internal/documentconverter-xrechnung:main AS xrechnung

###################################################################################################
# documentconverter service stage, using JRE and project builds from build stage
###################################################################################################
# TODO NEXT Distroless ? Evn Vars...
FROM        registry-proxy.sre.cloud.oxoe.io/library/debian:bookworm-slim

ENV         PROJECT_HOME /opt/open-xchange

ENV         LANG C.UTF-8
ENV         LANGUAGE C.UTF-8
ENV         LC_ALL C.UTF-8

ARG         APP_VERSION
ARG         BUILD_TIMESTAMP
ARG         CI_COMMIT_SHA

ENV         APP_VERSION=$APP_VERSION
ENV         BUILD_TIMESTAMP=$BUILD_TIMESTAMP
ENV         CI_COMMIT_SHA=$CI_COMMIT_SHA

ARG         APT_TARGET_LIST=/etc/apt/sources.list.d/ox-documentconverter.list

RUN         echo "deb [trusted=yes] http://packages.docs.open-xchange.com/readerengine-main/DebianBullseye/ /" >> ${APT_TARGET_LIST} && \
            echo "deb [trusted=yes] http://packages.docs.open-xchange.com/open-xchange-pdftool-main/DebianBullseye/ /" >> ${APT_TARGET_LIST} && \
            apt-get update && apt-get -y upgrade && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
                apt-utils \
                bash \
                ca-certificates \
                coreutils \
                gnupg2 \
                locales \
                open-xchange-pdftool \
                readerengine \
                procps \
                sed \
                tini \
                tzdata && \
            echo "deb https://deb.debian.org/debian testing main" >> ${APT_TARGET_LIST} && \
            apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
                openjdk-21-jre-headless && \
            rm -rf /var/lib/apt/lists/* && \
            echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
            locale-gen en_US.UTF-8

COPY        --from=builder-stage build/libs/documentconverter-1.0.0.jar/ ${PROJECT_HOME}/
# Copy for remote debug
#COPY        --from=builder-stage /opt/java/openjdk/lib/libjdwp.so ${PROJECT_HOME}/lib
#COPY        --from=builder-stage /opt/java/openjdk/lib/libdt_socket.so ${PROJECT_HOME}/lib
COPY        --from=builder-stage nativeLib/ ${PROJECT_HOME}/lib/

# Copy configuration files
COPY        conf/documentconverter.properties ${PROJECT_HOME}/etc/
COPY        src/main/resources/logback.xml ${PROJECT_HOME}/etc/

COPY        --from=xrechnung /xrechnung-visualization ${PROJECT_HOME}/xrechnung-visualization

RUN         groupadd -r -g 2000 open-xchange && \
            useradd -r -g open-xchange -u 987 open-xchange

RUN         chown -R open-xchange:open-xchange /opt/open-xchange
RUN         mkdir -p /var/log/open-xchange && chown -R open-xchange:open-xchange /var/log/open-xchange && \
            mkdir -p /var/spool/open-xchange && chown -R open-xchange:open-xchange /var/spool/open-xchange && \
            mkdir -p /var/opt && chown -R open-xchange:open-xchange /var/opt

USER        open-xchange:open-xchange

WORKDIR     /opt/open-xchange

ENV         JAVA_TOOL_OPTIONS=" \
                -Djava.library.path=${PROJECT_HOME}/lib \
                -Duser.timezone=UTC \
                -Xms${CS_JVM_HEAP_SIZE_MB:-1024}m \
                -Xmx${CS_JVM_HEAP_SIZE_MB:-1024}m"

ENTRYPOINT  [ "/usr/bin/tini", "-g", "--", "/bin/sh", "-c", \
              "java -jar ${PROJECT_HOME}/documentconverter-1.0.0.jar \
                --spring.profiles.active=prod \
                --spring.config.additional-location=${PROJECT_HOME}/etc/documentconverter.properties \
                --logging.config=${PROJECT_HOME}/etc/logback.xml" \
            ]
EXPOSE      8008 8011 8017
