# DocumentConverter NextGen

## Abstract
The DocumentConverter (DC) service is a microservice, responsible for conversion of arbitrary document formats into other, generalized document file formats like PDF
or images for dedicated document pages for thumbnailing purposes.

The number of supported source document formats is very broad and includes 'modern' XML document formats (e.g. *.docx, *.xlxs, *.pptx, *.odf, *.ods, *.odp, ...)
as well as deprecated binary document formats (e.g. *.doc, *.ppt and *.xls).

In addition, many other common formats like *.csv etc. are supported to be converted to standard viewing formats like PDF and image thumbnails for previewing purposes.

Beside this standard conversion functionality, that other products like e.g. Collabora offer as well, an appropriate infrastructure has been incorporated
within the DC service and DC client to optimize performance by using extensive caching techniques as well as sophisticated error handling.

The current DC service is an established microservice, used by the AppSuite frontend via usage of DC client functionality integrated within the current
AppSuite middleware, but due to its nature as a standalone microservice, functionality is not restricted to this use case alone.

## Technical background and motivation to create a DocumentConverter NextGen microservice
The existing DC has been a standalone Microservice for a long time, the underlying framework used consisted of large parts of the OSGi
middleware stack for historical reasons, although only some essenial core functionality like consistent usage of config cascade, consistent logging,
usage of Grizzly Webservice etc. has been used.

This design led to constant maintenance overhead and the undesired neccessity to depend on servives like Hazelcast or Redis, not needed
for the DC functionality istself.

In addition, these indirect dependencies led to constant maintenance overhead for DC maintainers as well as middleware stack developers as well.
Examples in this case are adjustments to the used library baseline, which needed to be be done within DC codebase as well just to let the service start.

To overcome this maintenance overhead and to put pressure off the admins to configure depending services and configuration not really needed, the DC service has been
refactored at a larger scale to become the DocumentConverter NextGen (DC Next) service.

This new service incorporates most inner procedures and logic but is based on the industrial grade SpringBoot library, not needing any of the previous overhead anymore.

## Detailed changes
### Improved performance and lower network traffic by using shortest CacheService paths when possible
Previously all caching has been initiated and done by DC server side, so that there was an overhead of network traffic when e.g. getting an existing result from CacheService (CS).

With DC Next we took the chance to optimize this by adding a CS Client to the DC client side as well, allowing us to optimize all CS traffic
routes to their shortest possible ways, saving large amounts of network traffic from.

Example: 'DC client => DC server => CS server => DC server => DC client' traffic is now reduced to 'DC client => CS server => DC client' traffic of maybe large result files.

Necessary code change, already done: the DC client on middleware side needed an additional config property to specify the URL/loocation of CS server to use, if wanted.

To illustrate the detailed, optimized workflow the following diagram shows the general workflow for conversion of documents:
![Conversion process workflow](README_CONVERSION.png)

The following diagram shows the workflow for thumbnail generation:
![Thumbnail generation workflow](README_PREVIEW.png)

### Java API changes
All communication from middleware to DC server is done by using the DC client on middleware side.
There's no change in existing Java API on DC client/middleware side so that functionality is available as before for users of DC client.

### HTTP API changes
- There's no change for HTTP API on DC client side so that frontend to middleware calls need no adjustment at all.
- DC server HTTP API has been cleaned up to be used by DC client. For 8.x there are no other consumers of DC server HTTP API beside DC client known yet so that all adjustments could be done on DC client side alone.
- If previously existing DC server HTTP API functionality will be missed otherwise, we'd need to check such requests and adjust the HTTP API accordingly. Goal for the moment was to start with a clean and fresh DC server HTTP API and to only support the functionality that is really needed.

## Dependencies
The Documentconverter references most external dependencies from the gradle.build file. There are the following dependencies which are directly part of the git repository as these files are required in a specific version and are not available in maven repositories.

- LibreOffice Java UNO runtime
  - commonwizards.jar
  - form.jar
  - hsqldb.jar
  - java_uno.jar
  - juh.jar
  - jurt.jar
  - officebean.jar
  - query.jar
  - report.jar
  - ridl.jar
  - sdc_hsqldb.jar
  - table.jar
  - unoil.jar
  - unoloader.jar
  - xmerge.jar
- various other libraries
  - juniversalchardet-2.4.0.jar

## Request
Curl examples to test the Document Converter. You have to change the formfield `sourcefile`.

### Return as JSON
```console
curl -i -F "jobtype=pdf" -F "returntype=json" -F "sourcefile=@/home/xyz/test.txt" localhost:8080/documentconverterws
```
```console
curl -i -F "jobtype=image" -F "returntype=json" -F "sourcefile=@/home/york/Desktop/standup.md" localhost:8080/documentconverterws
```

### Return a file
#### PDF
```console
curl -O -J  -F "jobtype=pdf" -F "returntype=file" -F "sourcefile=@/home/york/Desktop/standup.md" localhost:8080/documentconverterws
```
#### Image / JPEG
```console
curl -O -J  -F "jobtype=image" -F "returntype=file" -F "sourcefile=@/home/york/Desktop/standup.md" localhost:8080/documentconverterws
```
Change the size of the image
```console
curl -O -J  -F "jobtype=image" -F "returntype=file" -F="pixelwidth=100" -F "pixelheight=100"  -F "sourcefile=@/home/york/Desktop/standup.md" localhost:8080/documentconverterws
```

### Return a Binary File
```console
curl -O -J -F "jobtype=image" -F "returntype=binary" -F "sourcefile=@/home/york/Desktop/standup.md" localhost:8080/documentconverterws
```

## Software change requests
- SCR-1492
- SCR-1494
