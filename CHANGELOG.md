# App Suite DocumentConverter

All notable changes to this project will be documented in this file.


## [8.34] - 2025-01-16

### Changed

- [`FCV-1`](https://jira.open-xchange.com/browse/FCV-1): Introducing DocumentConverter NextGen [`a45a3070`](https://gitlab.open-xchange.com/documents/documentconverter/commit/a45a3070e84badd1315291cd1cbbddb3b88530e6)
- [`SCR-1492`](https://jira.open-xchange.com/browse/SCR-1492): Setting CS Url for documentconverter-client [`4744738e`](https://gitlab.open-xchange.com/appsuite/platform/core/commit/4744738e800a304461ca8380ca1ce594d78fdc6f)
- [`SCR-1494`](https://jira.open-xchange.com/browse/SCR-1494): Introducing DocumentConverter NextGen [`a45a3070`](https://gitlab.open-xchange.com/documents/documentconverter/commit/a45a3070e84badd1315291cd1cbbddb3b88530e6)

<!-- References -->
[8.34]: https://gitlab.open-xchange.com/documents/documentconverter/-/compare/8.33.0...8.34.0
