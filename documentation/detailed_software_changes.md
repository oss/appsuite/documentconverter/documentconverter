---
title: Detailed software changes
icon: fa fa-info-circle
classes: no-counting
---

This page contains detailed information about software changes.


[8.34]
======

Configuration
-------------

### SCR-1492
**Summary**: New Helm chart property to specify DocumentConverter client CacheService URL

A new Helm chart property for core-mw allows to specify if CacheService should be used by DocumentConverter client (Default: false). If enabled, either the cluster internal CacheService URL is used or could be overwritten via optional value as well:

```
core-mw:
  ## -- Configuration for DocumentConverter client
  documentConverterClient:
    cache:
      remoteCache:
        enabled: false
        # url: https://optional-cacheservice-host:8001/cache
```

### SCR-1494
**Summary**: Changing Helm stackchert default for core-cacheservice.enabled from `false` to `true`

**Releases <= 8.33:** Deployment and usage of CacheService may be disabled by default:

```
core-cacheservice:
 enabled: false
```

If enabled, the DocumentConverter has to be configured to use it. An optional external URL might be specified as well:

```
core-documentconverter:
  documentConverter:
    cache:
      remoteCache:
        enabled: false
        # url: https://optional-cache-host:8001/cache
```

**Releases >= 8.34:** CacheService and its usage by Middleware and DocumentConverter is enabled by default. It can be disabled with the known property:

```
core-cacheservice:
 enabled: true
```

Middleware and DocumentConverter will adapt their behaviour accordingly. If CacheService is enabled it will be used, if disabled not. If required, Middleware and DocumentConverter usage of CacheService can be overridden and enabled / disabled independently of the high level configuration of the CacheService deployment:

```
core-documentconverter:
  documentConverter:
    cache:
      remoteCache:
        enabled: true
        # url: https://optional-cache-host:8001/cache

core-mw:
  documentConverterClient:
    cache:
      remoteCache:
        enabled: true
        # url: https://optional-cache-host:8001/cache
```

The configuration of CacheService database and object storage to use is unchanged. The DocumentConverter cache constraints are taken from DocumentConverter server configuration.

[8.33]
======


-[8.34]: https://gitlab.open-xchange.com/documents/documentconverter/-/compare/8.33.0...8.34.0
