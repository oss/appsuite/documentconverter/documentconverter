#!/bin/bash -e

export XRECHNUNG_IMAGE=registry.open-xchange.com/appsuite-core-internal/documentconverter-xrechnung:main

PRJ_ROOT_DIR=$(cd `dirname $0` && cd .. && pwd)
cd $PRJ_ROOT_DIR

mkdir -p $PRJ_ROOT_DIR/xrechnung-visualization
rm -rf $PRJ_ROOT_DIR/xrechnung-visualization/*

docker cp $(docker create --name xrechnung_tmp_name $XRECHNUNG_IMAGE):/xrechnung-visualization/ $PRJ_ROOT_DIR && docker rm xrechnung_tmp_name
