#!/bin/bash -e

export XRECHNUNG_IMAGE=registry.open-xchange.com/appsuite-core-internal/documentconverter-xrechnung:main

PRJ_ROOT_DIR=$(cd `dirname $0` && cd .. && pwd)
cd $PRJ_ROOT_DIR

tar czf OpenXRechnungToolbox.tar.gz -C ./xrechnung-visualization .
docker build -t $XRECHNUNG_IMAGE -f $PRJ_ROOT_DIR/scripts/dockerfile-xrechnung-image .
rm ./OpenXRechnungToolbox.tar.gz
docker push $XRECHNUNG_IMAGE
