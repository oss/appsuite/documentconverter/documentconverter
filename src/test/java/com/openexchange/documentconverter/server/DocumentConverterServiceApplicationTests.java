/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.logging.DocumentConverterJsonLayout;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.rest.DocumentConverterController;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
class DocumentConverterServiceApplicationTests {

    @Autowired
    private DocumentConverterController documentConverterController;

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ServerManager serverManager;

    @Autowired
    private ConverterJobFactory converterJobFactory;

    @Autowired
    private ManagedJobFactory managedJobFactory;

    @Autowired
    private MetricsRegistry metricsRegistry;

    @Autowired
    private Statistics statistics;

    @Autowired
    private ErrorCache errorCache;

    @Autowired
    private Cache cache;

    @Autowired
    private JobProcessor jobProcessor;

    @AfterEach
    void tearDown() {
        jobProcessor.shutdown();
        cache.shutdown();
    }

    @Test
    void contextLoads() {
        var layout = new DocumentConverterJsonLayout();
        assertNotNull(layout);
    }

}
