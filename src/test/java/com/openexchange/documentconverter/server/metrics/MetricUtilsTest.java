/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.metrics;

import com.openexchange.documentconverter.server.job.Job;
import com.openexchange.documentconverter.server.job.JobPriority;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MetricUtilsTest {

    @Test
    void testPriorityNotInList() {
        Map<JobPriority, ArrayList<AtomicLong>> timesMap = new EnumMap<>(JobPriority.class);
        var times = new ArrayList<AtomicLong>();
        times.add(new AtomicLong(10));
        times.add(new AtomicLong(10));
        times.add(new AtomicLong(10));
        times.add(new AtomicLong(10));
        timesMap.put(JobPriority.LOW, times);

        var result = MetricsUtils.getMedianTimeMillis(timesMap, JobPriority.HIGH);

        assertEquals(0.0, result);
    }

    @Test
    void testPriorityInList() {
        Map<JobPriority, ArrayList<AtomicLong>> timesMap = new EnumMap<>(JobPriority.class);
        var times = new ArrayList<AtomicLong>();
        times.add(new AtomicLong(10));
        times.add(new AtomicLong(10));
        times.add(new AtomicLong(20));
        times.add(new AtomicLong(20));
        timesMap.put(JobPriority.LOW, times);

        var result = MetricsUtils.getMedianTimeMillis(timesMap, JobPriority.LOW);

        assertEquals(15.0, result);
    }

    @Test
    void testPriorityInListWithMultiplePriorities() {
        Map<JobPriority, ArrayList<AtomicLong>> timesMap = new EnumMap<>(JobPriority.class);
        var timesLow = new ArrayList<AtomicLong>();
        timesLow.add(new AtomicLong(10));
        timesLow.add(new AtomicLong(10));
        timesLow.add(new AtomicLong(20));
        timesLow.add(new AtomicLong(20));
        timesMap.put(JobPriority.LOW, timesLow);

        var timesHigh = new ArrayList<AtomicLong>();
        timesHigh.add(new AtomicLong(13));
        timesHigh.add(new AtomicLong(17));
        timesHigh.add(new AtomicLong(1));
        timesHigh.add(new AtomicLong(20));
        timesHigh.add(new AtomicLong(22));

        timesMap.put(JobPriority.HIGH, timesHigh);

        var result = MetricsUtils.getMedianTimeMillis(timesMap, JobPriority.LOW);

        assertEquals(15.0, result);

        result = MetricsUtils.getMedianTimeMillis(timesMap, JobPriority.HIGH);

        assertEquals(17.0, result);
    }

    @Test
    void testWithoutPriorityWithMultiplePriorities() {
        Map<JobPriority, ArrayList<AtomicLong>> timesMap = new EnumMap<>(JobPriority.class);
        for (var jobPriority : JobPriority.values()) {
            timesMap.put(jobPriority, new ArrayList<>());
        }
        var timesLow = new ArrayList<AtomicLong>();
        timesLow.add(new AtomicLong(10));
        timesLow.add(new AtomicLong(10));
        timesLow.add(new AtomicLong(200));
        timesLow.add(new AtomicLong(20));
        timesMap.put(JobPriority.LOW, timesLow);

        var timesHigh = new ArrayList<AtomicLong>();
        timesHigh.add(new AtomicLong(13));
        timesHigh.add(new AtomicLong(15));
        timesHigh.add(new AtomicLong(1));
        timesHigh.add(new AtomicLong(20));
        timesHigh.add(new AtomicLong(22));

        timesMap.put(JobPriority.HIGH, timesHigh);

        var result = MetricsUtils.getMedianTimeMillis(timesMap, null);

        assertEquals(15.0, result);

    }
}
