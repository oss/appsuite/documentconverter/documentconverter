/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.metrics;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ManagedJob;
import com.openexchange.documentconverter.server.converterJob.DocumentConverterJob;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.*;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import lombok.NonNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class StatisticTest {

    private static final String JOBID = "dummyJobId";

    private Statistics sut;

    @Spy
    private MetricsRegistry metricsRegistry;

    private static List<AbstractMap.SimpleEntry<String, TagsType>> metrics = new ArrayList<>();


    private static AbstractMap.SimpleEntry<String, TagsType> getMetricsEntry(String key, TagsType type) {
        return new AbstractMap.SimpleEntry<String, TagsType>(key, type);
    }

    @BeforeAll
    public static void setup() {
        metrics.add(getMetricsEntry("documentconverter.jobs.processed.total", TagsType.JOBS));
        metrics.add(getMetricsEntry("documentconverter.jobs.processed.error.timeout.total", TagsType.JOBS));
        metrics.add(getMetricsEntry("documentconverter.jobs.processed.error.total", TagsType.JOBS));

        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_BACKGROUND));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_LOW));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_MEDIUM));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_HIGH));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_INSTANT));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time", TagsType.PRIO_ALL));

        metrics.add(getMetricsEntry("documentconverter.jobs.stateful.pending.count", TagsType.JOBS));
        metrics.add(getMetricsEntry("documentconverter.jobs.scheduled.total", TagsType.JOBS));

        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_BACKGROUND));
        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_LOW));
        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_MEDIUM));
        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_HIGH));
        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_INSTANT));
        metrics.add(getMetricsEntry("documentconverter.jobs.peak.count", TagsType.PRIO_ALL));

        metrics.add(getMetricsEntry("documentconverter.jobs.asyncqueue.scheduled.total", TagsType.JOBS_ASYNC));
        metrics.add(getMetricsEntry("documentconverter.jobs.asyncqueue.processed.total", TagsType.JOBS_ASYNC));
        metrics.add(getMetricsEntry("documentconverter.jobs.asyncqueue.dropped.total", TagsType.JOBS_ASYNC));
        metrics.add(getMetricsEntry("documentconverter.jobs.asyncqueue.peak.total", TagsType.QUEUE_ASYNC));

        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_BACKGROUND));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_LOW));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_MEDIUM));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_HIGH));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_INSTANT));
        metrics.add(getMetricsEntry("documentconverter.jobs.median.time.waiting", TagsType.PRIO_ALL));

        metrics.add(getMetricsEntry("documentconverter.cache.free.volume.size", TagsType.CACHE));
        metrics.add(getMetricsEntry("documentconverter.cache.hit.ratio", TagsType.CACHE));

        metrics.add(getMetricsEntry("documentconverter.errorcache.hit.ratio", TagsType.CACHE));

        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.and.pdftool.total", TagsType.CONVERSIONS));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.total", TagsType.CONVERSIONS));
        metrics.add(getMetricsEntry("documentconverter.conversions.pdftool.total", TagsType.CONVERSIONS));
        metrics.add(getMetricsEntry("documentconverter.conversions.needed.new.readerengine.total", TagsType.CONVERSIONS));

        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_GENERAL));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_TIMEOUT));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_DISPOSED));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_PASSWORD));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_NO_CONTENT));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.error.total", TagsType.ERROR_OUT_OF_MEMORY));
        metrics.add(getMetricsEntry("documentconverter.conversions.pdftool.error.total", TagsType.CONVERSIONS));

        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_BACKGROUND));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_LOW));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_MEDIUM));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_HIGH));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_INSTANT));
        metrics.add(getMetricsEntry("documentconverter.conversions.readerengine.median.time", TagsType.PRIO_ALL));
    }

    @BeforeEach
    void initUnitTest() {
        metricsRegistry = new MetricsRegistry();

        Metrics.globalRegistry.clear();
        // Needed to test the gauge value
        Metrics.addRegistry(new SimpleMeterRegistry());

        sut = new Statistics();
        ReflectionTestUtils.setField(sut, "metricsRegistry", metricsRegistry);
        sut.init();
    }

    /**
     * Test if all metrics are registered.
     */
    @Test
    void registerMetrics() {
        var globalRegistry = Metrics.globalRegistry;
        assertEquals(metrics.size(), globalRegistry.getMeters().size());
        globalRegistry.clear();
        assertEquals(0, globalRegistry.getMeters().size());
        sut.registerMetrics();
        assertEquals(metrics.size(), globalRegistry.getMeters().size());

        for (var metricEntry : metrics) {
            assertNotNull(globalRegistry.find(metricEntry.getKey()).tags(metricEntry.getValue().getTags()).gauge());
        }
    }

    @Test
    void jobsProcessedTotal() {
        var gauge = findGauge("jobs.processed.total", TagsType.JOBS);
        var beforeValue = gauge.value();
        sut.incrementProcessedJobCount(new JobErrorEx(), true);
        assertEquals(beforeValue + 1, gauge.value());
    }

    @Test
    void jobsProcessedErrorTimeoutTotal() {
        var gauge = findGauge("jobs.processed.error.timeout.total", TagsType.JOBS);
        var beforeValue = gauge.value();
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.TIMEOUT), true);
        assertEquals(beforeValue + 1, gauge.value());
    }

    @Test
    void jobsProcessedErrorTotal() {
        var gauge = findGauge("jobs.processed.error.total", TagsType.JOBS);
        var beforeValue = gauge.value();
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.TIMEOUT), true);
        assertEquals(beforeValue + 1, gauge.value());
    }

    public static Stream<Arguments> jobsMedianTimeSource() {
        return Stream.of(
                Arguments.of(TagsType.PRIO_BACKGROUND, JobPriority.BACKGROUND, JobStatus.FINISHED),
                Arguments.of(TagsType.PRIO_LOW, JobPriority.LOW, JobStatus.FINISHED),
                Arguments.of(TagsType.PRIO_MEDIUM, JobPriority.MEDIUM, JobStatus.FINISHED),
                Arguments.of(TagsType.PRIO_HIGH, JobPriority.HIGH, JobStatus.FINISHED),
                Arguments.of(TagsType.PRIO_INSTANT, JobPriority.INSTANT, JobStatus.FINISHED),
                Arguments.of(TagsType.PRIO_ALL, JobPriority.HIGH, JobStatus.FINISHED)
        );
    }

    @ParameterizedTest
    @MethodSource("jobsMedianTimeSource")
    void jobsMedianTime(TagsType tagsType, JobPriority jobPriority, JobStatus jobStatus) {
        var gauge = findGauge("jobs.median.time", tagsType);
        var beforeValue = gauge.value();
        changeJobStatus(jobPriority, jobStatus);
        sut.resetMeasurementPeriod();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    void jobsStatefulPendingCount() {
        var gauge = findGauge("jobs.stateful.pending.count", TagsType.JOBS);
        var beforeValue = gauge.value();
        sut.incrementStatefulJobCount();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void jobsScheduledTotal() {
        var gauge = findGauge("jobs.scheduled.total", TagsType.JOBS);
        // FIXME value should be set from outside and directly provided by the statistics instance
        var beforeValue = gauge.value();
        //doReturn(1).when(jobMonitor).getScheduledCount();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    void testIfAllPrioritiesAreSet() {
        var jobPrioMaps = new ArrayList<Map<JobPriority, ?>>();
        jobPrioMaps.add(getJobPriorityMap("curQueueCounts"));
        jobPrioMaps.add(getJobPriorityMap("peakQueueCounts"));
        jobPrioMaps.add(getJobPriorityMap("peakQueueCountsResult"));

        jobPrioMaps.add(getJobPriorityMap("queueTimes"));
        jobPrioMaps.add(getJobPriorityMap("queueTimesResult"));

        jobPrioMaps.add(getJobPriorityMap("conversionTimes"));
        jobPrioMaps.add(getJobPriorityMap("conversionTimesResult"));

        jobPrioMaps.add(getJobPriorityMap("jobTimes"));
        jobPrioMaps.add(getJobPriorityMap("jobTimesResult"));

        for (var priority : JobPriority.values()) {
            for (var map : jobPrioMaps) {
                assertTrue(map.containsKey(priority));
            }
        }
    }

    @Test
    void testIfBackendTypesAreSet() {
        var conversionCounts = (Map<BackendType, AtomicLong>)ReflectionTestUtils.getField(sut, "conversionCounts");
        for (var backenType : BackendType.values()) {
            assertTrue(conversionCounts.containsKey(backenType));
        }
    }

    @Test
    void testIfJobErrorsAreSet() {
        var conversionResult = (Map<JobError, AtomicLong>)ReflectionTestUtils.getField(sut, "conversionResult");
        for (var jobError : JobError.values()) {
            assertTrue(conversionResult.containsKey(jobError));
        }
    }

    @Test
    void testGetCacheHitRatioJobsProcessedIsEmapt() {
        assertEquals(1.0, sut.getCacheHitRatio());
    }

    @Test
    void testGetCacheHitRatioJobsProcessdIsNotEmpty() {
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);
        assertEquals(0.0, sut.getCacheHitRatio());
    }

    @Test
    void testGetCacheHitRatioJobsProcessdAndCacheHitRatioIsNotEmpty() {
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);
        // increment cacheHits
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.NONE), true);
        assertEquals(0.5, sut.getCacheHitRatio());
    }

    @Test
    void testGetErrorCacheHits() {
        assertEquals(0, sut.getErrorCacheHits());

        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);
        assertEquals(0, sut.getErrorCacheHits());
        // increment cacheHits
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), true);
        assertEquals(1, sut.getErrorCacheHits());
    }

    @Test
    void testGetErrorCacheHitRatio() {
        assertEquals(0.0, sut.getErrorCacheHitRatio());

        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);

        assertEquals(0.0, sut.getErrorCacheHitRatio());

        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), true);
        assertEquals(0.5, sut.getErrorCacheHitRatio());

    }

    @Test
    void testGetPeakAsyncJobCount() {
        assertEquals(0, sut.getPeakAsyncJobCount());
        sut.incrementAsyncQueueCount();
        sut.resetMeasurementPeriod();
        assertEquals(1, sut.getPeakAsyncJobCount());
    }

    @Test
    void testGetPendingStatefulJobCount() {
        assertEquals(0, sut.getPendingStatefulJobCount());
        sut.incrementStatefulJobCount();
        assertEquals(1, sut.getPendingStatefulJobCount());
        sut.decrementStatefulJobCount();
        assertEquals(0, sut.getPendingStatefulJobCount());
    }

    @Test
    void testStatusChangedRemoved() {

        var curTotalQueueCount = (AtomicInteger) ReflectionTestUtils.getField(sut, "curTotalQueueCount");
        assertEquals(0, curTotalQueueCount.get());

        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.BACKGROUND, mock(ManagedJob.class));
        jobEntry.setJobStatus(JobStatus.REMOVED);
        sut.statusChanged(JOBID, jobEntry);
        assertEquals(0, curTotalQueueCount.get());

        jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.BACKGROUND, mock(ManagedJob.class));
        jobEntry.setJobStatus(JobStatus.SCHEDULED);
        sut.statusChanged(JOBID, jobEntry);

        assertEquals(1, curTotalQueueCount.get());

        jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.BACKGROUND, mock(ManagedJob.class));
        jobEntry.setJobStatus(JobStatus.REMOVED);
        sut.statusChanged(JOBID, jobEntry);
        assertEquals(0, curTotalQueueCount.get());
    }

    private Map<JobPriority, ?> getJobPriorityMap(String fieldname) {
        return (Map<JobPriority, ?>)ReflectionTestUtils.getField(sut, fieldname);
    }

    public static Stream<Arguments> jobsPeakCountSource() {
        return Stream.of(
                Arguments.of(TagsType.PRIO_BACKGROUND, JobPriority.BACKGROUND),
                Arguments.of(TagsType.PRIO_LOW, JobPriority.LOW),
                Arguments.of(TagsType.PRIO_MEDIUM, JobPriority.MEDIUM),
                Arguments.of(TagsType.PRIO_HIGH, JobPriority.HIGH),
                Arguments.of(TagsType.PRIO_INSTANT, JobPriority.INSTANT),
                Arguments.of(TagsType.PRIO_ALL, JobPriority.BACKGROUND)
        );
    }

    @ParameterizedTest
    @MethodSource("jobsPeakCountSource")
    void jobsPeakCount(TagsType tagsType, JobPriority jobPriority) {
        var gauge = findGauge("jobs.peak.count", tagsType);
        var beforeValue = gauge.value();
        changeJobStatus(jobPriority, JobStatus.SCHEDULED);
        sut.resetMeasurementPeriod();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void jobsAsyncqueueScheduledTotal() {
        // TODO NEXT must be implemented if ServerManager is implemented
        var gauge = findGauge("jobs.asyncqueue.scheduled.total", TagsType.JOBS_ASYNC);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Async Job

        // Diese wird vom ServerManager erhöht
        sut.incrementAsyncQueueCount();

        // Aber das was die gauge abholt wird wieder vom servermanager geholt, wieso?
        sut.getAsyncJobCountScheduled(); // das holt die Gauge ab

        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void jobsAsyncqueueProcessedTotal() {
        // TODO NEXT must be implemented if ServerManager is implemented
        var gauge = findGauge("jobs.asyncqueue.processed.total", TagsType.JOBS_ASYNC);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Async Job
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void jobsAsyncqueueDroppedTotal() {
        // TODO NEXT must be implemented if ServerManager is implemented
        var gauge = findGauge("jobs.asyncqueue.dropped.total", TagsType.JOBS_ASYNC);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Async Job
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void jobsAsyncqueuePeakTotal() {
        // TODO NEXT must be implemented if ServerManager is implemented
        var gauge = findGauge("jobs.asyncqueue.peak.total", TagsType.QUEUE_ASYNC);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Async Job
        assertTrue(beforeValue < gauge.value());
    }

    public static Stream<Arguments> jobsMedianTimeWaitingSource() {
        return Stream.of(
                Arguments.of(TagsType.PRIO_BACKGROUND, JobPriority.BACKGROUND),
                Arguments.of(TagsType.PRIO_LOW, JobPriority.LOW),
                Arguments.of(TagsType.PRIO_MEDIUM, JobPriority.MEDIUM),
                Arguments.of(TagsType.PRIO_HIGH, JobPriority.HIGH),
                Arguments.of(TagsType.PRIO_INSTANT, JobPriority.INSTANT),
                Arguments.of(TagsType.PRIO_ALL, JobPriority.BACKGROUND)
        );
    }

    @ParameterizedTest
    @MethodSource("jobsMedianTimeWaitingSource")
    void jobsMedianTimeWaiting(TagsType tagsType, JobPriority jobPriority) throws InterruptedException {
        var gauge = findGauge("jobs.median.time.waiting", tagsType);
        var beforeValue = gauge.value();
        var jobEntry = getJobEntry(jobPriority, JobStatus.RUNNING);
        jobEntry.setJobStatus(JobStatus.FINISHED);
        sut.statusChanged(JOBID, jobEntry);
        sut.resetMeasurementPeriod();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void cacheEntryCount() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("cache.entry.count", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void cacheEntrySize() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("cache.entry.size", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void cacheFreeVolumeSize() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("cache.free.volume.size", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void cacheOldestEntryAge() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("cache.oldest.entry.age", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void cacheHitRatio() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("cache.hit.ratio", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    @Disabled
    void errorcacheHitRatio() {
        // TODO NEXT must be implemented if Cache is implemented
        var gauge = findGauge("errorcache.hit.ratio", TagsType.CACHE);
        var beforeValue = gauge.value();
        // TODO NEXT trigger Cache
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    void conversionsReaderengineAndPdftoolTotal() {
        var gauge = findGauge("conversions.readerengine.and.pdftool.total", TagsType.CONVERSIONS);
        var beforeValue = gauge.value();
        sut.incrementConversionCount(BackendType.PDFTOOL);
        sut.incrementConversionCount(BackendType.READERENGINE);
        assertEquals(beforeValue + 2D, gauge.value());
    }

    @Test
    void conversionsReaderengineTotal() {
        var gauge = findGauge("conversions.readerengine.total", TagsType.CONVERSIONS);
        var beforeValue = gauge.value();
        sut.incrementConversionCount(BackendType.READERENGINE);
        sut.incrementConversionCount(BackendType.READERENGINE);
        sut.incrementConversionCount(BackendType.READERENGINE);
        assertEquals(beforeValue + 3D, gauge.value());
    }

    @Test
    void conversionsPdftoolTotal() {
        var gauge = findGauge("conversions.pdftool.total", TagsType.CONVERSIONS);
        var beforeValue = gauge.value();
        sut.incrementConversionCount(BackendType.PDFTOOL);
        sut.incrementConversionCount(BackendType.PDFTOOL);
        assertEquals(beforeValue + 2D, gauge.value());
    }

    @Test
    void conversionsNeededNewReaderengineTotal() {
        var gauge = findGauge("conversions.needed.new.readerengine.total", TagsType.CONVERSIONS);
        var beforeValue = gauge.value();
        sut.incrementConversionWithMultipleRunsCount();
        sut.incrementConversionWithMultipleRunsCount();
        assertEquals(beforeValue + 2D, gauge.value());
    }

    @Test
    void testGetCacheHits() {
        assertEquals(0, sut.getCacheHits());
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);
        // increment cacheHits
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.NONE), true);
        assertEquals(1, sut.getCacheHits());
    }

    public static Stream<Arguments> conversionsReaderengineErrorTotalSource() {
        return Stream.of(
                Arguments.of(TagsType.ERROR_GENERAL, JobError.GENERAL),
                Arguments.of(TagsType.ERROR_TIMEOUT, JobError.TIMEOUT),
                Arguments.of(TagsType.ERROR_DISPOSED, JobError.DISPOSED),
                Arguments.of(TagsType.ERROR_PASSWORD, JobError.PASSWORD),
                Arguments.of(TagsType.ERROR_NO_CONTENT, JobError.NO_CONTENT),
                Arguments.of(TagsType.ERROR_OUT_OF_MEMORY, JobError.OUT_OF_MEMORY)
        );
    }

    @ParameterizedTest
    @MethodSource("conversionsReaderengineErrorTotalSource")
    void conversionsReaderengineErrorTotal(TagsType tagsType, JobError jobError) {
        var gauge = findGauge("conversions.readerengine.error.total", tagsType);
        var beforeValue = gauge.value();
        sut.incrementConversionErrorCount(jobError);
        sut.incrementConversionErrorCount(jobError);
        assertEquals(beforeValue + 2D, gauge.value());
    }

    @Test
    void conversionsPDFErrorTotal() {
        var gauge = findGauge("conversions.pdftool.error.total", TagsType.CONVERSIONS);
        var beforeValue = gauge.value();
        sut.incrementConversionErrorCount(JobError.PDFTOOL);
        sut.incrementConversionErrorCount(JobError.PDFTOOL);
        sut.incrementConversionErrorCount(JobError.TIMEOUT); // Can be any JobError only only JobError.PDFTOLL should have impact
        assertEquals(beforeValue + 2D, gauge.value());
    }

    public static Stream<Arguments> conversionsReaderengineMedianTimeSource() {
        return Stream.of(
                Arguments.of(TagsType.PRIO_BACKGROUND, JobPriority.BACKGROUND),
                Arguments.of(TagsType.PRIO_LOW, JobPriority.LOW),
                Arguments.of(TagsType.PRIO_MEDIUM, JobPriority.MEDIUM),
                Arguments.of(TagsType.PRIO_HIGH, JobPriority.HIGH),
                Arguments.of(TagsType.PRIO_INSTANT, JobPriority.INSTANT),
                Arguments.of(TagsType.PRIO_ALL, JobPriority.INSTANT)
        );
    }

    @ParameterizedTest
    @MethodSource("conversionsReaderengineMedianTimeSource")
    void conversionsReaderengineMedianTime(TagsType tagsType, JobPriority jobPriority) throws InterruptedException {
        var gauge = findGauge("conversions.readerengine.median.time", tagsType);
        var beforeValue = gauge.value();
        changeJobStatus(jobPriority, JobStatus.FINISHED);
        sut.resetMeasurementPeriod();
        assertTrue(beforeValue < gauge.value());
    }

    @Test
    void testGetLocalCachePersistentSize() {
        assertEquals(0, sut.getLocalCachePersistentSize());
    }

    @Test
    void testGetLocalCacheFreeVolumeSize() {
        assertEquals(0, sut.getLocalCacheFreeVolumeSize());
    }

    @Test
    void testConversionCounts() {
        assertEquals(0, sut.getConversionCount(null));
        assertEquals(0, sut.getConversionCount(BackendType.PDFTOOL));
        assertEquals(0, sut.getConversionCount(BackendType.READERENGINE));

        sut.incrementConversionCount(BackendType.PDFTOOL);
        assertEquals(1, sut.getConversionCount(null));
        assertEquals(1, sut.getConversionCount(BackendType.PDFTOOL));
        assertEquals(0, sut.getConversionCount(BackendType.READERENGINE));

        sut.incrementConversionCount(BackendType.READERENGINE);
        assertEquals(2, sut.getConversionCount(null));
        assertEquals(1, sut.getConversionCount(BackendType.PDFTOOL));
        assertEquals(1, sut.getConversionCount(BackendType.READERENGINE));
    }

    @Test
    void testMultipleConversionRunCount() {
        assertEquals(0, sut.getConversionCountWithMultipleRuns());
        sut.incrementConversionWithMultipleRunsCount();
        assertEquals(1, sut.getConversionCountWithMultipleRuns());
    }

    @Test
    void testConversionErrorsGeneral() {
        assertEquals(0, sut.getConversionErrorsGeneral());
        sut.incrementConversionErrorCount(JobError.GENERAL);
        sut.incrementConversionErrorCount(JobError.TIMEOUT);
        assertEquals(1, sut.getConversionErrorsGeneral());
    }

    @Test
    void testConversionErrorsTimeout() {
        assertEquals(0, sut.getConversionErrorsTimeout());
        sut.incrementConversionErrorCount(JobError.TIMEOUT);
        sut.incrementConversionErrorCount(JobError.GENERAL);
        assertEquals(1, sut.getConversionErrorsTimeout());
    }

    @Test
    void testConversionErrorsDisposed() {
        assertEquals(0, sut.getConversionErrorsDisposed());
        sut.incrementConversionErrorCount(JobError.DISPOSED);
        assertEquals(1, sut.getConversionErrorsDisposed());
    }

    @Test
    void testConversionErrorsPassword() {
        assertEquals(0, sut.getConversionErrorsPassword());
        sut.incrementConversionErrorCount(JobError.PASSWORD);
        assertEquals(1, sut.getConversionErrorsPassword());
    }

    @Test
    void testConversionErrorsOutOfMemory() {
        assertEquals(0, sut.getConversionErrorsOutOfMemory());
        sut.incrementConversionErrorCount(JobError.OUT_OF_MEMORY);
        sut.incrementConversionErrorCount(JobError.OUT_OF_MEMORY);
        assertEquals(2, sut.getConversionErrorsOutOfMemory());
    }

    @Test
    void testConversionErrorsPDFTool() {
        assertEquals(0, sut.getConversionErrorsPDFTool());
        sut.incrementConversionErrorCount(JobError.PDFTOOL);
        assertEquals(1, sut.getConversionErrorsPDFTool());
    }

    @Test
    void testConversionErrorsNoContent() {
        assertEquals(0, sut.getConversionErrorsNoContent());
        sut.incrementConversionErrorCount(JobError.NO_CONTENT);
        assertEquals(1, sut.getConversionErrorsNoContent());
    }

    @Test
    void testGetJobsProcessed() {
        assertEquals(0, sut.getJobsProcessed());
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), false);
        assertEquals(1, sut.getJobsProcessed());
    }

    @Test
    void testJobErrorsTimeout() {
        assertEquals(0, sut.getJobErrorsTimeout());
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.TIMEOUT), true);
        assertEquals(1, sut.getJobErrorsTimeout());
    }

    @Test
    void testJobErrorsTotal() {
        assertEquals(0, sut.getJobErrorsTotal());
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.TIMEOUT), true);
        sut.incrementProcessedJobCount(new JobErrorEx(JobError.GENERAL), true);
        assertEquals(2, sut.getJobErrorsTotal());
    }

    @Test
    void testGetMedianJobTimeMillis() throws InterruptedException {


        assertEquals(0, sut.getMedianJobTimeMillis(null));
        assertEquals(0, sut.getMedianJobTimeMillis(JobPriority.BACKGROUND));
        assertEquals(0, sut.getMedianJobTimeMillis(JobPriority.HIGH));

        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.BACKGROUND, mock(ManagedJob.class));
        Thread.sleep(100);
        jobEntry.setJobStatus(JobStatus.FINISHED);
        sut.statusChanged(JOBID, jobEntry);

        assertEquals(0, sut.getMedianJobTimeMillis(null));
        assertEquals(0, sut.getMedianJobTimeMillis(JobPriority.BACKGROUND));
        assertEquals(0, sut.getMedianJobTimeMillis(JobPriority.HIGH));

        sut.resetMeasurementPeriod();

        assertNotEquals(0, sut.getMedianJobTimeMillis(null));
        assertNotEquals(0, sut.getMedianJobTimeMillis(JobPriority.BACKGROUND));
        assertEquals(0, sut.getMedianJobTimeMillis(JobPriority.HIGH));
    }

    @Test
    void testGetScheduledJobCountInQueue() {
        assertEquals(0, sut.getScheduledJobCountInQueue());
    }

    @Test
    void testGetPeakJobCountInQueue() {
        assertEquals(0, sut.getPeakJobCountInQueue(null));
        assertEquals(0, sut.getPeakJobCountInQueue(JobPriority.HIGH));
        assertEquals(0, sut.getPeakJobCountInQueue(JobPriority.BACKGROUND));

        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.HIGH, mock(ManagedJob.class));
        jobEntry.setJobStatus(JobStatus.SCHEDULED);
        sut.statusChanged(JOBID, jobEntry);

        sut.resetMeasurementPeriod();

        assertEquals(1, sut.getPeakJobCountInQueue(null));
        assertEquals(1, sut.getPeakJobCountInQueue(JobPriority.HIGH));
        assertEquals(0, sut.getPeakJobCountInQueue(JobPriority.BACKGROUND));
    }

    @Test
    void testAsyncJobProcessedCount() {
        assertEquals(0, sut.getAsyncJobProcessedCount());
        sut.incrementAsyncJobProcessedCount();
        assertEquals(1, sut.getAsyncJobProcessedCount());
    }

    @Test
    void testAsyncJobDroppedCount() {
        assertEquals(0, sut.getAsyncJobCountDropped());
        sut.incrementAsyncJobDroppedCount();
        assertEquals(1, sut.getAsyncJobCountDropped());
    }

    @Test
    void testGetMedianQueueTimeMillis() throws InterruptedException {
        assertEquals(0, sut.getMedianQueueTimeMillis(null));
        assertEquals(0, sut.getMedianQueueTimeMillis(JobPriority.HIGH));
        assertEquals(0, sut.getMedianQueueTimeMillis(JobPriority.LOW));

        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.HIGH, mock(ManagedJob.class));
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.SCHEDULED);
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.RUNNING);
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.FINISHED);
        sut.statusChanged(JOBID, jobEntry);

        sut.resetMeasurementPeriod();

        assertNotEquals(0, sut.getMedianQueueTimeMillis(null));
        assertNotEquals(0, sut.getMedianQueueTimeMillis(JobPriority.HIGH));
        assertEquals(0, sut.getMedianQueueTimeMillis(JobPriority.LOW));
    }

    @Test
    void testGetMedianConversionTimeMillis() throws InterruptedException {
        assertEquals(0, sut.getMedianConversionTimeMillis(null));
        assertEquals(0, sut.getMedianConversionTimeMillis(JobPriority.HIGH));
        assertEquals(0, sut.getMedianConversionTimeMillis(JobPriority.LOW));

        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), JobPriority.HIGH, mock(ManagedJob.class));
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.SCHEDULED);
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.RUNNING);
        Thread.sleep(10);
        jobEntry.setJobStatus(JobStatus.FINISHED);
        sut.statusChanged(JOBID, jobEntry);

        sut.resetMeasurementPeriod();

        assertNotEquals(0, sut.getMedianConversionTimeMillis(null));
        assertNotEquals(0, sut.getMedianConversionTimeMillis(JobPriority.HIGH));
        assertEquals(0, sut.getMedianConversionTimeMillis(JobPriority.LOW));
    }

    @Test
    void testInitPeriodicResetTimer() throws InterruptedException {

        var resetTask = (TimerTask) ReflectionTestUtils.getField(sut, "resetTask");
        assertNull(resetTask);
        // trigger initPeriodicResetTimer()
        ReflectionTestUtils.invokeMethod(sut, "initPeriodicResetTimer");

        resetTask = (TimerTask) ReflectionTestUtils.getField(sut, "resetTask");
        assertNotNull(resetTask);
    }

    private Gauge findGauge(String name, TagsType tagsType) {
        return Metrics.globalRegistry.find(MetricsRegistry.NAME_PREFIX + name).tags(tagsType.getTags()).gauge();
    }

    private void changeJobStatus(@NonNull JobPriority jobPriority, JobStatus jobStatus) {
        sut.statusChanged(JOBID, getJobEntry(jobPriority, jobStatus));
    }

    private JobEntry getJobEntry(@NonNull JobPriority jobPriority, JobStatus jobStatus) {
        var jobEntry = new JobEntry(JOBID, mock(DocumentConverterJob.class), jobPriority, mock(ManagedJob.class));
        if (jobStatus != null) {
            jobEntry.setJobStatus(jobStatus);
        }
        return jobEntry;
    }

    @Test
    void testGetLocalPersistentSize() {
        var localPersistentSize = sut.getLocalCachePersistentSize();

        assertEquals(0, localPersistentSize);
    }

    @Test
    void testGetLocalFreeVolumeSize() {
        var localPersistentSize = sut.getLocalCacheFreeVolumeSize();
        assertEquals(0, localPersistentSize);
    }

}


