/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.cache.CacheEntry;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.sun.star.lang.DisposedException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * PDFToolThreadTest includes tests for the JobThread base class, too
 */
@ExtendWith(MockitoExtension.class)
public class PDFToolThreadTest {

    private static final byte[] INPUT_FILE_DATA = "DATA".getBytes(StandardCharsets.UTF_8);

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private ServerManager serverManager;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private Cache cache;

    @Mock
    private ErrorCache errorCache;

    @Mock
    private Watchdog watchdog;

    @Mock
    private Statistics statistics;

    @Mock
    private Job job;

    @Mock
    private JobEntry jobEntry;

    @Mock
    private CacheEntry cacheEntry;

    @TempDir
    private Path tempDir;

    private String jobId = UUID.randomUUID().toString();
    private String jobHash = "hash-" + UUID.randomUUID();
    private AtomicBoolean finishedJobEntry = new AtomicBoolean(false);
    private Map<String, Object> jobResultProperties = new HashMap<>();
    private Map<String, Object> jobProperties = new HashMap<>();
    private Object jobExecutionData = new Object();
    private final AtomicReference<JobErrorEx> detectedJobErrorEx = new AtomicReference<>(null);

    private PDFToolThread sut;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    public void setUp() {
        setupLogListAppenderServerManager();

        sut = new PDFToolThread(serverConfig, serverManager, jobProcessor, cache, errorCache, watchdog, statistics);
    }

    @Test
    @DisplayName("Test PDFToolThread creation")
    public void testIsRunning() {
        assertNotNull(sut);
        assertTrue(sut.isRunning());
    }

    @Test
    @DisplayName("Test PDFToolThread copy creation")
    public void testCopy() {
        var copy = sut.createCopy();

        assertNotNull(copy);
        assertNotSame(sut, copy);
        assertNotEquals(sut.getName(), copy.getName());
    }

    @Test
    @DisplayName("Test PDFToolThread instance shutdown")
    public void testShutdownCurrentInstance() throws Exception {
        when(jobEntry.getJob()).thenReturn(job);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        var started = sut.startProcessingJob();
        assertTrue(started);

        sut.shutdownCurrentInstance();

        verify(job).kill();
    }

    @Test
    @DisplayName("Test PDFToolThread instance shutdown and exception on job kill()")
    public void testShutdownCurrentInstanceExceptionOnJobKill() throws Exception {
        when(jobEntry.getJob()).thenReturn(job);
        doThrow(new RuntimeException("Runtime exception for test")).when(job).kill();
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        var started = sut.startProcessingJob();
        assertTrue(started);

        sut.shutdownCurrentInstance();

        verify(job).kill();
    }

    @Test
    @DisplayName("Test PDFToolThread termination")
    public void testTerminate() {
        assertTrue(sut.isRunning());

        sut.terminate();

        assertFalse(sut.isRunning());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "finished shutdown"));
    }

    @Test
    @DisplayName("Test PDFToolThread getCurrentStartExecutionTimeMillis()")
    public void testGetCurrentStartExecutionTimeMillis() {
        var currentStartExecutionTimeMillis = sut.getCurrentStartExecutionTimeMillis();
        assertEquals(0, currentStartExecutionTimeMillis);

        sut.terminate();

        assertFalse(sut.isRunning());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "finished shutdown"));
    }

    @Test
    @DisplayName("Test PDFToolThread saveErrorFile() with normal error")
    public void testSaveErrorFileError() throws IOException {
        var errorDir = new File(tempDir.toFile(), "error");
        var timeoutDir = new File(tempDir.toFile(), "timeout");
        assertTrue(errorDir.mkdirs());
        assertTrue(timeoutDir.mkdirs());

        var inputFileName = "input-" + UUID.randomUUID() + ".odt";
        var inputFilePath = tempDir.resolve(inputFileName);
        Files.write(inputFilePath, INPUT_FILE_DATA);
        var inputFile = inputFilePath.toFile();

        var jobErrorEx = new JobErrorEx(JobError.DISPOSED);
        var jobProperties = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
        jobProperties.put(Properties.PROP_INFO_FILENAME, inputFileName);

        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobErrorEx()).thenReturn(jobErrorEx);
        when(jobEntry.getJobId()).thenReturn(jobId);
        when(job.getJobProperties()).thenReturn(jobProperties);
        when(serverConfig.getErrorDir()).thenReturn(tempDir.toString());

        sut.saveErrorFile(jobEntry);

        var timeoutFile = new File(timeoutDir, jobId + ".odt");
        var errorFile = new File(errorDir, jobId + ".odt");
        assertTrue(errorFile.exists());
        assertFalse(timeoutFile.exists());
    }

    @Test
    @DisplayName("Test PDFToolThread saveErrorFile() with timeout")
    public void testSaveErrorFileTimeout() throws IOException {
        var errorDir = new File(tempDir.toFile(), "error");
        var timeoutDir = new File(tempDir.toFile(), "timeout");
        assertTrue(errorDir.mkdirs());
        assertTrue(timeoutDir.mkdirs());

        var inputFileName = "input-" + UUID.randomUUID() + ".odt";
        var inputFilePath = tempDir.resolve(inputFileName);
        Files.write(inputFilePath, INPUT_FILE_DATA);
        var inputFile = inputFilePath.toFile();

        var jobErrorEx = new JobErrorEx(JobError.TIMEOUT);
        var jobProperties = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
        jobProperties.put(Properties.PROP_INFO_FILENAME, inputFileName);

        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobErrorEx()).thenReturn(jobErrorEx);
        when(jobEntry.getJobId()).thenReturn(jobId);
        when(job.getJobProperties()).thenReturn(jobProperties);
        when(serverConfig.getErrorDir()).thenReturn(tempDir.toString());

        sut.saveErrorFile(jobEntry);

        var timeoutFile = new File(timeoutDir, jobId + ".odt");
        var errorFile = new File(errorDir, jobId + ".odt");
        assertTrue(timeoutFile.exists());
        assertFalse(errorFile.exists());
    }

    @Test
    @DisplayName("Test PDFToolThread executeJob() with normal execution")
    public void testExecuteJobSuccess() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenReturn(ConverterStatus.OK);
        when(job.getJobProperties()).thenReturn(jobProperties);
        when(jobEntry.getJob()).thenReturn(job);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        when(cache.isEnabled()).thenReturn(true);
        when(cacheEntry.updateAndMakePersistent()).thenReturn(true);

        var fileName = "file" + UUID.randomUUID() + ".odt";
        jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);

        try (MockedStatic<Cache> cacheStaticMock = Mockito.mockStatic(Cache.class)) {
            cacheStaticMock.when(() -> Cache.isValidHash(eq(jobHash))).thenReturn(true);

            var started = sut.executeJob(jobEntry, jobExecutionData);
            assertTrue(started);
        }

        verify(cache).addEntry(any(CacheEntry.class), eq(true), eq(fileName));
    }

    @Test
    @DisplayName("Test PDFToolThread executeJob() with invalid hash for job")
    public void testExecuteJobFailsDueToInvalidHash() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(jobEntry.getJob()).thenReturn(job);

        var fileName = "file" + UUID.randomUUID() + ".odt";
        jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);

        try (MockedStatic<Cache> cacheStaticMock = Mockito.mockStatic(Cache.class)) {
            cacheStaticMock.when(() -> Cache.isValidHash(eq(jobHash))).thenReturn(false);

            var started = sut.executeJob(jobEntry, jobExecutionData);
            assertFalse(started);
        }

        verify(cache, never()).addEntry(any(CacheEntry.class), eq(true), eq(fileName));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"executeJob call with invalid hash not possible"));
    }

    @Test
    @DisplayName("Test PDFToolThread executeJob() fails due to exception on job.execute()")
    public void testExecuteJobFailsDueToJobExecuteException() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenThrow(new IOException("Exception for test"));
        when(jobEntry.getJob()).thenReturn(job);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        when(cache.isEnabled()).thenReturn(true);

        try (MockedStatic<Cache> cacheStaticMock = Mockito.mockStatic(Cache.class)) {
            cacheStaticMock.when(() -> Cache.isValidHash(eq(jobHash))).thenReturn(true);

            assertThrowsExactly(IOException.class, () -> sut.executeJob(jobEntry, jobExecutionData));
        }

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"error executing job: "));
    }

    @Test
    @DisplayName("Test PDFToolThread executeJob() fails due to job encounters error")
    public void testExecuteJobFailsDueToJobFailsWithError() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenReturn(ConverterStatus.ERROR);
        when(jobEntry.getJob()).thenReturn(job);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        when(cache.isEnabled()).thenReturn(true);

        jobResultProperties.put(Properties.PROP_RESULT_ERROR_CODE, new JobErrorEx(JobError.GENERAL));

        try (MockedStatic<Cache> cacheStaticMock = Mockito.mockStatic(Cache.class)) {
            cacheStaticMock.when(() -> Cache.isValidHash(eq(jobHash))).thenReturn(true);

            var started = sut.executeJob(jobEntry, jobExecutionData);
            assertFalse(started);
        }

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"finished executing job:"));
    }

    @Test
    @DisplayName("Test PDFToolThread run() with success on 1st run")
    public void testRunSuccessFirstRun() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenReturn(ConverterStatus.OK);
        when(job.getResultProperties()).thenReturn(jobResultProperties);
        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobLocale()).thenReturn("en_US");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry, (JobEntry)null);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        when(cache.isEnabled()).thenReturn(true);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        doAnswer(invocation -> {
            finishedJobEntry.set(true);
            return null;
        }).when(jobProcessor).finishedJobEntry(eq(BackendType.PDFTOOL), eq(jobEntry), any(JobStatus.class));

        sut.setJobExecutionData(jobExecutionData);

        var backThreadForTesting = startRunMethodWithMocking();
        syncWithJobProcessingThread(() -> finishedJobEntry.get());

        assertEquals(1, sut.getInstanceJobsProcessed());

        sut.terminate();

        backThreadForTesting.join();

        verify(cache).addEntry(eq(cacheEntry), eq(true), nullable(String.class));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"finished processing job: "));
        assertEquals(0, sut.getInstanceJobsProcessed());
    }

    @Test
    @DisplayName("Test PDFToolThread run() with error on process job")
    public void testRunFailsDueToErrorOnProcessJob() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenReturn(ConverterStatus.ERROR);
        when(job.getJobProperties()).thenReturn(jobProperties);
        when(job.getResultProperties()).thenReturn(jobResultProperties);
        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobLocale()).thenReturn("en_US");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        mockSetJobErrorExForJobEntry(jobEntry, detectedJobErrorEx);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry, (JobEntry)null);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        when(cache.isEnabled()).thenReturn(true);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        doAnswer(invocation -> {
            finishedJobEntry.set(true);
            return null;
        }).when(jobProcessor).finishedJobEntry(eq(BackendType.PDFTOOL), eq(jobEntry), any(JobStatus.class));

        var inputFileName = "input-" + UUID.randomUUID() + ".odt";
        var inputFilePath = tempDir.resolve(inputFileName);
        Files.write(inputFilePath, INPUT_FILE_DATA);
        var inputFile = inputFilePath.toFile();
        jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
        jobResultProperties.put(Properties.PROP_RESULT_ERROR_CODE, new JobErrorEx(JobError.GENERAL));

        sut.setJobExecutionData(jobExecutionData);

        var backThreadForTesting = startRunMethodWithMocking();
        syncWithJobProcessingThread(() -> finishedJobEntry.get());

        assertEquals(1, sut.getInstanceJobsProcessed());

        sut.terminate();

        backThreadForTesting.join();

        assertEquals(JobError.GENERAL.getErrorCode(), detectedJobErrorEx.get().getErrorCode());
        verify(cache, never()).addEntry(eq(cacheEntry), eq(true), nullable(String.class));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"error processing job (Execute path): "));
        assertEquals(0, sut.getInstanceJobsProcessed());
    }

    @Test
    @DisplayName("Test PDFToolThread run() with error on process job")
    public void testRunFailsDueToExceptionInExecuteJob() throws Exception {
        when(job.getHash()).thenReturn(jobHash);
        when(job.execute(eq(jobExecutionData))).thenThrow(new DisposedException("Exception for test"));
        when(job.getJobProperties()).thenReturn(jobProperties);
        when(job.getResultProperties()).thenReturn(jobResultProperties);
        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobLocale()).thenReturn("en_US");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        mockSetJobErrorExForJobEntry(jobEntry, detectedJobErrorEx);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry, (JobEntry)null);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        when(cache.isEnabled()).thenReturn(true);
        when(cache.startNewEntry(eq(jobHash))).thenReturn(cacheEntry);
        doAnswer(invocation -> {
            finishedJobEntry.set(true);
            return null;
        }).when(jobProcessor).finishedJobEntry(eq(BackendType.PDFTOOL), eq(jobEntry), any(JobStatus.class));

        var inputFileName = "input-" + UUID.randomUUID() + ".odt";
        var inputFilePath = tempDir.resolve(inputFileName);
        Files.write(inputFilePath, INPUT_FILE_DATA);
        var inputFile = inputFilePath.toFile();
        jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
        jobResultProperties.put(Properties.PROP_RESULT_ERROR_CODE, new JobErrorEx(JobError.GENERAL));

        sut.setJobExecutionData(jobExecutionData);

        var backThreadForTesting = startRunMethodWithMocking();
        syncWithJobProcessingThread(() -> finishedJobEntry.get());

        assertEquals(1, sut.getInstanceJobsProcessed());

        sut.terminate();

        backThreadForTesting.join();

        assertEquals(JobError.DISPOSED.getErrorCode(), detectedJobErrorEx.get().getErrorCode());
        verify(cache, never()).addEntry(eq(cacheEntry), eq(true), nullable(String.class));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"error processing job (Exception path): "));
        assertEquals(0, sut.getInstanceJobsProcessed());
    }

    @Test
    @DisplayName("Test PDFToolThread handleWatchdogNotification")
    public void testHandleWatchdogNotification() {
        var curTimeMillis = System.currentTimeMillis();
        var startTimeMillis = curTimeMillis - 1000L;
        var notificationJobErrorEx = new JobErrorEx(JobError.GENERAL);

        when(jobEntry.getJob()).thenReturn(job);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        sut.setJobExecutionData(jobExecutionData);
        sut.startProcessingJob();

        sut.handleWatchdogNotification(curTimeMillis, startTimeMillis, notificationJobErrorEx);

        verify(jobEntry).setJobErrorEx(eq(notificationJobErrorEx));
    }

    @Test
    @DisplayName("Test PDFToolThread startProcessingJob with cached job entry & no error cache entry")
    public void testStartProcessingJobWithCachedJobEntryAndNoErrorCacheEntry() {
        var jobErrorEx = new JobErrorEx(JobError.NONE);
        var filename = "input.odt";
        jobProperties.put(Properties.PROP_INFO_FILENAME, filename);
        var resultStream = new ByteArrayInputStream(INPUT_FILE_DATA);

        when(job.getHash()).thenReturn(jobHash);
        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobErrorEx()).thenReturn(jobErrorEx);
        when(jobEntry.getJobProperties()).thenReturn(jobProperties);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        when(cache.getCachedResult(eq(jobHash), eq(filename), ArgumentMatchers.any(), eq(false))).thenReturn(resultStream);

        sut.setJobExecutionData(jobExecutionData);
        sut.startProcessingJob();

        verify(jobEntry).setCacheHit();
        verify(jobEntry, never()).setErrorCacheHit(any(JobErrorEx.class));
        verify(statistics).incrementConversionCount(eq(BackendType.PDFTOOL));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"cache hit in job thread"));
    }

    @Test
    @DisplayName("Test PDFToolThread startProcessingJob with cached job entry & with error cache entry")
    public void testStartProcessingJobWithCachedJobEntryAndWithErrorCacheEntry() {
        var jobErrorEx = new JobErrorEx(JobError.GENERAL);
        var filename = "input.odt";
        jobProperties.put(Properties.PROP_INFO_FILENAME, filename);

        when(job.getHash()).thenReturn(jobHash);
        when(jobEntry.getJob()).thenReturn(job);
        when(jobEntry.getJobErrorEx()).thenReturn(jobErrorEx);
        when(errorCache.getJobErrorExForHash(eq(jobHash))).thenReturn(jobErrorEx);
        when(jobProcessor.getNextJobEntryToProcess(eq(BackendType.PDFTOOL))).thenReturn(jobEntry);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        sut.setJobExecutionData(jobExecutionData);
        sut.startProcessingJob();

        verify(jobEntry, never()).setCacheHit();
        verify(jobEntry).setErrorCacheHit(eq(jobErrorEx));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager,"errorcache hit in job thread"));
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

    private void mockSetJobErrorExForJobEntry(JobEntry mock, AtomicReference<JobErrorEx> toStore) {
        doAnswer(invocation -> {
            toStore.set(invocation.getArgument(0));
            return null;
        }).when(mock).setJobErrorEx(any(JobErrorEx.class));
    }

    private Thread startRunMethodWithMocking() {
        var backThreadForTesting = new Thread(() -> {
            try (MockedStatic<Cache> cacheStaticMock = mockStatic(Cache.class)) {
                cacheStaticMock.when(() -> Cache.isValidHash(eq(jobHash))).thenReturn(true);

                sut.run();
            }
        });
        backThreadForTesting.start();
        return backThreadForTesting;
    }

    @FunctionalInterface
    public interface AsyncJobProcessingFinishedFunction {
        boolean isFinished();
    }

    private void syncWithJobProcessingThread(AsyncJobProcessingFinishedFunction asyncJobProcessingFinished) {
        var TIMEOUT = 1000L;
        var startTimeMillis = System.currentTimeMillis();
        var jobProcessed = false;

        try {
            while (!jobProcessed) {
                Thread.sleep(10L);
                jobProcessed = asyncJobProcessingFinished.isFinished();
                if ((System.currentTimeMillis() - startTimeMillis) > TIMEOUT) {
                    fail("Job processing took too long");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
