/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.cache.Cache;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class JobEntryQueueTest {

    @Mock
    JobProcessor jobProcessor;

    private JobEntryQueue sut;

    @BeforeEach
    void setUp() {
        sut = new JobEntryQueue(jobProcessor);
    }

    @Test
    void terminate() {
        sut.terminate();
    }

    @Test
    void isRunning() {
        var isRunning = sut.isRunning();

        assertTrue(isRunning);
    }

    @Test
    void size() {
        var size = sut.size();

        assertEquals(0, size);
    }

    @Test
    void scheduleFirstJob() {
        var jobPriority = JobPriority.MEDIUM;
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobPriority()).thenReturn(jobPriority);

        sut.schedule(jobEntry);

        var size = sut.size();
        assertEquals(1, size);

        verify(jobProcessor).updateServerJobStatus(eq(jobEntry), eq(JobStatus.SCHEDULED));
    }

    @Test
    void schedule2ndJobWithHigherPriority() {
        var jobPriority1 = JobPriority.MEDIUM;
        var jobEntry1 = mock(JobEntry.class);
        when(jobEntry1.getJobPriority()).thenReturn(jobPriority1);

        sut.schedule(jobEntry1);

        var size = sut.size();
        assertEquals(1, size);
        verify(jobProcessor).updateServerJobStatus(eq(jobEntry1), eq(JobStatus.SCHEDULED));

        var jobPriority2 = JobPriority.HIGH;
        var jobEntry2 = mock(JobEntry.class);
        when(jobEntry2.getJobPriority()).thenReturn(jobPriority2);

        sut.schedule(jobEntry2);

        size = sut.size();
        assertEquals(2, size);
        verify(jobProcessor).updateServerJobStatus(eq(jobEntry2), eq(JobStatus.SCHEDULED));
    }

    @Test
    void getFound() {
        var jobId = UUID.randomUUID().toString();
        var jobPriority = JobPriority.MEDIUM;
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobPriority()).thenReturn(jobPriority);
        when(jobEntry.getJobId()).thenReturn(jobId);

        sut.schedule(jobEntry);

        var getJobEntry = sut.get(jobId);
        assertNotNull(getJobEntry);
        assertEquals(jobEntry, getJobEntry);
    }

    @Test
    void getNotFound() {
        var jobId = UUID.randomUUID().toString();

        var getJobEntry = sut.get(jobId);
        assertNull(getJobEntry);
    }

    @CsvSource({ "true", "false" })
    @ParameterizedTest
    void removeJobEntryScheduledExisting(boolean scheduledOnly) {
        var jobId = UUID.randomUUID().toString();
        var jobPriority = JobPriority.MEDIUM;
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobPriority()).thenReturn(jobPriority);
        when(jobEntry.getJobId()).thenReturn(jobId);

        sut.schedule(jobEntry);

        var removedJobEntry = sut.remove(jobId, scheduledOnly);

        assertNotNull(removedJobEntry);
        // times(2) : scheduled & remove
        verify(jobProcessor, times(2)).updateServerJobStatus(nullable(JobEntry.class), any());
    }

    @Test
    void removeJobEntryNotExisting() {
        var jobId = UUID.randomUUID().toString();

        var removedJobEntry = sut.remove(jobId, false);

        assertNull(removedJobEntry);
        verify(jobProcessor, never()).updateServerJobStatus(nullable(JobEntry.class), any());
    }

    @Test
    void finishedJobEntry() {
        var jobEntry = mock(JobEntry.class);
        var jobStatus = JobStatus.FINISHED;

        sut.finishedJobEntry(jobEntry, jobStatus);

        verify(jobProcessor).updateServerJobStatus(eq(jobEntry), eq(jobStatus));
    }

    @Test
    void getNextJobEntryToProcessNoEntryAvailable() {
        var notEmptyCondition = mock(Condition.class);
        try (var ignore = mockConstruction(ReentrantLock.class,
            (mock, context) -> doReturn(notEmptyCondition).when(mock).newCondition())) {
            sut = new JobEntryQueue(jobProcessor);

            // ensure that we don't wait forever - thread terminates JobEntryQueue
            var backThread = new Thread(() -> {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                sut.terminate();
            });
            backThread.start();

            var nextJobEntry = sut.getNextJobEntryToProcess();

            assertNull(nextJobEntry);
            assertFalse(sut.isRunning());
            verify(jobProcessor, never()).updateServerJobStatus(nullable(JobEntry.class), any());
            verify(jobProcessor, never()).updateServerJobStatus(any(JobEntry.class), any());
        }
    }

    @Test
    void getNextJobEntryToProcessEntryAvailable() {
        var jobHash1 = UUID.randomUUID().toString();
        var jobHash2 = UUID.randomUUID().toString();
        var jobPriority1 = JobPriority.MEDIUM;
        var jobPriority2 = JobPriority.HIGH;
        var jobEntry1 = mock(JobEntry.class);
        var jobEntry2 = mock(JobEntry.class);
        when(jobEntry1.getJobPriority()).thenReturn(jobPriority1);
        when(jobEntry1.getHash()).thenReturn(jobHash1);
        when(jobEntry2.getJobPriority()).thenReturn(jobPriority2);
        when(jobEntry2.getHash()).thenReturn(jobHash2);

        sut.schedule(jobEntry1);
        sut.schedule(jobEntry2);

        try (var staticCacheMock = mockStatic(Cache.class)) {
            staticCacheMock.when(() -> Cache.isValidHash(anyString())).thenReturn(true);

            var nextJobEntry = sut.getNextJobEntryToProcess();

            assertNotNull(nextJobEntry);
            assertEquals(jobEntry2, nextJobEntry);
            verify(jobProcessor, never()).updateServerJobStatus(eq(jobEntry1), eq(JobStatus.RUNNING));
            verify(jobProcessor, times(1)).updateServerJobStatus(eq(jobEntry2), eq(JobStatus.RUNNING));

            nextJobEntry = sut.getNextJobEntryToProcess();

            assertNotNull(nextJobEntry);
            assertEquals(jobEntry1, nextJobEntry);
            verify(jobProcessor, times(1)).updateServerJobStatus(eq(jobEntry1), eq(JobStatus.RUNNING));
            verify(jobProcessor, times(1)).updateServerJobStatus(eq(jobEntry2), eq(JobStatus.RUNNING));
        }

        assertTrue(sut.isRunning());
    }
}
