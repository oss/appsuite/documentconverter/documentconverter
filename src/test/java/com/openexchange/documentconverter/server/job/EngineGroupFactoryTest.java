/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EngineGroupFactoryTest {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private ServerManager serverManager;

    @Mock
    private Cache cache;

    @Mock
    private ErrorCache errorCache;

    @Mock
    private Statistics statistics;

    @InjectMocks
    private EngineGroupFactory sut;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();
    }

    @Test
    void testCreateEngineGroupWithPDFToolBackendType() {
        var engineType = BackendType.PDFTOOL;
        int engineCount = 5;
        var jobProcessor = mock(JobProcessor.class);
        var instanceProvider = mock(REInstanceProvider.class);
        var jobWatchdog = mock(Watchdog.class);

        var engineGroup = sut.createEngineGroup(engineType, engineCount, jobProcessor, instanceProvider, jobWatchdog);

        assertNotNull(engineGroup);
        assertEquals(engineCount, engineGroup.getJobThreads().size());
        assertTrue(engineGroup.getJobThreads().stream().allMatch(engineThread -> engineThread instanceof PDFToolThread));
        assertNotNull(engineGroup.getJobEntryQueue());
    }

    @Test
    void testCreateEngineGroupWithReaderEngineBackendType() {
        BackendType engineType = BackendType.READERENGINE;
        int engineCount = 3;
        JobProcessor jobProcessor = mock(JobProcessor.class);
        REInstanceProvider instanceProvider = mock(REInstanceProvider.class);
        Watchdog jobWatchdog = mock(Watchdog.class);

        EngineGroup engineGroup = sut.createEngineGroup(engineType, engineCount, jobProcessor, instanceProvider, jobWatchdog);

        assertNotNull(engineGroup);
        assertEquals(engineCount, engineGroup.getJobThreads().size());
        assertTrue(engineGroup.getJobThreads().stream().allMatch(engineThread -> engineThread instanceof REThread));
        assertNotNull(engineGroup.getJobEntryQueue());
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

}
