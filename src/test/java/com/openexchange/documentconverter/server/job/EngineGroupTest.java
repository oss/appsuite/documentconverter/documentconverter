/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class EngineGroupTest {

    @Mock
    private JobEntryQueue entryQueue;

    @Mock
    private JobThread jobThread;

    private EngineGroup sut;

    private Vector<JobThread> engineVector;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();

        engineVector = new Vector<>();
        sut = new EngineGroup(engineVector, entryQueue);
    }

    @Test
    void getGroupName_shouldReturnBackendTypeUpperCase() {
        when(jobThread.getBackendType()).thenReturn(BackendType.READERENGINE);
        engineVector.add(jobThread);

        var groupName = sut.getGroupName();

        assertEquals("READERENGINE", groupName);
    }

    @Test
    void getGroupName_shouldReturnUnknownWhenEngineVectorIsEmpty() {
        var groupName = sut.getGroupName();

        assertEquals("Unknown", groupName);
    }

    @Test
    void getJobEntryQueue_shouldReturnEntryQueue() {
        var result = sut.getJobEntryQueue();

        assertEquals(entryQueue, result);
    }

    @Test
    void getJobThreads_shouldReturnEngineVector() {
        var result = sut.getJobThreads();

        assertEquals(engineVector, result);
    }

    @Test
    void start_shouldStartAllJobThreads() {
        var jobThread1 = mock(JobThread.class);
        var jobThread2 = mock(JobThread.class);
        engineVector.add(jobThread1);
        engineVector.add(jobThread2);

        sut.start();

        verify(jobThread1).start();
        verify(jobThread2).start();
    }

    @Test
    void terminate_shouldTerminateEntryQueueAndJoinAllJobThreads() throws InterruptedException {
        var jobThread1 = mock(JobThread.class);
        var jobThread2 = mock(JobThread.class);
        engineVector.add(jobThread1);
        engineVector.add(jobThread2);
        when(jobThread1.getBackendType()).thenReturn(BackendType.READERENGINE);

        sut.terminate();

        verify(entryQueue).terminate();
        verify(jobThread1).terminate();
        verify(jobThread2).terminate();
        verify(jobThread1).join();
        verify(jobThread2).join();

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC starting shutdown of EngineGroup READERENGINE: ..."));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC finished shutdown of EngineGroup READERENGINE: "));
    }

    @Test
    void terminate_shouldTerminateEntryQueueAndAndExceptionOnThreadTerminate() throws InterruptedException {
        var jobThread1 = mock(JobThread.class);
        var jobThread2 = mock(JobThread.class);
        engineVector.add(jobThread1);
        engineVector.add(jobThread2);
        when(jobThread1.getBackendType()).thenReturn(BackendType.READERENGINE);
        doThrow(new RuntimeException("Test exception")).when(jobThread1).terminate();

        sut.terminate();

        verify(entryQueue).terminate();
        verify(jobThread1).terminate();
        verify(jobThread2).terminate();
        verify(jobThread1).join();
        verify(jobThread2).join();
    }

    @Test
    void terminate_shouldTerminateEntryQueueAndAndExceptionOnThreadJoin() throws InterruptedException {
        var jobThread1 = mock(JobThread.class);
        var jobThread2 = mock(JobThread.class);
        engineVector.add(jobThread1);
        engineVector.add(jobThread2);
        when(jobThread1.getBackendType()).thenReturn(BackendType.READERENGINE);
        doThrow(new RuntimeException("Test exception")).when(jobThread1).join(anyLong());
        sut.terminate();

        verify(entryQueue).terminate();
        verify(jobThread1).terminate();
        verify(jobThread2).terminate();
        verify(jobThread1).join();
        verify(jobThread2).join();
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

}
