/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JobPriorityTest {

    private JobPriority jobPriority;

    @BeforeEach
    void setUp() {
        jobPriority = null; // Initialize the jobPriority before each test
    }

    @Test
    void lowest() {
        jobPriority = JobPriority.lowest();
        assertEquals(JobPriority.BACKGROUND, jobPriority);
    }

    @Test
    void highest() {
        jobPriority = JobPriority.highest();
        assertEquals(JobPriority.INSTANT, jobPriority);
    }

    @Test
    void byString_existingPriority() {
        var jobPriorityAsString = "MEDIUM";
        jobPriority = JobPriority.fromString(jobPriorityAsString);
        assertEquals(JobPriority.MEDIUM, jobPriority);
    }

    @Test
    void byString_nonExistingPriority() {
        var jobPriorityAsString = "INVALID";
        jobPriority = JobPriority.fromString(jobPriorityAsString);
        assertEquals(JobPriority.BACKGROUND, jobPriority);
    }
}
