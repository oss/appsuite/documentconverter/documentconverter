/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.*;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REDescriptor;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JobProcessorTest {

    private static final long NO_RESPONSE_TIMEOUT = 1000L;
    protected static final long NO_RESPONSE_TIMEOUT_PERCENTAGE = 125L;

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private ConverterJobFactory converterJobFactory;

    @Mock
    private ServerManager serverManager;

    @Mock
    private REDescriptor readerEngineDescriptor;

    @Mock
    private Cache cache;

    @Mock
    private ErrorCache errorCache;

    @Mock
    private Statistics statistics;

    @Mock
    private EngineGroupFactory engineGroupFactory;

    @InjectMocks
    private JobProcessor sut;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        lenient().when(serverConfig.getJobExecutionTimeoutMilliseconds()).thenReturn(NO_RESPONSE_TIMEOUT);
        lenient().when(serverConfig.getJobProcessorPoolSize()).thenReturn(1);
        lenient().when(serverConfig.getJobProcessorCount()).thenReturn(1);

        setupLogListAppenderServerManager();
    }

    @Test
    void testInit() {
        assertTrue(sut.isRunning());
        assertNull(sut.getReaderEngineExecutorService());

        var engineGroup = mock(EngineGroup.class);
        when(engineGroupFactory.createEngineGroup(any(BackendType.class), anyInt(), eq(sut), any(), any())).thenReturn(engineGroup);

        try {
            sut.init();

            assertTrue(sut.isRunning());
            assertNotNull(sut.getReaderEngineExecutorService());
        } finally {
            sut.shutdown();
        }
    }

    @Test
    void testIsRunning() {
        var engineGroup = mock(EngineGroup.class);
        when(engineGroupFactory.createEngineGroup(any(BackendType.class), anyInt(), eq(sut), any(), any())).thenReturn(engineGroup);

        assertTrue(sut.isRunning());

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(REThread.class, (mock, context) -> {
                doNothing().when(mock).terminate();
                when(mock.getBackendType()).thenReturn(BackendType.READERENGINE);
            })) {
                try (var ignored3 = mockConstruction(PDFToolThread.class, (mock, context) -> {
                    doNothing().when(mock).terminate();
                    when(mock.getBackendType()).thenReturn(BackendType.PDFTOOL);
                })) {
                    sut.init();
                    sut.shutdown();
                }
            }
        }

        assertFalse(sut.isRunning());
    }

    public static Stream<Arguments> jobTypesAndServerJobClass() {
        return Stream.of(
            Arguments.of("pdf", DocumentConverterJob.class),
            Arguments.of("pdfa", DocumentConverterJob.class),
            Arguments.of("ooxml", DocumentConverterJob.class),
            Arguments.of("odf", DocumentConverterJob.class),
            Arguments.of("graphic", GraphicConverterJob.class),
            Arguments.of("shape2png", Shape2PNGConverterJob.class)
        );
    }

    @ParameterizedTest
    @MethodSource("jobTypesAndServerJobClass")
    void testCreateServerJob(String jobType, Class<? extends ConverterJob> clazz) {
        var jobProperties = new HashMap<String, Object>();
        var resultProperties = new HashMap<String, Object>();

        var job = mock(clazz);
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        when(converterJobFactory.createJob(eq(jobType), "123", any(), any())).thenReturn(job);
        when(engineGroupFactory.createEngineGroup(eq(BackendType.READERENGINE), anyInt(), eq(sut), any(), any())).thenReturn(engineGroupRE);
        when(engineGroupFactory.createEngineGroup(eq(BackendType.PDFTOOL), anyInt(), eq(sut), any(), any())).thenReturn(engineGroupPDF);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var serverJob = sut.createServerJob(jobType, "123", jobProperties, resultProperties);

                assertNotNull(serverJob);
                assertTrue(clazz.isInstance(serverJob));
            }
        }
    }

    @Test
    void testScheduleJob() {
        var jobDescription = "Test DocumentConverter Job";
        var job = mock(DocumentConverterJob.class);
        var jobStatusListener = mock(JobStatusListener.class);
        var engineGroup = mock(EngineGroup.class);
        var jobEntryQueue = mock(JobEntryQueue.class);

        ReflectionTestUtils.setField(engineGroup, "entryQueue", jobEntryQueue);
        when(job.backendTypeNeeded()).thenReturn(BackendType.READERENGINE);
        when(engineGroupFactory.createEngineGroup(any(BackendType.class), anyInt(), eq(sut), any(), any())).thenReturn(engineGroup);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();
                var jobId = sut.scheduleJob(job, JobPriority.MEDIUM, jobStatusListener, jobDescription);

                assertNotNull(jobId);
                verify(engineGroup.entryQueue, times(1)).schedule(any(JobEntry.class));
            }
        }
    }

    @Test
    void testKillScheduledJob() {
        var jobDescription = "Test DocumentConverter Kill Job";
        var job = mock(DocumentConverterJob.class);
        var jobStatusListener = mock(JobStatusListener.class);
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobThreadRE = engineGroupsParts.engineGroupRE.jobThread;
        var jobThreadPDF = engineGroupsParts.engineGroupPDF.jobThread;
        when(job.backendTypeNeeded()).thenReturn(BackendType.READERENGINE);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();
                var jobId = sut.scheduleJob(job, JobPriority.MEDIUM, jobStatusListener, jobDescription);

                assertNotNull(jobId);
                verify(engineGroupRE.entryQueue, times(1)).schedule(any(JobEntry.class));

                var jobEntry = mock(JobEntry.class);
                when(jobEntry.getJobId()).thenReturn(jobId);
                when(jobThreadRE.getCurrentJobEntry()).thenReturn(jobEntry);

                var jobErrorEx = new JobErrorEx(JobError.NONE);
                sut.killJob(jobId, jobErrorEx);

                verify(jobThreadRE, times(1)).handleWatchdogNotification(anyLong(), anyLong(), eq(jobErrorEx));
                verify(jobThreadPDF, never()).handleWatchdogNotification(anyLong(), anyLong(), eq(jobErrorEx));
            }
        }
    }

    @Test
    void testKillNotScheduledJobDoesNothing() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobThreadRE = engineGroupsParts.engineGroupRE.jobThread;
        var jobThreadPDF = engineGroupsParts.engineGroupPDF.jobThread;

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var jobId = UUID.randomUUID().toString();
                when(jobThreadRE.getCurrentJobEntry()).thenReturn(null);

                var jobErrorEx = new JobErrorEx(JobError.NONE);
                sut.killJob(jobId, jobErrorEx);

                verify(jobThreadRE, never()).handleWatchdogNotification(anyLong(), anyLong(), eq(jobErrorEx));
                verify(jobThreadPDF, never()).handleWatchdogNotification(anyLong(), anyLong(), eq(jobErrorEx));
            }
        }
    }

    @Test
    void testRemoveJobExisting() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobEntryQueueRE = engineGroupsParts.engineGroupRE.jobEntryQueue;
        when(engineGroupRE.getGroupName()).thenReturn("ReaderEngine");

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var jobId = UUID.randomUUID().toString();
                when(jobEntryQueueRE.remove(eq(jobId), eq(false))).thenReturn(mock(JobEntry.class));

                var removed = sut.removeJob(jobId);

                assertTrue(removed);
                verify(jobEntryQueueRE, times(1)).remove(eq(jobId), eq(false));
                assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC removed job from ReaderEngine queue"));
            }
        }
    }

    @Test
    void testRemoveJobNonExisting() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobEntryQueueRE = engineGroupsParts.engineGroupRE.jobEntryQueue;
        var jobEntryQueuePDF = engineGroupsParts.engineGroupPDF.jobEntryQueue;

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var jobId = UUID.randomUUID().toString();
                when(jobEntryQueueRE.remove(eq(jobId), anyBoolean())).thenReturn(null);

                var removed = sut.removeJob(jobId);

                assertFalse(removed);
                verify(jobEntryQueueRE, times(1)).remove(eq(jobId), eq(false));
                verify(jobEntryQueuePDF, times(1)).remove(eq(jobId), eq(false));
                assertFalse(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC removed job from ReaderEngine queue"));
            }
        }
    }

    @Test
    void testRemoveScheduledJobExisting() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobEntryQueueRE = engineGroupsParts.engineGroupRE.jobEntryQueue;
        when(engineGroupRE.getGroupName()).thenReturn("ReaderEngine");

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var jobId = UUID.randomUUID().toString();
                when(jobEntryQueueRE.remove(eq(jobId), eq(true))).thenReturn(mock(JobEntry.class));

                var removed = sut.removeScheduledJob(jobId);

                assertTrue(removed);
                verify(jobEntryQueueRE, times(1)).remove(eq(jobId), eq(true));
                assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC removed scheduled job from ReaderEngine queue"));
            }
        }
    }

    @Test
    void testRemoveScheduledJobNonExisting() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobEntryQueueRE = engineGroupsParts.engineGroupRE.jobEntryQueue;
        var jobEntryQueuePDF = engineGroupsParts.engineGroupPDF.jobEntryQueue;

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {

                sut.init();

                var jobId = UUID.randomUUID().toString();
                when(jobEntryQueueRE.remove(eq(jobId), anyBoolean())).thenReturn(null);

                var removed = sut.removeScheduledJob(jobId);

                assertFalse(removed);
                verify(jobEntryQueueRE, times(1)).remove(eq(jobId), eq(true));
                verify(jobEntryQueuePDF, times(1)).remove(eq(jobId), eq(true));
                assertFalse(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC removed scheduled job from ReaderEngine queue"));
            }
        }
    }

    @Test
    void testAddJobStatusChangeListener() {
        sut.addJobStatusChangeListener(mock(JobStatusListener.class));
    }

    @Test
    void testRemoveJobStatusChangeListener() {
        var jobStatusListener = mock(JobStatusListener.class);
        sut.addJobStatusChangeListener(jobStatusListener);
        sut.removeJobStatusChangeListener(jobStatusListener);
    }

    @Test
    void testHandleWatchdogNotification() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        var jobThreadRE = engineGroupsParts.engineGroupRE.jobThread;
        var jobThreadRE2 = mock(JobThread.class);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var jobEntry = mock(JobEntry.class);
                when(jobThreadRE.getCurrentJobEntry()).thenReturn(jobEntry);
                when(jobThreadRE.createCopy()).thenReturn(jobThreadRE2);

                var curTimeMillis = System.currentTimeMillis();
                var startTimeMillis = curTimeMillis - (NO_RESPONSE_TIMEOUT * NO_RESPONSE_TIMEOUT_PERCENTAGE);
                var jobErrorEx = new JobErrorEx(JobError.TIMEOUT);

                sut.handleWatchdogNotification(curTimeMillis, startTimeMillis, jobErrorEx);

                verify(jobThreadRE, times(1)).createCopy();
                verify(jobThreadRE, times(1)).terminate();
                verify(jobThreadRE2, times(1)).start();
                assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC received 'NO RESPONSE' timeout => replacing job threads"));
            }
        }
    }

    @Test
    void testShutdown() throws IOException {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        setupEngineGroups(engineGroupRE, engineGroupPDF);
        var oslPipeDummy = new File("/tmp/OSL_PIPE_DUMMY");
        FileUtils.write(oslPipeDummy, "dummy", Charset.defaultCharset());
        assertTrue(oslPipeDummy.exists());

        try {
            sut.init();
        } finally {
            sut.shutdown();
            assertFalse(oslPipeDummy.exists());
        }
    }

    @Test
    void testGetCache() {
        var cache = sut.getCache();
        assertNotNull(cache);
    }

    @Test
    void testGetErrorCache() {
        var errorCache = sut.getErrorCache();
        assertNotNull(errorCache);
    }

    @Test
    void testGetJobMonitor() {
        var jobMonitor = sut.getJobMonitor();
        assertNull(jobMonitor);

        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        setupEngineGroups(engineGroupRE, engineGroupPDF);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                jobMonitor = sut.getJobMonitor();
                assertNotNull(jobMonitor);
            }
        }
    }

    @Test
    void testGetReaderEngineExecutorService() {
        var readerEngineExecutorService = sut.getReaderEngineExecutorService();
        assertNull(readerEngineExecutorService);

        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        setupEngineGroups(engineGroupRE, engineGroupPDF);

        try {
            sut.init();

            readerEngineExecutorService = sut.getReaderEngineExecutorService();
            assertNotNull(readerEngineExecutorService);
        } finally {
            sut.shutdown();
        }
    }

    @Test
    void testGetEngineStatus() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        var engineGroupsParts = setupEngineGroups(engineGroupRE, engineGroupPDF);
        when(engineGroupRE.getJobThreads()).thenCallRealMethod();
        when(engineGroupPDF.getJobThreads()).thenCallRealMethod();
        var jobThreadRE = engineGroupsParts.engineGroupRE.jobThread;
        var jobThreadPDF = engineGroupsParts.engineGroupPDF.jobThread;
        when(jobThreadRE.isAlive()).thenReturn(true);
        when(jobThreadPDF.isAlive()).thenReturn(true);
        when(jobThreadRE.isRunning()).thenReturn(true);
        when(jobThreadPDF.isRunning()).thenReturn(true);

        try (var ignored = mockConstruction(REInstanceProvider.class, (mock, context) -> doNothing().when(mock).terminate())) {
            try (var ignored2 = mockConstruction(Watchdog.class)) {
                sut.init();

                var engineStatusMap = sut.getEngineStatus();

                assertNotNull(engineStatusMap);
                assertEquals(2, engineStatusMap.size());
                assertTrue(engineStatusMap.containsKey(BackendType.READERENGINE));
                assertTrue(engineStatusMap.containsKey(BackendType.PDFTOOL));
                var engineStatusRE = engineStatusMap.get(BackendType.READERENGINE);
                assertEquals(1, engineStatusRE.totalCount());
                assertEquals(1, engineStatusRE.runningCount());
                var engineStatusPDF = engineStatusMap.get(BackendType.PDFTOOL);
                assertEquals(1, engineStatusPDF.totalCount());
                assertEquals(1, engineStatusPDF.runningCount());
            }
        }
    }

    @Test
    void testUpdateServerJobStatus() {
        var engineGroupRE = mock(EngineGroup.class);
        var engineGroupPDF = mock(EngineGroup.class);
        setupEngineGroups(engineGroupRE, engineGroupPDF);

        try {
            sut.init();

            var jobId = UUID.randomUUID().toString();
            var jobEntry = mock(JobEntry.class);
            var jobStatus = JobStatus.NEW;
            when(jobEntry.getJobStatus()).thenReturn(jobStatus);
            when(jobEntry.getJobId()).thenReturn(jobId);

            var jobStatusListener = mock(JobStatusListener.class);
            sut.addJobStatusChangeListener(jobStatusListener);

            sut.updateServerJobStatus(jobEntry, JobStatus.SCHEDULED);

            verify(jobEntry, times(1)).setJobStatus(eq(JobStatus.SCHEDULED));
            verify(jobStatusListener, times(1)).statusChanged(eq(jobId), eq(jobEntry));
        } finally {
            sut.shutdown();
        }
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

    record EngineGroupParts(JobEntryQueue jobEntryQueue, JobThread jobThread, Vector<JobThread> engineVector) { }
    record EngineGroups(EngineGroupParts engineGroupRE, EngineGroupParts engineGroupPDF) { }
    private EngineGroups setupEngineGroups(EngineGroup engineGroupRE, EngineGroup engineGroupPDF) {
        var jobEntryQueueRE = mock(JobEntryQueue.class);
        var jobEntryQueuePDF = mock(JobEntryQueue.class);
        var jobThreadRE = mock(JobThread.class);
        var jobThreadPDF = mock(JobThread.class);
        var engineVectorRE = new Vector<JobThread>(1);
        var engineVectorPDF = new Vector<JobThread>(1);
        engineVectorRE.add(jobThreadRE);
        engineVectorPDF.add(jobThreadPDF);

        ReflectionTestUtils.setField(engineGroupRE, "entryQueue", jobEntryQueueRE);
        ReflectionTestUtils.setField(engineGroupRE, "engineVector", engineVectorRE);
        ReflectionTestUtils.setField(engineGroupPDF, "entryQueue", jobEntryQueuePDF);
        ReflectionTestUtils.setField(engineGroupPDF, "engineVector", engineVectorPDF);
        when(engineGroupFactory.createEngineGroup(eq(BackendType.READERENGINE), anyInt(), eq(sut), any(), any())).thenReturn(engineGroupRE);
        lenient().when(engineGroupFactory.createEngineGroup(eq(BackendType.PDFTOOL), anyInt(), eq(sut), any(), any())).thenReturn(engineGroupPDF);

        var engineGroupDataRE = new EngineGroupParts(jobEntryQueueRE, jobThreadRE, engineVectorRE);
        var engineGroupDataPDF = new EngineGroupParts(jobEntryQueuePDF, jobThreadPDF, engineVectorPDF);
        return new EngineGroups(engineGroupDataRE, engineGroupDataPDF);
    }

}
