/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import static org.junit.jupiter.api.Assertions.*;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.XComponentContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class REThreadTest {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private ServerManager serverManager;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private REInstanceProvider instanceProvider;

    @Mock
    private Cache cache;

    @Mock
    private ErrorCache errorCache;

    @Mock
    private Watchdog watchdog;

    @Mock
    private Statistics statistics;

    @InjectMocks
    private REThread sut;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();
    }

    @Test
    void isDisposed_shouldReturnTrueWhenInstanceIsNull() {
        assertTrue(sut.isDisposed());
    }

    @Test
    void isRunning()  {
        assertTrue(sut.isRunning());
    }
    @Test
    void isDisposed_shouldReturnTrueWhenInstanceIsDisposed() {
        var instance = mockAndSetupREInstance(sut);
        when(instance.isDisposed()).thenReturn(true);

        assertTrue(sut.isDisposed());
    }

    @Test
    void isDisposed_shouldReturnFalseWhenInstanceIsNotNullAndNotDisposed() {
        var instance = mockAndSetupREInstance(sut);
        when(instance.isDisposed()).thenReturn(false);

        assertFalse(sut.isDisposed());
    }

    @Test
    void getSupportedBackendRuns_shouldReturnExpectedMap() {
        var errorRunCountMap = sut.getSupportedBackendRuns();

        assertNotNull(errorRunCountMap);
        assertFalse(errorRunCountMap.isEmpty());
    }

    @Test
    void createCopy_shouldReturnNewREThreadInstance() {
        var copy = sut.createCopy();

        assertNotSame(sut, copy);
        assertInstanceOf(REThread.class, copy);
    }

    @Test
    void shutdownCurrentInstance_shouldKillInstanceAndSetToNull() {
        var instance = mockAndSetupREInstance(sut);

        sut.shutdownCurrentInstance();

        verify(instance).kill();
        assertNull(ReflectionTestUtils.getField(sut, "instance"));
    }

    @Test
    void prepareProcessingJob_shouldReturnTrueAndSetJobExecutionDataWhenInstanceIsNotNull() {
        var instance = mockAndSetupREInstance(sut);

        boolean result = sut.prepareProcessingJob();

        assertTrue(result);
        assertNotNull(instance);
    }

    @Test
    void prepareProcessingJob_shouldReturnFalseWhenInstanceIsNull() {
        boolean result = sut.prepareProcessingJob();

        assertFalse(result);
    }

    @Test
    void handlePrepareProcessingJobFailed_shouldSleepThreadForConnectTimeout() {
        var connectTimeout = 100L;
        ReflectionTestUtils.setField(sut, "connectTimeout", connectTimeout);

        try (MockedStatic<ServerManager> serverManagerMockedStatic = Mockito.mockStatic(ServerManager.class)) {
            serverManagerMockedStatic.when(() -> ServerManager.isLogError()).thenReturn(true);

            sut.handlePrepareProcessingJobFailed();

            serverManagerMockedStatic.verify(() -> ServerManager.logError(anyString()), times(1));
            serverManagerMockedStatic.verify(() -> ServerManager.sleepThread(eq(connectTimeout), eq(true)), times(1));
        }
    }

    @Test
    void setLocale_shouldSetLocalePropertyWhenCurrentInstanceIsNotNull() throws Exception {
        var instance = mockAndSetupREInstance(sut);
        var serviceFactory = mock(XMultiComponentFactory.class);
        var serviceContext = mock(XComponentContext.class);
        var xLocaleService = mock(XPropertySet.class);

        when(instance.getComponentFactory()).thenReturn(serviceFactory);
        when(instance.getDefaultContext()).thenReturn(serviceContext);
        when(serviceFactory.createInstanceWithContext(anyString(), eq(serviceContext))).thenReturn(xLocaleService);

        String jobLocale = "en_US";
        String result = sut.setLocale(jobLocale);

        assertEquals(jobLocale, result);
        verify(xLocaleService).setPropertyValue(eq("OxLocale"), eq("en-US"));
    }

    @Test
    void setLocale_shouldSetDefaultLocaleWhenCurrentInstanceIsNull() {
        var jobLocale = "en_US";
        var result = sut.setLocale(jobLocale);

        assertEquals(jobLocale, result);
    }

    @Test
    void endProcessingJob_shouldShutdownCurrentInstanceWhenJobErrorHasError() {
        var instance = mockAndSetupREInstance(sut);

        JobEntry curJobEntry = mock(JobEntry.class);
        when(curJobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.TIMEOUT));
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        sut.endProcessingJob(curJobEntry);

        verify(instance).kill();
    }

    @Test
    void endProcessingJob_shouldShutdownCurrentInstanceWhenRestartCountIsLessThan2() {
        var instance = mockAndSetupREInstance(sut);

        JobEntry curJobEntry = mock(JobEntry.class);
        when(curJobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        when(instance.getRestartCount()).thenReturn(1);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        sut.endProcessingJob(curJobEntry);

        verify(instance).kill();
    }

    @Test
    void endProcessingJob_shouldShutdownCurrentInstanceWhenInstanceJobsProcessedIsMultipleOfRestartCount() {
        var instance = mockAndSetupREInstance(sut);

        JobEntry curJobEntry = mock(JobEntry.class);
        when(curJobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        when(instance.getRestartCount()).thenReturn(2);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);

        sut.endProcessingJob(curJobEntry);

        verify(instance).kill();
    }

    @Test
    void endProcessingJob_shouldNotShutdownCurrentInstanceWhenConditionsAreNotMet() {
        var instance = mockAndSetupREInstance(sut);

        JobEntry curJobEntry = mock(JobEntry.class);
        when(curJobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));
        when(instance.getRestartCount()).thenReturn(2);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        ReflectionTestUtils.setField(sut, "instanceJobsProcessed", 1);

        sut.endProcessingJob(curJobEntry);

        verify(instance, never()).kill();
    }

    @Test
    void getRestartCount_shouldReturnRestartCountFromCurrentInstanceWhenNotNull() {
        var instance = mockAndSetupREInstance(sut);
        when(instance.getRestartCount()).thenReturn(2);

        int result = sut.getRestartCount();

        assertEquals(2, result);
    }

    @Test
    void getRestartCount_shouldReturnSuperRestartCountWhenCurrentInstanceIsNull() {
        int result = sut.getRestartCount();

        assertEquals(1, result);
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

    private REInstance mockAndSetupREInstance(REThread reThread) {
        var instance = mock(REInstance.class);
        ReflectionTestUtils.setField(reThread, "instance", instance);
        return instance;
    }

}
