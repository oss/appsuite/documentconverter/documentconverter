/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.DocumentConverterJob;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class JobEntryTest {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private Job job;

    @Mock
    private JobStatusListener jobStatusListener;

    private JobEntry sut;

    @BeforeEach
    public void setUp() {
        // TODO seup needed ??
    }

    @Test
    public void testGetHash() {
        var jobId = UUID.randomUUID().toString();
        when(job.getHash()).thenReturn("hash");

        sut = new JobEntry(jobId, job, JobPriority.MEDIUM, jobStatusListener);

        assertEquals("hash", sut.getHash());
    }

    @Test
    public void testGetQueueIdentifierAndJobWithHash() {
        var jobId = UUID.randomUUID().toString();
        when(job.getHash()).thenReturn("hash");

        sut = new JobEntry(jobId, job, JobPriority.MEDIUM, jobStatusListener);

        assertEquals("hash", sut.getQueueIdentifier());
    }

    @Test
    public void testGetQueueIdentifierAndJobWithoutHash() {
        var jobId = UUID.randomUUID().toString();

        sut = new JobEntry(jobId, job, JobPriority.MEDIUM, jobStatusListener);

        assertEquals(jobId, sut.getQueueIdentifier());
    }

    @Test
    public void testGetJobProperties() {
        // Set up
        Map<String, Object> jobProperties = new HashMap<>();
        jobProperties.put("key1", "value1");
        jobProperties.put("key2", "value2");
        when(job.getJobProperties()).thenReturn(jobProperties);

        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(jobProperties, sut.getJobProperties());
    }

    @Test
    public void testGetResultProperties() {
        // Set up
        Map<String, Object> resultProperties = new HashMap<>();
        resultProperties.put("key1", "value1");
        resultProperties.put("key2", "value2");
        when(job.getResultProperties()).thenReturn(resultProperties);

        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(resultProperties, sut.getResultProperties());
    }

    @Test
    public void testGetJobStatus() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(JobStatus.NEW, sut.getJobStatus());
    }

    @Test
    public void testSetJobStatus() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        sut.setJobStatus(JobStatus.SCHEDULED);
        assertEquals(JobStatus.SCHEDULED, sut.getJobStatus());

        sut.setJobStatus(JobStatus.RUNNING);
        assertEquals(JobStatus.RUNNING, sut.getJobStatus());

        sut.setJobStatus(JobStatus.SCHEDULED);
        assertEquals(JobStatus.SCHEDULED, sut.getJobStatus());
    }

    @Test
    public void testGetJobErrorEx() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertNotNull(sut.getJobErrorEx());
    }

    @Test
    public void testSetJobErrorEx() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Set up
        var jobErrorEx = new JobErrorEx();
        sut.setJobErrorEx(jobErrorEx);

        // Verify
        assertEquals(jobErrorEx, sut.getJobErrorEx());
    }

    @Test
    public void testGetJobPriority() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(JobPriority.MEDIUM, sut.getJobPriority());
    }

    @Test
    public void testGetScheduledAtTimeMillis() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(0, sut.getScheduledAtTimeMillis());
    }

    @Test
    public void testGetQueueTimeMillis() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(0, sut.getQueueTimeMillis());
    }

    @Test
    public void testGetConversionTimeMillis() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(0, sut.getConversionTimeMillis());
    }

    @Test
    public void testGetJobTimeMillis() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals(0, sut.getJobTimeMillis());
    }

    @Test
    public void testSetCacheHit() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Set up
        sut.setCacheHit();

        // Verify
        assertTrue(sut.isCacheHit());
    }

    @Test
    public void testIsCacheHit() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertFalse(sut.isCacheHit());
    }

    @Test
    public void testSetErrorCacheHit() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Set up
        var jobErrorEx = new JobErrorEx(JobError.GENERAL);
        sut.setErrorCacheHit(jobErrorEx);

        // Verify
        assertTrue(sut.isErrorCacheHit());
        assertEquals(jobErrorEx, sut.getJobErrorEx());
    }

    @Test
    public void testIsErrorCacheHit() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertFalse(sut.isErrorCacheHit());
    }

    @Test
    public void testIsMultipleSchedule() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertFalse(sut.isMultipleSchedule());
    }

    @Test
    public void testGetJobLocale() {
        sut = new JobEntry("jobId", job, JobPriority.MEDIUM, jobStatusListener);

        // Verify
        assertEquals("en_US", sut.getJobLocale());
    }
}
