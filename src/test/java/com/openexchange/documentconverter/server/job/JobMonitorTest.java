/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
public class JobMonitorTest {

    private static final int QUEUE_LIMIT_LOW = 5;
    private static final int QUEUE_LIMIT_HIGH = 10;

    @Mock
    private JobProcessor jobProcessor;

    private Statistics statistics;

    private JobMonitor sut;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    public void setUp() {
        jobProcessor = mock(JobProcessor.class);
        statistics = new Statistics();

        setupLogListAppenderServerManager();

        sut = new JobMonitor(jobProcessor, QUEUE_LIMIT_HIGH, QUEUE_LIMIT_LOW, 60, statistics);
    }

    @Test
    public void testJobScheduledNotLimit() {
        var jobId = "jobId";
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobId()).thenReturn(jobId);

        sut.jobScheduled(jobEntry);

        assertTrue(statistics.getScheduledJobCountInQueue() > 0);
        assertFalse(sut.isQueueLimitReached());

        verify(jobEntry).getJobId();
    }

    @Test
    public void testJobScheduledLimitReached() {
        generateJobEntries(QUEUE_LIMIT_HIGH).forEach(sut::jobScheduled);

        assertTrue(statistics.getScheduledJobCountInQueue() > 0);
        assertTrue(sut.isQueueLimitReached());
    }

    @Test
    public void testJobSchedulingFinished() {
        var jobId = "jobId";
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobId()).thenReturn(jobId);

        sut.jobScheduled(jobEntry);
        sut.jobSchedulingFinished(jobEntry);

        assertEquals(0, statistics.getScheduledJobCountInQueue());
        assertFalse(sut.isQueueLimitReached());

        verify(jobEntry, times(2)).getJobId();
    }

    @Test
    public void testJobSchedulingFinishedWithLimitReached() {
        var jobEntries = generateJobEntries(QUEUE_LIMIT_HIGH);
        jobEntries.forEach(sut::jobScheduled);

        assertTrue(statistics.getScheduledJobCountInQueue() > 0);
        assertTrue(sut.isQueueLimitReached());

        var jobEntry = jobEntries.get(0);
        jobEntries.subList(0, QUEUE_LIMIT_LOW).forEach(sut::jobSchedulingFinished);

        assertEquals(QUEUE_LIMIT_HIGH - QUEUE_LIMIT_LOW, statistics.getScheduledJobCountInQueue());
        assertFalse(sut.isQueueLimitReached());

        verify(jobEntry, times(2)).getJobId();
    }

    @Test
    public void testTimeoutTimerAndQueueLimitReached() {
        var jobEntries = generateJobEntries(QUEUE_LIMIT_HIGH);

        sut = new JobMonitor(jobProcessor, QUEUE_LIMIT_HIGH, QUEUE_LIMIT_LOW, 5, 10, statistics);

        jobEntries.forEach(sut::jobScheduled);

        try {
            Thread.sleep(15);
        } catch (InterruptedException e) {
            fail("Thread interrupted not expected");
        }

        assertEquals(QUEUE_LIMIT_HIGH, statistics.getScheduledJobCountInQueue());
        assertTrue(sut.isQueueLimitReached());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC JobMonitor JobCountInQueueLimitHigh reached => requests are rejected"));
    }

    @Test
    public void testTerminate() {
        var jobId = "jobId";
        var jobEntry = mock(JobEntry.class);
        when(jobEntry.getJobId()).thenReturn(jobId);

        sut.jobScheduled(jobEntry);
        sut.terminate();

        // TODO: clarify if this is the expected behavior
        // Terminated JobMonitor should not be able to have scheduled jobs
        assertEquals(1, statistics.getScheduledJobCountInQueue());
        assertTrue(sut.isQueueLimitReached());
    }

    public List<JobEntry> generateJobEntries(int numOfJobEntries) {
        List<JobEntry> jobEntries = new ArrayList<>();
        for (int i = 0; i < numOfJobEntries; i++) {
            var jobEntry = mock(JobEntry.class);
            when(jobEntry.getJobId()).thenReturn(UUID.randomUUID().toString());
            jobEntries.add(jobEntry);
        }
        return jobEntries;
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

}
