/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.async;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.job.JobPriority;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
public class AsyncRunnableTest {

    @TempDir
    File tempFolder;

    @Autowired
    ServerConfig serverConfig;

    @Autowired
    AsyncServerExecutor asyncServerExecutor;

    @Autowired
    Statistics statistics;

    @Autowired
    ManagedJobFactory managedJobFactory;

    @Test
    void testEmptyJobTypeAndProperties() {
        var sut = getAsyncRunnable("", new HashMap<>());
        assertFalse(sut.isValid());
    }

    @Test
    void testEmptyJobType() {
        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_CACHE_HASH, "CacheHash");
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
        var sut = getAsyncRunnable("", jobProperties);
        assertTrue(sut.isValid());
    }

    @Test
    void testEmptyJobTypeAndEmptyPROP_PRIORITY() {
        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_CACHE_HASH, "CacheHash");
        jobProperties.put(Properties.PROP_PRIORITY, null);
        var sut = getAsyncRunnable("", jobProperties);
        assertTrue(sut.isValid());
    }

    @Test
    void testEmptyPROP_REMOTE_CACHE_HASH() {
        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_CACHE_HASH, "");
        var sut = getAsyncRunnable("", jobProperties);
        assertFalse(sut.isValid());
    }

    @Test
    void testClassCastExceptionPROP_INPUT_FILE() {
        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_INPUT_FILE, "CacheHash");
        assertThrows(ClassCastException.class, () -> getAsyncRunnable("jobType", jobProperties));
    }

    @Test
    void testInvalidPROP_INPUT_STREAM() {

        try (MockedStatic<ServerManager> serverManagerMockedStatic = Mockito.mockStatic(ServerManager.class)) {

            var jobProperties  = new HashMap<String, Object>();
            jobProperties.put(Properties.PROP_INPUT_FILE, new File(""));
            jobProperties.put(Properties.PROP_INPUT_STREAM, "");

            getAsyncRunnable("jobType", jobProperties);

            serverManagerMockedStatic.verify(() -> ServerManager.logExcp(any()), times(1));
        }
    }

    @Test
    void testEmptyProperties() {
        var sut = getAsyncRunnable("test", new HashMap<>());
        assertFalse(sut.isValid());
    }

    @Test
    void testNullPROP_INPUT_FILEAndValidPROP_INPUT_STREAM() throws IOException {
        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);

        var fileForInputStream = new File(tempFolder, TestUtil.createUniqueFileName("inputstream", "txt"));
        Files.createFile(fileForInputStream.toPath());
        var inputStream  = new FileInputStream(fileForInputStream);
        jobProperties.put(Properties.PROP_INPUT_STREAM, inputStream);

        var sut = getAsyncRunnable("jobType", jobProperties);
        assertTrue(sut.isValid());

        assertFalse(sut.getJobProperties().containsKey(Properties.PROP_INPUT_STREAM));
        assertTrue(sut.getJobProperties().containsKey(Properties.PROP_INPUT_FILE));
    }

    private AsyncRunnable getAsyncRunnable(String jobType, HashMap<String, Object> jobProperties) {
        final String cacheHash = (String)jobProperties.getOrDefault(Properties.PROP_CACHE_HASH, "");
        return new AsyncRunnable(jobType, cacheHash, jobProperties, asyncServerExecutor, statistics, serverConfig, managedJobFactory);
    }

}
