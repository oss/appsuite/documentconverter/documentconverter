/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.async;

import ch.qos.logback.classic.Level;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.job.JobPriority;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AsyncServerExecutorTest {

    private AsyncServerExecutor sut;

    private Statistics statistics;

    private ServerManager serverManager;
    private ServerConfig serverConfig;

    private ManagedJobFactory managedJobFactory;

    @TempDir
    File tempFolder;

    private void init() throws IOException {
        initServerConfig();
        initAsyncServerExecutor();
    }

    @SafeVarargs
    private void initServerConfig(AbstractMap.SimpleEntry<String, Object> ... fieldValues) throws IOException {
        serverConfig = spy(new ServerConfig());
        ReflectionTestUtils.setField(serverConfig, "jobProcessorCount", 3);
        ReflectionTestUtils.setField(serverConfig, "jobAsyncQueueCountLimitHigh", 2048);
        ReflectionTestUtils.setField(serverConfig, "scratchDirPath", "/tmp");
        ReflectionTestUtils.setField(serverConfig, "reInstallDir", "/tmp");
        for (var fieldValue : fieldValues) {
            ReflectionTestUtils.setField(serverConfig, fieldValue.getKey(), fieldValue.getValue());
        }
        serverConfig.initConfig();
    }

    @SafeVarargs
    private void initAsyncServerExecutor(AbstractMap.SimpleEntry<String, Object> ... fieldValues) throws IOException {
        statistics = spy(new Statistics());
        serverManager = spy(new ServerManager(serverConfig));
        managedJobFactory = mock(ManagedJobFactory.class);

        sut = spy(new AsyncServerExecutor());
        ReflectionTestUtils.setField(sut, "serverConfig", serverConfig);
        ReflectionTestUtils.setField(sut, "statistics", statistics);
        ReflectionTestUtils.setField(sut, "managedJobFactory", managedJobFactory);

        for (var fieldValue : fieldValues) {
            ReflectionTestUtils.setField(sut, fieldValue.getKey(), fieldValue.getValue());
        }
        sut.init();

    }

    @Test
    void testAsyncServerExecutor() throws IOException {
        init();
        sut.increment();
        verify(statistics, times(1)).incrementAsyncQueueCount();

        sut.decrement();
        verify(statistics, times(1)).decrementAsyncQueueCount();
    }

    @Test
    void testGetLogData() throws IOException {
        init();
        var logData = statistics.getAsyncServerExcutorLogData();

        assertEquals(3, logData.length);

        assertTrue(TestUtil.containsLogDataKey(logData, "async_jobs_remaining"));
        assertTrue(TestUtil.containsLogDataKey(logData, "async_jobs_ran"));
        assertTrue(TestUtil.containsLogDataKey(logData, "async_job_omitted"));
    }

    @Test
    void testGetDropCount() throws IOException, InterruptedException {

        var retryCount = 0;
        var successful = false;
        while (retryCount < 4 && !successful)  {
            initServerConfig(new AbstractMap.SimpleEntry<>("jobProcessorCount", 1), new AbstractMap.SimpleEntry<>("jobAsyncQueueCountLimitHigh", 1));
            initAsyncServerExecutor();

            assertEquals(0, statistics.getAsyncJobCountDropped());

            var jobProperties = getDummyProperties();

            for (int i=0; i < 3000; i++) {
                sut.triggerExecution("DummyJobType" + i, "cacheHash", jobProperties, new HashMap<>());
            }

            Thread.sleep(10);
            successful = statistics.getAsyncJobCountDropped() > 0;
            retryCount++;
        }

        assertNotEquals(0, statistics.getAsyncJobCountDropped());
    }

    @Test
    void testGetProcessedCount() throws IOException, InterruptedException {
        init();

        var jobProperties = getDummyProperties();
        var logData = statistics.getAsyncServerExcutorLogData();
        assertEquals("0", TestUtil.getLogDataValue(logData, "async_jobs_ran"));

        sut.triggerExecution("DummyJobType", "CacheHash", jobProperties, new HashMap<String, Object>());
        sut.triggerExecution("DummyJobType2", "CacheHash", jobProperties, new HashMap<String, Object>());

        Thread.sleep(100);

        assertEquals(2, statistics.getAsyncJobProcessedCount());
        logData = statistics.getAsyncServerExcutorLogData();
        assertEquals("2", TestUtil.getLogDataValue(logData, "async_jobs_ran"));
    }

    @Test
    void testGetScheduledCount() throws IOException, InterruptedException {
        init();

        assertEquals(0, statistics.getAsyncJobCountScheduled());

        var jobProperties = getDummyProperties();
        for (int i=0; i < 10; i++) {
            sut.triggerExecution("DummyJobType" + i, "CacheHash", jobProperties, new HashMap<>());
        }
        Thread.sleep(100);
        verify(statistics, times(10)). incrementAsyncQueueCount();
        verify(statistics, times(10)). decrementAsyncQueueCount();

    }

    @Test
    void testTriggerExecutionNotValid() throws IOException {
        init();
        var jobProperties = getDummyProperties();
        final String cacheHash = (String)jobProperties.getOrDefault(Properties.PROP_CACHE_HASH, "CacheHash");
        jobProperties.replace(Properties.PROP_INPUT_STREAM, null);

        sut.triggerExecution("DummyJobType", cacheHash, jobProperties, new HashMap<>());
    }

    @Test
    void testImplLogAsyncDroppedStatus() throws IOException, InterruptedException {

        initServerConfig(new AbstractMap.SimpleEntry<>("jobProcessorCount", 1), new AbstractMap.SimpleEntry<>("jobAsyncQueueCountLimitHigh", 1));
        initAsyncServerExecutor(new AbstractMap.SimpleEntry<>("LOG_CONNECTION_STATUS_PERIOD_SECONDS", 1L));

        var jobProperties = getDummyProperties();
        for (int i=0; i < 100; i++) {
            sut.triggerExecution("DummyJobType" + i, "CacheHash", jobProperties, new HashMap<>());
        }

        Thread.sleep(1500);

        verify(sut, atLeastOnce()).logAsyncDroppedStatus();

    }

    @Test
    void testNeedsRunning() throws IOException {
        init();
        var asyncIdentifier = mock(AsyncRunnable.class);
        assertTrue(sut.needsRunning(asyncIdentifier));
        assertFalse(sut.needsRunning(asyncIdentifier));
    }

    @Test
    void testTriggerExecutionWithInvalidAsyncRunnable() throws IOException {
        init();
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        sut.triggerExecution("DummyJobType", "CacheHash", new HashMap<>(), new HashMap<>());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC is not able to execute async. job without file input, stream input or cached hash value set => please set source input property accordingly"));

    }

    @Test
    void testTriggerExecutionWithAsyncRunnableNeedsRunning() throws IOException {
        init();
        var logData = statistics.getAsyncServerExcutorLogData();

        assertEquals("0", TestUtil.getLogDataValue(logData,"async_job_omitted"));

        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_CACHE_HASH, "cacheHash");
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);

        sut.triggerExecution("DummyJobType", "CacheHash", jobProperties, new HashMap<>());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC scheduled asynchronous job"));
        verify(statistics, times(1)).incrementAsyncQueueCount();
    }

    // TODO: unit test failed, removed temporarily
    @Test
    @Disabled
    void testTriggerExecutionWithAsyncRunnableNotNeedsRunning() throws IOException {
        init();
        var logData = statistics.getAsyncServerExcutorLogData();

        assertEquals("0", TestUtil.getLogDataValue(logData,"async_job_omitted"));

        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        var jobProperties  = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_CACHE_HASH, "CacheHash");
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);

        // trigger execution for the same job twice to get false for needsRunning(asyncRunnable)
        sut.triggerExecution("DummyJobType", "CacheHash", jobProperties, new HashMap<>());
        verify(statistics, times(1)).incrementAsyncQueueCount();
        sut.triggerExecution("DummyJobType", "CacheHash", jobProperties, new HashMap<>());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC already scheduled same asynchronous job => omitting request"));

        // test omitCounter.incrementAndGet();
        logData = statistics.getAsyncServerExcutorLogData();
        assertEquals("1", TestUtil.getLogDataValue(logData,"async_job_omitted"));
    }

    @Test
    void testShutdown() throws IOException {
        init();
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        assertTrue(sut.isRunning());
        sut.shutdown();
        assertFalse(sut.isRunning());

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC AsyncExecutor starting shutdown..."));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC AsyncExecutor finished shutdown:"));
    }

    @Test
    void testTimerExecutor() throws IOException, InterruptedException {
        initServerConfig();
        initAsyncServerExecutor(new AbstractMap.SimpleEntry<>("CLEANUP_TIMEOUT_MILLIS", 5L));

        var asyncRunnable = mock(AsyncRunnable.class);
        var asyncIdentifier = spy(new AsyncIdentifier("testHash"));
        when(asyncRunnable.getAsyncIdentifier()).thenReturn(asyncIdentifier);

        assertTrue(sut.needsRunning(asyncRunnable));
        Thread.sleep(100);
        // If the testTimerExecutor not remove the asyncRunnable, sut.needsRunning(asyncRunnable) must be False
        assertTrue(sut.needsRunning(asyncRunnable));
        verify(asyncIdentifier, atLeastOnce()).getTimestampMillis();
    }

    // TODO (KA): needs to be checked and reenabled
    @Test
    @Disabled
    void testShutdownRequestQueuePoll() throws IOException, InterruptedException {
        initServerConfig(new AbstractMap.SimpleEntry<>("jobProcessorCount", 5), new AbstractMap.SimpleEntry<>("jobAsyncQueueCountLimitHigh", 7));
        statistics = spy(new Statistics());
        managedJobFactory = spy(new ManagedJobFactory() {
            public InputStream convert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                return null;
            }
        });

        sut = spy(new AsyncServerExecutor());
        ReflectionTestUtils.setField(sut, "serverConfig", serverConfig);
        ReflectionTestUtils.setField(sut, "statistics", statistics);

        ReflectionTestUtils.setField(sut, "managedJobFactory", managedJobFactory);

        sut.init();
        verify(sut, times(0)).decrement();
        var jobProperties = getDummyProperties();
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.LOW);

        for (int i=0; i < 100; i++) {
            sut.triggerExecution("DummyJobType" + i, "CacheHash", jobProperties, new HashMap<>());
        }

        var requestQueue = (BlockingQueue<Runnable>) ReflectionTestUtils.getField(sut, "requestQueue");
        assertNotNull(requestQueue);
        // requestQueue is not Empty before shutdown
        assertEquals(7, requestQueue.size());

        sut.shutdown();

        assertTrue(requestQueue.isEmpty());
    }


    private HashMap<String, Object> getDummyProperties() throws IOException {
        var jobProperties = new HashMap<String, Object>();
        jobProperties.put(Properties.PROP_INPUT_STREAM, new InputStream() {
            @Override
            public int read() { return 0; }}
        );

        var file = new File(tempFolder, TestUtil.createUniqueFileName("AsyncServerExecutorTest_Testfile", "txt"));
        Files.createFile(file.toPath());
        jobProperties.put(Properties.PROP_CACHE_HASH, "CacheHash");
        jobProperties.put(Properties.PROP_INPUT_FILE, file);
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
        return jobProperties;
    }

}
