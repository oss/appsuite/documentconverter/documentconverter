/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.async;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AsyncIdentifierTest {

    private static String ASYNC_HASH = "test";

    @Test
    void testHashCodeAsyncHashNotNull() {
        var sut = new AsyncIdentifier(ASYNC_HASH);
        assertEquals(31 + ASYNC_HASH.hashCode(), sut.hashCode());
    }

    @Test
    void testEqualsSameObjectAsyncHashNotNull() {
        var sut = new AsyncIdentifier(ASYNC_HASH);
        assertTrue(sut.equals(sut));
    }

    @Test
    void testEqualsNull() {
        assertFalse(new AsyncIdentifier(ASYNC_HASH).equals(null));
    }

    @Test
    void testEqualsWithTheSameHash() {
        assertTrue(new AsyncIdentifier(ASYNC_HASH).equals(new AsyncIdentifier(ASYNC_HASH)));
    }


    @Test
    void testTimestampMillis() throws InterruptedException {
        var currentTimeInMillis = System.currentTimeMillis();
        Thread.sleep(100);
        var sut = new AsyncIdentifier(ASYNC_HASH);
        assertTrue(currentTimeInMillis + 100 <= sut.getTimestampMillis());
    }
}
