/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import org.json.JSONObject;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PropertiesTest {

    private final Map<String, Object> properties = new HashMap<>();

    public static Stream<Arguments> propertiesValuesAndTypes() {
        return Stream.of(
            Arguments.of(Properties.PROP_PIXEL_HEIGHT, 200, null),
            Arguments.of(Properties.PROP_PIXEL_ZOOM, 1.25D, null),
            Arguments.of(Properties.PROP_INPUT_STREAM, new ByteArrayInputStream("test".getBytes()), null),
            Arguments.of(Properties.PROP_RESULT_ERROR_CODE, 16, Properties.JSON_KEY_ERRORCODE),
            Arguments.of(Properties.PROP_RESULT_EXTENSION, "pdf", Properties.JSON_KEY_EXTENSION),
            Arguments.of(Properties.PROP_RESULT_LOCALE, "en-US", Properties.JSON_KEY_LOCALE),
            Arguments.of(Properties.PROP_RESULT_ERROR_DATA, "error data", Properties.JSON_KEY_ERRORDATA),
            Arguments.of(Properties.PROP_RESULT_JOBID, "1234567890", Properties.JSON_KEY_JOBID),
            Arguments.of(Properties.PROP_RESULT_MIME_TYPE, "application/pdf", Properties.JSON_KEY_MIMETYPE),
            Arguments.of(Properties.PROP_RESULT_PAGE_COUNT, 60, Properties.JSON_KEY_PAGECOUNT),
            Arguments.of(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, 100, Properties.JSON_KEY_ORIGINALPAGECOUNT),
            Arguments.of(Properties.PROP_RESULT_PAGE_NUMBER, 1, Properties.JSON_KEY_PAGENUMBER),
            Arguments.of(Properties.PROP_RESULT_SHAPE_NUMBER, 9, Properties.JSON_KEY_SHAPENUMBER),
            Arguments.of(Properties.PROP_RESULT_TEMP_INPUT_FILE, new File("/dev/null"), Properties.JSON_KEY_INPUTFILE),
            Arguments.of(Properties.PROP_RESULT_TEMP_INFO_FILENAME, "test.odt", Properties.JSON_KEY_INFOFILENAME),
            Arguments.of(Properties.PROP_RESULT_TEMP_INPUT_TYPE, "pdf", Properties.JSON_KEY_INPUTTYPE),
            Arguments.of(Properties.PROP_RESULT_SINGLE_PAGE_JOBTYPE, "pdf", Properties.JSON_KEY_SINGLEPAGEJOBTYPE),
            Arguments.of(Properties.PROP_RESULT_ZIP_ARCHIVE, false, Properties.JSON_KEY_ZIPARCHIVE),
            Arguments.of(Properties.PROP_RESULT_ASYNC, true, Properties.JSON_KEY_ASYNC)
        );
    }

    @ParameterizedTest
    @MethodSource("propertiesValuesAndTypes")
    void toJSONResult(String propertyName, Object propertyValue, String jsonKey) {
        properties.put(propertyName, propertyValue);

        var jsonWithProps = Properties.toJSONResult(properties);

        if (jsonKey != null) {
            var jsonValue = jsonWithProps.get(jsonKey);
            if (propertyValue instanceof File propFile) {
                assertEquals(propFile.getPath(), jsonValue);
            } else {
                assertEquals(propertyValue, jsonWithProps.get(jsonKey));
            }
        } else {
            assertNull(jsonWithProps.opt(propertyName));
        }
    }

    public static Stream<Arguments> jsonKeyValeAndPropertyName() {
        return Stream.of(
            Arguments.of(Properties.JSON_KEY_INPUTFILE, new File("/dev/null"), Properties.PROP_RESULT_TEMP_INPUT_FILE),
            Arguments.of(Properties.JSON_KEY_PAGECOUNT, 999, Properties.PROP_RESULT_PAGE_COUNT),
            Arguments.of(Properties.JSON_KEY_ORIGINALPAGECOUNT, 111, Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT),
            Arguments.of(Properties.JSON_KEY_SHAPENUMBER, 333, Properties.PROP_RESULT_SHAPE_NUMBER),
            Arguments.of(Properties.JSON_KEY_ZIPARCHIVE, true, Properties.PROP_RESULT_ZIP_ARCHIVE),
            Arguments.of(Properties.JSON_KEY_ASYNC, true, Properties.PROP_RESULT_ASYNC),
            Arguments.of(Properties.JSON_KEY_PAGENUMBER, 1, Properties.PROP_RESULT_PAGE_NUMBER),
            Arguments.of(Properties.JSON_KEY_ERRORCODE, 16, Properties.PROP_RESULT_ERROR_CODE)
        );
    }
}
