/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.thread.ThreadFactory;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class REInstanceProviderTest {

    @Mock
    private ServerManager serverManager;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private REDescriptor descriptor;

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    public void setUp() {
        setupLogListAppenderServerManager();
    }

    @Test
    public void testInit_WithPositiveInstancePoolSize() {
        var instancePoolSize = 3;
        REInstanceProvider sut = null;

        try (var mockedREInstanceConstruction = mockConstruction(REInstance.class, (mock, context) ->
            when(mock.launch()).thenReturn(true))) {

            sut = new REInstanceProvider(serverManager, jobProcessor, descriptor, instancePoolSize);

            assertNotNull(sut.getREInstance());
            assertTrue(sut.isRunning());

            // other reinstances are created via the background thread asynchronously
            assertTrue(mockedREInstanceConstruction.constructed().size() >= 1);
        } finally {
            if (sut != null) {
                sut.terminate();
            }
        }

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ReaderEngine instance provider created thread pool"));
    }

    @Test
    public void testInit_WithZeroInstancePoolSize() {
        int instancePoolSize = 0;
        REInstanceProvider sut = null;

        try (var mockedREInstanceConstruction = mockConstruction(REInstance.class, (mock, context) ->
            when(mock.launch()).thenReturn(true))) {

            sut = new REInstanceProvider(serverManager, jobProcessor, descriptor, instancePoolSize);

            assertNotNull(sut.getREInstance());
            assertTrue(sut.isRunning());

            // direct call of getREInstance() creates one instance
            assertEquals(1, mockedREInstanceConstruction.constructed().size());
        } finally {
            if (sut != null) {
                sut.terminate();
            }
        }

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ReaderEngine instance provider is using no thread pool"));
    }

    @Test
    public void testTerminate() {
        int instancePoolSize = 1;

        try (var ignored = mockConstruction(REInstance.class, (mock, context) ->
            when(mock.launch()).thenReturn(true))) {

            var sut = new REInstanceProvider(serverManager, jobProcessor, descriptor, instancePoolSize);

            assertNotNull(sut);

            sut.terminate();

            assertFalse(sut.isRunning());
        }

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ReaderEngine instance provider starting shutdown..."));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ReaderEngine ReaderEngine instance provider finished shutdown:"));
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

}
