/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.ServerManager;
import com.sun.star.task.XInteractionContinuation;
import com.sun.star.task.XInteractionRequest;
import com.sun.star.task.XInteractionURLResolver;
import com.sun.star.uno.UnoRuntime;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class URLResolveHandlerTest {

    private final static int TEMP_DIR_ID = 1;

    @Mock
    private ServerManager serverManager;

    @TempDir
    private Path tempDir;

    private URLResolveHandler sut;

    private AtomicReference<String> tempDirPathName = new AtomicReference<>();

    @BeforeEach
    void setUp() {
        when(serverManager.createTempDir(anyString())).thenAnswer(invocation -> {
            var newTempFilePrefix = (String)invocation.getArgument(0);
            var tempDirFile = new File(tempDir.toFile(), newTempFilePrefix);
            tempDirPathName.set(tempDirFile.getAbsolutePath());
            return tempDirFile;
        });

        sut = new URLResolveHandler(serverManager, TEMP_DIR_ID);
    }

    @AfterEach
    void tearDown() {
        sut.kill();
    }

    @Test
    void handle_ValidSourceURL_ResolvesAndSetsTargetURL() throws Exception {
        var id = ((AtomicLong)ReflectionTestUtils.getField(URLResolveHandler.class, "TEMPFILE_ID")).get() + 1;
        var sourceUrl = "http://example.com/source.docx";
        var xRequest = mock(XInteractionRequest.class);
        var xContinuation = mock(XInteractionContinuation.class);
        var xURLResolver = mock(XInteractionURLResolver.class);

        when(xRequest.getContinuations()).thenReturn(new XInteractionContinuation[]{ xContinuation });
        when(xURLResolver.getSourceURL()).thenReturn(sourceUrl);

        try (var ignored = mockStatic(UnoRuntime.class)) {
            when(UnoRuntime.queryInterface(XInteractionURLResolver.class, xContinuation)).thenReturn(xURLResolver);

            try (var ignored2 = mockStatic(ServerManager.class)) {
                when(ServerManager.resolveURL(eq(sourceUrl), any(File.class), any(MutableObject.class))).thenReturn(new File("/temp/dir/1.docx"));

                sut.handle(xRequest);
            }
        }

        verify(xURLResolver).setTargetURL(eq("file://" + tempDirPathName.get() + "/" + id));
    }

    @Test
    void handle_InvalidSourceURL_DeletesOutputFile() {
        var xRequest = mock(XInteractionRequest.class);
        var xContinuation = mock(XInteractionContinuation.class);
        var xURLResolver = mock(XInteractionURLResolver.class);
        when(xRequest.getContinuations()).thenReturn(new XInteractionContinuation[]{xContinuation});
        when(xURLResolver.getSourceURL()).thenReturn("http://example.com/invalid.docx");

        try (var ignored = mockStatic(UnoRuntime.class)) {
            when(UnoRuntime.queryInterface(XInteractionURLResolver.class, xContinuation)).thenReturn(xURLResolver);

            try (var fileUtilsMock = mockStatic(FileUtils.class)) {
                try (var serverManagerMock = mockStatic(ServerManager.class)) {

                    sut.handle(xRequest);

                    serverManagerMock.verify(() -> ServerManager.resolveURL(eq("http://example.com/invalid.docx"), any(File.class), any(MutableObject.class)));
                    fileUtilsMock.verify(() -> FileUtils.deleteQuietly(any(File.class)));
                }
            }
        }

        verify(xURLResolver, never()).setTargetURL(anyString());
    }

    @Test
    void kill_DeletesTempDir() {
        var tempDir = (File)ReflectionTestUtils.getField(sut, "tempDir");

        try (var fileUtilsMock = mockStatic(FileUtils.class)) {
            sut.kill();

            fileUtilsMock.verify(() -> FileUtils.deleteQuietly(eq(tempDir)));
        }
    }
}
