/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.uno.Any;
import com.sun.star.uno.Type;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Path;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
public class REInstanceTest {

    @Mock
    private ServerManager serverManager;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private REDescriptor reDescriptor;

    @TempDir
    private Path tempDir;

    private REInstance sut;

    private ListAppender<ILoggingEvent> logListAppenderREInstance;

    @BeforeEach
    void setUp() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderREInstance = TestUtil.addListAppenderToLogger(REInstance.class);

        sut = new REInstance(serverManager, jobProcessor, reDescriptor);
    }

    @AfterAll
    public static void cleanup() {
        ReflectionTestUtils.setField(REInstance.class, "VALID_INSTALLATION", null);
    }

    @Test
    void testLaunch_ValidInstallation_ReturnsTrue() throws Exception {
        ReflectionTestUtils.setField(REInstance.class, "VALID_INSTALLATION", null);

        var installDirPath = tempDir.resolve("install").toFile();
        var userDirPath = tempDir.resolve("user").toFile();
        assertTrue(installDirPath.mkdirs());
        assertTrue(userDirPath.mkdirs());
        System.out.println("installDirPath: " + installDirPath.getAbsolutePath());

        var urlProxy = "http://localhost:8080";
        var execService = mock(ExecutorService.class);

        var processInputStream = new ByteArrayInputStream("".getBytes());
        var processMock = mock(Process.class);

        MockedConstruction<ProcessReaderRunnable> mockConstructionProcessReaderRunnable = null;
        MockedStatic<Bootstrap> mockedStaticBootstrap = null;
        MockedStatic<UnoRuntime> mockedStaticUnoRuntime = null;

        try (var ignored = mockConstruction(ProcessBuilder.class,
            (mock, context) -> when(mock.start()).thenReturn(processMock))) {

            when(reDescriptor.getInstallPath()).thenReturn(installDirPath.getAbsolutePath());
            when(reDescriptor.getUserPath()).thenReturn(userDirPath.getAbsolutePath());
            when(reDescriptor.getUrlLinkProxy()).thenReturn(urlProxy);
            when(jobProcessor.getReaderEngineExecutorService()).thenReturn(execService);
            when(processMock.exitValue()).thenThrow(new IllegalThreadStateException());
            when(processMock.getInputStream()).thenReturn(processInputStream);

            mockConstructionProcessReaderRunnable = mockConstruction(ProcessReaderRunnable.class);

            var xComponentContext = mock(XComponentContext.class);
            mockedStaticBootstrap = mockStatic(Bootstrap.class);
            when(Bootstrap.createInitialComponentContext(nullable(Hashtable.class))).thenReturn(xComponentContext);

            var xConnection = new Object();
            var xUnoUrlResolver = mock(XUnoUrlResolver.class);
            var xMultiComponentFactory = mock(XMultiComponentFactory.class);
            when(xUnoUrlResolver.resolve(anyString())).thenReturn(xConnection);
            when(xMultiComponentFactory.createInstanceWithContext(eq("com.sun.star.bridge.UnoUrlResolver"), eq(xComponentContext))).thenReturn(xUnoUrlResolver);
            when(xComponentContext.getServiceManager()).thenReturn(xMultiComponentFactory);

            mockedStaticUnoRuntime = mockStatic(UnoRuntime.class);
            var pidAsAny = new Any(Type.STRING, "1234");
            var xDesktop = mock(XDesktop.class);
            var xDefaultContext = mock(XComponentContext.class);
            var xPropertySet = mock(XPropertySet.class);
            var componentFactory = mock(XMultiComponentFactory.class);
            var serviceFactory = mock(XMultiServiceFactory.class);
            var oxAliveProperties = mock(XPropertySet.class);
            var remoteProxyProperties = mock(XPropertySet.class);
            when(xPropertySet.getPropertyValue(eq("DefaultContext"))).thenReturn(xDefaultContext);
            when(oxAliveProperties.getPropertyValue(eq("OxGetPID"))).thenReturn(pidAsAny);
            when(componentFactory.createInstanceWithContext(eq("com.sun.star.frame.Desktop"), eq(xDefaultContext))).thenReturn(xDesktop);
            when(componentFactory.createInstanceWithContext(eq("com.sun.star.document.FilterFactory"), eq(xDefaultContext))).thenReturn(serviceFactory);
            when(componentFactory.createInstanceWithContext(eq("com.sun.star.config.OxAliveService"), eq(xDefaultContext))).thenReturn(oxAliveProperties);
            when(componentFactory.createInstanceWithContext(eq("com.sun.star.config.OxRemoteProxyService"), eq(xDefaultContext))).thenReturn(remoteProxyProperties);
            when(UnoRuntime.queryInterface(eq(XMultiComponentFactory.class), eq(xConnection))).thenReturn(componentFactory);
            when(UnoRuntime.queryInterface(eq(XUnoUrlResolver.class), eq(xUnoUrlResolver))).thenReturn(xUnoUrlResolver);
            when(UnoRuntime.queryInterface(eq(XPropertySet.class), eq(componentFactory))).thenReturn(xPropertySet);
            when(UnoRuntime.queryInterface(eq(XComponentContext.class), eq(xDefaultContext))).thenReturn(xDefaultContext);
            when(UnoRuntime.queryInterface(eq(XDesktop.class), eq(xDesktop))).thenReturn(xDesktop);
            when(UnoRuntime.queryInterface(eq(XMultiComponentFactory.class), eq(xConnection))).thenReturn(componentFactory);
            when(UnoRuntime.queryInterface(eq(XUnoUrlResolver.class), eq(xUnoUrlResolver))).thenReturn(xUnoUrlResolver);
            when(UnoRuntime.queryInterface(eq(XPropertySet.class), eq(componentFactory))).thenReturn(xPropertySet);
            when(UnoRuntime.queryInterface(eq(XComponentContext.class), eq(xDefaultContext))).thenReturn(xDefaultContext);
            when(UnoRuntime.queryInterface(eq(XDesktop.class), eq(xDesktop))).thenReturn(xDesktop);
            when(UnoRuntime.queryInterface(eq(XMultiServiceFactory.class), eq(serviceFactory))).thenReturn(serviceFactory);
            when(UnoRuntime.queryInterface(eq(XPropertySet.class), eq(oxAliveProperties))).thenReturn(oxAliveProperties);
            when(UnoRuntime.queryInterface(eq(XPropertySet.class), eq(remoteProxyProperties))).thenReturn(remoteProxyProperties);

            var result = sut.launch();

            assertEquals(installDirPath.getAbsolutePath(), reDescriptor.getInstallPath());
            assertTrue(result);
        } finally {
            if (mockConstructionProcessReaderRunnable != null) { mockConstructionProcessReaderRunnable.close(); }
            if (mockedStaticBootstrap != null) { mockedStaticBootstrap.close(); }
            if (mockedStaticUnoRuntime != null) { mockedStaticUnoRuntime.close(); }
        }

        assertNotNull(sut.getComponentFactory());
        assertNotNull(sut.getDefaultContext());
        assertNotNull(sut.getFilterFactory());
        assertNotNull(sut.getDesktop());

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderREInstance, "DC ReaderEngine connected"));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderREInstance, "DC ReaderEngine launched"));
    }

    @Test
    void testLaunch_InvalidInstallation_ReturnsFalse() {
        when(reDescriptor.getInstallPath()).thenReturn("/invalid/installation");
        when(reDescriptor.getUserPath()).thenReturn("/path/to/user");

        var result = sut.launch();

        assertFalse(result);
    }

    @Test
    void testIsDisposed() {
        var sut = new REInstance(serverManager, jobProcessor, reDescriptor);

        assertTrue(sut.isDisposed());

        var pid = 9999;
        var process = mock(Process.class);
        ReflectionTestUtils.setField(sut, "process", process);
        ReflectionTestUtils.setField(sut, "instancePid", pid);

        // Attention: We need to handle both cases HAS_PROCFS = true & false
        try (var ignore = mockConstruction(File.class,
            (mock, context) -> lenient().when(mock.canRead()).thenReturn(true))) {

            try (var ignore2 = mockConstruction(ProcessBuilder.class,
                (mock, context) -> lenient().when(mock.start()).thenReturn(process))) {

                lenient().when(process.getInputStream()).thenReturn(new ByteArrayInputStream(("" + pid).getBytes()));

                assertFalse(sut.isDisposed());
            }
        }
    }

    @Test
    void testKill() throws Exception {
        var installDirPath = tempDir.resolve("install").toFile();
        var userDirPath = tempDir.resolve("user").toFile();
        assertTrue(installDirPath.mkdirs());
        assertTrue(userDirPath.mkdirs());

        var pid = 9999;
        var process = mock(Process.class);
        ReflectionTestUtils.setField(sut, "process", process);
        ReflectionTestUtils.setField(sut, "instancePid", pid);
        ReflectionTestUtils.setField(sut, "userDirFile", userDirPath);
        ReflectionTestUtils.setField(sut, "pipeName", "OSL_PIPE_c95eb1a6b8787acc7268357993174b6d");

        try (var ignore = mockConstruction(ProcessBuilder.class,
            (mock, context) -> when(mock.start()).thenReturn(process))) {

            sut.kill();

            verify(process).waitFor();
            verify(process).destroyForcibly();
        }

        assertNull(sut.getDesktop());
        assertNull(sut.getComponentFactory());
        assertNull(sut.getFilterFactory());
        assertNull(sut.getDefaultContext());
    }

    @Test
    void testGetComponentFactory() {
        assertNull(sut.getComponentFactory());
    }

    @Test
    void testGetDefaultContext() {
        assertNull(sut.getDefaultContext());
    }

    @Test
    void testGetFilterFactory() {
        assertNull(sut.getFilterFactory());
    }

    @Test
    void testGetDesktop() {
        assertNull(sut.getDesktop());
    }

    @Test
    void testRestartCount() {
        when(reDescriptor.getRestartCount()).thenReturn(3);
        assertEquals(reDescriptor.getRestartCount(), sut.getRestartCount());
    }

    @Test
    void testProcessTerminated() {
        var processMock = mock(Process.class);
        sut.processTerminated(processMock);
    }
}
