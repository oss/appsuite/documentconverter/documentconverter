/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProcessReaderRunnableTest {

    @Mock
    private REInstance instance;

    @Mock
    private Process process;

    private ListAppender<ILoggingEvent> logListAppenderProcessReaderRunnable;

    @BeforeEach
    void setUp() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderProcessReaderRunnable = TestUtil.addListAppenderToLogger(ProcessReaderRunnable.class);
    }

    @Test
    void testRunSuccess() {
        var inputStream = new ByteArrayInputStream("output1\noutput2\nwarn: warning message\noutput3".getBytes());

        var sut = new ProcessReaderRunnable(instance, process, inputStream);

        sut.run();

        verify(instance).processTerminated(process);
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderProcessReaderRunnable, "DC ReaderEngine output: output1"));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderProcessReaderRunnable, "DC ReaderEngine output: output2"));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderProcessReaderRunnable, "DC ReaderEngine output: output3"));
        assertFalse(TestUtil.listAppenderContainsAllMsg(logListAppenderProcessReaderRunnable, "DC ReaderEngine output: warn: warning message"));
    }
}
