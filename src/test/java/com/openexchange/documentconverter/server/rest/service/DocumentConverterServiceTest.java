/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.rest.service;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.DocumentConverterApplication;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.EngineStatus;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.metrics.TagsType;
import com.openexchange.documentconverter.server.mocks.EngineStatusMock;
import com.openexchange.documentconverter.server.rest.model.RConvertJSONAsyncResponseModel;
import com.openexchange.documentconverter.server.rest.model.RConvertJSONSynchronResponseModel;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import static com.openexchange.documentconverter.server.error.JobErrorEx.JSON_KEY_ERROR_CODE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DocumentConverterServiceTest {

    @Mock
    private ServerManager serverManager;

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private Cache cache;

    @Mock
    private ManagedJobFactory managedJobFactory;

    @Mock
    private AsyncServerExecutor asyncServerExecutor;

    @InjectMocks
    private DocumentConverterService sut;

    @BeforeEach
    void init() {
        Metrics.globalRegistry.clear();
    }
    @Test
    void testGetServerStatus() {
        // setup
        when(serverManager.isRunning()).thenReturn(true);

        var engineStatus = new HashMap<BackendType, EngineStatus>();
        engineStatus.put(BackendType.READERENGINE, new EngineStatusMock(10, 0));
        engineStatus.put(BackendType.PDFTOOL, new EngineStatusMock(-1, 10));
        when(jobProcessor.getEngineStatus()).thenReturn(engineStatus);

        var serverErrors = new ArrayList<String>();
        serverErrors.add("error1");
        serverErrors.add("error2");
        serverErrors.add(null);
        when(serverManager.getServerErrors()).thenReturn(serverErrors);

        // test
        var serverStatus = sut.getServerStatus();

        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_ID, serverStatus.getId());
        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_NAME, serverStatus.getName());
        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_API_VERSION, serverStatus.getApi());
        assertEquals("running", serverStatus.getStatus());

        assertEquals(2, serverStatus.getEngineStatus().size());
        for (var engineStatusEntry : engineStatus.entrySet()) {
            var resultEngineStatus = serverStatus.getEngineStatus().get(engineStatusEntry.getKey().toString());
            assertNotNull(resultEngineStatus);
            assertEquals(engineStatusEntry.getValue().runningCount(), resultEngineStatus.getRunningCount());
            assertEquals(engineStatusEntry.getValue().totalCount(), resultEngineStatus.getTotalCount());
        }

        assertEquals(3, serverStatus.getErrorMessages().size());
        assertEquals(serverErrors, serverStatus.getErrorMessages());
    }

    @Test
    void testGetServerStatus_NotRunning() {
        when(serverManager.isRunning()).thenReturn(false);

        var serverStatus = sut.getServerStatus();
        assertEquals("terminated", serverStatus.getStatus());
    }

    @Test
    void testGetMetrics_Empty() {
        var metrics = sut.getMetrics();
        assertEquals(0, metrics.size());
    }

    @Test
    void testGetMetrics() {
        // Needed to test the gauge value
        Metrics.addRegistry(new SimpleMeterRegistry());

        var metricsRegistry = new MetricsRegistry();
        metricsRegistry.addMetric("m1.total", metricsRegistry, (v) -> 1, TagsType.JOBS, "The total number of m.");
        metricsRegistry.addMetric("m2.ratio", metricsRegistry, (v) -> 2, TagsType.CACHE,"The ratio of m.");

        // Add Gauge with not registered prefix which should not be added to the metrics result
        var gaugeBuilder = Gauge.builder("prefixx.special.metric", null, (v) -> 3);
        gaugeBuilder.register(Metrics.globalRegistry);


        var metrics = sut.getMetrics();
        assertEquals(2, metrics.size());
        var m1 = metrics.get("m1.total_type_jobs");
        assertNotNull(m1);
        assertEquals(1, m1.longValue());
        var m2 = metrics.get("m2.ratio_type_cache");
        assertNotNull(m2);
        assertEquals(2.0, m2.doubleValue());
    }

    @Test
    void testGetDefaultJSONResponse_Async() {
        var response = sut.getDefaultJSONResponse(true, new HashMap<>());
        validateRConvertJSONAsyncResponseModel_empty(response);
    }

    void validateRConvertJSONAsyncResponseModel_empty(RConvertJSONAsyncResponseModel response) {
        assertTrue(response.isAsync());
        assertEquals(new JobErrorEx().getErrorData().toString(), response.getErrorData());
        assertEquals(0, response.getErrorCode());
    }

    @Test
    void testGetDefaultJSONResponse_Sync() {
        var response = sut.getDefaultJSONResponse(false, new HashMap<>());
        assertFalse(response.isAsync());
        assertEquals(new JobErrorEx().getErrorData().toString(), response.getErrorData());
        assertEquals(0, response.getErrorCode());
    }

    @Test
    void testGetDefaultJSONResponse_ResultProperties_ErrorCode() {
        var jobError = JobError.DISPOSED;
        var errorCode = jobError.getErrorCode();
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, errorCode);

        var response = sut.getDefaultJSONResponse(true, resultProperties);
        assertTrue(response.isAsync());
        assertEquals(new JobErrorEx(jobError).getErrorData().toString(), response.getErrorData());
        assertEquals(errorCode, response.getErrorCode());
    }

    @Test
    void testGetDefaultJSONResponse_ResultProperties_ErrorCode_invalidErrorData() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.DISPOSED.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, "TEST");

        var response = sut.getDefaultJSONResponse(true, resultProperties);
        assertTrue(response.isAsync());
        assertEquals(new JobErrorEx(JobError.GENERAL).getErrorData().toString(), response.getErrorData());
        assertEquals(JobError.GENERAL.getErrorCode(), response.getErrorCode());
    }

    @Test
    void testGetDefaultJSONResponse_ResultProperties_ErrorCode_ErrorData() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.DISPOSED.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, "{\"statusErrorCache\":\"ACTIVE\",\"description\":\"Error description\"}");

        var response = sut.getDefaultJSONResponse(true, resultProperties);
        assertTrue(response.isAsync());
        assertEquals(JobError.DISPOSED.getErrorCode(), response.getErrorCode());
        assertNotNull(response.getErrorData());
        var errorData = new JSONObject(response.getErrorData());
        assertEquals(3, errorData.length());
        assertEquals(JobError.DISPOSED.getErrorCodeRepresentation(), errorData.get(JSON_KEY_ERROR_CODE));
        assertEquals("active", errorData.get("statusErrorCache"));
        assertEquals("Error description", errorData.get("description"));

    }

    @Test
    void testGetSynchronJSONResponse_withEmptyResultProperties() {

        var resultProperties = new HashMap<String, Object>();

        var response = sut.getSynchronJSONResponse(null, resultProperties);

        validateRConvertJSONSynchronResponseModel_empty(response, null);
    }

    void validateRConvertJSONSynchronResponseModel_empty(RConvertJSONSynchronResponseModel response, JobError jobError ) {
        assertFalse(response.isAsync());
        if (jobError == null) {
            assertEquals(new JobErrorEx().getErrorData().toString(), response.getErrorData());
            assertEquals(0, response.getErrorCode());
        } else {
            assertEquals(new JobErrorEx(jobError).getErrorData().toString(), response.getErrorData());
            assertEquals(1, response.getErrorCode());
        }

        assertEquals("pdf", response.getExtension());
        assertNull(response.getJobId());
        assertNull(response.getLocale());
        assertNull(response.getMaxSourceSize());
        assertEquals("application/pdf", response.getMimeType());
        assertNull(response.getOriginalPageCount());
        assertNull(response.getPageCount());
        assertNull(response.getPageJobType());
        assertNull(response.getPageNumber());
        assertNull(response.getPasswordProtected());
        assertNull(response.getResult());
        assertNull(response.getShapeNumber());
        assertNull(response.getUnsupported());
    }

    @Test
    void testGetSynchronJSONResponse_withFullResultProperties() {
        var jobError = JobError.DISPOSED;
        var errorCode = jobError.getErrorCode();

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, errorCode);

        resultProperties.put(Properties.PROP_RESULT_EXTENSION, "extension_test");
        resultProperties.put(Properties.PROP_RESULT_JOBID, "jobId_test");
        resultProperties.put(Properties.PROP_RESULT_LOCALE, "local_test");
        resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, "mimeType_test");
        resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, 10);
        resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, 10);
        resultProperties.put(Properties.PROP_RESULT_SINGLE_PAGE_JOBTYPE, "pageJobType_test");
        resultProperties.put(Properties.PROP_RESULT_PAGE_NUMBER, 10);
        resultProperties.put(Properties.PROP_RESULT_SHAPE_NUMBER, 10);

        // result
        var resultBuffer = "resultBuffer_test".getBytes(StandardCharsets.UTF_8);
        resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, false);

        var response = sut.getSynchronJSONResponse(resultBuffer, resultProperties);

        assertFalse(response.isAsync());
        assertEquals(new JobErrorEx(jobError).getErrorData().toString(), response.getErrorData());
        assertEquals(errorCode, response.getErrorCode());

        assertEquals("extension_test", response.getExtension());
        assertEquals("jobId_test", response.getJobId());
        assertEquals("local_test", response.getLocale());
        assertNull(response.getMaxSourceSize());
        assertEquals("mimeType_test", response.getMimeType());
        assertEquals(10, response.getOriginalPageCount());
        assertEquals(10, response.getPageCount());
        assertEquals("pageJobType_test", response.getPageJobType());
        assertEquals(10, response.getPageNumber());
        assertNull(response.getPasswordProtected());
        assertEquals(10, response.getShapeNumber());
        assertNull(response.getUnsupported());

        // result
        assertTrue(response.getResult().contains("data:mimeType_test"));
        assertTrue(response.getResult().contains(";base64," + Base64.encodeBase64String(resultBuffer)));
    }

    @Test
    void testGetSynchronJSONResponse_withZipArchiveResult() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, "mimeType_test");

        // result
        var resultBuffer = "resultBuffer_test".getBytes(StandardCharsets.UTF_8);
        resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, true);

        var response = sut.getSynchronJSONResponse(resultBuffer, resultProperties);

        // result
        assertTrue(response.getResult().contains("data:application/zip"));
        assertTrue(response.getResult().contains(";base64," + Base64.encodeBase64String(resultBuffer)));
    }

    @Test
    void testGetSynchronJSONResponse_withToLowInteger() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, -1);
        resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, 0);
        resultProperties.put(Properties.PROP_RESULT_PAGE_NUMBER, -10);
        resultProperties.put(Properties.PROP_RESULT_SHAPE_NUMBER, 0);

        var response = sut.getSynchronJSONResponse(null, resultProperties);

        assertNull(response.getOriginalPageCount());
        assertNull(response.getPageCount());
        assertNull(response.getPageNumber());
        assertNull(response.getShapeNumber());
    }

    @Test
    void testGetSynchronJSONResponse_withPasswordProtectedTrue() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.PASSWORD.getErrorCode());

        var response = sut.getSynchronJSONResponse(null, resultProperties);

        assertNull(response.getMaxSourceSize());
        assertTrue(response.getPasswordProtected());
        assertNull(response.getUnsupported());
    }

    @Test
    void testGetSynchronJSONResponse_withMaxSourceSizeTrue() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.MAX_SOURCESIZE.getErrorCode());

        var response = sut.getSynchronJSONResponse(null, resultProperties);

        assertTrue(response.getMaxSourceSize());
        assertNull(response.getPasswordProtected());
        assertNull(response.getUnsupported());
    }

    @Test
    void testGetSynchronJSONResponse_withUnsupportedTrue() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.UNSUPPORTED.getErrorCode());

        var response = sut.getSynchronJSONResponse(null, resultProperties);

        assertNull(response.getMaxSourceSize());
        assertNull(response.getPasswordProtected());
        assertTrue(response.getUnsupported());
    }

    @Test
    void testGetFileResponse_emptyResultPropertiesAndEmptyBuffer() {
        var resultProperties = new HashMap<String, Object>();

        var fileResult = sut.getFileResponse(new byte[0], resultProperties);
        validateFileResult_empty(fileResult);
    }

    void validateFileResult_empty(FileResult fileResult) {
        assertEquals("result", fileResult.getFilename());
        assertFalse(fileResult.isZip());
        assertEquals(0, fileResult.getFile().length);
        assertEquals("pdf", fileResult.getExtension());
        assertEquals("application/pdf", fileResult.getMimeType());
    }


    @Test
    void testGetFileResponse_allResultPropertiesAndBuffer() {

        var fileResultBuffer = "testResultBuffer".getBytes(StandardCharsets.UTF_8);

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, "mimeType_test");
        resultProperties.put(Properties.PROP_RESULT_EXTENSION, "extension_test");
        resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, true);

        var response = sut.getFileResponse(fileResultBuffer, resultProperties);
        assertEquals("result", response.getFilename());
        assertTrue(response.isZip());
        assertEquals(fileResultBuffer, response.getFile());
        assertEquals("extension_test", response.getExtension());
        assertEquals("mimeType_test", response.getMimeType());
    }

    @Test
    void testGetRConvertResult_withInvalidReturnTypeSynchron() {
        var result = sut.getRConvertResult(new HashMap<>(), "-");
        assertFalse(result.isDone());
    }

    @Test
    void testGetRConvertResult_withInvalidReturnTypeAsynchron() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ASYNC, true);
        var result = sut.getRConvertResult(resultProperties, "-");
        assertTrue(result.isDone());
    }

    @Test
    void testGetRConvertResult_withReturnTypeJSONSynchron() {
        var result = sut.getRConvertResult(new HashMap<>(), "json");
        assertTrue(result.isDone());

        assertNotNull(result.getJsonSynchronResult());
        validateRConvertJSONSynchronResponseModel_empty(result.getJsonSynchronResult(), null);

        assertNull(result.getFileResult());
        assertNull(result.getJsonAsyncResult());
        assertEquals(0, result.getResultProperties().size());
    }

    @Test
    void testGetRConvertResult_withReturnTypeJSONAsynchron() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ASYNC, true);
        var result = sut.getRConvertResult(resultProperties, "json");
        assertTrue(result.isDone());

        assertNotNull(result.getJsonAsyncResult());
        validateRConvertJSONAsyncResponseModel_empty(result.getJsonAsyncResult());

        assertNull(result.getFileResult());
        assertNull(result.getJsonSynchronResult());
        assertEquals(1, result.getResultProperties().size());
    }

    @Test
    void testGetRConvertResult_withReturnTypeFileAndEmptyResultBuffer_Async() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ASYNC, true);
        var result = sut.getRConvertResult(resultProperties, "file");

        assertTrue(result.isDone());

        assertNull(result.getFileResult());
        assertNull(result.getJsonAsyncResult());
        assertNull(result.getJsonSynchronResult());
        assertEquals(1, result.getResultProperties().size());
    }

    @Test
    void testGetRConvertResult_withReturnTypeFileAndEmptyResultBuffer_Synchron() {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ASYNC, false);
        var result = sut.getRConvertResult(resultProperties, "file");

        assertFalse(result.isDone());

        assertNull(result.getFileResult());
        assertNull(result.getJsonAsyncResult());
        assertNull(result.getJsonSynchronResult());
        assertEquals(1, result.getResultProperties().size());
    }

    @Test
    void testGetRConvertResult_withReturnTypeFileAndResultBuffer() {

        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ASYNC, false);
        resultProperties.put(Properties.PROP_RESULT_BUFFER, new byte[0]);

        var result = sut.getRConvertResult(resultProperties, "file");

        assertTrue(result.isDone());
        var fileResult = result.getFileResult();
        assertNotNull(fileResult);
        validateFileResult_empty(fileResult);

        assertNull(result.getJsonAsyncResult());
        assertNull(result.getJsonSynchronResult());
        assertEquals(2, result.getResultProperties().size());
    }

    @Test
    void testRConvert() {
        var result = sut.rconvert(new RConvertRequestModel());

        assertTrue(result.isDone());

        assertNull(result.getFileResult());
        assertNull(result.getJsonAsyncResult());
        assertNotNull(result.getJsonSynchronResult());
        validateRConvertJSONSynchronResponseModel_empty(result.getJsonSynchronResult(), JobError.GENERAL);
        assertEquals(1, result.getResultProperties().size());
    }
}
