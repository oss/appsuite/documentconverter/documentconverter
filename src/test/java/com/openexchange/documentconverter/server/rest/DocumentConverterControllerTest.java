/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.documentconverter.server.DocumentConverterApplication;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.cache.CacheDescriptor;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.ErrorCode;
import com.openexchange.documentconverter.server.job.EngineGroupFactory;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REDescriptor;
import com.openexchange.documentconverter.server.rest.model.*;
import com.openexchange.documentconverter.server.rest.service.DocumentConverterService;
import com.openexchange.documentconverter.server.rest.service.FileResult;
import com.openexchange.documentconverter.server.rest.service.RConvertResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.MimeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.openexchange.documentconverter.server.rest.DocumentConverterController.OX_ERRORTEXT_GENERAL_ERROR;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DocumentConverterController.class)
@TestPropertySource(locations="classpath:test.properties")
class DocumentConverterControllerTest {

    @Autowired
    MockMvc mockMvc;

    @SpyBean
    private ServerManager serverManager;

    @SpyBean
    private ServerConfig serverConfig;

    @SpyBean
    private REDescriptor reDescriptor;

    @MockBean
    DocumentConverterService documentConverterService;

    @SpyBean
    JobProcessor jobProcessor;

    @SpyBean
    private ErrorCache errorCache;

    @SpyBean
    private EngineGroupFactory engineGroupFactory;

    @SpyBean
    private ConverterJobFactory converterJobFactory;

    @SpyBean
    private Cache cache;

    @SpyBean
    private AsyncServerExecutor asyncServerExecutor;

    @SpyBean
    private CacheDescriptor cacheDescriptor;

    @SpyBean
    private Statistics statistics;

    @SpyBean
    private MetricsRegistry metricsRegistry;

    @SpyBean
    private ManagedJobFactory managedJobFactory;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testProcessSuccess() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();
        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(true);
        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .file(mockMultipartFile)
                        .param("action", "convert"))
                .andExpect(status().isOk());
    }

    @Test
    void testProcessWithUnknownMethodResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                .file(mockMultipartFile)
                .param("action", "convert")
                .param("method", "unknown"))
            .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        var details = assertInstanceOf(List.class, errorResponse.getDetails());
        assertEquals(1, details.size());
        assertTrue(details.contains("method"));
    }

    @Test
    void testProcessWithUnknownJobTypeResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                .file(mockMultipartFile)
                .param("action", "convert")
                .param("jobtype", "unknown"))
            .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        var details = assertInstanceOf(List.class, errorResponse.getDetails());
        assertEquals(1, details.size());
        assertTrue(details.contains("jobtype"));
    }

    @Test
    void testProcessWithUnknownPriorityResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                .file(mockMultipartFile)
                .param("action", "convert")
                .param("priority", "unknown"))
            .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        var details = assertInstanceOf(List.class, errorResponse.getDetails());
        assertEquals(1, details.size());
        assertTrue(details.contains("priority"));
    }

    @Test
    void testProcessWithMultipleUnknownParameterResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                .file(mockMultipartFile)
                .param("action", "convert")
                .param("imagescaletype", "unknown")
                .param("priority", "unknown"))
            .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        var details = assertInstanceOf(List.class, errorResponse.getDetails());
        assertEquals(2, details.size());
        assertTrue(details.contains("imagescaletype"));
        assertTrue(details.contains("priority"));
    }

    @Test
    void testProcessWithUnknownImageScaleTypeResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .file(mockMultipartFile)
                        .param("action", "convert")
                        .param("imagescaletype", "unknown"))
                .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        var details = assertInstanceOf(List.class, errorResponse.getDetails());
        assertTrue(details.contains("imagescaletype"));
    }

    @Test
    void testProcessWithoutActionResultsInBadRequest() throws Exception {
        var mockMultipartFile = createMockMultipartFileWithXlsx();

        mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .file(mockMultipartFile)
                        .param("method", "NotValid"))
            .andExpect(status().isBadRequest());
    }

    @Test
    void testProcessJsonAsyncResult() throws Exception {
        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(true);
        var jsonAsyncResult = new RConvertJSONAsyncResponseModel();
        jsonAsyncResult.setAsync(true);
        jsonAsyncResult.setErrorData("ErrorData_Test");
        jsonAsyncResult.setErrorCode(110);
        rconvertResult.setJSONAsyncResult(jsonAsyncResult);

        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws"))
                .andExpect(status().isOk()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        var responseModel = objectMapper.readValue(bodyAsString, RConvertJSONAsyncResponseModel.class);
        assertTrue(responseModel.isAsync());
        assertEquals(jsonAsyncResult.getErrorCode(), responseModel.getErrorCode());
        assertEquals(jsonAsyncResult.getErrorData(), responseModel.getErrorData());
    }

    @Test
    void testProcessJsonSyncResult() throws Exception {

        var jsonSyncResult = new RConvertJSONSynchronResponseModel();
        jsonSyncResult.setAsync(false);
        jsonSyncResult.setErrorData("ErrorData_Test");
        jsonSyncResult.setErrorCode(0);
        jsonSyncResult.setResult("__Result--");
        jsonSyncResult.setMaxSourceSize(true);

        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(true);
        rconvertResult.setJSONSynchronResult(jsonSyncResult);

        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws"))
                .andExpect(status().isOk()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        var responseModel = objectMapper.readValue(bodyAsString, RConvertJSONSynchronResponseModel.class);
        assertFalse(responseModel.isAsync());
        assertEquals(jsonSyncResult.getErrorCode(), responseModel.getErrorCode());
        assertEquals(jsonSyncResult.getErrorData(), responseModel.getErrorData());
        assertTrue(responseModel.getMaxSourceSize());
        assertEquals(jsonSyncResult.getResult(), responseModel.getResult());
    }

    @Test
    void testProcessFileResult() throws Exception {

        var fileResult = new FileResult();
        var file = "image".getBytes();
        fileResult.setFile(file);
        fileResult.setFilename("image1");
        fileResult.setMimeType(MediaType.IMAGE_JPEG_VALUE);
        fileResult.setExtension("jpg");

        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(true);
        rconvertResult.setFileResult(fileResult);

        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .param("returntype", "file"))
                .andExpect(status().isOk()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        assertEquals(file.length, response.getContentLength());
        assertEquals(MediaType.IMAGE_JPEG_VALUE, response.getContentType());
        var contentDispositionHeader = response.getHeaders("Content-Disposition");
        assertNotNull(contentDispositionHeader);
        assertEquals(1, contentDispositionHeader.size());
        assertEquals("attachment; filename=" + fileResult.getFilename() + "." + fileResult.getExtension(), contentDispositionHeader.get(0));
        assertArrayEquals(file, response.getContentAsByteArray());

    }

    @Test
    void testProcessWithInvalidFileResult() throws Exception {

        var fileResult = new FileResult();

        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(true);
        rconvertResult.setFileResult(fileResult);

        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .param("returntype", "file"))
                .andExpect(status().isInternalServerError()).andReturn();

        var response = result.getResponse();
        assertEquals(OX_ERRORTEXT_GENERAL_ERROR, response.getContentAsString());
    }

    // TODO: unit test failed, removed temporarily
    @Test
    @Disabled
    void testProcessNotDoneJSON() throws Exception {

        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(false);

        var errorCode = 1222121;
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, errorCode);

        rconvertResult.setResultProperties(resultProperties);
        // RConvert result
        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        // Metrics for the error page
        var metrics = new HashMap<String, Number>();
        metrics.put("metric_test_1", 101);
        metrics.put("metric_2", null);
        when(documentConverterService.getMetrics()).thenReturn(metrics);


        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .param("returntype", "json"))
                .andExpect(status().isOk()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        assertEquals(MediaType.TEXT_HTML_VALUE, response.getContentType());
        for (var metric : metrics.entrySet()) {
            assertTrue(bodyAsString.contains(metric.getKey() + ": " + metric.getValue()));
        }

        assertTrue(bodyAsString.contains("Error Code: " + errorCode));
        verify(documentConverterService, times(1)).getMetrics();
    }

    @Test
    void testProcessNotDoneFile() throws Exception {

        var rconvertResult = new RConvertResult();
        rconvertResult.setDone(false);

        var errorCode = 1222121;
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, errorCode);

        rconvertResult.setResultProperties(resultProperties);
        // RConvert result
        when(documentConverterService.rconvert(any(RConvertRequestModel.class))).thenReturn(rconvertResult);

        var result = mockMvc.perform(MockMvcRequestBuilders.multipart("/documentconverterws")
                        .param("returntype", "file"))
                .andExpect(status().isInternalServerError()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        assertEquals(DocumentConverterController.OX_ERRORTEXT_GENERAL_ERROR, bodyAsString);
    }

    @Test
    void testStatusWithoutMetricsSuccess() throws Exception {
        var serverStatus = new ServerStatusModel();
        serverStatus.setId("1234");
        serverStatus.setName("document_converter");
        serverStatus.setApi("8");
        serverStatus.setStatus("running");
        serverStatus.setCacheServiceAvailable(false);
        serverStatus.setCacheServiceUrl("http://example.com");

        var engineStatus = new EngineStatusModel(1, 2);
        serverStatus.putEngineStatus("engine1", engineStatus);

        var errorMessages = new ArrayList<String>();
        errorMessages.add("error1");
        serverStatus.setErrorMessages(errorMessages);

        when(documentConverterService.getServerStatus()).thenReturn(serverStatus);
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/documentconverterws/status"))
            .andExpect(status().isOk()).andReturn();
        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        var statusResponse = objectMapper.readValue(bodyAsString, StatusResponse.class);
        var serverStatusResponse = statusResponse.getServerStatus();
        assertEquals(serverStatus.getId(), serverStatusResponse.getId());
        assertEquals(serverStatus.getName(), serverStatusResponse.getName());
        assertEquals(serverStatus.getApi(), serverStatusResponse.getApi());
        assertEquals(serverStatus.getStatus(), serverStatusResponse.getStatus());
        assertEquals(serverStatus.isCacheServiceAvailable(), serverStatusResponse.isCacheServiceAvailable());
        assertEquals(serverStatus.getCacheServiceUrl(), serverStatusResponse.getCacheServiceUrl());
        assertTrue(serverStatusResponse.getEngineStatus().containsKey("engine1"));

        assertNull(statusResponse.getMetrics());
        verify(documentConverterService, times(0)).getMetrics();

    }

    @Test
    void testStatusWithMetricsSuccess() throws Exception {
        var serverStatus = new ServerStatusModel();
        serverStatus.setId(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_ID);
        serverStatus.setName(DocumentConverterApplication.DOCUMENTCONVERTER_NAME);
        serverStatus.setApi(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_API_VERSION);
        when(documentConverterService.getServerStatus()).thenReturn(serverStatus);
        var metrics = new HashMap<String, Number>();
        metrics.put("metric1", 1);
        when(documentConverterService.getMetrics()).thenReturn(metrics);

        var result = mockMvc.perform(MockMvcRequestBuilders.get("/documentconverterws/status?metrics=true"))
                .andExpect(status().isOk()).andReturn();
        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);

        var statusResponse = objectMapper.readValue(bodyAsString, StatusResponse.class);
        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_ID, statusResponse.getServerStatus().getId());
        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_NAME, statusResponse.getServerStatus().getName());
        assertEquals(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_API_VERSION, statusResponse.getServerStatus().getApi());

        assertEquals(metrics, statusResponse.getMetrics());
        verify(documentConverterService, times(1)).getMetrics();
    }

    @Test
    void testStatusWithMetricsBadRequest() throws Exception {
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/documentconverterws/status?metrics=hello"))
                .andExpect(status().isBadRequest()).andReturn();

        var response = result.getResponse();

        var bodyAsString = response.getContentAsString();
        assertNotNull(bodyAsString);
        var errorResponse = objectMapper.readValue(bodyAsString, ErrorResponse.class);
        assertEquals(ErrorCode.ERROR_INVALID_PARAM.ordinal(), errorResponse.getErrorCode());
        assertEquals("metrics", errorResponse.getDetails());

        verify(documentConverterService, times(0)).getMetrics();
    }

    @Test
    void testGetFileResponseIsZip() {
        var sut = new DocumentConverterController();

        var fileResult = new FileResult();
        fileResult.setZip(true);
        var file = "Hello".getBytes();
        fileResult.setFile(file);
        fileResult.setFilename("hello");

        var response = sut.getFileResponse(fileResult);
        assertEquals(file.length, response.getHeaders().getContentLength());
        assertEquals(new MediaType(MimeType.valueOf("application/zip")), response.getHeaders().getContentType());
        var contentDispositionHeader = response.getHeaders().get("Content-Disposition");
        assertNotNull(contentDispositionHeader);
        assertEquals(1, contentDispositionHeader.size());
        assertEquals("attachment; filename=" + fileResult.getFilename() + ".zip", contentDispositionHeader.get(0));
        assertEquals(file, response.getBody());
    }

    @Test
    void testGetFileResponseIsImage() {
        var sut = new DocumentConverterController();

        var fileResult = new FileResult();
        var file = "image".getBytes();
        fileResult.setFile(file);
        fileResult.setFilename("image1");
        fileResult.setMimeType(MediaType.IMAGE_JPEG_VALUE);
        fileResult.setExtension("jpg");

        var response = sut.getFileResponse(fileResult);
        assertEquals(file.length, response.getHeaders().getContentLength());
        assertEquals(MediaType.IMAGE_JPEG, response.getHeaders().getContentType());
        var contentDispositionHeader = response.getHeaders().get("Content-Disposition");
        assertNotNull(contentDispositionHeader);
        assertEquals(1, contentDispositionHeader.size());
        assertEquals("attachment; filename=" + fileResult.getFilename() + "." + fileResult.getExtension(), contentDispositionHeader.get(0));
        assertEquals(file, response.getBody());
    }

/*
    @Test
    void testGetErrorResponseWithReturnTypeNotJSONAndErrorCodeMAX_QUEUE_COUNT() {
        var sut = new DocumentConverterController();
        var response = sut.getErrorResponse(JobError.MAX_QUEUE_COUNT.getErrorCode());
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
        // TODO: take care of JSONObject error response assertEquals(DocumentConverterController.OX_ERRORTEXT_QUEUE_COUNT_LIMIT_REACHED, response.getBody());
    }

    @Test
    void testGetErrorResponseWithReturnTypeNotJSONAndErrorCodeQUEUE_TIMEOUT() {
        var sut = new DocumentConverterController();
        var response = sut.getErrorResponse(JobError.QUEUE_TIMEOUT.getErrorCode());
        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
        // TODO: take care of JSONObject error response assertEquals(DocumentConverterController.OX_ERRORTEXT_QUEUE_TIMEOUT, response.getBody());
    }

    @Test
    void testGetErrorResponseWithReturnTypeNotJSONAndErrorCodeBAD_REQUEST() {
        var sut = new DocumentConverterController();
        var response = sut.getErrorResponse(HttpStatus.BAD_REQUEST.value());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        // TODO: take care of JSONObject error response assertEquals(DocumentConverterController.OX_ERRORTEXT_BAD_REQUEST, response.getBody());
    }

    @Test
    void testGetErrorResponseWithReturnTypeNotJSONAndErrorCodeNull() {
        var sut = new DocumentConverterController();
        var response = sut.getErrorResponse(null);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        // TODO: take care of JSONObject error response assertEquals(DocumentConverterController.OX_ERRORTEXT_BAD_REQUEST, response.getBody());
    }

    @Test
    void testGetErrorResponseWithReturnTypeNotJSONAndErrorCodeUnknown() {
        var sut = new DocumentConverterController();
        var response = sut.getErrorResponse(-123);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        // TODO: take care of JSONObject error response assertEquals(OX_ERRORTEXT_GENERAL_ERROR, response.getBody());
    }

    // TODO: unit test failed, removed temporarily
    @Test
    @Disabled
    void testGetErrorResponseWithReturnTypeJSONAndErrorCodeNull() {
        var sut = new DocumentConverterController();
        ReflectionTestUtils.setField(sut, "documentConverterService", documentConverterService);
        var metrics = new HashMap<String, Number>();
        metrics.put("metric_test_1", 101);
        when(documentConverterService.getMetrics()).thenReturn(metrics);

        var response = sut.getErrorResponse(null);
        assertEquals(MediaType.TEXT_HTML, response.getHeaders().getContentType());
        var html = (String) response.getBody();
        assertNotNull(html);
        for (var metric : metrics.entrySet()) {
            assertTrue(html.contains(metric.getKey() + ": " + metric.getValue()));
        }
        assertTrue(html.contains("Error Code: " + HttpStatus.BAD_REQUEST.value()));
        verify(documentConverterService, times(1)).getMetrics();
    }

    // TODO: unit test failed, removed temporarily
    @Test
    @Disabled
    void testGetErrorResponseWithReturnTypeJSONAndErrorCode() {
        var errorCode = 98989898;

        var sut = new DocumentConverterController();
        ReflectionTestUtils.setField(sut, "documentConverterService", documentConverterService);
        var metrics = new HashMap<String, Number>();
        metrics.put("metric_test_1", 101);
        metrics.put("metric_2", null);

        when(documentConverterService.getMetrics()).thenReturn(metrics);

        var response = sut.getErrorResponse(errorCode);
        assertEquals(MediaType.TEXT_HTML, response.getHeaders().getContentType());
        var html = (String) response.getBody();
        assertNotNull(html);
        for (var metric : metrics.entrySet()) {
            assertTrue(html.contains(metric.getKey() + ": " + metric.getValue()));
        }

        assertTrue(html.contains("Error Code: " + errorCode));
        verify(documentConverterService, times(1)).getMetrics();
    }
*/

    private MockMultipartFile createMockMultipartFileWithXlsx() throws Exception {
        var resourcePathAndFileName = "test.xlsx";
        return new MockMultipartFile(
            "file",
            resourcePathAndFileName,
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            new ClassPathResource(resourcePathAndFileName).getInputStream());
    }
}
