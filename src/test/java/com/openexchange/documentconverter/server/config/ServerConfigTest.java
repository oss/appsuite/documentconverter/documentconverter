/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.config;

import com.openexchange.documentconverter.server.cache.Cache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ServerConfig.class)
@TestPropertySource(locations="classpath:test.properties")
class ServerConfigTest {

    @MockBean
    private Cache cache;

    @Autowired
    private ServerConfig sut;

    @Test
    void getSSL_CONTEXT() {
        assertNull(sut.getSSL_CONTEXT());
    }

    @Test
    void getReInstallDir() {
        assertEquals("/tmp", sut.getReInstallDir());
    }

    @Test
    void getCacheDir() {
        assertEquals("/var/spool/open-xchange/documentconverter/readerengine.cache", sut.getCacheDir());
    }

    @Test
    void getScratchDir() {
        assertEquals("/tmp/oxdc.tmp", sut.getScratchDirPath());
    }

    @Test
    void getCacheDirDepth() {
        assertEquals(0, sut.getCacheDirDepth());
    }

    @Test
    void getErrorDir() {
        assertEquals("/tmp", sut.getErrorDir());
    }

    @Test
    void getUrlBlacklistFile() {
        assertEquals("/opt/open-xchange/etc/readerengine.blacklist", sut.getUrlBlacklistFile());
    }

    @Test
    void getUrlWhitelistFile() {
        assertEquals("/opt/open-xchange/etc/readerengine.whitelist", sut.getUrlWhitelistFile());
    }

    @Test
    void getUrlLinkLimit() {
        assertEquals(200, sut.getUrlLinkLimit());
    }

    @Test
    void getUrlLinkProxy() {
        assertNull(sut.getUrlLinkProxy());
    }

    @Test
    void getCacheServiceUrl() {
        assertEquals("http://localhost:8080/remoteCache", sut.getCacheServiceUrl());
    }

    @Test
    void getJobProcessorCount() {
        assertEquals(3, sut.getJobProcessorCount());
    }

    @Test
    void getJobProcessorPoolSize() {
        assertEquals(0, sut.getJobProcessorPoolSize());
    }

    @Test
    void getJobRestartCount() {
        assertEquals(50, sut.getJobRestartCount());
    }

    @Test
    void isJobRestartAfterTimeout() {
        assertFalse(sut.getJobRestartAfterTimeout());
    }

    @Test
    void getJobExecutionTimeoutMilliseconds() {
        assertEquals(60000, sut.getJobExecutionTimeoutMilliseconds());
    }

    @Test
    void getMaxVMemMB() {
        assertEquals(2048, sut.getMaxVMemMB());
    }

    @Test
    void getMaxSourceFileSizeMB() {
        assertEquals(128, sut.getMaxSourceFileSizeMB());
    }

    @Test
    void getMinFreeVolumeSizeMB() {
        assertEquals(1024, sut.getMinFreeVolumeSizeMB());
    }

    @Test
    void getMaxCacheSizeMB() {
        assertEquals(-1, sut.getMaxCacheSizeMB());
    }

    @Test
    void getMaxCacheEntries() {
        assertEquals(4000000, sut.getMaxCacheEntries());
    }

    @Test
    void getCacheCleanupThresholdUpperPercentage() {
        assertEquals(95, sut.getCacheCleanupThresholdUpperPercentage());
    }

    @Test
    void getCacheCleanupThresholdLowerPercentage() {
        assertEquals(95, sut.getCacheCleanupThresholdLowerPercentage());
    }

    @Test
    void getCacheEntryTimeoutSeconds() {
        assertEquals(2592000, sut.getCacheEntryTimeoutSeconds());
    }

    @Test
    void getCacheCleanupPeriodSeconds() {
        assertEquals(300, sut.getCacheCleanupPeriodSeconds());
    }

    @Test
    void getUpRecoveryPeriodSeconds() {
        assertEquals(20, sut.getUpRecoveryPeriodSeconds());
    }

    @Test
    void getPdftoolMaxVMemMB() {
        assertEquals(1024, sut.getPdftoolMaxVMemMB());
    }

    @Test
    void isAllowLocalFileUrls() {
        assertFalse(sut.isAllowLocalFileUrls());
    }

    @Test
    void getErrorCacheTimeoutSeconds() {
        assertEquals(600, sut.getErrorCacheTimeoutSeconds());
    }

    @Test
    void getAutoCleanupTimeoutSeconds() {
        assertEquals(900, sut.getAutoCleanupTimeoutSeconds());
    }

    @Test
    void getErrorCacheMaxCycleCount() {
        assertEquals(5, sut.getErrorCacheMaxCycleCount());
    }

    @Test
    void getJobAsyncQueueCountLimitHigh() {
        assertEquals(2048, sut.getJobAsyncQueueCountLimitHigh());
    }

    @Test
    void getJobQueueCountLimitHigh() {
        assertEquals(40, sut.getJobQueueCountLimitHigh());
    }

    @Test
    void getJobQueueCountLimitLow() {
        assertEquals(30, sut.getJobQueueCountLimitLow());
    }

    @Test
    void getJobQueueTimeoutSeconds() {
        assertEquals(300, sut.getJobQueueTimeoutSeconds());
    }

    @Test
    void getPdfJpegQualityPercentage() {
        assertEquals(75, sut.getPdfJpegQualityPercentage());
    }

    @Test
    void getLivenessPeriodSeconds() {
        assertEquals(10, sut.getLivenessPeriodSeconds());
    }

    @Test
    void getLivenessDownAfterReadinessDownSeconds() {
        assertEquals(15, sut.getLivenessDownAfterReadinessDownSeconds());
    }

    @Test
    void getReadinessPeriodSeconds() {
        assertEquals(5, sut.getReadinessPeriodSeconds());
    }

    @Test
    void getReadinessDownAfterUsedServiceUnavailabilitySeconds() {
        assertEquals(300, sut.getReadinessDownAfterUsedServiceUnavailabilitySeconds());
    }

}
