/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.ConverterJob;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.*;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ManagedJobTest {

    private static final String DATA = "data";

    @TempDir
    private File tempDir;

    @Mock
    private ConverterJobFactory jobFactory;

    @Mock
    private ServerManager serverManager;

    @Mock
    private JobProcessor jobProcessor;

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private Cache cache;

    @Mock
    private Statistics statistics;

    @Mock
    private JobMonitor jobMonitor;

    private final Map<String, Object> jobProperties = new HashMap<>();

    private final Map<String, Object> resultProperties = new HashMap<>();

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    public void setUp() {
        jobProperties.clear();
        resultProperties.clear();

        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

    @Test
    void testConstructor() {
        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, null, serverConfig, cache, statistics);
        assertNull(ReflectionTestUtils.getField(sut, "jobMonitor"));
        sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        assertNotNull(ReflectionTestUtils.getField(sut, "jobMonitor"));
    }

    @Test
    void testProcessNullJobType() {
        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);

        var inputStream = sut.process(null, "123", new HashMap<>(), resultProperties);
        assertNull(inputStream);
        assertEquals(JobError.GENERAL.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        verify(jobProcessor, never()).isRunning();
    }

    @Test
    void testProcessBlankJobType() {
        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);

        var inputStream = sut.process("", "123", new HashMap<>(), resultProperties);
        assertNull(inputStream);
        assertEquals(JobError.GENERAL.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        verify(jobProcessor, never()).isRunning();
    }

    @Test
    void testProcessWithNotRunningJobProcessor() {
        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        var inputStream = sut.process("test", "123", new HashMap<>(), resultProperties);
        assertNull(inputStream);
        assertEquals(JobError.GENERAL.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        verify(jobProcessor, times(1)).isRunning();
    }

    @Test
    void testProcessWithoutJobInputFile() {
        try (MockedStatic<ServerManager> serverManagerStaticMock = Mockito.mockStatic(ServerManager.class)) {
            when(jobProcessor.isRunning()).thenReturn(true);

            var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);

            var inputStream = sut.process("test", "123", new HashMap<>(), resultProperties);

            assertNull(inputStream);

            verify(jobProcessor, times(1)).isRunning();
            verify(serverManager, times(1)).getJobInputFile(any(), any());

            serverManagerStaticMock.verify(() -> ServerManager.ensureInputTypeSet(any()), times(1));
        }
    }

    @ParameterizedTest
    @ValueSource(strings = { "pdf", "pdfa" })
    void testProcessWithJobInputFilePDFs(String pdfType) throws IOException {
        when(jobProcessor.isRunning()).thenReturn(true);

        var jobInputFile = TestUtil.createUniqueFile(tempDir, "test", "pdf");
        jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        when(serverManager.getJobInputFile(ArgumentMatchers.<HashMap<String, Object>>any(), ArgumentMatchers.any())).thenReturn(jobInputFile);

        var inputStream = sut.process(pdfType, "123", jobProperties, resultProperties);

        assertNotNull(inputStream);
        verify(jobProcessor, times(1)).isRunning();
        verify(serverManager, times(1)).getJobInputFile(any(), any());

        assertTrue(resultProperties.containsKey(Properties.PROP_RESULT_BUFFER));
        assertTrue(resultProperties.containsKey(Properties.PROP_CACHE_HASH));

        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    public static Stream<Arguments> docTypesAndExtensions() {
        return Stream.of(
            Arguments.of("odf", "odt"),
            Arguments.of("ooxml", "docx")
        );
    }

    @ParameterizedTest
    @MethodSource("docTypesAndExtensions")
    void testProcessWithJobInputFileAndConvertToPDF(String docType, String ext) throws IOException {
        var inputFileName = "test." + ext;
        var jobInputFile = TestUtil.createUniqueFile(tempDir, "test", ext);
        jobProperties.put(Properties.PROP_INPUT_TYPE, docType);
        jobProperties.put(Properties.PROP_INPUT_URL, jobInputFile.toURI().toString());
        jobProperties.put(Properties.PROP_JOBTYPE, "pdf");
        jobProperties.put(Properties.PROP_CACHE_HASH, UUID.randomUUID().toString());
        jobProperties.put(Properties.PROP_LOCALE, "en-US");
        jobProperties.put(Properties.PROP_FEATURES_ID, 1);
        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.MEDIUM);
        jobProperties.put(Properties.PROP_INFO_FILENAME, inputFileName);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        when(jobProcessor.isRunning()).thenReturn(true);
        when(jobProcessor.getErrorCache()).thenReturn(mock(ErrorCache.class));
        when(serverManager.getJobInputFile(ArgumentMatchers.<HashMap<String, Object>>any(), ArgumentMatchers.any())).thenReturn(jobInputFile);
        var jobHash = UUID.randomUUID().toString();
        var job = mock(ConverterJob.class);
        when(job.getHash()).thenReturn(jobHash);
        when(jobFactory.createJob(eq("pdf"), jobHash, ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(job);
        var resultInputStream = new ByteArrayInputStream(DATA.getBytes(Charset.defaultCharset()));
        when(cache.getCachedResult(eq(jobHash), eq(inputFileName), ArgumentMatchers.any(), anyBoolean())).thenReturn(resultInputStream);

        var inputStream = sut.process("pdf", jobHash, jobProperties, resultProperties);

        assertNotNull(inputStream);
        verify(jobProcessor, times(1)).isRunning();
        verify(serverManager, times(2)).getJobInputFile(any(), any());

        assertTrue(resultProperties.containsKey(Properties.PROP_RESULT_BUFFER));
        assertTrue(resultProperties.containsKey(Properties.PROP_CACHE_HASH));

        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @ParameterizedTest
    @MethodSource("docTypesAndExtensions")
    void testProcessWithJobInputFileAndConvertToPDFWithEmptyResultStream(String docType, String ext) throws IOException {
        var jobInputFile = TestUtil.createUniqueFile(tempDir, "test", ext);
        jobProperties.put(Properties.PROP_INPUT_TYPE, docType);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);

        var jobHash = UUID.randomUUID().toString();
        var errorCache = mock(ErrorCache.class);
        when(errorCache.getJobErrorExForHash(eq(jobHash))).thenReturn(new JobErrorEx(JobError.DISPOSED));
        when(jobProcessor.isRunning()).thenReturn(true);
        when(jobProcessor.getErrorCache()).thenReturn(errorCache);
        when(serverManager.getJobInputFile(ArgumentMatchers.<HashMap<String, Object>>any(), ArgumentMatchers.any())).thenReturn(jobInputFile);
        var job = mock(ConverterJob.class);
        when(job.getHash()).thenReturn(jobHash);
        when(jobFactory.createJob(eq("pdf"), jobHash, ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(job);

        var inputStream = sut.process("pdf", jobHash, jobProperties, resultProperties);

        assertNull(inputStream);
        verify(jobProcessor, atLeast(1)).isRunning();
        verify(serverManager, times(1)).getJobInputFile(any(), any());

        assertEquals(JobError.DISPOSED.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC failed ReaderEngine PDF job conversion"));
    }

    @Test
    @DisplayName("Test process method with valid job type and running job processor")
    void testProcessWithValidJobTypeAndRunningJobProcessorButWithoutJobInputFileOSGI() {
        var jobType = "pdf";
        jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
        jobProperties.put(Properties.PROP_RESULT_BUFFER, new byte[0]);

        when(jobProcessor.isRunning()).thenReturn(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);

        var result = sut.process(jobType, "123", jobProperties, resultProperties);

        assertNull(result);
        assertTrue(resultProperties.isEmpty());
        verify(jobProcessor, times(1)).isRunning();
        verify(jobProcessor, never()).createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @DisplayName("Test process method with valid job type, running job processor and with valid job input file")
    void testProcessWithValidJobTypeAndRunningJobProcessorButWithJobInputFileOSGI() throws IOException {
        var jobType = "pdf";
        jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
        jobProperties.put(Properties.PROP_RESULT_BUFFER, new byte[0]);

        var jobInputFile = getJobInputFile();

        when(serverManager.getJobInputFile(ArgumentMatchers.<HashMap<String, Object>>any(), ArgumentMatchers.any())).thenReturn(jobInputFile);
        when(jobProcessor.isRunning()).thenReturn(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        var result = sut.process(jobType, "123", jobProperties, resultProperties);

        assertNotNull(result);
        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        verify(jobProcessor, times(1)).isRunning();
        verify(jobProcessor, never()).createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @DisplayName("Test process method with invalid job type and running job processor")
    void testProcessWithInvalidJobTypeAndRunningJobProcessorOSGI() {
        var jobType = "invalid";

        when(jobProcessor.isRunning()).thenReturn(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        var result = sut.process(jobType, "123", jobProperties, resultProperties);

        assertNull(result);
        assertTrue(resultProperties.isEmpty());
        verify(jobProcessor, times(1)).isRunning();
        verify(jobProcessor, never()).createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @DisplayName("Test process method with job processor not running")
    void testProcessWithJobProcessorNotRunningOSGI() {
        var jobType = "pdf";

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        var result = sut.process(jobType, "123", jobProperties, resultProperties);

        assertNull(result);
        assertEquals(JobError.GENERAL.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        verify(jobProcessor, times(1)).isRunning();
        verify(jobProcessor, never()).createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    @DisplayName("Test statusChanged with transition from SCHEDULED to REMOVED")
    void testStatusChangedScheduleToRemovedOSGI() {
        var jobId = "testJobId";
        var serverJob = mock(ConverterJob.class);
        var statusListener = mock(ManagedJob.class);

        var data = new JobEntry(jobId, serverJob, JobPriority.MEDIUM, statusListener);

        var scheduledJobStatus = JobStatus.SCHEDULED;
        data.setJobStatus(scheduledJobStatus);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.statusChanged(jobId, data);

        // nothing happens
        assertEquals(scheduledJobStatus, data.getJobStatus());
        verify(statusListener, never()).statusChanged(eq(jobId), any());

        var removedJobStatus = JobStatus.REMOVED;
        data.setJobStatus(removedJobStatus);

        sut.statusChanged(jobId, data);

        assertEquals(JobError.QUEUE_TIMEOUT.getErrorCode(), data.getJobErrorEx().getErrorCode());
        verify(statusListener, never()).statusChanged(eq(jobId), any());
    }

    @Test
    @DisplayName("Test statusChanged with transition from NEW to REMOVED")
    void testStatusChangedFinishedOSGI() {
        var jobId = "testJobId";
        var serverJob = mock(ConverterJob.class);
        var statusListener = mock(ManagedJob.class);

        var data = new JobEntry(jobId, serverJob, JobPriority.MEDIUM, statusListener);
        data.setJobStatus(JobStatus.FINISHED);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.statusChanged(jobId, data);

        assertEquals(JobError.NONE.getErrorCode(), data.getJobErrorEx().getErrorCode());
        verify(statusListener, never()).statusChanged(eq(jobId), any());
    }

    @Test
    @DisplayName("Test statusChanged with transition from NEW to ERROR")
    void testStatusChangedErrorOSGI() {
        var jobId = "testJobId";
        var serverJob = mock(ConverterJob.class);
        var statusListener = mock(ManagedJob.class);

        var data = new JobEntry(jobId, serverJob, JobPriority.MEDIUM, statusListener);
        data.setJobErrorEx(new JobErrorEx(JobError.PASSWORD));
        data.setJobStatus(JobStatus.ERROR);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.statusChanged(jobId, data);

        assertEquals(JobError.PASSWORD.getErrorCode(), data.getJobErrorEx().getErrorCode());
        verify(statusListener, never()).statusChanged(eq(jobId), any());
    }

    @Test
    void testProcessWithOdfJobTypeAndQueueLimitReached() {
        var jobType = "odf";
        var jobInputFile = new File("test.odt");
        jobProperties.put(Properties.PROP_INPUT_TYPE, "odf");
        jobProperties.put(Properties.PROP_RESULT_BUFFER, new byte[0]);

        when(serverManager.getJobInputFile(ArgumentMatchers.<HashMap<String, Object>>any(), ArgumentMatchers.any())).thenReturn(jobInputFile);
        var job = mock(ConverterJob.class);
        when(job.backendTypeNeeded()).thenReturn(BackendType.READERENGINE);
        when(jobProcessor.isRunning()).thenReturn(true);
        when(jobProcessor.createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(job);
        when(jobProcessor.getErrorCache()).thenReturn(mock(ErrorCache.class));
        var jobMonitor = mock(JobMonitor.class);
        when(jobMonitor.isQueueLimitReached()).thenReturn(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        var inputStream = sut.process(jobType, "123", jobProperties, resultProperties);

        assertNull(inputStream);
        assertEquals(JobError.MAX_QUEUE_COUNT.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertNotNull(resultProperties.get(Properties.PROP_RESULT_ERROR_DATA));
        verify(jobProcessor, atLeast(1)).isRunning();
        verify(jobProcessor, times(1)).createServerJob(anyString(), "123", ArgumentMatchers.any(), ArgumentMatchers.any());
        // TODO check logdata
    }

    @Test
    void testDeleteFileIfFlaggedWithFalse() throws IOException {
        var file = TestUtil.createUniqueFile(tempDir, "test", "txt");
        var deleteFileParameter = new MutableBoolean(false);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.deleteFileIfFlagged(file, deleteFileParameter);

        assertEquals(false, deleteFileParameter.getValue());
    }

    @Test
    void testDeleteFileIfFlaggedWithTrueAndFileNull() {
        var deleteFileParameter = new MutableBoolean(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.deleteFileIfFlagged(null, deleteFileParameter);

        assertEquals(true, deleteFileParameter.getValue());
    }

    @Test
    void testDeleteFileIfFlaggedWithTrueAndExistingFile() throws IOException {
        File file = TestUtil.createUniqueFile(tempDir, "test", "txt");
        assertTrue(file.exists());

        var deleteFileParameter = new MutableBoolean(true);

        var sut = new ManagedJob(jobFactory, serverManager, jobProcessor, jobMonitor, serverConfig, cache, statistics);
        sut.deleteFileIfFlagged(file, deleteFileParameter);

        assertEquals(false, deleteFileParameter.getValue());
        assertFalse(file.exists());
    }

    File getJobInputFile() throws IOException {
        var jobInputFile = TestUtil.createUniqueFile(tempDir, "input", "pdf");
        FileUtils.writeStringToFile(jobInputFile, DATA, Charset.defaultCharset());
        return jobInputFile;
    }

}
