/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converter;

import com.openexchange.documentconverter.server.util.TestUtil;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class JobDataTest {

    @TempDir
    private File tempDir;

    @Test
    void testTimestamp() throws InterruptedException {
        var sut = new JobData(mock(File.class), "jobType", new HashMap<String, Object>());
        var timeStamp = sut.getTimeStampMillis();
        Thread.sleep(10);
        sut.updateTimestamp();
        assertTrue(timeStamp < sut.getTimeStampMillis());
    }

    @Test
    void testClear() throws IOException {
        var pdfInputFile = TestUtil.createUniqueFile(tempDir, "cache", "txt");
        var sut = new JobData(pdfInputFile, "jobType", new HashMap<String, Object>());
        assertTrue(pdfInputFile.isFile());
        sut.clear();
        assertFalse(pdfInputFile.isFile());
    }

    @Test
    void testGetPageObjectType() throws InterruptedException {
        var sut = new JobData(mock(File.class), "jobType", new HashMap<String, Object>());
        assertEquals("jobType", sut.getPageJobType());
    }

    @Test
    void testGetPageJobProperties() throws InterruptedException {
        var pageJobProperties = new HashMap<String, Object>();
        pageJobProperties.put("Hello", new String("Test"));
        var sut = new JobData(mock(File.class), "jobType", pageJobProperties);

        assertEquals(pageJobProperties.size(), sut.getPageJobProperties().size());
        assertEquals(pageJobProperties, sut.getPageJobProperties());
    }
}
