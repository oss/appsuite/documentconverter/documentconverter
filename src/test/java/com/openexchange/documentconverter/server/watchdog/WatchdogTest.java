/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.watchdog;

import ch.qos.logback.classic.Level;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.util.TestUtil;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class WatchdogTest {

    @Test
    void testGetTimeoutMilliSeconds() {
        var timeout = 1000;
        var sut = new Watchdog("test", timeout, WatchdogMode.TIMEOUT);
        assertEquals(timeout, sut.getTimeoutMilliSeconds());
    }

    @Test
    void testIsRunning() {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        assertTrue(sut.isRunning());
        sut.terminate();
        assertFalse(sut.isRunning());
    }

    @Test
    void testIsTimeoutOnly() {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        assertFalse(sut.isTimeoutOnly());
        sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT);
        assertTrue(sut.isTimeoutOnly());
    }

    @Test
    void testTerminate() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        var name = "TestWatchDog";
        var sut = new Watchdog(name, 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        sut.terminate();
        assertFalse(sut.isRunning());

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC starting shutdown " + name));
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC finished shutdown " + name));

    }

    @Test
    void testAddWatchdogHandlerTerminated() {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        sut.terminate();
        var result = sut.addWatchdogHandler(mock(WatchdogHandler.class));
        assertEquals(0, result);
    }

    @Test
    void testAddWatchdogHandlerWithNewWatchdogEntry() {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogEntries = (Queue<WatchdogEntry>) ReflectionTestUtils.getField(sut, "watchdogEntries");
        assertEquals(0, watchdogEntries.size());
        var result = sut.addWatchdogHandler(mock(WatchdogHandler.class));
        assertTrue(System.currentTimeMillis() < result + 1000);
        assertEquals(1, watchdogEntries.size());

    }

    @Test
    void testAddWatchdogHandlerWithExistingWatchdogEntry() {

        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogEntries = (Queue<WatchdogEntry>) ReflectionTestUtils.getField(sut, "watchdogEntries");
        assertEquals(0, watchdogEntries.size());
        var watchdogHandler1 = new WatchdogHandlerMock();

        // Add the watchdogHandler first time
        sut.addWatchdogHandler(watchdogHandler1);
        assertEquals(1, watchdogEntries.size());

        // Add the watchdogHandler second time
        sut.addWatchdogHandler(watchdogHandler1);
        assertEquals(1, watchdogEntries.size());

        // Add another WatchdogHandler
        var watchdogHandler2 = new WatchdogHandlerMock();
        sut.addWatchdogHandler(watchdogHandler2);
        assertEquals(2, watchdogEntries.size());

        //Test the position of the watchdogHandlers
        var watchdogIterator = watchdogEntries.iterator();
        assertEquals(watchdogHandler1, watchdogIterator.next().getWatchdogHandler());
        assertEquals(watchdogHandler2, watchdogIterator.next().getWatchdogHandler());

        // Add watchdogHandler1 again to  move it to the end of the queue
        sut.addWatchdogHandler(watchdogHandler1);
        watchdogIterator = watchdogEntries.iterator();
        assertEquals(watchdogHandler2, watchdogIterator.next().getWatchdogHandler());
        assertEquals(watchdogHandler1, watchdogIterator.next().getWatchdogHandler());
    }

    @Test
    void testRemoveWatchdogHandler() {

        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogEntries = (Queue<WatchdogEntry>) ReflectionTestUtils.getField(sut, "watchdogEntries");
        assertEquals(0, watchdogEntries.size());

        var watchdogHandler = new WatchdogHandlerMock();

        sut.addWatchdogHandler(watchdogHandler);
        assertEquals(1, watchdogEntries.size());

        // Try to remove a not existing WatchdogHandler
        sut.removeWatchdogHandler(new WatchdogHandlerMock());
        assertEquals(1, watchdogEntries.size());

        // Remove the added WatchdogHandler
        sut.removeWatchdogHandler(watchdogHandler);
        assertEquals(0, watchdogEntries.size());
    }

    @Test
    void testRunWithJobErrorTimeout() throws InterruptedException {
        var sut = new Watchdog("test", 0, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogHandler = new WatchdogHandlerMock();

        sut.addWatchdogHandler(watchdogHandler);
        assertEquals(-1, watchdogHandler.getCurTimeMillis());
        assertEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNull(watchdogHandler.getNotificationJobErrorEx());
        sut.start();
        Thread.sleep(500);
        assertNotEquals(-1, watchdogHandler.getCurTimeMillis());
        assertNotEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNotNull(watchdogHandler.getNotificationJobErrorEx());
        assertEquals(JobError.TIMEOUT, watchdogHandler.getNotificationJobErrorEx().getJobError());
    }

    @Test
    void testRunWithJobErrorNone() throws InterruptedException {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogHandler = new WatchdogHandlerMock();

        sut.addWatchdogHandler(watchdogHandler);
        assertEquals(-1, watchdogHandler.getCurTimeMillis());
        assertEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNull(watchdogHandler.getNotificationJobErrorEx());
        sut.start();
        Thread.sleep(500);
        assertEquals(-1, watchdogHandler.getCurTimeMillis());
        assertEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNull(watchdogHandler.getNotificationJobErrorEx());
    }

    @Test
    void testRunWithJobErrorDisposed() throws InterruptedException {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogHandler = new WatchdogHandlerMock();
        watchdogHandler.setDisposed(true);
        sut.addWatchdogHandler(watchdogHandler);
        assertEquals(-1, watchdogHandler.getCurTimeMillis());
        assertEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNull(watchdogHandler.getNotificationJobErrorEx());
        sut.start();
        Thread.sleep(500);
        assertNotEquals(-1, watchdogHandler.getCurTimeMillis());
        assertNotEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNotNull(watchdogHandler.getNotificationJobErrorEx());
        assertEquals(JobError.DISPOSED, watchdogHandler.getNotificationJobErrorEx().getJobError());
    }

    @Test
    void testRunWithJobErrorTimeoutAfterTerminate() throws InterruptedException {
        var sut = new Watchdog("test", 100, WatchdogMode.TIMEOUT_AND_DISPOSED);
        var watchdogHandler = new WatchdogHandlerMock();

        sut.addWatchdogHandler(watchdogHandler);
        assertEquals(-1, watchdogHandler.getCurTimeMillis());
        assertEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNull(watchdogHandler.getNotificationJobErrorEx());
        sut.terminate();
        sut.start();

        Thread.sleep(500);
        assertNotEquals(-1, watchdogHandler.getCurTimeMillis());
        assertNotEquals(-1, watchdogHandler.getStartTimeMillis());
        assertNotNull(watchdogHandler.getNotificationJobErrorEx());
        assertEquals(JobError.TIMEOUT, watchdogHandler.getNotificationJobErrorEx().getJobError());
    }

}
