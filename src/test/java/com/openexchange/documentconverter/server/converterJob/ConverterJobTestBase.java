/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.converterJob.util.FileContent;
import com.openexchange.documentconverter.server.mocks.XDesktopMock;
import com.openexchange.documentconverter.server.mocks.XModelMock;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.sun.star.beans.XPropertySetInfo;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.sheet.XConditionalFormat;
import com.sun.star.uno.Exception;
import com.sun.star.uno.XComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

public class ConverterJobTestBase {

    protected ListAppender<ILoggingEvent> logListAppenderServerManager;
    protected final Map<String, Object> jobProperties = new HashMap<>();
    protected final Map<String, Object> resultProperties = new HashMap<>();

    public void resetJobAndResultProperties() {
        jobProperties.clear();
        resultProperties.clear();
    }

    public void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

    public void mockReInstanceMethods(REInstance reInstanceMock, XDesktopMock xDesktop) {
        lenient().when(reInstanceMock.isConnected()).thenReturn(true);
        lenient().when(reInstanceMock.getDesktop()).thenReturn(xDesktop);
    }

    public void mockReInstanceAndDesktopMethodsForFactoryContextAndFilterFactory(REInstance reInstanceMock, XDesktopMock xDesktop, XComponentContext xContext, String instanceName, Object createdObject, XMultiServiceFactory xFilterFactory, String filterName, Object filterInstance) throws Exception {
        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstanceMock, xDesktop, xContext, instanceName, createdObject);
        when(reInstanceMock.getFilterFactory()).thenReturn(xFilterFactory);
        when(xFilterFactory.createInstanceWithArguments(eq(filterName), any())).thenReturn(filterInstance);
    }

    public void mockReInstanceAndDesktopMethodsForFactoryAndContext(REInstance reInstanceMock, XDesktopMock xDesktop, XComponentContext xContext, String instanceName, Object createdObject) throws Exception {
        mockReInstanceMethods(reInstanceMock, xDesktop);
        lenient().when(reInstanceMock.getComponentFactory()).thenReturn(xDesktop);
        lenient().when(reInstanceMock.getDefaultContext()).thenReturn(xContext);
        lenient().when(xDesktop.createInstanceWithContext(eq(instanceName), eq(xContext))).thenReturn(createdObject);
    }

    public void mockMethodsForLoadComponentFromUrl(XDesktopMock xDesktop, XModelMock xComponent, XPropertySetInfo xPropertySetInfo, String serviceName) throws com.sun.star.io.IOException {
        lenient().when(xDesktop.loadComponentFromURL(anyString(), anyString(), anyInt(), any())).thenReturn(xComponent);
        lenient().when(xComponent.getPropertySetInfo()).thenReturn(xPropertySetInfo);
        lenient().when(xComponent.supportsService(eq(serviceName))).thenReturn(true);
    }
    public String extractMockedSimpleClassName(String simpleClassName) {
        var indexOfFirstDollar = simpleClassName.indexOf('$');
        return simpleClassName.substring(0, (indexOfFirstDollar > 0) ? indexOfFirstDollar : simpleClassName.length());
    }

    public record IOFileData(File inputFile, File outputFile) {}

    public IOFileData prepareInputAndOutputFiles(Path tempDir) throws IOException {
        var inputFileName = "input-" + UUID.randomUUID();
        var inputFilePath = tempDir.resolve(inputFileName);
        Files.write(inputFilePath, FileContent.TEST_FILE_CONTENT.getBytes());
        var outputFileName = "output-" + UUID.randomUUID();
        var inputFile = tempDir.resolve(inputFileName).toFile();
        var outputFile = tempDir.resolve(outputFileName).toFile();
        return new IOFileData(inputFile, outputFile);
    }

}
