/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.util.FileContent;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.mocks.XDesktopMock;
import com.openexchange.documentconverter.server.mocks.XModelMock;
import com.openexchange.documentconverter.server.mocks.XPageStylesMock;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySetInfo;
import com.sun.star.io.IOException;
import com.sun.star.lang.DisposedException;
import com.sun.star.task.PDFExportException;
import com.sun.star.task.XInteractionContinuation;
import com.sun.star.task.XInteractionHandler;
import com.sun.star.task.XInteractionPassword;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
class DocumentConverterJobTest extends ConverterJobTestBase {

    // See ConverterJobInteractionHandler in ConverterJob class
    private static final int PDF_ERRORCODE_NO_DOCUMENT_CONTENT = 0x01000001;

    @Autowired
    private ServerConfig serverConfig;

    @Mock
    private REInstance reInstance;

    @TempDir
    private Path tempDir;

    private final String jobType = "docx";

    private DocumentConverterJob sut;

    @BeforeEach
    void setUp() {
        resetJobAndResultProperties();
        setupLogListAppenderServerManager();
    }

    @Test
    @SuppressWarnings("DataFlowIssue")
    void constructorWithNullServerConfig() {
        // provide null to check correct handling
        assertThrowsExactly(NullPointerException.class, () -> new DocumentConverterJob(null, jobProperties, resultProperties, jobType));
    }

    @Test
    void getHashWithoutInputFile() {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var hash = sut.getHash();

        // a job without input file, input file hash or cache hash has not a valid hash
        assertNull(hash);
    }

    @Test
    void getHashWithInputFile() {
        var file = new File("dev/null");
        jobProperties.put(Properties.PROP_INPUT_FILE, file);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var hash = sut.getHash();

        assertNotNull(hash);
        assertTrue(StringUtils.isNotEmpty(hash));
    }

    @Test
    void getHashWithCacheHash() {
        var cacheHash = UUID.randomUUID().toString();
        jobProperties.put(Properties.PROP_CACHE_HASH, cacheHash);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var hash = sut.getHash();

        assertEquals(cacheHash, hash);
        assertTrue(StringUtils.isNotEmpty(hash));
    }

    @Test
    void getHashWithZipArchive() {
        jobProperties.put(Properties.PROP_INPUT_FILE, new File("dev/null"));
        jobProperties.put(Properties.PROP_ZIP_ARCHIVE, false);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var hashWithoutZipArchive = sut.getHash();

        assertNotNull(hashWithoutZipArchive);

        jobProperties.put(Properties.PROP_ZIP_ARCHIVE, true);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var hashWithZipArchive = sut.getHash();

        assertNotNull(hashWithZipArchive);
        assertNotEquals(hashWithoutZipArchive, hashWithZipArchive);
    }

    @Test
    void backendTypeNeeded() {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var backendType = sut.backendTypeNeeded();

        assertEquals(BackendType.READERENGINE, backendType);
    }

    @Test
    void doExecuteREInstanceOkWithTextAndPageRange() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "text");
        jobProperties.put(Properties.PROP_PAGE_RANGE, "1-2");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    public static Stream<Arguments> documentServicesAndInputType() {
        return Stream.of(
            Arguments.of("com.sun.star.sheet.SpreadsheetDocument", null),
            Arguments.of("com.sun.star.text.TextDocument", "text"),
            Arguments.of("com.sun.star.presentation.PresentationDocument", null),
            Arguments.of("com.sun.star.drawing.DrawingDocument", null)
        );
    }

    @ParameterizedTest
    @MethodSource("documentServicesAndInputType")
    void doExecuteREInstanceOkWith(String serviceName, String inputType) throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, serviceName);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        if (inputType != null) {
            jobProperties.put(Properties.PROP_INPUT_TYPE, inputType);
        }
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    public static Stream<Arguments> filterShortNamesAndServices() {
        return Stream.of(
            Arguments.of("html", "com.sun.star.text.TextDocument"),
            Arguments.of("pdf", "com.sun.star.text.TextDocument"),
            Arguments.of("odf", "com.sun.star.text.TextDocument"),
            Arguments.of("ooxml", "com.sun.star.text.TextDocument"),
            Arguments.of("odf", "com.sun.star.sheet.SpreadsheetDocument"),
            Arguments.of("ooxml", "com.sun.star.sheet.SpreadsheetDocument"),
            Arguments.of("odf", "com.sun.star.presentation.PresentationDocument"),
            Arguments.of("ooxml", "com.sun.star.presentation.PresentationDocument"),
            Arguments.of("odf", "com.sun.star.drawing.DrawingDocument"),
            Arguments.of("ooxml", "com.sun.star.drawing.DrawingDocument")
        );
    }

    @ParameterizedTest
    @MethodSource("filterShortNamesAndServices")
    void doExecuteREInstanceWithDifferentFileShortNames(String filterShortName, String serviceName) throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, serviceName);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, filterShortName);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    @Test
    void doExecuteREInstanceExceptionOnStore() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        doThrow(new com.sun.star.io.IOException("Cannot write to file")).when(xComponent).storeToURL(anyString(), any());

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "text");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        assertThrowsExactly(com.sun.star.io.IOException.class, () -> sut.doExecute(reInstance));

        assertTrue(resultProperties.isEmpty());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ReaderEngine error storing document with exception: Cannot write to file"));
        verify(xComponent,never()).close(anyBoolean());
    }

    @Test
    void doExecuteREInstanceExceptionOnCloseWithEmptyOutputFile() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        doThrow(new com.sun.star.lang.DisposedException()).when(xComponent).close(anyBoolean());

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "text");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        assertThrowsExactly(com.sun.star.lang.DisposedException.class, () -> sut.doExecute(reInstance));

        verify(xComponent).close(anyBoolean());
    }

    @Test
    void doExecuteREInstanceExceptionOnCloseWithNonEmptyOutputFile() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        doThrow(new com.sun.star.lang.DisposedException()).when(xComponent).close(anyBoolean());

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "text");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK_WITH_ENGINE_ERROR, converterStatus);
        verify(xComponent).close(anyBoolean());
    }

    @Test
    void doExecuteREInstanceNotConnected() throws Exception {
        var fileName = UUID.randomUUID().toString();
        var filePath = tempDir.resolve(fileName);
        Files.write(filePath, FileContent.TEST_FILE_CONTENT.getBytes());

        when(reInstance.isConnected()).thenReturn(false);

        jobProperties.put(Properties.PROP_INPUT_FILE, tempDir.resolve(fileName).toFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "text");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.ERROR, converterStatus);
    }

    @Test
    void applyJobProperties() {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);
        var inputFileHash = UUID.randomUUID().toString();
        var inputFile = new File("dev/null");
        var addJobProps = new HashMap<String, Object>();
        addJobProps.put(Properties.PROP_INPUT_FILE, inputFile);

        sut.addJobProperties(addJobProps);

        var newJobProps = sut.getJobProperties();
        assertEquals(inputFile, newJobProps.get(Properties.PROP_INPUT_FILE));
    }

    @Test
    public void getResultProperties() {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var resProps = sut.getResultProperties();

        assertTrue(resProps.isEmpty());
    }

    @Test
    public void executeWithoutInputFileResultsInError() throws Exception {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var status = sut.execute(new Object());

        assertEquals(ConverterStatus.ERROR, status);
        var resProps = sut.getResultProperties();
        assertEquals(1, resProps.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    public void executeWithInputFileThatCannotBeReadResultIsnError() throws Exception {
        var inputFile = new File("dev/null");
        jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var status = sut.execute(new Object());

        assertEquals(ConverterStatus.ERROR, status);

        var resProps = sut.getResultProperties();
        assertEquals(1, resProps.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    public void executeWithValidInputFileOk() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        var mediaDescriptor = new PropertyValue[1];
        mediaDescriptor[0] = new PropertyValue();
        mediaDescriptor[0].Name = "FilterData";
        mediaDescriptor[0].Value = new PropertyValue[1];
        ((PropertyValue[])mediaDescriptor[0].Value)[0] = new PropertyValue();
        ((PropertyValue[])mediaDescriptor[0].Value)[0].Name = Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT;
        ((PropertyValue[])mediaDescriptor[0].Value)[0].Value = 9;

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        when(xComponent.getArgs()).thenReturn(mediaDescriptor);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.execute(reInstance);

        var resProps = sut.getResultProperties();
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
        assertEquals(0, resProps.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    public void executeWithValidInputFileAndInputTypeCsvThrowsExceptionResultsInError() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPageStyles = mock(XPageStylesMock.class);
        var xDefaultPagePropSet = mock(XPageStylesMock.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xComponent, "com.sun.star.text.TextDocument");
        when(xComponent.hasPropertyByName(eq("RedlineDisplayType"))).thenReturn(true);
        when(xComponent.getStyleFamilies()).thenReturn(xComponent);
        when(xComponent.getByName(eq("PageStyles"))).thenReturn(xPageStyles);
        when(xPageStyles.hasByName(eq("Default"))).thenReturn(true);
        when(xPageStyles.getByName(eq("Default"))).thenReturn(xDefaultPagePropSet);
        when(xDefaultPagePropSet.getPropertySetInfo()).thenReturn(xDefaultPagePropSet);
        when(xDefaultPagePropSet.hasPropertyByName(eq("HeaderIsOn"))).thenReturn(true);
        doThrow(new IllegalArgumentException()).when(xDefaultPagePropSet).setPropertyValue(eq("HeaderIsOn"), eq(false));

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "csv");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        assertThrowsExactly(DisposedException.class, () -> sut.execute(reInstance));

        verify(xDefaultPagePropSet).setPropertyValue(eq("HeaderIsOn"), eq(false));
        verify(xDefaultPagePropSet, never()).setPropertyValue(eq("HeaderOn"), eq(false));
    }

    @Test
    public void executeWithValidInputFileAndInputTypeCsvOk() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPageStyles = mock(XPageStylesMock.class);
        var xDefaultPagePropSet = mock(XPageStylesMock.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xComponent, "com.sun.star.text.TextDocument");
        when(xComponent.hasPropertyByName(eq("RedlineDisplayType"))).thenReturn(true);
        when(xComponent.getStyleFamilies()).thenReturn(xComponent);
        when(xComponent.getByName(eq("PageStyles"))).thenReturn(xPageStyles);
        when(xPageStyles.hasByName(eq("Default"))).thenReturn(true);
        when(xPageStyles.getByName(eq("Default"))).thenReturn(xDefaultPagePropSet);
        when(xDefaultPagePropSet.getPropertySetInfo()).thenReturn(xDefaultPagePropSet);
        when(xDefaultPagePropSet.hasPropertyByName(eq("HeaderIsOn"))).thenReturn(true);
        when(xDefaultPagePropSet.hasPropertyByName(eq("HeaderOn"))).thenReturn(true);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_INPUT_TYPE, "csv");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.execute(reInstance);

        var resProps = sut.getResultProperties();
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
        assertEquals(0, resProps.get(Properties.PROP_RESULT_ERROR_CODE));

        verify(xDefaultPagePropSet).setPropertyValue(eq("HeaderIsOn"), eq(false));
        verify(xDefaultPagePropSet).setPropertyValue(eq("HeaderOn"), eq(false));
    }

    @Test
    public void executeWithPasswordProtectedFileAndInteractionHandlerResultsInError() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        prepareInteractionHandlerForLoadComponentFromUrl(xDesktop, xComponent, mock(XInteractionPassword.class));

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.execute(reInstance);

        var resProps = sut.getResultProperties();
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.ERROR, converterStatus);
        assertEquals(JobError.PASSWORD.getErrorCode(), resProps.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    public void executeWithInputFileAndPDFExportExceptionOnInteractionHandlerResultsInError() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(ioFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        mockReInstanceMethods(reInstance, xDesktop);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        var pdfExportException = mock(PDFExportException.class);
        pdfExportException.ErrorCodes = new int[]{ PDF_ERRORCODE_NO_DOCUMENT_CONTENT };
        prepareInteractionHandlerForLoadComponentFromUrl(xDesktop, xComponent, pdfExportException);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        var converterStatus = sut.execute(reInstance);

        var resProps = sut.getResultProperties();
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.ERROR, converterStatus);
        assertEquals(JobError.NO_CONTENT.getErrorCode(), resProps.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    public void killDoesNothing() {
        sut = new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);

        sut.kill();
    }

    private void prepareInteractionHandlerForLoadComponentFromUrl(XDesktopMock xDesktop, XModelMock xComponent, Object caseObject) throws IOException {
        when(xDesktop.loadComponentFromURL(anyString(), anyString(), anyInt(), any())).thenAnswer(invocation -> {
            var args = invocation.getArguments();
            var propertyValues = (PropertyValue[]) args[3];
            var interactionHandler = (XInteractionHandler)null;

            for (var propertyValue : propertyValues) {
                if (propertyValue.Name.equals("InteractionHandler")) {
                    interactionHandler = (XInteractionHandler)propertyValue.Value;
                }
            }

            if (interactionHandler != null) {
                var interactionRequest = mock(com.sun.star.task.XInteractionRequest.class);
                var classSimpleName = extractMockedSimpleClassName(caseObject.getClass().getSimpleName());
                switch (classSimpleName) {
                    case "PDFExportException" -> when(interactionRequest.getRequest()).thenReturn(caseObject);
                    case "XInteractionPassword", "XInteractionPassword2", "XInteractionSupplyAuthentication" -> {
                        var xInteractionPassword = (XInteractionContinuation)caseObject;
                        when(interactionRequest.getContinuations()).thenReturn(new XInteractionContinuation[]{xInteractionPassword});
                    }
                    default -> throw new IllegalArgumentException("Unknown case object type");
                }
                interactionHandler.handle(interactionRequest);
            }
            return xComponent;
        });
    }

}
