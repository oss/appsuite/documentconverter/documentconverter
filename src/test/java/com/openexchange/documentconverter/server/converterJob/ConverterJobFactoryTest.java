/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ConverterJobFactoryTest {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private ServerManager serverManager;

    @Mock
    private Cache cache;

    @InjectMocks
    private ConverterJobFactory sut;

    private Map<String, Object> jobProperties = new HashMap<>();
    private Map<String, Object> resultProperties = new HashMap<>();

    @BeforeEach
    void setUp() {
        jobProperties.clear();
    }

    public static Stream<Arguments> jobTypesAndJobClasses() {
        return Stream.of(
            Arguments.of("pdf", DocumentConverterJob.class),
            Arguments.of("pdfa", DocumentConverterJob.class),
            Arguments.of("ooxml", DocumentConverterJob.class),
            Arguments.of("odf", DocumentConverterJob.class),
            Arguments.of("graphic", GraphicConverterJob.class),
            Arguments.of("shape2png", Shape2PNGConverterJob.class)
        );
    }

    @ParameterizedTest
    @MethodSource("jobTypesAndJobClasses")
    void createJob(String jobType, Class<? extends ConverterJob> expectedClass) {
        var job = sut.createJob(jobType, "123", jobProperties, resultProperties);
        var jobClass = job.getClass();
        assertEquals(expectedClass, jobClass);
    }
}
