/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.cache.CacheEntry;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.converterJob.util.FileContent;
import com.openexchange.documentconverter.server.mocks.XDesktopMock;
import com.openexchange.documentconverter.server.mocks.XModelMock;
import com.openexchange.documentconverter.server.mocks.XShapeMock;
import com.openexchange.documentconverter.server.mocks.XSpreadsheetsMock;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.sun.star.beans.XPropertySetInfo;
import com.sun.star.drawing.XDrawPage;
import com.sun.star.drawing.XDrawPageSupplier;
import com.sun.star.drawing.XDrawPages;
import com.sun.star.drawing.XGraphicExportFilter;
import com.sun.star.uno.XComponentContext;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class Shape2PNGConverterJobTest extends ConverterJobTestBase {

    @Mock
    private REInstance reInstance;

    @Mock
    private Cache cache;

    @TempDir
    private Path tempDir;

    private Shape2PNGConverterJob sut;

    @BeforeEach
    void setUp() {
        resetJobAndResultProperties();
        setupLogListAppenderServerManager();
    }

    @Test
    void backendTypeNeeded() {
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        var backendType = sut.backendTypeNeeded();

        assertEquals(BackendType.READERENGINE, backendType);
    }

    @Test
    void doExecuteOk() throws Exception {
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        // this job tries to get the list of replacements from the input document and is
        // adding each replacement into the cache
        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    public static Stream<Arguments> documentServices() {
        return Stream.of(
            Arguments.of("com.sun.star.sheet.SpreadsheetDocument", null),
            Arguments.of("com.sun.star.text.TextDocument", "text"),
            Arguments.of("com.sun.star.presentation.PresentationDocument", null),
            Arguments.of("com.sun.star.drawing.DrawingDocument", null)
        );
    }

    @ParameterizedTest
    @MethodSource("documentServices")
    void doExecuteWithShapeNumberAndInOutputFileOk(String serviceName) throws Exception {
        var pageNumber = 1;
        var shapeNumber = 9;
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var xDrawPage = mock(XDrawPage.class);
        var xShape = mock(XShapeMock.class);
        var xSheets = mock(XSpreadsheetsMock.class);
        var xDrawPageSupplier = mock(XDrawPageSupplier.class);
        var xDrawPages = mock(XDrawPages.class);
        var xGraphicExportFiler = mock(XGraphicExportFilter.class);
        var inOutFileData = prepareInputAndOutputFiles(tempDir);
        jobProperties.put(Properties.PROP_PAGE_NUMBER, pageNumber);
        jobProperties.put(Properties.PROP_SHAPE_NUMBER, shapeNumber);
        jobProperties.put(Properties.PROP_OUTPUT_FILE, inOutFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, inOutFileData.inputFile());
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.drawing.GraphicExportFilter", xGraphicExportFiler);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, serviceName);
        lenient().when(xComponent.getDrawPage()).thenReturn(xDrawPage);
        lenient().when(xComponent.getSheets()).thenReturn(xSheets);
        lenient().when(xComponent.getDrawPages()).thenReturn(xDrawPages);
        lenient().when(xSheets.getByIndex(eq(pageNumber - 1))).thenReturn(xDrawPageSupplier);
        lenient().when(xDrawPages.getByIndex(eq(pageNumber - 1))).thenReturn(xDrawPage);
        lenient().when(xDrawPageSupplier.getDrawPage()).thenReturn(xDrawPage);
        lenient().when(xDrawPage.getByIndex(eq(shapeNumber - 1))).thenReturn(xShape);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    @Test
    void doExecuteWithShapeNumberButWithoutOutputFileResultsInError() throws Exception {
        jobProperties.put(Properties.PROP_SHAPE_NUMBER, 9);
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.ERROR, converterStatus);
    }

    @Test
    void doExecuteWithoutShapeNumberOk() throws Exception {
        var shapeNumber = 9;
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var xGraphicExportFiler = mock(XGraphicExportFilter.class);
        var xDrawPage = mock(XDrawPage.class);
        var xShape = mock(XShapeMock.class);
        var cacheEntry = mock(CacheEntry.class);
        var inOutFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(inOutFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        var hash = UUID.randomUUID().toString();
        var test = new JSONObject();
        test.put(hash, createResourceName(1, shapeNumber));

        jobProperties.put(Properties.PROP_OUTPUT_FILE, inOutFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, inOutFileData.inputFile());
        jobProperties.put(Properties.PROP_SHAPE_REPLACEMENTS, test.toString());
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.drawing.GraphicExportFilter", xGraphicExportFiler);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");
        when(xComponent.getDrawPage()).thenReturn(xDrawPage);
        when(xDrawPage.getByIndex(eq(shapeNumber))).thenReturn(xShape);
        when(cache.startNewEntry(anyString())).thenReturn(cacheEntry);
        when(cacheEntry.getResultFile()).thenReturn(inOutFileData.outputFile());

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    @Test
    void doExecuteWithoutShapeNumberAndBadJsonInReplacementFails() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var xGraphicExportFiler = mock(XGraphicExportFilter.class);
        var inOutFileData = prepareInputAndOutputFiles(tempDir);
        Files.write(inOutFileData.outputFile().toPath(), FileContent.TEST_FILE_CONTENT.getBytes());

        var hash = UUID.randomUUID().toString();
        var badJsonAsStr = "{\""+ hash + "\",\"a1.b1.c1.d\"}";

        jobProperties.put(Properties.PROP_OUTPUT_FILE, inOutFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, inOutFileData.inputFile());
        jobProperties.put(Properties.PROP_SHAPE_REPLACEMENTS, badJsonAsStr);
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.drawing.GraphicExportFilter", xGraphicExportFiler);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");

        var converterStatus = sut.doExecute(reInstance);

        // JsonException will be swallowed and ignored
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    @Test
    void doExecuteWithoutShapeNumberAndBadSyntaxInReplacementFails() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xPropertySetInfo = mock(XPropertySetInfo.class);
        var xGraphicExportFiler = mock(XGraphicExportFilter.class);
        var inOutFileData = prepareInputAndOutputFiles(tempDir);

        var hash = UUID.randomUUID().toString();
        var badJsonAsStr = "{\""+ hash + "\":\"a1!b1!c1.d\"}";

        jobProperties.put(Properties.PROP_OUTPUT_FILE, inOutFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, inOutFileData.inputFile());
        jobProperties.put(Properties.PROP_SHAPE_REPLACEMENTS, badJsonAsStr);
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);

        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.drawing.GraphicExportFilter", xGraphicExportFiler);
        mockMethodsForLoadComponentFromUrl(xDesktop, xComponent, xPropertySetInfo, "com.sun.star.text.TextDocument");

        var converterStatus = sut.doExecute(reInstance);

        // Exception due bad syntax will be swallowed and ignored
        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC server, shape2png, wrong syntax in replacement map"));
        verify(cache, never()).startNewEntry(anyString());
    }

    @Test
    void applyJobProperties() {
        var pageNumber = 7;
        var shapeNumber = 3;
        var addProperties = new HashMap<String, Object>();
        sut = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);
        addProperties.put(Properties.PROP_SHAPE_REPLACEMENTS, "test");
        addProperties.put(Properties.PROP_PAGE_NUMBER, pageNumber);
        addProperties.put(Properties.PROP_SHAPE_NUMBER, shapeNumber);

        sut.addJobProperties(addProperties);

        var newJobProps = sut.getJobProperties();
        assertEquals("test", newJobProps.get(Properties.PROP_SHAPE_REPLACEMENTS));
        assertEquals(pageNumber, newJobProps.get(Properties.PROP_PAGE_NUMBER));
        assertEquals(shapeNumber, newJobProps.get(Properties.PROP_SHAPE_NUMBER));
    }

    private String createResourceName(int pageNumber, int shapeNumber) {
        return String.format("h1.p%d.s%d.i1.", pageNumber, shapeNumber);
    }

}
