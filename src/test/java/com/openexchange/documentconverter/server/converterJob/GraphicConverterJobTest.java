/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.mocks.XDesktopMock;
import com.openexchange.documentconverter.server.mocks.XGraphicProviderMock;
import com.openexchange.documentconverter.server.mocks.XModelMock;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.util.TestUtil;
import com.sun.star.beans.PropertyValue;
import com.sun.star.graphic.XGraphic;
import com.sun.star.uno.XComponentContext;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GraphicConverterJobTest extends ConverterJobTestBase {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private REInstance reInstance;

    @TempDir
    private Path tempDir;

    private GraphicConverterJob sut;

    @BeforeEach
    void setUp() {
        resetJobAndResultProperties();
        setupLogListAppenderServerManager();
    }

    @Test
    void getHashWithoutInputFile() {
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var hash = sut.getHash();

        // a job without input file, input file hash or cache hash has not a valid hash
        assertNull(hash);
    }

    @Test
    void getHashWithInputFile() {
        var file = new File("dev/null");
        jobProperties.put(Properties.PROP_INPUT_FILE, file);
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var hash = sut.getHash();

        assertNotNull(hash);
        assertTrue(StringUtils.isNotEmpty(hash));
    }

    @Test
    void getHashWithCacheHash() {
        var cacheHash = UUID.randomUUID().toString();
        jobProperties.put(Properties.PROP_CACHE_HASH, cacheHash);
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var hash = sut.getHash();

        assertEquals(cacheHash, hash);
        assertTrue(StringUtils.isNotEmpty(hash));
    }

    @Test
    void backendTypeNeeded() {
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var backendType = sut.backendTypeNeeded();

        assertEquals(BackendType.READERENGINE, backendType);
    }

    @Test
    void doExecuteOk() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xGraphicProvider = mock(XGraphicProviderMock.class);
        var xGraphic = mock(XGraphic.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.graphic.GraphicProvider", xGraphicProvider);
//        when(xDesktop.loadComponentFromURL(anyString(), anyString(), anyInt(), any())).thenReturn(xComponent);
        when(xGraphicProvider.queryGraphic(any())).thenReturn(xGraphic);
//        when(xDesktop.loadComponentFromURL(anyString(), anyString(), anyInt(), any())).thenReturn(xComponent);
        when(xGraphicProvider.queryGraphic(any())).thenReturn(xGraphic);

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);

        verify(xGraphicProvider).storeGraphic(eq(xGraphic), any());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC converting graphic"));
    }

    @Test
    void doExecuteWithPixelWidthAndHeightOk() throws Exception {
        var xDesktop = mock(XDesktopMock.class);
        var xComponent = mock(XModelMock.class);
        var xContext = mock(XComponentContext.class);
        var xGraphicProvider = mock(XGraphicProviderMock.class);
        var xGraphic = mock(XGraphic.class);
        var ioFileData = prepareInputAndOutputFiles(tempDir);

        final var optFilterData = new AtomicReference<PropertyValue[]>();
        mockReInstanceAndDesktopMethodsForFactoryAndContext(reInstance, xDesktop, xContext, "com.sun.star.graphic.GraphicProvider", xGraphicProvider);
//        when(xDesktop.loadComponentFromURL(anyString(), anyString(), anyInt(), any())).thenReturn(xComponent);
        when(xGraphicProvider.queryGraphic(any())).thenReturn(xGraphic);
        doAnswer(invocation -> {
            var properties = (PropertyValue[])invocation.getArgument(1);
            assertNotNull(properties);
            assertTrue(properties.length > 0);
            for (var property : properties) {
                if ("FilterData".equals(property.Name)) {
                    optFilterData.set((PropertyValue[]) property.Value);
                }
            }
            return null;
        }).when(xGraphicProvider).storeGraphic(eq(xGraphic), any());

        jobProperties.put(Properties.PROP_OUTPUT_FILE, ioFileData.outputFile());
        jobProperties.put(Properties.PROP_INPUT_FILE, ioFileData.inputFile());
        jobProperties.put(Properties.PROP_PIXEL_WIDTH, 640);
        jobProperties.put(Properties.PROP_PIXEL_HEIGHT, 480);
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var converterStatus = sut.doExecute(reInstance);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
        assertNotNull(optFilterData.get());
        var pixelProps = optFilterData.get();
        for (var property : pixelProps) {
            if ("PixelWidth".equals(property.Name)) {
                assertEquals(640, property.Value);
            } else if ("PixelHeight".equals(property.Name)) {
                assertEquals(480, property.Value);
            }
        }
    }

    @Test
    void doExecuteFailDueToReInstanceIsNullOrNotReInstanceType() throws Exception {
        sut = new GraphicConverterJob(jobProperties, resultProperties);

        var converterStatus = sut.doExecute(null);

        assertNotNull(converterStatus);
        // TODO: why is this NOT an error?
        assertEquals(ConverterStatus.OK, converterStatus);

        var xComponent = mock(XModelMock.class);
        converterStatus = sut.doExecute(xComponent);

        assertNotNull(converterStatus);
        assertEquals(ConverterStatus.OK, converterStatus);
    }

    @Test
    void applyJobProperties() {
        sut = new GraphicConverterJob(jobProperties, resultProperties);
        var inputFileHash = UUID.randomUUID().toString();
        var inputFile = new File("dev/null");
        var addJobProps = new HashMap<String, Object>();
        addJobProps.put(Properties.PROP_INPUT_FILE, inputFile);

        sut.addJobProperties(addJobProps);

        var newJobProps = sut.getJobProperties();
        assertEquals(inputFile, newJobProps.get(Properties.PROP_INPUT_FILE));
    }
}
