/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.concurrency;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.atomic.AtomicBoolean;

public class IdLockerTest {

    @Test
    public void testUnlockMode() {
        var unlockMode = IdLocker.UnlockMode.STANDARD;
        assertEquals(IdLocker.UnlockMode.STANDARD, unlockMode);
    }

    @Test
    public void testLock() {
        var element = "element1";

        var locked = IdLocker.lock(element);

        assertTrue(locked);
    }

    @Test
    public void testLockWithNullElement() {
        var locked = IdLocker.lock(null);

        assertFalse(locked);
    }

    @Test
    public void testUnlock() {
        var element = "element1";
        IdLocker.lock(element);
        IdLocker.unlock(element);

        // Assert that the lock is released
        var locked = IdLocker.lock(element);
        assertTrue(locked);
    }

    @Test
    public void testUnlockWithNullElement() {
        assertDoesNotThrow( () -> IdLocker.unlock(null));
    }

    @Test
    public void testLockWithTryLockMode() {
        var element = "element1";
        var locked = IdLocker.lock(element, IdLocker.Mode.TRY_LOCK);
        assertTrue(locked);
    }

    @Test
    public void testLockWithTryLockModeAndExistingLock() throws InterruptedException {
        var element = "element1";
        IdLocker.lock(element);

        final var lockSuccess = new AtomicBoolean(false);
        var secondThread = new Thread(() -> lockSuccess.set(IdLocker.lock(element, IdLocker.Mode.TRY_LOCK)));
        secondThread.start();
        secondThread.join();

        assertFalse(lockSuccess.get());
    }

    @Test
    public void testLockWithTryLockModeAndNullElement() {
        var locked = IdLocker.lock(null, IdLocker.Mode.TRY_LOCK);

        assertFalse(locked);
    }
}
