/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.job.JobEntry;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class ErrorCacheTest {

    @Mock
    private ServerConfig serverConfig;

    @Mock
    private JobEntry jobEntry;

    @InjectMocks
    private ErrorCache sut;

    private HashMap<String, Object> jobProperties = new HashMap<>();

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();
    }

    @Test
    void testInit_WhenErrorCacheEnabled_ShouldInitializeWatchdog() {
        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);

        sut.init();

        assertTrue(sut.isEnabled());
    }

    @Test
    void testInit_WhenErrorCacheDisabled_ShouldNotInitializeWatchdog_Timeout() {
        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(0L);

        sut.init();

        assertFalse(sut.isEnabled());
    }

    @Test
    void testInit_WhenErrorCacheDisabled_ShouldNotInitializeWatchdog_MaxCycleCount() {
        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(0);

        sut.init();

        assertFalse(sut.isEnabled());
    }

    @Test
    void testTerminate_WhenWatchdogNotNull_ShouldTerminateWatchdog() {
        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);

        sut.init();

        sut.terminate();

    }

    @Test
    void testTerminate_WhenWatchdogNull_ShouldNotThrowException() {
        // Act & Assert
        assertDoesNotThrow(() -> sut.terminate());
    }

    @Test
    void testNotifyJobFinish_WhenErrorCacheEnabledAndJobHasError_ShouldAddToErrorHashMap() {
        jobProperties.put(Properties.PROP_INFO_FILENAME, "filename");

        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);
        when(jobEntry.getHash()).thenReturn("hash");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.GENERAL));
        when(jobEntry.getJobProperties()).thenReturn(jobProperties);

        sut.init();

        try (var cacheMock = mockStatic(Cache.class)) {
            cacheMock.when(() -> Cache.isValidHash("hash")).thenReturn(true);

            var activationCount = sut.notifyJobFinish(jobEntry);

            assertEquals(1, activationCount);
        }
    }

    @Test
    void testNotifyJobFinish_WhenErrorCacheEnabledAndJobHasNoError_ShouldRemoveFromErrorHashMap() {
        jobProperties.put(Properties.PROP_INFO_FILENAME, "filename");

        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);
        when(jobEntry.getHash()).thenReturn("hash");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.GENERAL));
        when(jobEntry.getJobProperties()).thenReturn(jobProperties);

        sut.init();

        try (var cacheMock = mockStatic(Cache.class)) {
            cacheMock.when(() -> Cache.isValidHash("hash")).thenReturn(true);

            var activationCount = sut.notifyJobFinish(jobEntry);
            assertEquals(1, activationCount);

            var jobErrorEx = sut.getJobErrorExForHash("hash");

            assertNotNull(jobErrorEx);
            assertEquals(JobError.GENERAL, jobErrorEx.getJobError());

            // now we change the error code to NONE
            when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.NONE));

            activationCount = sut.notifyJobFinish(jobEntry);
            assertEquals(0, activationCount);

            jobErrorEx = sut.getJobErrorExForHash("hash");

            assertNull(jobErrorEx);
        }
    }

    @Test
    void testGetJobErrorExForHash_WhenErrorCacheEnabledAndEntryIsActive_ShouldReturnJobErrorEx() {
        jobProperties.put(Properties.PROP_INFO_FILENAME, "filename");

        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);
        when(jobEntry.getHash()).thenReturn("hash");
        when(jobEntry.getJobErrorEx()).thenReturn(new JobErrorEx(JobError.GENERAL));
        when(jobEntry.getJobProperties()).thenReturn(jobProperties);

        sut.init();

        try (var cacheMock = mockStatic(Cache.class)) {
            cacheMock.when(() -> Cache.isValidHash("hash")).thenReturn(true);

            var activationCount = sut.notifyJobFinish(jobEntry);
            assertEquals(1, activationCount);

            var jobErrorEx = sut.getJobErrorExForHash("hash");

            assertNotNull(jobErrorEx);
            assertEquals(JobError.GENERAL, jobErrorEx.getJobError());
        }
    }

    @Test
    void testGetJobErrorExForHash_WhenErrorCacheEnabledAndEntryNotFound() {
        when(serverConfig.getErrorCacheTimeoutSeconds()).thenReturn(60L);
        when(serverConfig.getErrorCacheMaxCycleCount()).thenReturn(10);

        sut.init();

        var jobErrorEx = sut.getJobErrorExForHash("hash");

        assertNull(jobErrorEx);
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }

}
