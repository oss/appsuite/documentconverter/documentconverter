/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.job.JobEntry;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class ErrorCacheEntryTest {

    private static final int ERROR_CACHE_MAX_CYCLE_COUNT = 5;

    @Mock
    private JobEntry jobEntry;

    private JobErrorEx jobErrorEx = new JobErrorEx(JobError.GENERAL);

    private ErrorCacheEntry sut;


    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();

        when(jobEntry.getJobErrorEx()).thenReturn(jobErrorEx);
        when(jobEntry.getHash()).thenReturn(UUID.randomUUID().toString());
        sut = new ErrorCacheEntry(jobEntry, ERROR_CACHE_MAX_CYCLE_COUNT);
    }

    @Test
    void handleWatchdogNotification_deactivatesEntryWhenActivationCountIsLessThanMaxCycleCount() {
        sut.activate();
        sut.handleWatchdogNotification(1000, 500, new JobErrorEx());

        assertFalse(sut.isActive());
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC ErrorCache deactivated entry after timeout"));
    }

    @Test
    void handleWatchdogNotification_logsPermanentAdditionWhenActivationCountIsEqualToMaxCycleCount() {
        var activations = ERROR_CACHE_MAX_CYCLE_COUNT;
        for (var i = 0; i < activations; i++) {
            sut.activate();
        }

        sut.handleWatchdogNotification(1000, 500, new JobErrorEx());

        assertEquals(5, sut.getActivationCount());
        var expectedMsg = "DC ErrorCache permanently added entry after " + activations + " activations";
        assertTrue(TestUtil.listAppenderContainsAllMsgOnLogLevel(logListAppenderServerManager, Level.INFO, expectedMsg));
    }

    @Test
    void isActive_returnsFalseByDefault() {
        assertFalse(sut.isActive());
    }

    @Test
    void activate_incrementsActivationCountAndSetsIsActiveToTrue() {
        long activationCount = sut.activate();

        assertTrue(sut.isActive());
        assertEquals(1, activationCount);
    }

    @Test
    void getActivationCount_returnsCorrectActivationCount() {
        sut.activate();
        sut.activate();
        sut.activate();

        assertEquals(3, sut.getActivationCount());
    }

    @Test
    void getHash_returnsCorrectHash() {
        var hash = UUID.randomUUID().toString();
        when(jobEntry.getHash()).thenReturn(hash);
        sut = new ErrorCacheEntry(jobEntry, ERROR_CACHE_MAX_CYCLE_COUNT);

        assertEquals(hash, sut.getHash());
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);
    }
}
