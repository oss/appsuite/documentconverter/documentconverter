/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExceptionUtilsTest {

    private ListAppender<ILoggingEvent> logListAppenderServerManager;

    @BeforeEach
    void setUp() {
        setupLogListAppenderServerManager();
    }

    @Test
    void handleThrowable_threadDeath_shouldThrowAndLog() {
        var td = new ThreadDeath();

        assertThrows(ThreadDeath.class, () -> ExceptionUtils.handleThrowable(td));

        assertTrue(TestUtil.listAppenderContainsAllMsgOnLogLevel(logListAppenderServerManager, Level.ERROR, "Thread death"));
    }

    @Test
    void handleThrowable_outOfMemoryError_shouldThrowAndLog() {
        var oom = new OutOfMemoryError();

        assertThrows(OutOfMemoryError.class, () -> ExceptionUtils.handleThrowable(oom));

        assertTrue(TestUtil.listAppenderContainsAllMsgOnLogLevel(logListAppenderServerManager, Level.ERROR, "Out of memory"));
    }

    @Test
    void handleThrowable_virtualMachineError_shouldThrowAndLog() {
        var vme = new VirtualMachineError() {};

        assertThrows(VirtualMachineError.class, () -> ExceptionUtils.handleThrowable(vme));

        assertTrue(TestUtil.listAppenderContainsAllMsgOnLogLevel(logListAppenderServerManager, Level.ERROR, "Virtual machine error"));
    }

    @Test
    void handleThrowable_otherThrowable_shouldNotThrowAndNotLog() {
        var throwable = new Throwable();

        assertDoesNotThrow(() -> ExceptionUtils.handleThrowable(throwable));

        assertEquals(0, logListAppenderServerManager.list.size());
    }

    private void setupLogListAppenderServerManager() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);

        logListAppenderServerManager = TestUtil.addListAppenderToLogger(ExceptionUtils.class);
    }

}
