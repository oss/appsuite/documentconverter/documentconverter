/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import com.openexchange.documentconverter.server.Properties;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


class JobErrorExTest {

    private JobErrorEx sut;

    private JSONObject jsonErrorData;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(JobErrorEx.class, "DEFAULT_STATUS_ERROR_CACHE", JobErrorEx.StatusErrorCache.NONE);
        sut = new JobErrorEx();
    }

    @Test
    void testDefaultConstructor() {
        var jobError = JobError.NONE;
        jsonErrorData = getErrorDataFromJobError(jobError);

        assertEquals(jobError, sut.getJobError());
        assertFalse(sut.hasError());
        assertTrue(sut.hasNoError());
        assertEquals(0, sut.getErrorCode());
        assertEquals(jobError.toString().toLowerCase(), sut.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.NONE, sut.getStatusErrorCache());
        assertEquals(jsonErrorData.toString(), sut.getErrorData().toString());
    }

    @Test
    void testParameterizedConstructor() {
        var jobError = JobError.GENERAL;
        jsonErrorData = getErrorDataFromJobError(jobError);

        sut = new JobErrorEx(jobError);

        assertEquals(jobError, sut.getJobError());
        assertTrue(sut.hasError());
        assertFalse(sut.hasNoError());
        assertEquals(jobError.getErrorCode(), sut.getErrorCode());
        assertEquals(jobError.getErrorText(), sut.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.NONE, sut.getStatusErrorCache());
        assertEquals(jsonErrorData.toString(), sut.getErrorData().toString());
    }

    @Test
    void testSetStatusErrorCache() {
        var statusErrorCache = JobErrorEx.StatusErrorCache.ACTIVE;

        sut.setStatusErrorCache(statusErrorCache);

        assertEquals(statusErrorCache, sut.getStatusErrorCache());
    }

    @Test
    void testSetErrorData() {
        var errorDescription = "Error description";
        sut.setErrorData(errorDescription);

        assertEquals(errorDescription, sut.getErrorData().getString(JobErrorEx.JSON_KEY_DESCRIPTION));
    }

    @Test
    void testSetErrorDataWithDescriptionAndData() {
        var errorDescription = "Error description";
        var errorData = new JSONObject();
        errorData.put("key", "value");

        sut.setErrorData(errorDescription, errorData);

        assertEquals(errorDescription, sut.getErrorData().getString(JobErrorEx.JSON_KEY_DESCRIPTION));
        // get error data provides a json object with pre-defined properties
        assertEquals("", sut.getErrorData().optString("key"));
    }

    @Test
    void testGetErrorData() {
        var jobError = JobError.GENERAL;
        sut = new JobErrorEx(jobError);

        var errorData = sut.getErrorData();

        assertNotNull(errorData);
        assertEquals(jobError.getErrorCodeRepresentation(), errorData.getString("errorCode"));
        assertEquals("", errorData.optString("statusErrorCache"));
        assertEquals("", errorData.optString("description"));
        assertNull(errorData.optJSONObject("descriptionData"));
    }

    @Test
    void testFromResultPropertiesWithNullProperties() {
        var result = JobErrorEx.fromResultProperties(null);

        assertEquals(JobError.NONE, result.getJobError());
        assertFalse(result.hasError());
        assertTrue(result.hasNoError());
        assertEquals(0, result.getErrorCode());
        assertEquals(JobError.NONE.toString().toLowerCase(), result.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.NONE, result.getStatusErrorCache());
        assertNotNull(result.getErrorData());
    }

    @Test
    void testFromResultPropertiesWithValidProperties() {
        Map<String, Object> resultProperties = new HashMap<>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.GENERAL.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, "{\"statusErrorCache\":\"ACTIVE\",\"description\":\"Error description\",\"descriptionData\":{\"key\":\"value\"}}");

        var result = JobErrorEx.fromResultProperties(resultProperties);

        assertEquals(JobError.GENERAL, result.getJobError());
        assertTrue(result.hasError());
        assertFalse(result.hasNoError());
        assertEquals(JobError.GENERAL.getErrorCode(), result.getErrorCode());
        assertEquals(JobError.GENERAL.getErrorText(), result.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.ACTIVE, result.getStatusErrorCache());

        JSONObject errorData = result.getErrorData();
        assertNotNull(errorData);
        assertEquals(JobError.GENERAL.getErrorCodeRepresentation(), errorData.getString("errorCode"));
        assertEquals("ACTIVE".toLowerCase(), errorData.getString("statusErrorCache"));
        assertEquals("Error description", errorData.getString("description"));
        assertNotNull(errorData.getJSONObject("descriptionData"));
        assertEquals("value", errorData.getJSONObject("descriptionData").getString("key"));
    }

    @Test
    void testFromResultPropertiesWithInvalidProperties() {
        Map<String, Object> resultProperties = new HashMap<>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.GENERAL.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, "{\"statusErrorCache\":\"INVALID\",\"description\":\"Error description\",\"descriptionData\":{\"key\":\"value\"}}");

        var result = JobErrorEx.fromResultProperties(resultProperties);

        assertEquals(JobError.GENERAL, result.getJobError());
        assertTrue(result.hasError());
        assertFalse(result.hasNoError());
        assertEquals(JobError.GENERAL.getErrorCode(), result.getErrorCode());
        assertEquals(JobError.GENERAL.getErrorText(), result.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.NONE, result.getStatusErrorCache());
    }

    @Test
    void testForEach() {
        var jobErrors = new ArrayList<>();
        JobErrorEx.forEach(jobErrors::add);

        assertEquals(Arrays.asList(JobError.values()), jobErrors);
    }

    @Test
    void testSetErrorCacheDisabled() {
        JobErrorEx.setErrorCacheDisabled();

        var jobError = JobError.GENERAL;

        sut = new JobErrorEx(jobError);

        assertEquals(jobError, sut.getJobError());
        assertTrue(sut.hasError());
        assertFalse(sut.hasNoError());
        assertEquals(jobError.getErrorCode(), sut.getErrorCode());
        assertEquals(jobError.getErrorText(), sut.getErrorText());
        assertEquals(JobErrorEx.StatusErrorCache.DISABLED, sut.getStatusErrorCache());
    }

    private JSONObject getErrorDataFromJobError(JobError jobError) {
        var jsonErrorData = new JSONObject();
        jsonErrorData.put(JobErrorEx.JSON_KEY_ERROR_CODE, jobError.getErrorCodeRepresentation());
        return jsonErrorData;
    }
}
