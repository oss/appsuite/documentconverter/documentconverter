/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.executor;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJob;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.job.JobPriority;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class JobExecutorTests {

    @TempDir
    File tempDir;

    ServerManager serverManager;

    ServerConfig serverConfig;

    RConvertRequestModel rConvertRequestModel;

    JobExecutor sut;

    Cache cache;

    ManagedJobFactory managedJobFactory;

    AsyncServerExecutor asyncServerExecutor;

    @BeforeEach
    void beforeEach() {
        rConvertRequestModel = new RConvertRequestModel();
        serverManager = mock(ServerManager.class);
        serverConfig = mock(ServerConfig.class);
        when(serverConfig.getScratchDir()).thenReturn(tempDir);

        cache = mock(Cache.class);
        managedJobFactory = mock(ManagedJobFactory.class);
        asyncServerExecutor = mock(AsyncServerExecutor.class);
    }

    void setJobExecutor() {
        sut = spy(new JobExecutor(serverManager, serverConfig, cache, rConvertRequestModel, managedJobFactory, asyncServerExecutor));
    }

    @Test
    void testProcessConvert_WithoutCacheHash() {
        setJobExecutor();
        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();

        sut.processConvert("filename", jobProperties, resultProperties);
        verify(cache, never()).getCachedResult(any(),any(), any(), anyBoolean());
    }

    @Test
    void testProcessConvert_WithCacheHash() {
        rConvertRequestModel.setCacheHash("CashHash");
        setJobExecutor();

        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();
        sut.processConvert("filename", jobProperties, resultProperties);
        verify(cache, atLeastOnce()).getCachedResult(any(),any(), any(), anyBoolean());
    }

    @Test
    void testProcessConvert_WithSerializedObject() throws IOException {
        setJobExecutor();
        var serializedObject = TestUtil.createUniqueFile(tempDir, "serial", "jpeg");
        sut.transferObject.setSerializedObject(serializedObject);
        sut.transferObject.setMimeType(MediaType.IMAGE_JPEG_VALUE);

        when(managedJobFactory.createManagedJob()).thenReturn(mock(ManagedJob.class));

        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();

        sut.processConvert("filename", jobProperties, resultProperties);
        verify(cache, never()).getCachedResult(any(),any(), any(), anyBoolean());
        verify(managedJobFactory, atLeastOnce()).createManagedJob();

        assertFalse(jobProperties.containsKey(Properties.PROP_FEATURES_ID));
        assertFalse(jobProperties.containsKey(Properties.PROP_AUTOSCALE));
        assertNotNull(jobProperties.get(Properties.PROP_INPUT_FILE));
        assertFalse(serializedObject.exists());
    }

    @Test
    void testProcessConvert_WithSerializedObjectAndFeaturesId() throws IOException {
        rConvertRequestModel.setFeaturesId(100);
        setJobExecutor();

        var serializedObject = TestUtil.createUniqueFile(tempDir, "serial", "jpeg");
        sut.transferObject.setSerializedObject(serializedObject);
        sut.transferObject.setMimeType(MediaType.IMAGE_JPEG_VALUE);

        when(managedJobFactory.createManagedJob()).thenReturn(mock(ManagedJob.class));

        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();

        sut.processConvert("filename", jobProperties, resultProperties);
        verify(cache, never()).getCachedResult(any(),any(), any(), anyBoolean());
        verify(managedJobFactory, atLeastOnce()).createManagedJob();

        assertEquals(100, jobProperties.get(Properties.PROP_FEATURES_ID));
        assertFalse(jobProperties.containsKey(Properties.PROP_AUTOSCALE));
        assertNotNull(jobProperties.get(Properties.PROP_INPUT_FILE));
        assertFalse(serializedObject.exists());

    }


    @Test
    void testProcessConvert_WithSerializedObjectAndAutoscale() throws IOException {
        rConvertRequestModel.setJobType("html");
        rConvertRequestModel.setAutoScale(true);
        setJobExecutor();
        var serializedObject = TestUtil.createUniqueFile(tempDir, "serial", "jpeg");
        sut.transferObject.setSerializedObject(serializedObject);
        sut.transferObject.setMimeType(MediaType.IMAGE_JPEG_VALUE);

        when(managedJobFactory.createManagedJob()).thenReturn(mock(ManagedJob.class));

        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();

        sut.processConvert("filename", jobProperties, resultProperties);
        verify(cache, never()).getCachedResult(any(),any(), any(), anyBoolean());
        verify(managedJobFactory, atLeastOnce()).createManagedJob();

        assertFalse(jobProperties.containsKey(Properties.PROP_FEATURES_ID));
        assertEquals(true, jobProperties.get(Properties.PROP_AUTOSCALE));
        assertNotNull(jobProperties.get(Properties.PROP_INPUT_FILE));
        assertFalse(serializedObject.exists());
    }

    @Test
    void testExecuteDocumentConverterConversion_async() {
        rConvertRequestModel.setAsync(true);
        setJobExecutor();
        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();
        var inputStream = sut.executeDocumentConverterConversion(rConvertRequestModel.getJobType(), "CacheHash", jobProperties, resultProperties);
        assertNull(inputStream);
        verify(asyncServerExecutor, atLeastOnce()).triggerExecution(any(), any(), any(), any());
        assertEquals(true, resultProperties.get(Properties.PROP_RESULT_ASYNC));
        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    void testExecuteDocumentConverterConversion_Synchron() {
        setJobExecutor();
        var managedJob = mock(ManagedJob.class);
        when(managedJob.process(any(), "123", any(), any())).thenReturn(mock(FileInputStream.class));
        when(managedJobFactory.createManagedJob()).thenReturn(managedJob);

        var jobProperties = sut.getJobProperties();
        var resultProperties = new HashMap<String, Object>();
        var inputStream = sut.executeDocumentConverterConversion(rConvertRequestModel.getJobType(), "CacheHash", jobProperties, resultProperties);
        assertNotNull(inputStream);
        verify(managedJobFactory, atLeastOnce()).createManagedJob();
        verify(managedJob, atLeastOnce()).process(any(), "123", any(), any());

        assertEquals(0, resultProperties.size());
    }

    @Test
    void testAddToMapIfNotNull_NullValue() {
        setJobExecutor();
        var map = new HashMap<String, Object>();
        sut.addToMapIfNotNull("KEY", null, map);
        assertTrue(map.isEmpty());
    }

    @Test
    void testAddToMapIfNotNull() {
        setJobExecutor();
        var map = new HashMap<String, Object>();
        sut.addToMapIfNotNull("KEY", 100, map);
        assertEquals(100, map.get("KEY"));
    }

    @Test
    void testGetJobProperties_AddIfNotNullPropertiesAreNull() {
        rConvertRequestModel.setJobId(null);
        rConvertRequestModel.setLocale(null);
        rConvertRequestModel.setFilterShortName(null);
        rConvertRequestModel.setInputType(null);
        rConvertRequestModel.setPixelHeight(null);
        rConvertRequestModel.setPixelWidth(null);
        rConvertRequestModel.setMimeType(null);
        rConvertRequestModel.setPageNumber(null);
        rConvertRequestModel.setShapeNumber(null);
        rConvertRequestModel.setZipArchive(null);
        rConvertRequestModel.setCacheOnly(null);
        rConvertRequestModel.setImageResolution(null);
        rConvertRequestModel.setAsync(null);
        rConvertRequestModel.setHideChanges(null);
        rConvertRequestModel.setHideComments(null);
        rConvertRequestModel.setRemoteUrl(null);
        rConvertRequestModel.setUserRequest(null);

        setJobExecutor();
        var properties = sut.getJobProperties();

        assertFalse(properties.containsKey(Properties.PROP_JOBID));
        assertFalse(properties.containsKey(Properties.PROP_LOCALE));
        assertFalse(properties.containsKey(Properties.PROP_FILTER_SHORT_NAME));
        assertFalse(properties.containsKey(Properties.PROP_INPUT_TYPE));
        assertFalse(properties.containsKey(Properties.PROP_PIXEL_WIDTH));
        assertFalse(properties.containsKey(Properties.PROP_PIXEL_HEIGHT));
        assertFalse(properties.containsKey(Properties.PROP_MIME_TYPE));
        assertFalse(properties.containsKey(Properties.PROP_PAGE_NUMBER));
        assertFalse(properties.containsKey(Properties.PROP_SHAPE_NUMBER));
        assertFalse(properties.containsKey(Properties.PROP_ZIP_ARCHIVE));
        assertFalse(properties.containsKey(Properties.PROP_IMAGE_RESOLUTION));
        assertFalse(properties.containsKey(Properties.PROP_ASYNC));
        assertFalse(properties.containsKey(Properties.PROP_HIDE_CHANGES));
        assertFalse(properties.containsKey(Properties.PROP_HIDE_COMMENTS));
        assertFalse(properties.containsKey(Properties.PROP_USER_REQUEST));
    }

    @Test
    void testGetJobProperties_AddIfNotNullPropertiesAreNotNull() {
        rConvertRequestModel.setJobId("JOB_ID");
        rConvertRequestModel.setLocale("LOCAL");
        rConvertRequestModel.setFilterShortName("FILTER_SHORT_NAME");
        rConvertRequestModel.setInputType("INPUT_TYPE");
        rConvertRequestModel.setPixelHeight(100);
        rConvertRequestModel.setPixelWidth(200);
        rConvertRequestModel.setMimeType("MIME_TYPE");
        rConvertRequestModel.setPageNumber(300);
        rConvertRequestModel.setShapeNumber(400);
        rConvertRequestModel.setZipArchive(true);
        rConvertRequestModel.setCacheOnly(true);
        rConvertRequestModel.setImageResolution(500);
        rConvertRequestModel.setAsync(true);
        rConvertRequestModel.setHideChanges(true);
        rConvertRequestModel.setHideComments(true);
        rConvertRequestModel.setRemoteUrl("REMOTE_URL");
        rConvertRequestModel.setUserRequest(true);

        setJobExecutor();
        var properties = sut.getJobProperties();

        assertEquals("JOB_ID", properties.get(Properties.PROP_JOBID));
        assertEquals("LOCAL", properties.get(Properties.PROP_LOCALE));
        assertEquals("FILTER_SHORT_NAME", properties.get(Properties.PROP_FILTER_SHORT_NAME));
        assertEquals("INPUT_TYPE", properties.get(Properties.PROP_INPUT_TYPE));
        assertEquals(100, properties.get(Properties.PROP_PIXEL_HEIGHT));
        assertEquals(200, properties.get(Properties.PROP_PIXEL_WIDTH));
        assertEquals("MIME_TYPE", properties.get(Properties.PROP_MIME_TYPE));
        assertEquals(300, properties.get(Properties.PROP_PAGE_NUMBER));
        assertEquals(400, properties.get(Properties.PROP_SHAPE_NUMBER));
        assertEquals(true, properties.get(Properties.PROP_ZIP_ARCHIVE));
        assertEquals(500, properties.get(Properties.PROP_IMAGE_RESOLUTION));
        assertEquals(true, properties.get(Properties.PROP_ASYNC));
        assertEquals(true, properties.get(Properties.PROP_HIDE_CHANGES));
        assertEquals(true, properties.get(Properties.PROP_HIDE_COMMENTS));
        assertEquals(true, properties.get(Properties.PROP_USER_REQUEST));
    }

    @Test
    void testGetJobProperties_AddPropertiesEveryTimeWithNull() {

        rConvertRequestModel.setMethod(null);
        rConvertRequestModel.setJobType(null);
        rConvertRequestModel.setImageScaleType(null);
        rConvertRequestModel.setPriority(null);

        setJobExecutor();
        var properties = sut.getJobProperties();
        assertTrue(properties.containsKey(Properties.PROP_REMOTE_METHOD));
        assertNull(properties.get(Properties.PROP_REMOTE_METHOD));

        assertTrue(properties.containsKey(Properties.PROP_JOBTYPE));
        assertNull(properties.get(Properties.PROP_JOBTYPE));

        assertTrue(properties.containsKey(Properties.PROP_IMAGE_SCALE_TYPE));
        assertNull(properties.get(Properties.PROP_IMAGE_SCALE_TYPE));

        assertTrue(properties.containsKey(Properties.PROP_PRIORITY));
        assertEquals(JobPriority.BACKGROUND, properties.get(Properties.PROP_PRIORITY));
    }

    @Test
    void testGetJobProperties_AddPropertiesEveryTimeWithNotNull() {

        rConvertRequestModel.setMethod("METHOD");
        rConvertRequestModel.setJobType("JOB_TYPE");
        rConvertRequestModel.setImageScaleType("IMAGE_SCALE_TYPE");
        rConvertRequestModel.setPriority("medium");
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertTrue(properties.containsKey(Properties.PROP_REMOTE_METHOD));
        assertEquals("METHOD", properties.get(Properties.PROP_REMOTE_METHOD));

        assertTrue(properties.containsKey(Properties.PROP_JOBTYPE));
        assertEquals("JOB_TYPE", properties.get(Properties.PROP_JOBTYPE));

        assertTrue(properties.containsKey(Properties.PROP_IMAGE_SCALE_TYPE));
        assertEquals("IMAGE_SCALE_TYPE", properties.get(Properties.PROP_IMAGE_SCALE_TYPE));

        assertTrue(properties.containsKey(Properties.PROP_PRIORITY));
        assertEquals(JobPriority.MEDIUM, properties.get(Properties.PROP_PRIORITY));
    }

    @Test
    void testGetJobProperties_IsValidHashIsInvalid() {
        rConvertRequestModel.setCacheHash("#e");
        setJobExecutor();
        var properties = sut.getJobProperties();

        assertFalse(properties.containsKey(Properties.PROP_CACHE_HASH));
    }

    @Test
    void testGetJobProperties_IsValidHashIsValid() {
        rConvertRequestModel.setCacheHash("1Abc-3");
        setJobExecutor();
        var properties = sut.getJobProperties();

        assertEquals("1Abc-3", properties.get(Properties.PROP_CACHE_HASH));
    }

    @Test
    void testGetJobProperties_InputUrlIsNull() {
        rConvertRequestModel.setInputUrl(null);
        setJobExecutor();
        var properties = sut.getJobProperties();

        assertFalse(properties.containsKey(Properties.PROP_INPUT_URL));
    }

    @Test
    void testGetJobProperties_InputUrlIsInvalid() {
        rConvertRequestModel.setInputUrl("http://www.example.com/%0AHello");
        setJobExecutor();
        var properties = sut.getJobProperties();

        var inputUrl = properties.get(Properties.PROP_INPUT_URL);
        assertNotEquals(rConvertRequestModel.getInputUrl(), inputUrl);
        assertEquals("http://www.example.com/\nHello", inputUrl);
    }

    @Test
    void testGetJobProperties_InputUrlIsValid() {
        rConvertRequestModel.setInputUrl("http://www.example.com/Hello");
        setJobExecutor();
        var properties = sut.getJobProperties();

        var inputUrl = properties.get(Properties.PROP_INPUT_URL);
        assertEquals(rConvertRequestModel.getInputUrl(), inputUrl);
    }

    @Test
    void testGetJobProperties_ShapeReplacementsIsNull() {
        rConvertRequestModel.setShapeReplacements(null);
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertFalse(properties.containsKey(Properties.PROP_SHAPE_REPLACEMENTS));
    }

    @Test
    void testGetJobProperties_ShapeReplacementsReplaceChars() {
        rConvertRequestModel.setShapeReplacements(URLEncoder.encode("Hello !?", StandardCharsets.UTF_8));
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("Hello !?", properties.get(Properties.PROP_SHAPE_REPLACEMENTS));
    }

    @Test
    void testGetJobProperties_PageRangeIsNull() {
        rConvertRequestModel.setPageRange(null);
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertFalse(properties.containsKey(Properties.PROP_PAGE_RANGE));
    }

    @Test
    void testGetJobProperties_PageRangeIsInvalid() {
        rConvertRequestModel.setPageRange("1a-2");
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("1", properties.get(Properties.PROP_PAGE_RANGE));
    }

    @Test
    void testGetJobProperties_PageRangeIsValid() {
        rConvertRequestModel.setPageRange("1-2,8-100");
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("1-2,8-100", properties.get(Properties.PROP_PAGE_RANGE));
    }

    @Test
    void testGetJobProperties_InfofilenameIsNull() {
        rConvertRequestModel.setInfoFileName(null);
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertFalse(properties.containsKey(Properties.PROP_INFO_FILENAME));
    }

    @Test
    void testGetJobProperties_InfofilenameReplaceChars() {
        rConvertRequestModel.setInfoFileName(URLEncoder.encode("Hello !?", StandardCharsets.UTF_8));
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("Hello !?", properties.get(Properties.PROP_INFO_FILENAME));
    }

    @Test
    void testGetJobProperties_InputTypeIsNullWithoutInfoFilename() {
        rConvertRequestModel.setInputType(null);
        rConvertRequestModel.setInfoFileName(null);
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertFalse(properties.containsKey(Properties.PROP_INPUT_TYPE));
    }

    @Test
    void testGetJobProperties_InputTypeIsNullWithFilename() {
        rConvertRequestModel.setInputType(null);
        rConvertRequestModel.setInfoFileName("test.pdf");
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("pdf", properties.get(Properties.PROP_INPUT_TYPE));
    }

    @Test
    void testGetJobProperties_InputTypeIsNullAndFilenameWithoutExtension() {
        rConvertRequestModel.setInputType(null);
        rConvertRequestModel.setInfoFileName("test");
        setJobExecutor();
        var properties = sut.getJobProperties();
        assertEquals("", properties.get(Properties.PROP_INPUT_TYPE));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithInputStreamNull() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(null, resultProperties);
        assertNull(buffer);
        assertEquals(JobError.GENERAL.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithInputStreamNullOverwriteErrorCode() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.MAX_QUEUE_COUNT.getErrorCode());

        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(null, resultProperties);
        assertNull(buffer);
        assertEquals(JobError.MAX_QUEUE_COUNT.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithNullPROP_RESULT_BUFFERAndEmptyInputStream() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_BUFFER, null);

        var inputFile = TestUtil.createUniqueFile(tempDir, "input", "txt");

        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(new FileInputStream(inputFile), resultProperties);
        assertNull(buffer);
        assertEquals(JobError.NO_CONTENT.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertArrayEquals("".getBytes(StandardCharsets.UTF_8), (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithNullPROP_RESULT_BUFFERAndInputStream() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_BUFFER, null);

        var inputFile = TestUtil.createUniqueFile(tempDir, "input", "txt");
        Files.writeString(inputFile.toPath(), "hello");

        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(new FileInputStream(inputFile), resultProperties);
        assertArrayEquals("hello".getBytes(StandardCharsets.UTF_8), buffer);
        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertEquals(buffer, resultProperties.get(Properties.PROP_RESULT_BUFFER));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithEmptyPROP_RESULT_BUFFERAndEmptyInputStream() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_BUFFER, "".getBytes(StandardCharsets.UTF_8));

        var inputFile = TestUtil.createUniqueFile(tempDir, "input", "txt");

        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(new FileInputStream(inputFile), resultProperties);
        assertNull(buffer);
        assertEquals(JobError.NO_CONTENT.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertArrayEquals("".getBytes(StandardCharsets.UTF_8), (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER));
    }

    @Test
    void testEnsureValidResultPropertiesAndGetBuffer_WithPROP_RESULT_BUFFERAndInputStream() throws IOException {
        var resultProperties = new HashMap<String, Object>();
        resultProperties.put(Properties.PROP_RESULT_BUFFER, "prop_buffer_test".getBytes(StandardCharsets.UTF_8));

        var inputFile = TestUtil.createUniqueFile(tempDir, "input", "txt");
        Files.writeString(inputFile.toPath(), "hello");

        var buffer = JobExecutor.ensureValidResultPropertiesAndGetBuffer(new FileInputStream(inputFile), resultProperties);
        assertArrayEquals("prop_buffer_test".getBytes(StandardCharsets.UTF_8), buffer);
        assertEquals(JobError.NONE.getErrorCode(), resultProperties.get(Properties.PROP_RESULT_ERROR_CODE));
        assertEquals(buffer, resultProperties.get(Properties.PROP_RESULT_BUFFER));
    }
}
