/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.executor;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ExecutorTests {

    private static final String LOCAL_FILE_PREFIX = "file://";

    @TempDir
    File tempDir;

    ServerConfig serverConfig;

    RConvertRequestModel rConvertRequestModel;

    Executor sut;

    @BeforeEach
    void beforeEach() {
        rConvertRequestModel = new RConvertRequestModel();
        serverConfig = mock(ServerConfig.class);
        when(serverConfig.getScratchDir()).thenReturn(tempDir);

    }

    private Executor getExecutor() {
        return spy(new Executor(serverConfig, rConvertRequestModel) {
            @Override
            protected Map<String, Object> execute() {
                return null;
            }
        });
    }

    @Test
    void testExtractFormData_SourceFileIsNull() {
        sut = getExecutor();
        sut.extractFormData();
        assertNull(sut.transferObject.getSerializedObject());
    }

    @Test
    void testExtractFormData_SourceFileNotNull() throws IOException {
        var sourceFile = mock(MultipartFile.class);
        var inputFile = TestUtil.createUniqueFile(tempDir,"sourceFile", "txt");
        when(sourceFile.getInputStream()).thenReturn(new FileInputStream(inputFile));
        rConvertRequestModel.setSourcefile(sourceFile);

        sut = getExecutor();
        assertNotNull(sut.transferObject.getSerializedObject());
    }

    @Test
    void testIsValidHash_Null() {
        assertFalse(Executor.isValidHash(null));
    }

    @Test
    void testIsValidHash_Empty() {
        assertFalse(Executor.isValidHash(""));
    }

    @Test
    void testIsValidHash_InvalidChars() {
        assertFalse(Executor.isValidHash("A$"));
        assertFalse(Executor.isValidHash("A\\"));
        assertFalse(Executor.isValidHash("A*"));
    }

    @Test
    void testIsValidHash_ValidStartChar() {
        assertTrue(Executor.isValidHash("A"));
        assertTrue(Executor.isValidHash("b"));
        assertTrue(Executor.isValidHash("0"));
        assertTrue(Executor.isValidHash("9"));
    }

    @Test
    void testIsValidHash_InValidStartChar() {
        assertFalse(Executor.isValidHash("#A"));
    }

    @Test
    void testIsValidHash_ValidEndChar() {
        assertTrue(Executor.isValidHash("A_-.,@#"));
    }

    @Test
    void testIsValidRange_Null() {
        assertFalse(Executor.isValidPageRange(null));
    }

    @Test
    void testIsValidRange_Empty() {
        assertFalse(Executor.isValidPageRange(""));
    }

    @Test
    void testIsValidRange_InvalidChars() {
        assertFalse(Executor.isValidPageRange("A"));
        assertFalse(Executor.isValidPageRange("a"));
        assertFalse(Executor.isValidPageRange("*"));
        assertFalse(Executor.isValidPageRange("#"));
    }

    @Test
    void testIsValidRange() {
        assertTrue(Executor.isValidPageRange("1"));
        assertTrue(Executor.isValidPageRange("1-1"));
        assertTrue(Executor.isValidPageRange("1-1,"));
        assertTrue(Executor.isValidPageRange("1-,1,5--0"));
    }

    @Test
    void testCreateTempInputFileFromSerializedObject_SerializedObjectIsNull() {

        sut = getExecutor();
        sut.transferObject.setSerializedObject(null);

        var file = sut.createTempInputFileFromSerializedObject(new boolean[0], new HashMap<>());
        assertNull(file);
    }

    @Test
    void testCreateTempInputFileFromSerializedObject_SerializedObjectNotFile() {
        sut = getExecutor();
        sut.transferObject.setSerializedObject("");
        var file = sut.createTempInputFileFromSerializedObject(new boolean[0], new HashMap<>());
        assertNull(file);
    }

    @Test
    void testCreateTempInputFileFromSerializedObject_SerializedObjectIsImage() throws IOException {
        sut = getExecutor();
        var serializedObject = TestUtil.createUniqueFile(tempDir, "serial", "jpeg");
        sut.transferObject.setSerializedObject(serializedObject);
        sut.transferObject.setMimeType(MediaType.IMAGE_JPEG_VALUE);
        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromSerializedObject(isPDFSource, new HashMap<>());
        assertNotNull(file);
        assertNull(sut.transferObject.getSerializedObject());
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromSerializedObject_SerializedObjectIsPDF() throws IOException {
        sut = getExecutor();
        var serializedObject = TestUtil.createUniqueFile(tempDir, "serial", "pdf");
        sut.transferObject.setSerializedObject(serializedObject);
        sut.transferObject.setMimeType(MediaType.APPLICATION_PDF_VALUE);
        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromSerializedObject(isPDFSource, new HashMap<>());
        assertNotNull(file);
        assertNull(sut.transferObject.getSerializedObject());
        assertTrue(isPDFSource[0]);
    }

//    data:image/png;base64,
    @Test
    void testCreateTempInputFileFromBase64_WithEmptyUrl(){
        sut = getExecutor();
        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromBase64("", isPDFSource, new HashMap<>());
        assertNull(file);
    }

    @Test
    void testCreateTempInputFileFromBase64_PDFUrl(){
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var url = "data:application/pdf;base64,123";
        var file = sut.createTempInputFileFromBase64(url, isPDFSource, new HashMap<>());

        assertNotNull(file);
        assertTrue(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromBase64_JpegUrl(){
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var url = "data:image/jpeg;base64,123";
        var file = sut.createTempInputFileFromBase64(url, isPDFSource, new HashMap<>());

        assertNotNull(file);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromBase64_JpegUrlWithoutData(){
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var url = "data:image/jpeg;base64,1";
        var file = sut.createTempInputFileFromBase64(url, isPDFSource, new HashMap<>());

        assertNull(file);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromHttpUrl_EmptyUrl() {
        sut = getExecutor();
        boolean[] isPDFSource = { false };

        var file = sut.createTempInputFileFromHttpUrl("", isPDFSource, new HashMap<>());

        assertNull(file);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromHttpUrl_WithPDF() {
        try (MockedStatic<ServerManager> serverManagerMockedStatic = Mockito.mockStatic(ServerManager.class)) {
            serverManagerMockedStatic.when(() -> ServerManager.resolveURL(anyString(), any(), any())).thenAnswer(i -> {
                assertInstanceOf(MutableObject.class, i.getArgument(2));
                var extensionWrapper = (MutableObject<String>) i.getArgument(2);
                extensionWrapper.setValue("pdf");
                return TestUtil.createUniqueFile(tempDir, "urlPDF", "pdf");
            });

            sut = getExecutor();
            boolean[] isPDFSource = { false };

            var file = sut.createTempInputFileFromHttpUrl("", isPDFSource, new HashMap<>());
            assertNotNull(file);
            assertTrue(isPDFSource[0]);
        }
    }

    @Test
    void testCreateTempInputFileFromHttpUrl_WithJpeg() {
        try (MockedStatic<ServerManager> serverManagerMockedStatic = Mockito.mockStatic(ServerManager.class)) {
            serverManagerMockedStatic.when(() -> ServerManager.resolveURL(anyString(), any(), any())).thenAnswer(i -> {
                assertInstanceOf(MutableObject.class, i.getArgument(2));
                var extensionWrapper = (MutableObject<String>) i.getArgument(2);
                extensionWrapper.setValue("");
                return TestUtil.createUniqueFile(tempDir, "urlImage", "jpeg");
            });

            sut = getExecutor();
            boolean[] isPDFSource = { false };

            var file = sut.createTempInputFileFromHttpUrl("", isPDFSource, new HashMap<>());
            assertNotNull(file);
            assertFalse(isPDFSource[0]);
        }
    }

    @Test
    @Disabled // Set file to not readable don't work on GitLab
    void testCreateTempInputFileFromLocalFile_NotReadableFile() throws IOException {
        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "txt");
        assertTrue(localFile.setReadable(false));
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromLocalFile(LOCAL_FILE_PREFIX + localFile.getPath(), isPDFSource, new HashMap<>());
        assertNull(file.file());
        assertNull(file.filename());
        assertEquals(1, isPDFSource.length);
        assertFalse(isPDFSource[0]);
    }

    @Test
    @Disabled // Set file to not readable don't work on GitLab
    void testCreateTempInputFileFromLocalFile_NotReadableFile_PROP_INFO_FILENAME() throws IOException {

        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "txt");
        assertTrue(localFile.setReadable(false));
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        properties.put(Properties.PROP_INFO_FILENAME, "test_file_name");
        var file = sut.createTempInputFileFromLocalFile(LOCAL_FILE_PREFIX + localFile.getPath(), isPDFSource, properties);
        assertNull(file.file());
        assertEquals("test_file_name", file.filename());
        assertEquals(1, isPDFSource.length);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromLocalFile() throws IOException {
        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "txt");
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromLocalFile(LOCAL_FILE_PREFIX + localFile.getPath(), isPDFSource, new HashMap<>());
        assertNotNull(file.file());
        assertEquals(localFile.getPath(), file.filename());
        assertEquals(1, isPDFSource.length);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromLocalFile_PROP_INFO_FILENAME() throws IOException {
        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "txt");
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        properties.put(Properties.PROP_INFO_FILENAME, "test_file_name");

        var file = sut.createTempInputFileFromLocalFile(LOCAL_FILE_PREFIX + localFile.getPath(), isPDFSource, properties);
        assertNotNull(file.file());
        assertEquals("test_file_name", file.filename());
        assertEquals(1, isPDFSource.length);
        assertFalse(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFileFromLocalFile_PDF() throws IOException {
        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "pdf");
        sut = getExecutor();

        boolean[] isPDFSource = { false };
        var file = sut.createTempInputFileFromLocalFile(LOCAL_FILE_PREFIX + localFile.getPath(), isPDFSource, new HashMap<>());
        assertNotNull(file.file());
        assertEquals(localFile.getPath(), file.filename());
        assertEquals(1, isPDFSource.length);
        assertTrue(isPDFSource[0]);
    }

    @Test
    void testCreateTempInputFile_URLIsNull() {
        sut = getExecutor();
        rConvertRequestModel.setUrl(null);
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);
        verify(sut).createTempInputFileFromSerializedObject(any(), any());

        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromHttpUrl(any(), any(), any());
        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());
    }

    @Test
    void testCreateTempInputFile_Base64() {
        sut = getExecutor();
        rConvertRequestModel.setUrl("data:");
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);

        verify(sut).createTempInputFileFromBase64(any(), any(), any());

        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromHttpUrl(any(), any(), any());
        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());
    }

    @Test
    void testCreateTempInputFile_Http() {
        sut = getExecutor();
        rConvertRequestModel.setUrl("http://");
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);

        verify(sut).createTempInputFileFromHttpUrl(any(), any(), any());

        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());
    }

    @Test
    void testCreateTempInputFile_Https() {
        sut = getExecutor();
        rConvertRequestModel.setUrl("https://");
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);

        verify(sut).createTempInputFileFromHttpUrl(any(), any(), any());

        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());
    }

    @Test
    void testCreateTempInputFile_LocalFile() throws IOException {
        var localFile = TestUtil.createUniqueFile(tempDir, "localFile", "txt");

        sut = getExecutor();
        when(serverConfig.isAllowLocalFileUrls()).thenReturn(true);
        rConvertRequestModel.setUrl("file://" + localFile.getPath());
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);

        verify(sut).createTempInputFileFromLocalFile(any(), any(), any());

        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromHttpUrl(any(), any(), any());
        assertEquals(localFile.getPath(), properties.get(Properties.PROP_INFO_FILENAME));
    }

    @Test
    void testCreateTempInputFile_LocalFileAllowLocalFileUrlsIsFalse() {
        sut = getExecutor();
        when(serverConfig.isAllowLocalFileUrls()).thenReturn(false);
        rConvertRequestModel.setUrl("file:///temp");
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        sut.createTempInputFile(isPDFSource, properties);

        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());

        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromHttpUrl(any(), any(), any());
    }

    @Test
    void testCreateTempInputFile_InvalidUrl() {
        sut = getExecutor();
        rConvertRequestModel.setUrl("ftp://");
        boolean[] isPDFSource = { false };
        var properties = new HashMap<String, Object>();
        var file = sut.createTempInputFile(isPDFSource, properties);
        assertNull(file);
        verify(sut, never()).createTempInputFileFromLocalFile(any(), any(), any());
        verify(sut, never()).createTempInputFileFromBase64(any(), any(), any());
        verify(sut, never()).createTempInputFileFromSerializedObject(any(), any());
        verify(sut, never()).createTempInputFileFromHttpUrl(any(), any(), any());
    }
}
