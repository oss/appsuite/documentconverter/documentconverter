/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class OrderedHashContainerTest {

    @TempDir
    File tempDir;

    @Test
    void testAdd() {
        var sut = new OrderedHashContainer();
        sut.add(null);
        assertFalse(sut.iterator().hasNext());
        sut.add(new CacheEntryReference("test"));
        assertTrue(sut.iterator().hasNext());
    }

    @Test
    void testClear() {
        var sut = new OrderedHashContainer();
        sut.add(new CacheEntryReference("test"));
        assertTrue(sut.iterator().hasNext());

        sut.clear();
        assertFalse(sut.iterator().hasNext());
    }

    @Test
    void testContains() {
        var hash = "XeOn";

        var sut = new OrderedHashContainer();
        assertFalse(sut.contains(null));
        assertFalse(sut.contains(hash));

        sut.add(new CacheEntryReference(hash));
        assertTrue(sut.contains(hash));
    }

    @Test
    void testGet() {
        var hash = "XeOn";

        var sut = new OrderedHashContainer();
        assertNull(sut.get(null));
        assertNull(sut.get(hash));
        var lastAccessTime = 11111111111L;
        var cacheEntryReference = new CacheEntryReference(new CacheHashFileMapper(tempDir, 0), hash, lastAccessTime);

        sut.add(cacheEntryReference);

        var getResult = sut.get(hash);
        assertNotNull(getResult);
        assertEquals(lastAccessTime, getResult.getLastAccessTimeMillis());
    }

    @Test
    void testGetFirstEntry() {
        var hash1 = "XeOn";
        var hash2 = "TurBo";

        var sut = new OrderedHashContainer();
        sut.add(new CacheEntryReference(hash1));
        sut.add(new CacheEntryReference(hash2));

        assertEquals(hash1, sut.getFirstEntry().getHash());
    }

    @Test
    void testIterator() {
        var hash1 = "XeOn";
        var hash2 = "TurBo";

        var sut = new OrderedHashContainer();
        sut.add(new CacheEntryReference(hash1));
        sut.add(new CacheEntryReference(hash2));

        var iterator = sut.iterator();
        assertEquals(hash1, iterator.next().getHash());
        assertEquals(hash2, iterator.next().getHash());
        assertFalse(iterator.hasNext());
    }

    @Test
    void testRemove() {
        var hash1 = "XeOn";
        var hash2 = "TurBo";

        var sut = new OrderedHashContainer();

        sut.add(new CacheEntryReference(hash1));
        sut.add(new CacheEntryReference(hash2));

        var removedCacheEntryRef = sut.remove(hash1);
        assertEquals(hash1, removedCacheEntryRef.getHash());
        assertFalse(sut.contains(hash1));
        assertTrue(sut.contains(hash2));

        assertNull(sut.remove("--"));
        assertNull(sut.remove(null));
    }
}
