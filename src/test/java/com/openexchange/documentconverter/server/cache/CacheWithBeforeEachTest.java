/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import ch.qos.logback.classic.Level;
import com.openexchange.documentconverter.server.concurrency.IdLocker;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CacheWithBeforeEachTest extends CacheTestBase {

    @BeforeEach
    void beforeTest() {
        setupCache();
        startCache();
    }

    @Test
    void testIsValidHashTrue() {
        assertTrue(Cache.isValidHash("test"));
    }

    @Test
    void testIsValidHashFalse() {
        assertFalse(Cache.isValidHash(null));
        assertFalse(Cache.isValidHash(""));
    }

    @Test
    void testAddEntryWithInvalidEntry() {
        var entry = createValidCacheEntry();
        entry.clear();
        var added = sut.addEntry(entry, false, null);
        assertFalse(added);

        added = sut.addEntry(null, false, null);
        assertFalse(added);
    }

    @Test
    void testAddEntry() {
        try (MockedStatic<IdLocker> idLockerMockedStaticStaticMock = Mockito.mockStatic(IdLocker.class)) {

            var added = sut.addEntry(createValidCacheEntry(sut.getHashFileMapper()), false, null);
            assertTrue(added);

            // One for createValidCacheEntry and one for sut.addEntry
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.lock(any()), times(2));
            // One for createValidCacheEntry and one for sut.addEntry
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.unlock(any()), times(2));

            assertTrue(sut.containsHash(TEST_CACHE_ENTRY_HASH));

            var count = sut.getLocalEntryCount();
            assertEquals(1, count);
        }
    }

    @Test
    void testGetEntryWithNotExistingHash() {
        try (MockedStatic<IdLocker> idLockerMockedStaticStaticMock = Mockito.mockStatic(IdLocker.class)) {
            var entry = sut.getEntry(TEST_CACHE_ENTRY_HASH, null);
            assertNull(entry);
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.lock(any()), times(1));
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.unlock(any()), times(1));
        }
    }

    @Test
    void testGetEntryWithInvalidHash() {
        try (MockedStatic<IdLocker> idLockerMockedStaticStaticMock = Mockito.mockStatic(IdLocker.class)) {
            var entry = sut.getEntry("", null);
            assertNull(entry);
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.lock(any()), times(0));
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.unlock(any()), times(0));
        }
    }

    @Test
    void testGetEntryWithExistingHash() {
        sut.addEntry(createValidCacheEntry(sut.getHashFileMapper()), false, null);

        var entry = sut.getEntry(TEST_CACHE_ENTRY_HASH, null);
        assertNotNull(entry);
    }

    @Test
    void testRemoveEntry() {
        try (MockedStatic<IdLocker> idLockerMockedStaticStaticMock = Mockito.mockStatic(IdLocker.class)) {
            sut.addEntry(createValidCacheEntry(sut.getHashFileMapper()), false, null);
            assertTrue(sut.containsHash(TEST_CACHE_ENTRY_HASH));
            assertEquals(1, sut.getLocalEntryCount());

            sut.removeEntry(TEST_CACHE_ENTRY_HASH);

            assertFalse(sut.containsHash(TEST_CACHE_ENTRY_HASH));
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.lock(any()), times(3));
            idLockerMockedStaticStaticMock.verify(() -> IdLocker.unlock(any()), times(3));

            assertEquals(0, sut.getLocalEntryCount());

        }
    }

    @Test
    void testGetCachedResult() {
        sut.addEntry(createValidCacheEntry(sut.getHashFileMapper()), false, null);
        var properties = new HashMap<String, Object>();
        var inputStream = sut.getCachedResult(TEST_CACHE_ENTRY_HASH, null, properties, false);
        assertNotNull(inputStream);
    }

    @Test
    void testGetCachedResultWithUpdateCacheHitStatistic() {

        sut.addEntry(createValidCacheEntry(sut.getHashFileMapper()), false, null);
        var properties = new HashMap<String, Object>();
        assertEquals(0, statistics.getJobsProcessed());
        var inputStream = sut.getCachedResult(TEST_CACHE_ENTRY_HASH, null, properties, true);
        assertNotNull(inputStream);
        assertEquals(1, statistics.getJobsProcessed());
    }

    @Test
    void testGetCachedResultWithInvalidHash() {
        var inputStream = sut.getCachedResult("", null, new HashMap<>(), true);
        assertNull(inputStream);
    }

    @Test
    void testImplCreateCacheEntryWithInvalidHash() {
        var logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logbackLogger.setLevel(Level.TRACE);
        logbackLogger.setAdditive(false);
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        var cacheEntry = sut.createCacheEntry("");
        assertNull(cacheEntry);
        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "could not find cache entry in local cache: "));

    }

    @Test
    void testImplCreateCacheEntryWithValidHash() {
        var cacheEntry = createValidCacheEntry();
        var cacheEntryByHash = sut.createCacheEntry(cacheEntry.getHash());
        assertNotNull(cacheEntry);
        assertEquals(cacheEntry.getHash(), cacheEntryByHash.getHash());
    }

    @Test
    void testCacheAndGetUID() {
        var cacheUid = sut.getUID();
        assertEquals(UUID.fromString(cacheUid).toString(), cacheUid);
    }

    @Test
    void testGetLocalEntryCount() {
        var localEntryCount = sut.getLocalEntryCount();

        assertEquals(0, localEntryCount);
    }

    @Test
    void testGetLocalOldestEntrySeconds() {

        var localOldestEntrySeconds = sut.getLocalOldestEntrySeconds();

        assertEquals(0, localOldestEntrySeconds);
    }

    @Test
    void testCleanup() throws InterruptedException {

        var success = sut.addEntry(createValidCacheEntry(), false, null);

        assertTrue(success);
        sut.cleanup();

        var localEntryCount = sut.getLocalEntryCount();
        //  Test equals 1 because maxLocalCacheCount was -1
        assertEquals(0, localEntryCount);
    }

    @Test
    void testGetHashFileMapper() {

        var hashFileMapper = sut.getHashFileMapper();

        assertNotNull(hashFileMapper);
        assertNotNull(hashFileMapper.getRootDir());
        assertEquals(0, hashFileMapper.getDepth());
        try {
            hashFileMapper.mapHashToFile(null);
            fail("NullPointerException expected!");
        } catch (NullPointerException e) {
            // expected
        }
    }


    @Test
    void testStartNewEntry() {
        var cacheEntry = createValidCacheEntry();
        var success = sut.addEntry(cacheEntry, false, null);
        assertTrue(success);

        var newEntry = sut.startNewEntry(cacheEntry.getHash());
        assertNotNull(newEntry);

        var nullEntry = sut.startNewEntry(null);
        assertNull(nullEntry);
    }


    @Test
    void testStartNewEntryDuplicate() {

        var entryHash = UUID.randomUUID().toString();

        var cacheEntry = sut.startNewEntry(entryHash);
        assertNotNull(cacheEntry);
        var duplicateCacheEntry = sut.startNewEntry(entryHash);
        assertNull(duplicateCacheEntry);
    }

    @Test
    void testEndNewEntry() {
        var cacheEntry = createValidCacheEntry();
        var success = sut.addEntry(cacheEntry, false, null);
        assertTrue(success);

        var newEntry = sut.startNewEntry(cacheEntry.getHash());
        assertNotNull(newEntry);

        sut.endNewEntry(newEntry);

        var getEntry = sut.getEntry(cacheEntry.getHash(), null);
        assertNotNull(getEntry);
    }

    @Test
    void testShutdown() {

        var success = sut.addEntry(createValidCacheEntry(), false, null);
        assertTrue(success);

        sut.shutdown();
        var count = sut.getLocalEntryCount();
        assertEquals(0, count);
    }


    @Test
    void testEnabeAndIsEnabled() {
        sut = new Cache();
        sut.enable();
        assertTrue(sut.isEnabled());
    }

    @Test
    void testIsValidHash() {
        var entryHash = UUID.randomUUID().toString();
        assertTrue(Cache.isValidHash(entryHash));
    }

    @Test
    void testCalculateDirectorySizeWithNullDir() {
        assertEquals(0, Cache.calculateDirectorySize(null));
    }

    @Test
    void testCalculateDirectorySize() {
        assertEquals(Cache.BLOCK_SIZE_DEFAULT, Cache.calculateDirectorySize(tempDir));
    }

    @Test
    void testCalculateDirectorySizeWithChildDir() throws IOException {
        FileUtils.forceMkdir(new File(tempDir, "test"));
        assertNotEquals(Cache.BLOCK_SIZE_DEFAULT, Cache.calculateDirectorySize(tempDir));
    }

    @Test
    void testClearEmptyEntryCount() {
        var removedCount = sut.clear(-1);
        assertEquals(0, removedCount);
    }

    @Test
    void testClearWithOneEntry() {
        var cacheEntry = createValidCacheEntry();
        sut.addEntry(cacheEntry, false, null);
        var removedCount = sut.clear(-1);
        assertEquals(1, removedCount);
    }

    @Test
    void testClearWithMultipleEntries() {
        var entryCount = 5;
        for (var i = 0; i < entryCount; i++)  {
            var cacheEntry = createValidCacheEntry(TEST_CACHE_ENTRY_HASH + i, new HashMap<>());
            sut.addEntry(cacheEntry, false, null);
        }
        var removedCount = sut.clear(-1);
        assertEquals(5, removedCount);
    }

    @Test
    void testClearWithMultipleEntriesAndClearCount() {
        var entryCount = 5;
        var clearCount = 3;
        for (var i = 0; i < entryCount; i++)  {
            var cacheEntry = createValidCacheEntry(TEST_CACHE_ENTRY_HASH + i, new HashMap<>());
            sut.addEntry(cacheEntry, false, null);
        }
        var removedCount = sut.clear(clearCount);
        assertEquals(clearCount, removedCount);
    }

    @Test
    void testGetFileFreeVolumeSizeWithNotExistingDir() {
        var file = new File("/notExistingDir" + UUID.randomUUID());
        while (file.exists()) {
            file = new File("/notExistingDir" + UUID.randomUUID());
        }
        assertEquals(0, sut.getFileFreeVolumeSize(file));
    }

    @Test
    void testGetFileFreeVolumeSizeWithExistingDir() {
        assertNotEquals(0, sut.getFileFreeVolumeSize(tempDir));
    }

    @Test
    void testGetUID() {
        var uID = sut.getUID();
        assertEquals(UUID.fromString(uID).toString(), uID);
    }

    @Test
    void testGetLocalOldestEntrySecondsWithoutEntries() {
        var seconds = sut.getLocalOldestEntrySeconds();
        assertEquals(0, seconds);
    }

    @Test
    void testGetLocalOldestEntrySecondsWithEntries() throws InterruptedException {
        var entry = createValidCacheEntry();
        sut.addEntry(entry, false, "");
        Thread.sleep(100);
        var seconds = sut.getLocalOldestEntrySeconds();
        assertEquals(0, seconds);
    }

    @Test
    void testCalculateFileBlockSizeWithNotExistingDir() {
        var file = new File("/notExistingDir" + UUID.randomUUID());
        while (file.exists()) {
            file = new File("/notExistingDir" + UUID.randomUUID());
        }
        var blockSize = sut.calculateFileBlockSize(file.getAbsolutePath());
        assertEquals(Cache.BLOCK_SIZE_DEFAULT, blockSize);
    }

    @Test
    void testCalculateFileBlockSizeWithExistingDir() {
        var blockSize = sut.calculateFileBlockSize(tempDir.getAbsolutePath());
        assertNotEquals(0, blockSize);

    }
}
