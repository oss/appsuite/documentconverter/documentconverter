/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import lombok.NonNull;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CacheTestClassesBase {

    @TempDir
    File tempDir;

    static final String TEST_CACHE_ENTRY_PROPERTIES_KEY = "1";
    static final String TEST_CACHE_ENTRY_PROPERTIES_VALUE = "value";
    static final String TEST_CACHE_ENTRY_CONTENT = "Hello";
    static final String TEST_CACHE_ENTRY_HASH = "hashx";

    CacheHashFileMapper getCacheHashFileMapper() {
        return new CacheHashFileMapper(tempDir, 0);
    }

    CacheEntry createValidCacheEntry() {
        return createValidCacheEntry(TEST_CACHE_ENTRY_HASH, new HashMap<>());
    }

    CacheEntry createValidCacheEntry(CacheHashFileMapper cacheHashFileMapper) {
        return createValidCacheEntry(TEST_CACHE_ENTRY_HASH, new HashMap<>(), cacheHashFileMapper);
    }

    CacheEntry createValidCacheEntry(@NonNull String hash, @NonNull Map<String, Object> properties, CacheHashFileMapper cacheHashFileMapper) {
        properties.put(TEST_CACHE_ENTRY_PROPERTIES_KEY, TEST_CACHE_ENTRY_PROPERTIES_VALUE);
        // the CacheEntry creates the folder the properties and result file which is needed for a valid CacheHashFileMapper
        return CacheEntry.createAndMakePersistent(cacheHashFileMapper, hash, properties, TEST_CACHE_ENTRY_CONTENT.getBytes());
    }

    CacheEntry createValidCacheEntry(@NonNull String hash, @NonNull Map<String, Object> properties) {
        var cacheHashFileMapper = getCacheHashFileMapper();
        return createValidCacheEntry(hash,properties,cacheHashFileMapper);
    }
}
