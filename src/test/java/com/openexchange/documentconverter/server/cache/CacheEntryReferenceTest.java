/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class CacheEntryReferenceTest extends CacheTestClassesBase {

    @Test
    void testSimpleConstructor() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertEquals(0, sut.getLastAccessTimeMillis());
        assertEquals(0, sut.getPersistentSize());
        assertEquals(TEST_CACHE_ENTRY_HASH, sut.getHash());
    }

    @Test
    void testExtendedConstructor() {
        var cacheHashFileMapper = new CacheHashFileMapper(tempDir, 0);
        createValidCacheEntry(cacheHashFileMapper);
        var lastAccessTime = 111;
        var sut = new CacheEntryReference(cacheHashFileMapper, TEST_CACHE_ENTRY_HASH, lastAccessTime);
        assertEquals(lastAccessTime, sut.getLastAccessTimeMillis());
        assertNotEquals(0, sut.getPersistentSize());
        assertEquals(TEST_CACHE_ENTRY_HASH, sut.getHash());
    }

    @Test
    void testHashCode() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertEquals(Arrays.hashCode(TEST_CACHE_ENTRY_HASH.getBytes(StandardCharsets.US_ASCII)), sut.hashCode());
    }

    @Test
    void testEquals() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        var sut2 = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertTrue(sut.equals(sut));
        assertFalse(sut.equals(null));
        assertTrue(sut.equals(sut2));
        assertFalse(sut.equals(new String()));
    }

    @Test
    void testCompareTo() {
        var sut = new CacheEntryReference(new CacheHashFileMapper(tempDir, 0), TEST_CACHE_ENTRY_HASH, 1000);
        var sut2 = new CacheEntryReference(new CacheHashFileMapper(tempDir, 0), TEST_CACHE_ENTRY_HASH, 10);
        assertEquals(-1, sut2.compareTo(sut));
        assertEquals(0, sut.compareTo(sut));
        assertEquals(1, sut.compareTo(sut2));
    }

    @Test
    void testGetHash() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertEquals(new String(TEST_CACHE_ENTRY_HASH.getBytes(StandardCharsets.US_ASCII)), sut.getHash());
    }

    @Test
    void testIsValidTimestamp() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertTrue(sut.isValidTimestamp(0, 10));
        assertTrue(sut.isValidTimestamp(100, 200));
        assertFalse(sut.isValidTimestamp(200, 100));
        sut = new CacheEntryReference(new CacheHashFileMapper(tempDir, 0), TEST_CACHE_ENTRY_HASH, 1000);
        assertTrue(sut.isValidTimestamp(0, 10));
        assertTrue(sut.isValidTimestamp(100, 200));
        assertTrue(sut.isValidTimestamp(200, 100));
        assertFalse(sut.isValidTimestamp(1200, 100));
    }

    @Test
    void testGetLastAccessTimeMillis() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertEquals(0, sut.getLastAccessTimeMillis());
        sut = new CacheEntryReference(new CacheHashFileMapper(tempDir, 0), TEST_CACHE_ENTRY_HASH, 1000);
        assertEquals(1000, sut.getLastAccessTimeMillis());
    }

    @Test
    void testGetPersistentSize() {
        var sut = new CacheEntryReference(TEST_CACHE_ENTRY_HASH);
        assertEquals(0, sut.getPersistentSize());

        var cacheHashFileMapper = new CacheHashFileMapper(tempDir, 0);
        createValidCacheEntry(cacheHashFileMapper);
        sut = new CacheEntryReference(cacheHashFileMapper, TEST_CACHE_ENTRY_HASH, 0);
        assertNotEquals(0, sut.getPersistentSize());
    }

    @Test
    void testClear() {
        var cacheHashFileMapper = new CacheHashFileMapper(tempDir, 0);
        createValidCacheEntry(cacheHashFileMapper);
        var sut = new CacheEntryReference(cacheHashFileMapper, TEST_CACHE_ENTRY_HASH, 0);
        var file = sut.getPersistentDir(cacheHashFileMapper);
        assertTrue(file.isDirectory());

        sut.clear(cacheHashFileMapper);
        assertFalse(file.isDirectory());
    }

    @Test
    void testTouch() throws InterruptedException {
        var cacheHashFileMapper = new CacheHashFileMapper(tempDir, 0);
        createValidCacheEntry(cacheHashFileMapper);
        var sut = new CacheEntryReference(cacheHashFileMapper, TEST_CACHE_ENTRY_HASH, 2);
        assertEquals(2, sut.getLastAccessTimeMillis());

        sut.touch(cacheHashFileMapper);
        assertTrue(sut.getLastAccessTimeMillis() <= System.currentTimeMillis());
    }
}
