/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.util.TestUtil;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class CacheEntryTest extends CacheTestClassesBase {

    @Test
    void testGetCacheEntryDir() {
        var hash = "test-x";
        var file = CacheEntry.getCacheEntryDir(getCacheHashFileMapper(), hash);
        assertTrue(file.getPath().endsWith(hash));
    }

    @Test
    void testStaticIsValidWithInvalidHash() {
        var valid = CacheEntry.isValid(getCacheHashFileMapper(), "");
        assertFalse(valid);
    }

    @Test
    void testStaticIsValidWithCacheHashFileMapper() {
        var valid = CacheEntry.isValid(mock(CacheHashFileMapper.class), "");
        assertFalse(valid);
    }

    @Test
    void testStaticIsValid() {
        var cacheHashFileMapper = getCacheHashFileMapper();
        // the CacheEntry creates the folder the properties and result file which is needed for a valid CacheHashFileMapper
        createValidCacheEntry();
        var valid = CacheEntry.isValid(cacheHashFileMapper, TEST_CACHE_ENTRY_HASH);
        assertTrue(valid);
    }

    @Test
    void testCreateAndMakePersistentInvalidHash() {
        var sut = CacheEntry.createAndMakePersistent(mock(CacheHashFileMapper.class), "", new HashMap<>(), new byte[0]);
        assertNull(sut);
    }

    @Test
    void testCreateAndMakePersistent() {
        var hash = "test-hash";
        var properties = new HashMap<String, Object>();
        properties.put("1", "value");
        var sut = CacheEntry.createAndMakePersistent(getCacheHashFileMapper(), hash, properties, new byte[0]);
        assertNotNull(sut);
        assertEquals(hash, sut.getHash());
        assertEquals(properties.get("1"), sut.getResultProperties().get("1"));
        assertEquals(hash, sut.getResultProperties().get(Properties.PROP_CACHE_PERSIST_ENTRY_NAME));
        assertEquals(new File(tempDir, hash), sut.getPersistentDirectory());
        assertEquals(new File(new File(tempDir, hash), Properties.CACHE_RESULT_FILENAME), sut.getResultFile());
    }

    @Test
    void testCreateFromWithInvalidHash() {
        var sut = CacheEntry.createFrom(mock(CacheHashFileMapper.class), "");
        assertNull(sut);
    }

    @Test
    void testCreateFromWithInvalidCacheHashFileMapper() {
        var hash = "hashy";

        var sut = CacheEntry.createFrom(getCacheHashFileMapper(), hash);
        assertNull(sut);
    }

    @Test
    void testCreateFrom() {
        var hash = "hashy";
        var properties = new HashMap<String, Object>();
        createValidCacheEntry(hash, properties);
        var sut = CacheEntry.createFrom(getCacheHashFileMapper(), hash);
        assertNotNull(sut);
        assertEquals(hash, sut.getHash());
        assertEquals(properties.get("1"), sut.getResultProperties().get("1"));
        assertEquals(hash, sut.getResultProperties().get(Properties.PROP_CACHE_PERSIST_ENTRY_NAME));
        assertEquals(new File(tempDir, hash), sut.getPersistentDirectory());
        assertEquals(new File(new File(tempDir, hash), Properties.CACHE_RESULT_FILENAME), sut.getResultFile());
    }

    @Test
    void testGetJobResult() {
        var hash = "hashy";
        var properties = new HashMap<String, Object>();

        var sut = createValidCacheEntry(hash, properties);
        var buffer = (byte[]) sut.getJobResult().get(Properties.PROP_RESULT_BUFFER);
        assertEquals(TEST_CACHE_ENTRY_CONTENT, new String(buffer));
    }

    @Test
    void testGetResultContent() {
        var hash = "hashy";
        var properties = new HashMap<String, Object>();

        var sut = createValidCacheEntry(hash, properties);
        var buffer = sut.getResultContent();
        assertEquals(TEST_CACHE_ENTRY_CONTENT, new String(buffer));
    }

    @Test
    void testGetResultContentInvalid() {
        var hash = "hashy";
        var properties = new HashMap<String, Object>();

        var sut = createValidCacheEntry(hash, properties);
        sut.clear();
        var buffer = sut.getResultContent();
        assertNull(buffer);
    }

    @Test
    void testGetResultProperties() {
        var sut = createValidCacheEntry();
        assertEquals(TEST_CACHE_ENTRY_PROPERTIES_VALUE, sut.getResultProperties().get(TEST_CACHE_ENTRY_PROPERTIES_KEY));
    }

    @Test
    void testGetHash() {
        var sut = createValidCacheEntry();
        assertEquals(TEST_CACHE_ENTRY_HASH, sut.getHash());
    }

    @Test
    void testIsValid() {
        var sut = createValidCacheEntry();
        assertTrue(sut.isValid());
    }

    @Test
    void testIsValidWithInvalidHash() {
        var sut = createValidCacheEntry();
        sut.clear();
        assertFalse(sut.isValid());
    }

    @Test
    void testClear() {
        var sut = createValidCacheEntry();
        assertTrue(sut.isValid());
        assertFalse(sut.getResultProperties().isEmpty());
        assertNotNull(ReflectionTestUtils.getField(sut, "entryDir"));
        sut.clear();
        assertFalse(sut.isValid());
        assertTrue(sut.getResultProperties().isEmpty());
        assertNull(ReflectionTestUtils.getField(sut, "entryDir"));
    }

    @Test
    void testEqualsSameObject() {
        var sut = createValidCacheEntry();
        assertEquals(sut, sut);
    }

    @Test
    void testEqualsNull() {
        var sut = createValidCacheEntry();
        assertNotEquals(null, sut);
    }

    @Test
    void testEqualsOtherClass() {
        var sut = createValidCacheEntry();
        assertNotEquals(sut, new String());
    }

    @Test
    void testEqualsEntryDirIsNull() {
        var sut = createValidCacheEntry();
        var sut2 = createValidCacheEntry();
        sut.clear();
        assertNotEquals(sut, sut2);

        sut = createValidCacheEntry();
        sut2 = createValidCacheEntry();
        sut2.clear();
        assertNotEquals(sut, sut2);
    }

    @Test
    void testEqualsHashIsNull() {
        var sut = createValidCacheEntry();
        var sut2 = createValidCacheEntry();
        ReflectionTestUtils.setField(sut, "hash", null);
        assertNotEquals(sut, sut2);

        sut = createValidCacheEntry();
        sut2 = createValidCacheEntry();
        ReflectionTestUtils.setField(sut2, "hash", null);
        assertNotEquals(sut, sut2);
    }
    @Test
    void testEqualsHashSameHash() {
        var sut = createValidCacheEntry();
        var sut2 = createValidCacheEntry();
        assertEquals(sut, sut2);
    }

    @Test
    void testEqualsHashDifferentHash() {
        var sut = createValidCacheEntry();
        var sut2 = createValidCacheEntry("notSameHash", new HashMap<>());
        assertNotEquals(sut, sut2);
    }

    @Disabled
    @Test
    void getGetHashCode() {
        var sut = createValidCacheEntry();
        var hashCode = sut.hashCode();
        assertNotEquals(961, hashCode);
    }

    @Test
    void getGetHashCodeAfterClear() {
        var sut = createValidCacheEntry();
        sut.clear();
        assertEquals(961, sut.hashCode());
    }

    @Test
    void testGetAccessTime() {
        var sut = createValidCacheEntry();
        assertNotEquals(System.currentTimeMillis(), sut.getAccessTime().getTime());
    }

    @Test
    void testGetResultContentWithException() {
        var logListAppenderServerManager = TestUtil.addListAppenderToLogger(ServerManager.class);

        var sut = createValidCacheEntry();
        ReflectionTestUtils.setField(sut, "entryDir", null);
        sut.getResultContent();

        assertTrue(TestUtil.listAppenderContainsAllMsg(logListAppenderServerManager, "DC cache entry result content could not be read:"));
    }
}
