/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.openexchange.documentconverter.server.config.ServerConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

public class CacheDescriptorTest {

    ServerConfig serverConfig;

    CacheDescriptor sut;

    @Test
    void testInitConfig() throws IOException, NoSuchAlgorithmException {
        var SSL_CONTEXT = SSLContext.getDefault();

        serverConfig = spy(new ServerConfig(SSL_CONTEXT));
        var cacheDir = "/cacheDir";
        ReflectionTestUtils.setField(serverConfig, "cacheDir", cacheDir);
        var cacheDirDepth = 100;
        ReflectionTestUtils.setField(serverConfig, "cacheDirDepth", cacheDirDepth);
        var minFreeVolumeSizeMB = 10;
        ReflectionTestUtils.setField(serverConfig, "minFreeVolumeSizeMB", minFreeVolumeSizeMB);
        var maxCacheSizeMB = 99;
        ReflectionTestUtils.setField(serverConfig, "maxCacheSizeMB", maxCacheSizeMB);
        var maxCacheEntries = 11;
        ReflectionTestUtils.setField(serverConfig, "maxCacheEntries", maxCacheEntries);
        var cacheCleanupThresholdUpperPercentage = 9;
        ReflectionTestUtils.setField(serverConfig, "cacheCleanupThresholdUpperPercentage", cacheCleanupThresholdUpperPercentage);
        var cacheCleanupThresholdLowerPercentage = 2;
        ReflectionTestUtils.setField(serverConfig, "cacheCleanupThresholdLowerPercentage", cacheCleanupThresholdLowerPercentage);
        var cacheEntryTimeoutSeconds = 21;
        ReflectionTestUtils.setField(serverConfig, "cacheEntryTimeoutSeconds", cacheEntryTimeoutSeconds);
        var cacheCleanupPeriodSeconds = 22;
        ReflectionTestUtils.setField(serverConfig, "cacheCleanupPeriodSeconds", cacheCleanupPeriodSeconds);
        var upRecoveryPeriodSeconds = 23;
        ReflectionTestUtils.setField(serverConfig, "upRecoveryPeriodSeconds", upRecoveryPeriodSeconds);
        var cacheServiceUrl = "example.com";
        ReflectionTestUtils.setField(serverConfig, "cacheServiceUrl", cacheServiceUrl);

        // these paths are needed for successful initConfig
        ReflectionTestUtils.setField(serverConfig, "scratchDirPath", "/tmp");
        ReflectionTestUtils.setField(serverConfig, "reInstallDir", "/tmp");
        serverConfig.initConfig();

        sut = new CacheDescriptor(serverConfig);
        ReflectionTestUtils.setField(sut, "serverConfig", serverConfig);

        sut.init();

        assertEquals(cacheDir, sut.getCacheDirectory());
        assertEquals(cacheDirDepth, sut.getCacheDirectoryDepth());
        assertEquals(minFreeVolumeSizeMB * 1024 * 1024, sut.getMinimumFreeVolumeSize());
        assertEquals(maxCacheSizeMB * 1024 * 1024, sut.getMaximumPersistSize());
        assertEquals(maxCacheEntries, sut.getMaximumEntryCount());
        assertEquals(cacheCleanupThresholdUpperPercentage, sut.getCleanupThresholdUpperPercentage());
        assertEquals(cacheCleanupThresholdLowerPercentage, sut.getCleanupThresholdLowerPercentage());
        assertEquals(cacheEntryTimeoutSeconds * 1000, sut.getTimeoutMillis());
        assertEquals(cacheCleanupPeriodSeconds * 1000, sut.getCleanupPeriodMillis());
        assertEquals(upRecoveryPeriodSeconds * 1000, sut.getRecoverPeriodMillis());
        assertEquals(cacheServiceUrl, sut.getCacheServiceUrl());
        assertEquals(SSL_CONTEXT, sut.getSslContext());
    }


    @Test
    void testInitConfigDifferentValues() throws IOException, NoSuchAlgorithmException {
        serverConfig = spy(new ServerConfig());
        var maxCacheSizeMB = -1;
        ReflectionTestUtils.setField(serverConfig, "maxCacheSizeMB", maxCacheSizeMB);
        String cacheServiceUrl = null;
        ReflectionTestUtils.setField(serverConfig, "cacheServiceUrl", cacheServiceUrl);

        // these paths are needed for successful initConfig
        ReflectionTestUtils.setField(serverConfig, "scratchDirPath", "/tmp");
        ReflectionTestUtils.setField(serverConfig, "reInstallDir", "/tmp");
        serverConfig.initConfig();

        sut = new CacheDescriptor(serverConfig);
        ReflectionTestUtils.setField(sut, "serverConfig", serverConfig);

        sut.init();

        assertEquals(maxCacheSizeMB, sut.getMaximumPersistSize());
        assertNull(sut.getCacheServiceUrl());
        assertNull(sut.getSslContext());
    }
}
