/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class CacheHashFileMapperTest {

    @TempDir
    File tempDir;

    @Test
    void testConstructor() {
        var sut = new CacheHashFileMapper(tempDir, 11);
        assertNotNull(sut);
        assertEquals(4, sut.getDepth());
        assertEquals(tempDir, sut.getRootDir());

        sut = new CacheHashFileMapper(tempDir, 2);
        assertNotNull(sut);
        assertEquals(2, sut.getDepth());
        assertEquals(tempDir, sut.getRootDir());

        sut = new CacheHashFileMapper(tempDir, -1);
        assertNotNull(sut);
        assertEquals(0, sut.getDepth());
        assertEquals(tempDir, sut.getRootDir());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 1, 2, 3, 4, 5})
    void testMapHashToFile(int depth) {

        var sut = new CacheHashFileMapper(tempDir, depth);
        var currentDepth = sut.getDepth();
        var separatorCountDir = StringUtils.countMatches(tempDir.getPath(), File.separatorChar);

        var hash = "XeNon";
        var file = sut.mapHashToFile(hash);
        var separatorCountFile = StringUtils.countMatches(file.getPath(), File.separatorChar);

        assertTrue(file.getPath().endsWith(File.separatorChar + hash));

        var folderDepthCount = separatorCountFile - separatorCountDir - 1; // -1 is for the separator before the hash
        assertEquals(currentDepth, folderDepthCount);
    }
}
