/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.metrics.Statistics;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.mock;

public class CacheTestBase extends CacheTestClassesBase {

    final static String CACHESERVICE_URL = "http://localhost:8080";

    @SpyBean
    Statistics statistics;

    CacheDescriptor cacheDescriptor;

    @Autowired
    Cache sut;

    void setupCache() {
        sut = new Cache();
        sut.enable();
        statistics = new Statistics();
        ReflectionTestUtils.setField(sut, "statistics", statistics);
        ReflectionTestUtils.setField(sut, "cacheHashFileMapper", new CacheHashFileMapper(tempDir, 0));
        ReflectionTestUtils.setField(sut, "metricsRegistry", new MetricsRegistry());

        cacheDescriptor = mock(CacheDescriptor.class);
        cacheDescriptor.cacheServiceUrl = CACHESERVICE_URL;
        cacheDescriptor.cacheDirectory = tempDir.getPath();
        ReflectionTestUtils.setField(sut, "cacheDescriptor", cacheDescriptor);
    }

    void startCache() {
        var cacheWorker = new CacheWorkerMock(sut, Thread.currentThread());

        try (MockedStatic<CacheWorker> cacheWorkerMockedStatic = Mockito.mockStatic(CacheWorker.class)) {
            cacheWorkerMockedStatic.when(() -> CacheWorker.createCacheWorker(sut)).thenReturn(cacheWorker);
            sut.init();
        }

        cacheWorker.waitForReady();
    }
}
