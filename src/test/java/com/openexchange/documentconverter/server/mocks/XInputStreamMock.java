/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.mocks;

import com.sun.star.io.BufferSizeExceededException;
import com.sun.star.io.IOException;
import com.sun.star.io.NotConnectedException;

import java.io.ByteArrayInputStream;

public class XInputStreamMock implements com.sun.star.io.XInputStream {

    private ByteArrayInputStream inputStream;

    public XInputStreamMock(ByteArrayInputStream inputStream) {
        this.inputStream = inputStream;
    }

    public XInputStreamMock(byte[] data) {
        this.inputStream = new ByteArrayInputStream(data);
    }

    @Override
    public int readBytes(byte[][] buffer, int bufferSize) throws NotConnectedException, BufferSizeExceededException, IOException {
        if (inputStream == null || buffer.length == 0) {
            throw new NotConnectedException("Input stream is null");
        }

        try {
            buffer[0] = new byte[bufferSize];
            return inputStream.read(buffer[0], 0, bufferSize);
        } catch (IndexOutOfBoundsException e) {
            throw new BufferSizeExceededException(e.getMessage());
        }
    }

    @Override
    public int readSomeBytes(byte[][] buffer, int i) throws NotConnectedException, BufferSizeExceededException, IOException {
        return readBytes(buffer, i);
    }

    @Override
    public void skipBytes(int i) throws NotConnectedException, BufferSizeExceededException, IOException {
        if (inputStream == null) {
            throw new NotConnectedException("Input stream is null");
        }

        try {
            inputStream.skipNBytes(i);
        } catch (java.io.IOException e) {
            throw new IOException(e);
        }
    }

    @Override
    public int available() throws NotConnectedException, IOException {
        if (inputStream == null) {
            throw new NotConnectedException("Input stream is null");
        }

        return inputStream.available();
    }

    @Override
    public void closeInput() throws NotConnectedException, IOException {
        if (inputStream == null) {
            throw new NotConnectedException("Input stream is null");
        }

        try {
            inputStream.close();
        } catch (java.io.IOException e) {
            throw new IOException(e.getMessage());
        }
    }
}
