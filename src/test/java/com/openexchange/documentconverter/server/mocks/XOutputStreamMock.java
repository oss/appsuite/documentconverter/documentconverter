/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.mocks;

import com.sun.star.io.BufferSizeExceededException;
import com.sun.star.io.IOException;
import com.sun.star.io.NotConnectedException;

import java.io.ByteArrayOutputStream;

public class XOutputStreamMock implements com.sun.star.io.XOutputStream {

    private ByteArrayOutputStream outputStream;

    public XOutputStreamMock() {
        this.outputStream = new ByteArrayOutputStream();
    }

    public XOutputStreamMock(ByteArrayOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public void writeBytes(byte[] bytes) throws NotConnectedException, BufferSizeExceededException, IOException {
        if (outputStream == null) {
            throw new NotConnectedException("Output stream is null");
        }

        outputStream.write(bytes, 0, bytes.length);
    }

    @Override
    public void flush() throws NotConnectedException, BufferSizeExceededException, IOException {
        if (outputStream == null) {
            throw new NotConnectedException("Output stream is null");
        }

        try {
            outputStream.flush();
        } catch (java.io.IOException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void closeOutput() throws NotConnectedException, BufferSizeExceededException, IOException {
        if (outputStream == null) {
            throw new NotConnectedException("Output stream is null");
        }

        try {
            outputStream.close();
        } catch (java.io.IOException e) {
            throw new IOException(e);
        }
    }
}
