/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.openexchange.documentconverter.server.logging.LogData;
import lombok.NonNull;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

public class TestUtil {

    public static final String BASE64_DATA_URI_HELLO_WORLD_TXT = "data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==";
    public static final String BASE64_DATA_URI_HELLO_WORLD_TXT_CONTENT = "Hello, World!";

    private TestUtil() {
        // nothing to do - static helper methods
    }


    public static boolean containsLogDataKey(@NonNull LogData[] logData, String key) {
        for (var i=0; i < logData.length; i++) {
            if (logData[i].getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public static String getLogDataValue(@NonNull LogData[] logData, String key) {
        for (var i=0; i < logData.length; i++) {
            if (logData[i].getKey().equals(key)) {
                return logData[i].getValue();
            }
        }
        throw new NullPointerException();
    }

    public static ListAppender<ILoggingEvent> addListAppenderToLogger(@NonNull Class<?> loggerClass) {
        var logger = (Logger) LoggerFactory.getLogger(loggerClass);

        var listAppender = new ListAppender<ILoggingEvent>();
        listAppender.start();
        logger.addAppender(listAppender);

        return listAppender;
    }

    public static boolean listAppenderContainsAllMsg(@NonNull ListAppender<ILoggingEvent> listAppender, @NonNull String... msgs) {
        List<ILoggingEvent> logs = listAppender.list;

        for (var log : logs) {
            if (stringContainsAll(log.getMessage(), msgs)) {
                return true;
            }
        }

        return false;
    }

    public static boolean listAppenderContainsAllMsgOnLogLevel(@NonNull ListAppender<ILoggingEvent> listAppender, Level logLevel, @NonNull String... msgs) {
        List<ILoggingEvent> logs = listAppender.list;

        for (var log : logs) {
            if (stringContainsAll(log.getMessage(), msgs) && log.getLevel().equals(logLevel)) {
                return true;
            }
        }

        return false;
    }

    public static boolean stringContainsAll(@NonNull String string, @NonNull String... strings) {
        var containsAll = true;
        for (var msg: strings) {
            if (!string.contains(msg)) {
                containsAll = false;
                break;
            }
        }
        return containsAll;
    }

    public static String createUniqueFileName(String prefix, String extension) {
        var fileName = prefix + "-" + UUID.randomUUID();
        if (extension != null) {
            fileName += "." + extension;
        }
        return fileName;
    }

    public static File createUniqueFile(File dir, String fileNamePrefix, String extension) throws IOException {
        var file = new File(dir, TestUtil.createUniqueFileName(fileNamePrefix, extension));
        Files.createFile(file.toPath());
        return file;
    }

    public static boolean containsString(String[] arbitraryStrings, String search) {
        for (var param : arbitraryStrings) {
            if ((param != null) && param.contains(search)) {
                return true;
            }
        }
        return false;
    }

}
