/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.config.ServerConfig;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// TODO NEXT Remove this class its only a ServerConfig mapper
@Component
@Getter
public class REDescriptor {
    private String installPath = null;
    private String userPath = null;
    private long maxVMemMB = -1;
    private int restartCount = 1;
    private String blacklistFile = null;
    private String whitelistFile = null;
    private int urlLinkLimit = -1;
    private String urlLinkProxy = null;

    @Autowired
    private ServerConfig serverConfig;

    @PostConstruct
    public void init() {
        installPath = serverConfig.getReInstallDir();
        userPath = serverConfig.getScratchDirPath();
        maxVMemMB = serverConfig.getMaxVMemMB();
        restartCount = serverConfig.getJobRestartCount();
        blacklistFile = serverConfig.getUrlBlacklistFile();
        whitelistFile = serverConfig.getUrlWhitelistFile();
        urlLinkLimit = serverConfig.getUrlLinkLimit();
        urlLinkProxy = serverConfig.getUrlLinkProxy();
    }
}
