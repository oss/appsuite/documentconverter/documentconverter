/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.thread.ThreadFactory;
import jakarta.annotation.Nullable;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * {@link REInstanceProvider}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class REInstanceProvider implements Runnable {

    private final ServerManager serverManager;

    private final JobProcessor jobProcessor;

    private final REDescriptor descriptor;

    private BlockingQueue<REInstance> instanceQueue;

    private final int instancePoolSize;

    private Thread backgroundThread;

    private ExecutorService instanceCreateThreadPool;

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    public REInstanceProvider(@NonNull ServerManager serverManager,
                              @NonNull JobProcessor jobProcessor, @NonNull REDescriptor descriptor, int instancePoolSize) {
        this.serverManager = serverManager;
        this.jobProcessor = jobProcessor;
        this.descriptor = descriptor;
        this.instancePoolSize = instancePoolSize;
        init();
    }

    private void init() {
        if (instancePoolSize > 0) {
            instanceQueue = new ArrayBlockingQueue<>(instancePoolSize);
            instanceCreateThreadPool = Executors.newFixedThreadPool(instancePoolSize);

            backgroundThread = ThreadFactory.createThread(this, "DC ReaderEngine Instance Provider");
            backgroundThread.start();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC ReaderEngine instance provider created thread pool",
                    new LogData("capacity", Integer.toString(instancePoolSize)));
            }
        } else {
            instanceQueue = null;
            instanceCreateThreadPool = null;

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC ReaderEngine instance provider is using no thread pool");
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        while (isRunning()) {
            var poolInstancesToCreate = (instancePoolSize - instanceQueue.size());
            Collection<Callable<Boolean>> callables = new ArrayList<>(poolInstancesToCreate);

            for (int i = 0; i < poolInstancesToCreate; ++i) {
                callables.add(() -> {
                    var newPoolInstance = implCreateInstance();
                    var ret = false;

                    if (null != newPoolInstance) {
                        ret = instanceQueue.offer(newPoolInstance);

                        if (ret) {
                            if (ServerManager.isLogTrace()) {
                                ServerManager.logTrace(new StringBuilder(128).
                                    append("DC ReaderEngine instance provider added new instance to pool").
                                    append(" (").
                                    append(instanceQueue.size()).
                                    append('/').
                                    append(instancePoolSize).
                                    append(')').toString());
                            }
                        } else {
                            newPoolInstance.kill();

                            if (ServerManager.isLogTrace()) {
                                ServerManager.logTrace("DC ReaderEngine instance provider was not able to add new instance to pool due to capacity constraints or termination in progress");
                            }
                        }
                    }

                    return ret;
                });
            }

            if (!callables.isEmpty()) {
                try {
                    instanceCreateThreadPool.invokeAll(callables);
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted for whatever reason
                }
            }

            // wait for 1s max. till next insert check or
            // if this thread is (preferably) notified
            // after taking a pool instance
            synchronized (this) {
                try {
                    wait(1000);
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted for whatever reason
                }
            }
        }

        // tracing thread has finished
        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo("DC ReaderEngine instance provider thread finished");
        }
    }

    /**
     *
     */
    public void terminate() {
        if (isRunning.compareAndSet(true, false)) {
            var trace = serverManager.isLogTrace();
            var traceStartTimeMillis = 0L;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine instance provider starting shutdown...");
            }

            if (null != instanceCreateThreadPool) {
                instanceCreateThreadPool.shutdownNow();
            }

            if (null != backgroundThread) {
                backgroundThread.interrupt();
                try {
                    backgroundThread.join(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC ReaderEngine ReaderEngine instance provider finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return isRunning.get();
    }

    /**
     * @return
     */
    @Nullable
    public REInstance getREInstance() {
        REInstance ret = null;

        if (isRunning()) {
            if ((null != instanceQueue) && !instanceQueue.isEmpty()) {
                try {
                    ret = instanceQueue.take();

                    if (null != ret) {
                        synchronized (this) {
                            // notify thread worker that space has become available
                            // and a new pool instance can be added to the pool
                            notify();
                        }

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace("DC ReaderEngine instance provider returned instance from pool");
                        }
                    }
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we need to create a new instance
                }
            }

            if (null == ret) {
                ret = implCreateInstance();
            }
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected REInstance implCreateInstance() {
        REInstance ret = null;

        if (isRunning()) {
            if (!(ret = new REInstance(serverManager, jobProcessor, descriptor)).launch()) {
                ret.kill();
                ret = null;
            }

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace(((null != ret) ?
                    "DC ReaderEngine instance provider created and launched new instance: " :
                    "DC ReaderEngine instance provider was not able to create and launch new instance:") +
                    descriptor.getInstallPath());
            }
        }

        return ret;
    }

}
