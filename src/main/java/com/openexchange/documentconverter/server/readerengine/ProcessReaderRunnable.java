/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.ServerManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * {@link ProcessReaderRunnable}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */

@Slf4j
class ProcessReaderRunnable implements Runnable {
    private REInstance instance = null;
    private Process instanceProcess = null;
    private InputStream processInputStream = null;

    /**
     * Initializes a new {@link ProcessReaderRunnable}.
     *
     * @param processInputStream
     */
    ProcessReaderRunnable(REInstance instance, Process process, InputStream processInputStream) {
        super();

        this.instance = instance;
        this.instanceProcess = process;
        this.processInputStream = processInputStream;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        if (null != processInputStream) {
            var trace = log.isTraceEnabled();

            try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(processInputStream))) {
                String outputStr = null;

                while (null != (outputStr = bufferedReader.readLine())) {
                    if (trace && StringUtils.isNotEmpty(outputStr = implFilterOutput(outputStr))) {
                        log.trace("DC ReaderEngine output: " + outputStr);
                    }
                }
            } catch (final Exception e) {
                log.error("Exception caught trying to read process input stream", e);
            }

            log.trace("DC ReaderEngine instance thread finished");

            instance.processTerminated(instanceProcess);
            instance = null;
            instanceProcess = null;

            ServerManager.close(processInputStream);
        }
    }

    /**
     * @param outputStr
     * @return
     */
    private String implFilterOutput(final String outputStr) {
        String ret = null;

        if (StringUtils.isNotEmpty(outputStr)) {
            if ((-1 == outputStr.indexOf("warn:")) &&
                (-1 == outputStr.indexOf("Can't open storage"))) {

                ret = outputStr;
            }
        }

        return ret;
    }
}
