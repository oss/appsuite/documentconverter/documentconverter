/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.logging.LogData;
import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class REInstance {

    public static final int CONNECT_TIMEOUT_MS = 60000;

    protected static final int BEFORE_CONNECT_DELAY_MS = 250;
    protected static final int RETRY_CONNECT_TIMEOUT_MS = 500;
    protected static final int ONE_HOUR_AS_MS = 60 * 60 * 1000;

    private static final String UNO_SERVICENAME_ALIVESERVICE = "com.sun.star.config.OxAliveService";
    private static final String UNO_SERVICENAME_REMOTEPROXY = "com.sun.star.config.OxRemoteProxyService";
    private static final String UNO_PROPERYTY_GETPID = "OxGetPID";
    private static final String UNO_PROPERTY_REMOTEPROXY = "OxRemoteProxy";
    private static final String UNO_PROPERTY_URLRESOLVER_HANDLER = "OxURLResolverHandler";

    // get ProcFS status:
    // /proc/cpuinfo needs to be readable and last modification must not be older than 1 hour
    private static boolean HAS_PROCSFS = false;
    static {
        var procCheckFile = new File("/proc/cpuinfo");
        HAS_PROCSFS = (procCheckFile.canRead() && ((System.currentTimeMillis() - procCheckFile.lastModified()) <= ONE_HOUR_AS_MS));
    }
    private static AtomicBoolean VALID_INSTALLATION = null;

    private final ServerManager serverManager;
    private final JobProcessor jobProcessor;
    private final REDescriptor descriptor;
    private volatile Process process = null;
    private volatile int instancePid = 0;
    private volatile XDesktop desktop = null;
    private volatile XMultiComponentFactory componentFactory = null;
    private volatile XMultiServiceFactory serviceFactory = null;
    private volatile XComponentContext defaultContext = null;
    private File programFile = null;
    private File userDirFile = null;
    private String pipeName = null;
    private URLResolveHandler urlResolveHandler = null;

    /**
     * Initializes a new {@link REInstance}.
     */
    public REInstance(
        @NonNull ServerManager serverManager,
        @NonNull JobProcessor jobProcessor,
        @NonNull REDescriptor reDescriptor) {

        super();

        this.serverManager = serverManager;
        this.jobProcessor = jobProcessor;
        this.descriptor = reDescriptor;
    }

    /**
     * @return
     */
    public boolean launch() {
        var ret = false;

        // kill any existing instance
        kill();

        // launch new instance
        var installPath = descriptor.getInstallPath();
        var userPath = descriptor.getUserPath();

        if (implIsValidInstallation(installPath, userPath)) {
            final long launchStartTime = System.currentTimeMillis();

            try {
                String uuid = null;

                do {
                    // create unique user directory name to be used for this server instance
                    // remove all dashes from the UUID, since the MD5 algorithm will produce
                    // different results than the LO MD5 algorithm with these characters
                    uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    userDirFile = new File(userPath, pipeName = ("ooxserver_" + uuid));
                } while (userDirFile.exists());

                if (ServerManager.validateOrMkdir(userDirFile)) {
                    final File programDirFile = new File(installPath, "program").getCanonicalFile();
                    final String userDirURL = "file://" + userDirFile.getCanonicalPath();
                    String executableName = "soffice";

                    // add exe extension on Windows
                    if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
                        executableName += ".exe";
                    }

                    programFile = new File(programDirFile, executableName);

                    Vector<String> cmdVector = new Vector<>();

                    cmdVector.add(programFile.getCanonicalPath());

                    // max vmem
                    if (descriptor.getMaxVMemMB() > 0) {
                        cmdVector.add("-maxvmem");
                        cmdVector.add(Long.toString(descriptor.getMaxVMemMB()));
                    }

                    // set tmp dir for readerEngine processes
                    cmdVector.add("-tmpdir");
                    cmdVector.add(userDirFile.getCanonicalPath());
                    cmdVector.add("--accept=pipe,name=" + pipeName + ";urp;StarOffice.ServiceManager");
                    cmdVector.add("--headless");
                    cmdVector.add("--invisible");
                    cmdVector.add("--minimized");
                    cmdVector.add("--nocrashreport");
                    cmdVector.add("--nodefault");
                    cmdVector.add("--nofirststartwizard");
                    cmdVector.add("--nologo");
                    cmdVector.add("--norestore");
                    cmdVector.add("-env:UserInstallation=" + userDirURL);

                    var procBuilder = new ProcessBuilder(cmdVector);

                    procBuilder.directory(userDirFile);
                    procBuilder.redirectErrorStream(true);

                    if (null != (process = procBuilder.start())) {
                        @SuppressWarnings("resource") // ok, since procInputStream will be deleted when thread finishes
                        var procInputStream = process.getInputStream();

                        if (null != procInputStream) {
                            var processReaderRunnable = new ProcessReaderRunnable(this, process, procInputStream);
                            var readerEngineExecutorService = jobProcessor.getReaderEngineExecutorService();

                            if (null != readerEngineExecutorService) {
                                readerEngineExecutorService.execute(processReaderRunnable);
                            } else {
                                (new Thread(processReaderRunnable)).start();
                            }
                        }
                    }
                }
            } catch (java.lang.Exception e) {
                log.error("", e);
                process = null;
            }

            // try to get a connection with a defined delay time, retry timeout and final timeout
            if (null != process) {
                var isAlive = false;

                // check if process has already been exited;
                // ugly test via Exception, but Java 1.8 impl.
                // for Process#isAlive obviously does the same internally
                try {
                    process.exitValue();
                } catch (@SuppressWarnings("unused") final IllegalThreadStateException e) {
                    isAlive = true;
                }

                if (isAlive) {
                    var startTime = System.currentTimeMillis();

                    ServerManager.sleepThread(BEFORE_CONNECT_DELAY_MS, true);

                    while (!implInitConnection() && (System.currentTimeMillis() < (startTime + CONNECT_TIMEOUT_MS))) {
                        ServerManager.sleepThread(RETRY_CONNECT_TIMEOUT_MS, true);
                    }

                    implPrepareInstance();
                }
            }

            // force process destroy and cleanup in case we really got no connection;
            if (!isConnected()) {
                kill();
            } else if (log.isInfoEnabled()) {
                log.info("DC ReaderEngine connected",
                    new LogData("launchtime", Long.valueOf(System.currentTimeMillis() - launchStartTime).toString() + "ms"));
            }
        } else if (log.isErrorEnabled()) {
            if (StringUtils.isEmpty(installPath)  || !(new File(installPath).canRead())) {
                log.error("DC installation: install dir is not readable: " + installPath);
            } else if (StringUtils.isEmpty(userPath) || !(new File(userPath).canWrite())) {
                log.error("DC installation: user dir is not writable: " + userPath);
            }
        }

        // set the launch flag to indicate a failure of the last launch try
        if (isConnected()) {
            implInitializeReaderEngineServices();
            ret = true;

            log.info("DC ReaderEngine launched", new LogData("max.VMem MB", Long.toString(descriptor.getMaxVMemMB())));
        } else {
            log.error("DC ReaderEngine could not be launched", new LogData("installpath", descriptor.getInstallPath()));
        }

        return ret;
    }

    /**
     *
     */
    public synchronized void kill() {
        var curProcess = process;
        final var curPid = instancePid;

        process = null;
        desktop = null;
        serviceFactory = null;
        componentFactory = null;
        defaultContext = null;
        instancePid = 0;

        // force a hard destroy of the process
        if (null != curProcess) {
            try {
                // if we do have a PID, use SIGKILL (kill -9) to make
                // sure the process is really killed in time first
                if (0 != curPid) {
                    var pidStr = Integer.toString(curPid);
                    var killProcessBuilder = new ProcessBuilder("kill", "-9", pidStr);
                    killProcessBuilder.redirectErrorStream(true);
                    var killProcess = killProcessBuilder.start();

                    if (null != killProcess) {
                        killProcess.waitFor();
                    }
                }
            } catch (Exception e) {
                log.error("", e);
            } finally {
                // finally call destroyForcibly (most possibly a SIGTERM (kill -15)
                // internally) to destroy the Java Process object in a correct manner
                curProcess.destroyForcibly();
                curProcess = null;
            }
        }

        // remove user directory and URL content directory for this server instance
        FileUtils.deleteQuietly(userDirFile);

        // kill URL resolve handler for this instance
        if (null != urlResolveHandler) {
            urlResolveHandler.kill();
            urlResolveHandler = null;
        }

        // get rid of OSL_PIPE_* files located in LO/AOO hard coded /tmp or /var/tmp directories
        if ((null != userDirFile) && (null != pipeName)) {
            var tmpDir = new File("/tmp");

            if (!tmpDir.canWrite()) {
                tmpDir = new File("/var/tmp");
            }

            if (tmpDir.canWrite()) {
                var tmpDirEntries = tmpDir.list();

                if (null != tmpDirEntries) {
                    // LO/AOO uses a sal_Unicode md5 hash and not a character md5 hash,
                    // so translate each character into a pair of bytes; each byte is
                    // afterward translated into its corresponding hex value, omitting
                    // possible leading zeros for each byte to hex conversion
                    String md5String = null;
                    String userDirURL = null;

                    try {
                        userDirURL = "file://" + userDirFile.getCanonicalPath();

                        final char[] charArray = userDirURL.toCharArray();

                        if (charArray.length > 0) {
                            var byteArray = new byte[charArray.length << 1];

                            // translate char array to byte array
                            for (int i = 0, k = 0; i < charArray.length;) {
                                var curChar = charArray[i++];

                                byteArray[k++] = (byte) (curChar & 0xFF);
                                byteArray[k++] = (byte) ((curChar >> 8) & 0xFF);
                            }

                            // get the 16 byte MD5 Digest
                            var md5 = DigestUtils.md5(byteArray);

                            // translate the 16 bytes array into the corresponding, LO conform hex string
                            if ((null != md5) && (md5.length == 16)) {
                                try (var md5StringWriter = new StringWriter(32);
                                     var md5PrintWriter = new PrintWriter(md5StringWriter)) {
                                    for (int n = 0; n < 16;) {
                                        md5PrintWriter.printf("%x", md5[n++]);
                                    }

                                    md5PrintWriter.flush();
                                    md5String = md5StringWriter.toString();
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error("", e);
                    }

                    for (var curEntry : tmpDirEntries) {
                        if (curEntry.startsWith("OSL_PIPE_") && ((curEntry.endsWith(pipeName) || ((null != md5String) && curEntry.endsWith(md5String))))) {
                            FileUtils.deleteQuietly(new File(tmpDir, curEntry));
                        }
                    }
                }
            }
        }

        programFile = userDirFile = null;
        pipeName = null;
    }

    /**
     * @return
     */
    public boolean isDisposed() {
        var curPid = 0;
        var isAlive = false;
        Process curProcess = null;

        synchronized (this) {
            curProcess = process;
            curPid = instancePid;
        }

        // in case of an obviously running process, deeply check
        // if the system process with instance Pid is really running;
        // return true for a disposed instance process
        if ((null != curProcess) && (0 != curPid)) {
            var pidStr = "" + curPid;

            if (HAS_PROCSFS) {
                isAlive = (new File("/proc/" + pidStr, "status")).canRead();
            } else {
                try {
                    var psProcessBuilder = new ProcessBuilder("ps", "-p", pidStr);

                    psProcessBuilder.redirectErrorStream(true);

                    var psProcess = psProcessBuilder.start();
                    if (null != psProcess) {
                        try (var psProcessReader = new BufferedReader(new InputStreamReader(psProcess.getInputStream()))) {
                            String psProcessLine = null;

                            while (!isAlive && (null != (psProcessLine = psProcessReader.readLine()))) {
                                if (StringUtils.isNotEmpty(psProcessLine) && (psProcessLine.indexOf(pidStr) != -1)) {
                                    isAlive = true;
                                }
                            }
                        }
                    }
                } catch (final IOException e) {
                    log.error("IOException caught trying to determine process state", e);
                }
            }
        }

        return !isAlive;
    }

    /**
     * @return
     */
    public boolean isConnected() {
        return ((null != process) && (null != componentFactory) && (null != defaultContext) && (null != serviceFactory) && (null != desktop));
    }

    /**
     * @return
     */
    public int getRestartCount() {
        return descriptor.getRestartCount();
    }

    /**
     * @return
     */
    public XMultiComponentFactory getComponentFactory() {
        return componentFactory;
    }

    /**
     * @return
     */
    public XComponentContext getDefaultContext() {
        return defaultContext;
    }

    /**
     * @return
     */
    public com.sun.star.lang.XMultiServiceFactory getFilterFactory() {
        return serviceFactory;
    }

    /**
     * @return
     */
    public com.sun.star.frame.XDesktop getDesktop() {
        return desktop;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return true if a connection has been established (new or already connected)
     */
    private boolean implInitConnection() {
        var ret = isConnected();

        if (!ret) {
            try {
                final XComponentContext xComponentContext = Bootstrap.createInitialComponentContext(null);
                final XMultiComponentFactory xMultiComponentFactory = xComponentContext.getServiceManager();
                final Object aObjectUrlResolver = xMultiComponentFactory.createInstanceWithContext(
                    "com.sun.star.bridge.UnoUrlResolver",
                    xComponentContext);

                componentFactory = UnoRuntime.queryInterface(
                    XMultiComponentFactory.class,
                    UnoRuntime.queryInterface(XUnoUrlResolver.class, aObjectUrlResolver).resolve(
                        "uno:pipe,name=" + pipeName + ";urp;StarOffice.ServiceManager"));

                if (null != componentFactory) {
                    defaultContext = UnoRuntime.queryInterface(
                        XComponentContext.class,
                        UnoRuntime.queryInterface(XPropertySet.class, componentFactory).getPropertyValue("DefaultContext"));

                    desktop = UnoRuntime.queryInterface(
                        XDesktop.class,
                        componentFactory.createInstanceWithContext("com.sun.star.frame.Desktop", defaultContext));

                    serviceFactory = UnoRuntime.queryInterface(
                        XMultiServiceFactory.class,
                        componentFactory.createInstanceWithContext("com.sun.star.document.FilterFactory", defaultContext));
                }
            } catch (@SuppressWarnings("unused") com.sun.star.connection.NoConnectException e) {
                // try again later
            } catch (java.lang.Exception e) {
                log.error("", e);
            } finally {
                if (!(ret = isConnected())) {
                    serviceFactory = null;
                    desktop = null;
                    componentFactory = null;
                    defaultContext = null;
                }
            }
        }

        return ret;
    }

    /**
     *
     */
    private void implPrepareInstance() {
        try {
            Thread.sleep(BEFORE_CONNECT_DELAY_MS, 0);
        } catch (@SuppressWarnings("unused") final InterruptedException e) {
            // Ok
        }

        synchronized (this) {
            if (isConnected()) {
                try {
                    var serviceFactory = getComponentFactory();
                    var serviceContext = getDefaultContext();

                    if ((null != serviceFactory) && (null != serviceContext)) {
                        try {
                            var oxAliveProperties = UnoRuntime.queryInterface(
                                XPropertySet.class,
                                serviceFactory.createInstanceWithContext(UNO_SERVICENAME_ALIVESERVICE, serviceContext));

                            if (null != oxAliveProperties) {
                                instancePid = Integer.parseInt(AnyConverter.toString(
                                    oxAliveProperties.getPropertyValue(UNO_PROPERYTY_GETPID)));
                            }
                        } catch (final Exception e) {
                            log.error("", e);
                        }
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }
    }

    /**
     *
     */
    private void implInitializeReaderEngineServices() {
        var serviceFactory = getComponentFactory();
        var serviceContext = getDefaultContext();
        XPropertySet xRemoteProxyService = null;

        if ((null != serviceFactory) && (null != serviceContext)) {
            try {
                xRemoteProxyService = UnoRuntime.queryInterface(XPropertySet.class,
                    serviceFactory.createInstanceWithContext(UNO_SERVICENAME_REMOTEPROXY, serviceContext));

                if (null != xRemoteProxyService) {
                    // set Remote proxy at ReaderEngine
                    var proxy = descriptor.getUrlLinkProxy();

                    if (StringUtils.isNotEmpty(proxy)) {
                        try {
                            xRemoteProxyService.setPropertyValue(UNO_PROPERTY_REMOTEPROXY, proxy);
                        } catch (final Exception e) {
                            log.error("", e);
                        }
                    }

                    // set URL resolver at ReaderEngine
                    if (null != urlResolveHandler) {
                        urlResolveHandler.kill();
                        urlResolveHandler = null;
                    }

                    try {
                        urlResolveHandler = new URLResolveHandler(serverManager, instancePid);
                        xRemoteProxyService.setPropertyValue(UNO_PROPERTY_URLRESOLVER_HANDLER, urlResolveHandler);
                    } catch (final Exception e) {
                        log.error("", e);
                    }
                }
            } catch (final Exception e) {
                log.error("", e);
            }
        }
    }

    /**
     * @param installDir
     * @param userBaseDir
     * @return true in case the given files specify a valid readerEngine installation
     */
    private synchronized boolean implIsValidInstallation(String installDir, String userBaseDir) {
        if (null == VALID_INSTALLATION) {
            var instDir = (StringUtils.isNotEmpty(installDir)) ? new File(installDir) : null;
            var userDir = (StringUtils.isNotEmpty(userBaseDir)) ? new File(userBaseDir) : null;

            VALID_INSTALLATION = new AtomicBoolean(
                (null != instDir) && instDir.canRead() &&
                    (null != userDir) &&  ServerManager.validateOrMkdir(userDir));
        }

        return VALID_INSTALLATION.get();
    }

    /**
     * @param processTerminated notify that a given process has been terminated unexpectedly and react accordingly
     */
    protected void processTerminated(Process processTerminated) {
        if ((null != processTerminated) && processTerminated.equals(this.process)) {
            log.trace("DC ReaderEngine instance thread finished unexpectedly => forcing process destruction of terminated instance");
            kill();
        }
    }

}
