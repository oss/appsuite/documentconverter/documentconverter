/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.readerengine;

import com.openexchange.documentconverter.server.ServerManager;
import com.sun.star.task.XInteractionHandler;
import com.sun.star.task.XInteractionRequest;
import com.sun.star.task.XInteractionURLResolver;
import com.sun.star.uno.UnoRuntime;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;

import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

/**
 * {@link URLResolveHandler}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */

@Slf4j
class URLResolveHandler implements XInteractionHandler {

    /**
     * Initializes a new {@link URLResolveHandler}.
     */
    URLResolveHandler(@NonNull ServerManager serverManager, final int tempDirId) {
        super();

        tempDir = serverManager.createTempDir("oxurl-" + tempDirId + "-");
    }

    /* (non-Javadoc)
     * @see com.sun.star.task.XInteractionHandler#handle(com.sun.star.task.XInteractionRequest)
     */
    @Override
    public void handle(XInteractionRequest xRequest) {
        if (null != xRequest) {
            var xContinuations = xRequest.getContinuations();

            if (null != xContinuations) {
                for (int i = 0; i < xContinuations.length; ++i) {
                    var curContinuation = xContinuations[i];

                    if (null != curContinuation) {
                        try {
                            var xURLResolver = UnoRuntime.queryInterface(XInteractionURLResolver.class, curContinuation);

                            if (null != xURLResolver) {
                                var sourceURL = xURLResolver.getSourceURL();
                                var outputFile = new File(tempDir, Long.toString(TEMPFILE_ID.incrementAndGet()));
                                var extensionWrapper = new MutableObject<String>(null);
                                var ret = ServerManager.resolveURL(sourceURL, outputFile, extensionWrapper);

                                if (null == ret) {
                                    FileUtils.deleteQuietly(outputFile);
                                } else {
                                    final String extension = extensionWrapper.getValue();
                                    final File outputFileWithExtension = (StringUtils.isNotBlank(extension)) ?
                                        new File(outputFile.getAbsolutePath() + "." + extension) :
                                        outputFile;

                                    if (outputFile != outputFileWithExtension) {
                                        outputFile.renameTo(outputFileWithExtension);
                                    }

                                    xURLResolver.setTargetURL("file://" + outputFileWithExtension.getAbsolutePath());
                                }
                            }
                        } catch (final Exception e) {
                            log.error("Exception caught", e);
                        }
                    }
                }
            }
        }
    }

    // - Implementation ----------------------------------------------------

    /**
     *
     */
    public void kill() {
        FileUtils.deleteQuietly(tempDir);
    }

    // - Static Members ----------------------------------------------------

    private static final AtomicLong TEMPFILE_ID = new AtomicLong(0);

    // - Members -----------------------------------------------------------

    private final File tempDir;
}
