/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.async;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.job.JobPriority;
import com.openexchange.documentconverter.server.metrics.Statistics;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


// TODO make necessary changes to the class
public class AsyncRunnable implements Runnable {
    final private String jobType;
    final private String cacheHash;
    final private HashMap<String, Object> jobProperties;
    final private AsyncIdentifier asyncId;
    private AsyncServerExecutor asyncServerExecutor;
    private Statistics statistics;

    private ServerConfig serverConfig;

    private ManagedJobFactory managedJobFactory;

    protected AsyncRunnable(@NonNull String jobType, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, @NonNull AsyncServerExecutor asyncServerExecutor, @NonNull Statistics statistics, @NonNull ServerConfig serverConfig, @NonNull ManagedJobFactory managedJobFactory) {
        this.jobType = jobType;
        this.cacheHash = cacheHash;
        this.asyncId = new AsyncIdentifier((cacheHash));
        this.jobProperties = new HashMap<>(jobProperties);

        // TODO NEXT remove asyncServerExecutor it's only for logging
        this.asyncServerExecutor = asyncServerExecutor;
        this.statistics = statistics;
        this.serverConfig = serverConfig;
        this.managedJobFactory = managedJobFactory;
        if (!(this.jobProperties.get(Properties.PROP_PRIORITY) instanceof  JobPriority)) {
            // TODO NEXT Debug log?
            this.jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
        }

        if (StringUtils.isNotEmpty(this.jobType)) {
            var inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);
            // TODO NEXT why only ServerManager.logExcp(e) on e.g. ClassCastException and
            // PROP_INPUT_FILE can throw ClassCastException
            try (var inputStm = (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM)) {
                if ((null != inputFile) || (null != inputStm)) {
                    var tempInputFile = ServerManager.createTempFile("oxasync", this.serverConfig.getScratchDir());
                    if (null != tempInputFile) {
                        if (null != inputFile) {
                            FileUtils.copyFile(inputFile, tempInputFile);
                            this.jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                        } else {
                            FileUtils.copyInputStreamToFile(inputStm, tempInputFile);

                            // replace the original input content file temp. input file
                            this.jobProperties.remove(Properties.PROP_INPUT_STREAM);
                            this.jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                        }
                    }
                }
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }
        }
    }

    @Override
    public void run() {
        // reduce parent queue count by one
        statistics.decrementAsyncQueueCount();
        if (isValid()) {
            asyncServerExecutor.logDebug("DC started asynchronous conversion", jobProperties);

            var resultProperties = new HashMap<String, Object>(8);
            ServerManager.close(managedJobFactory.createManagedJob().process(jobType, cacheHash, jobProperties, resultProperties));

            // increment run counter by one
            statistics.incrementAsyncJobProcessedCount();
            asyncServerExecutor.logDebug("DC finished asynchronous conversion", jobProperties);
        }

        clear();
    }

    protected HashMap<String, Object> getJobProperties() {
        return jobProperties;
    }

    protected AsyncIdentifier getAsyncIdentifier() {
        return asyncId;
    }

    protected synchronized boolean isValid() {
        return !jobProperties.isEmpty();
    }

    synchronized void clear() {
        FileUtils.deleteQuietly((File)jobProperties.remove(Properties.PROP_INPUT_FILE));
        jobProperties.clear();
    }
}
