/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.async;

import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.metrics.Statistics;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

// TODO NEXT Refactor
@Component
public class AsyncServerExecutor {

    // It's not final to change it in tests
    private static long LOG_CONNECTION_STATUS_PERIOD_SECONDS = 60L;
    private static final String LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR = LOG_CONNECTION_STATUS_PERIOD_SECONDS + "s";
    private static long CLEANUP_TIMEOUT_MILLIS = 600000;

    @Autowired
    private Statistics statistics;

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ManagedJobFactory managedJobFactory;

    private final ScheduledExecutorService logTimerExecutor = Executors.newScheduledThreadPool(1);
    private long lastDroppedAsyncCount = 0;
    private ThreadPoolExecutor requestExecutor = null;
    private BlockingQueue<Runnable> requestQueue;
    private final ScheduledExecutorService timerExecutor = Executors.newScheduledThreadPool(1);
    private final LinkedHashSet<AsyncIdentifier> runSet = new LinkedHashSet<>();
    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    @PostConstruct
    public void init() {
        var maxThreadCount = Math.max(1, serverConfig.getJobProcessorCount() - 1);

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo("DC asynchronous executor using " + maxThreadCount + " thread(s)");
        }

        requestQueue = new ArrayBlockingQueue<>(serverConfig.getJobAsyncQueueCountLimitHigh());

        initRequestExecutor(maxThreadCount);

        logTimerExecutor.scheduleWithFixedDelay(() -> logAsyncDroppedStatus(),
                LOG_CONNECTION_STATUS_PERIOD_SECONDS, LOG_CONNECTION_STATUS_PERIOD_SECONDS, TimeUnit.SECONDS);
    }

    private void initRequestExecutor(int maxThreadCount) {
        if (maxThreadCount <= 0) {
            return;
        }

        requestExecutor = new ThreadPoolExecutor(maxThreadCount, maxThreadCount, 0, TimeUnit.MILLISECONDS, requestQueue);

        // ensure, that entry is cleaned up (e.g. remove tmp. files) if execution is rejected
        requestExecutor.setRejectedExecutionHandler((runnable, executor) -> {
            if (null != runnable) {
                statistics.incrementAsyncJobDroppedCount();
                decrement();
                ((AsyncRunnable) runnable).clear();
            }
        });

        // initialize timer to clean up run set
        timerExecutor.scheduleAtFixedRate(() -> {
            var curTimeMillis = System.currentTimeMillis();

            synchronized (runSet) {
                for (var asyncIdIter = runSet.iterator(); asyncIdIter.hasNext();) {
                    if ((curTimeMillis - asyncIdIter.next().getTimestampMillis()) > CLEANUP_TIMEOUT_MILLIS) {
                        asyncIdIter.remove();
                    } else {
                        // since the items are ordered by time, we
                        // can stop the search when the first item
                        // did not reach its timeout
                        break;
                    }
                }
            }
        }, CLEANUP_TIMEOUT_MILLIS, CLEANUP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
    }

    void increment() {
        statistics.incrementAsyncQueueCount();
    }

    void decrement() {
        statistics.decrementAsyncQueueCount();
    }

    synchronized void logAsyncDroppedStatus() {
        var droppedAsyncTotal = statistics.getAsyncJobCountDropped();
        var droppedAsyncPeriod = droppedAsyncTotal - lastDroppedAsyncCount;

        if (droppedAsyncPeriod > 0) {
            ServerManager.logWarn("DC JobMonitor dropped asynchronous jobs during last observation period of " + LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR,
                    new LogData("dropped_total", Long.toString(droppedAsyncTotal)),
                    new LogData("dropped_period", Long.toString(droppedAsyncPeriod)));
        }

        lastDroppedAsyncCount = droppedAsyncTotal;
    }

    public void shutdown() {
        if (isRunning.compareAndSet(true, false)) {
            var trace = ServerManager.isLogTrace();
            var traceStartTimeMillis = 0L;

            AsyncRunnable curRunnable = null;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC AsyncExecutor starting shutdown...");
            }

            while((curRunnable = (AsyncRunnable) requestQueue.poll()) != null) {
                // decrement queue count by one
                decrement();
                curRunnable.clear();
            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                        append("DC AsyncExecutor finished shutdown: ").
                        append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                        append('!').toString());
            }
        }
    }

    boolean isRunning() {
        return isRunning.get();
    }

    public void triggerExecution(@NonNull String jobType, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, @NonNull Map<String, Object> resultProperties) {
        if (isRunning() && null != requestExecutor) {
            var asyncRunnable = new AsyncRunnable(jobType, cacheHash, jobProperties, this, statistics, serverConfig, managedJobFactory);

            if (asyncRunnable.isValid()) {
                if (needsRunning(asyncRunnable)) {
                    logDebug("DC scheduled asynchronous job", asyncRunnable.getJobProperties());

                    // increment queue count by one
                    increment();
                    requestExecutor.execute(asyncRunnable);
                } else {
                    logDebug("DC already scheduled same asynchronous job => omitting request", asyncRunnable.getJobProperties());

                    statistics.incrementAndGetAsyncServerExecutorOmitCounter();
                    asyncRunnable.clear();
                }
            } else {
                if (ServerManager.isLogWarn()) {
                    ServerManager.logWarn("DC is not able to execute async. job without file input, stream input or cached hash value set => please set source input property accordingly");
                }
            }
        }
    }

    protected boolean needsRunning(@NonNull AsyncRunnable asyncRunnable) {
        var asyncId = asyncRunnable.getAsyncIdentifier();
        var ret = true;

        synchronized (runSet) {
            // if entry with same AsyncId is already contained
            // in run set => remove old entry and put new entry
            // with up to date timestamp at end of time based
            // ordered LinkedHashSet
            if (runSet.remove(asyncId)) {
                ret = false;
            }

            // put entry at end of the set in every case
            runSet.add(asyncId);
        }


        return ret;
    }

    protected void logDebug(@NonNull String text, @NonNull Map<String, Object> jobProperties) {
        if (ServerManager.isLogDebug()) {
            ServerManager.logDebug(text, jobProperties, statistics.getAsyncServerExcutorLogData());
        }
    }

    // TODO NEXT moved to statistic.getAsyncJobCountScheduled()
//    public int getScheduledCount() {
//        return m_requestQueue.size();
//    }
    // TODO NEXT moved to statistic.getAsyncJobProcessedCount()
//    public long getProcessedCount() {
//        return m_ranCounter.get();
//    }
    // TODO NEXT moved to statistic.getAsyncJobCountDropped()
//    public long getDroppedCount() {
//        return m_droppedCounter.get();
//    }
}
