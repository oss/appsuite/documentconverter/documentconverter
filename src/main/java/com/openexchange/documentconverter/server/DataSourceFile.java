/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * Compatibility class for deprecated multipart attachments (e.g. 7.6.3).<br>
 * <b>!!! Don't change or remove, since it might be used when serializing multipart content !!!</b>
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public class DataSourceFile implements Serializable {

    private static final long serialVersionUID = 1L;

    private File sourceFile;

    private transient byte[] dataSinkBuffer = null;

    public DataSourceFile(@NonNull File sourceFile) {
        super();
        this.sourceFile = sourceFile;
    }

    public File getSourceFile() {
        return sourceFile;
    }

    /**
     * @return The byte array containing the source file content after reading
     */
    public byte[] getDataSinkBuffer() {
        return dataSinkBuffer;
    }

    @Serial
    private synchronized void readObject(ObjectInputStream objInputStm) throws IOException, ClassNotFoundException {
        sourceFile = null;
        dataSinkBuffer = null;

        if (objInputStm != null) {

            objInputStm.defaultReadObject();
            var readObj = objInputStm.readObject();

            if (readObj instanceof Integer) {
                var size = (Integer) readObj;

                if (size > 0) {
                    dataSinkBuffer = IOUtils.toByteArray(objInputStm);
                }
            }

            sourceFile = null;
        }
    }

    @Serial
    private synchronized void writeObject(ObjectOutputStream objOutputStm) throws IOException {
        if (objOutputStm != null) {
            var sourceFileCanRead = sourceFile != null && sourceFile.canRead();
            var fileSize = Integer.valueOf(sourceFileCanRead ? (int) sourceFile.length() : 0);

            // write members by default
            objOutputStm.defaultWriteObject();

            // write file size as Long object
            objOutputStm.writeObject(fileSize);

            // write content of data file, if readable and size > 0
            if (fileSize.longValue() > 0) {
                try {
                    FileUtils.copyFile(sourceFile, objOutputStm);
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }
}
