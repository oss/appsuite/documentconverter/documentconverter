/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.documentconverter.server.cache.CacheDescriptor;
import com.openexchange.documentconverter.server.logging.LogData;
import javax.net.ssl.SSLContext;
import jakarta.mail.BodyPart;
import jakarta.mail.internet.MimeMultipart;
import jakarta.mail.util.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.google.common.base.Throwables;
import lombok.NonNull;
import jakarta.annotation.Nullable;

/**
 * {@link CacheServiceClient}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class CacheServiceClient {

    final public static String HTTP_CACHESERVICE_SERVER_ID = "7ec74ed2-c928-11eb-bd36-6fd14434831d";

    final private static long CACHESERVER_STANDARD_CHECK_PERIOD_MILLIS = 5000;

    final private static long CACHESERVER_RECOVER_CHECK_PERIOD_MILLIS = 1000;

    final private static long CACHESERVER_WARNING_DURATION_MILLIS = 1000;


    /**
     * {@link CacheInfo}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class CacheInfo {

        /**
         * Initializes a new {@link CacheInfo}.
         *
         * @param timestampMillis
         * @param keyCount
         * @param groupLength
         */
        public CacheInfo(final long timestampMillis, final long keyCount, final long maxKeyAgeMillis, final long groupLength) {
            super();

            m_timestampMillis = timestampMillis;
            m_keyCount = keyCount;
            m_maxKeyAgeMillis = maxKeyAgeMillis;
            m_groupLength = groupLength;
        }

        /**
         * @return
         */
        public long getTimestampMillis() {
            return m_timestampMillis;
        }

        /**
         * @return
         */
        public long getKeyCount() {
            return m_keyCount;
        }

        /**
         * @return
         */
        public long getMaxKeyAgeMillis() {
            return m_maxKeyAgeMillis;
        }

        /**
         * @return
         */
        public long getGroupLength() {
            return m_groupLength;
        }

        // - Members -----------------------------------------------------------

        final private long m_timestampMillis;

        final private long m_keyCount;

        final private long m_maxKeyAgeMillis;

        final private long m_groupLength;
    }

    /**
     * Initializes a new {@link CacheServiceClient}.
     */
    public CacheServiceClient(@NonNull final CacheDescriptor cacheDescriptor) throws Exception {
        super();

        final boolean trace = ServerManager.isLogTrace();

        m_recoverPeriodMillis = cacheDescriptor.recoverPeriodMillis;
        m_sslContext = cacheDescriptor.sslContext;
        m_cacheServiceUrl = cacheDescriptor.cacheServiceUrl;
        m_versionValidator = new VersionValidator(
                // getVersionHandler
                (final @NonNull VersionValidator versionValidator) -> {
                    try {
                        final JSONObject jsonResponse = implExecuteJsonRequest(implCreateJsonGetRequest(PATH_CACHESERVICE_SERVICE_HEALTH));
                        if (null != jsonResponse) {
                            final String serverId = jsonResponse.optString(JSON_KEY_SERVERID);

                            if (HTTP_CACHESERVICE_SERVER_ID.equalsIgnoreCase(serverId)) {
                                return jsonResponse.optInt(JSON_KEY_API, VersionValidator.VERSION_INVALID);
                            }
                        }
                    } catch (IOException e) {
                        if (trace) {
                            ServerManager.logTrace("DC Server CacheService client received exception while validating server response: " + Throwables.getRootCause(e));
                        }
                    } catch (Exception e) {
                        ServerManager.logError("DC Server CacheService client received exception in #getVersion handler call: " + Throwables.getRootCause(e));
                    }

                    return VersionValidator.VERSION_INVALID;
                },
                // versionChangedHandler
                (final @NonNull VersionValidator versionValidator, final int oldVersion, final int newVersion) -> {
                    if (newVersion > VersionValidator.VERSION_INVALID) {
                        ServerManager.logInfo("DC Server CacheService client established remote connection to: " + m_cacheServiceUrl);

                        if ((m_recoverPeriodMillis > 0) && !isCacheServerRecoverPeriodNotReached() && m_recoverPeriodStartTimeMillis.compareAndSet(0, System.currentTimeMillis())) {
                            ServerManager.logInfo("DC Server CacheService recover period started",
                                    new LogData("recoverPeriod", m_recoverPeriodMillis + "ms"));
                        }

                        try {
                            // we need to register the OX_DC group after each reconnect since
                            // we dont't know if we're already registered at current server
                            final JSONObject jsonDCGroup = new JSONObject();
                            jsonDCGroup.put(JSON_KEY_MAXGROUPSIZE, cacheDescriptor.maximumPersistSize);
                            jsonDCGroup.put(JSON_KEY_MAXKEYCOUNT, cacheDescriptor.maximumEntryCount);
                            jsonDCGroup.put(JSON_KEY_TIMEOUT_MILLIS, cacheDescriptor.timeoutMillis);
                            jsonDCGroup.put(JSON_KEY_CLEANUP_PERIOD_MILLIS, cacheDescriptor.cleanupPeriodMillis);
                            final HttpPost httpRequest = implCreateJsonPostRequest(PATH_CACHESERVICE_SERVICE_REGISTER_GROUP);
                            if (httpRequest!=null) {
                                httpRequest.addHeader(HttpHeaders.CONTENT_TYPE, MIME_TYPE_JSON);
                                httpRequest.setEntity(new StringEntity(jsonDCGroup.toString(), ContentType.APPLICATION_JSON));
                                if (null == implExecuteJsonRequest(httpRequest)) {
                                    versionValidator.setConnectionInvalid();
                                }
                            }
                        } catch (final Exception e) {
                            ServerManager.logExcp(e);
                        }
                    }
                },
                // lostValidVersionHandler
                (final @NonNull VersionValidator versionValidator, final int oldVersion, final int newVersion) -> {
                    ServerManager.logWarn("DC Server CacheService client lost remote connection to: " + m_cacheServiceUrl);

                    if ((m_recoverPeriodMillis > 0) && !isCacheServerRecoverPeriodNotReached() && (m_recoverPeriodStartTimeMillis.get() > 0)) {
                        final long curTimeMillis = System.currentTimeMillis();
                        final long recoverPeriodRuntimeMillis = curTimeMillis - m_recoverPeriodStartTimeMillis.get();

                        if ((recoverPeriodRuntimeMillis < m_recoverPeriodMillis) && m_recoveryPeriodNotReached.compareAndSet(false, true)) {
                            ServerManager.logWarn("DC Server CacheService recover period was not reached",
                                    new LogData("recoverPeriodRuntime", recoverPeriodRuntimeMillis + "ms"));
                        }
                    }
                });


        if ((null == m_cacheServiceUrl) || !StringUtils.startsWith(m_cacheServiceUrl, "http")) {
            throw new Exception("DC Server CacheService client service URL not valid: " + ((null != m_cacheServiceUrl) ? DocumentConverterUtil.STR_NOT_AVAILABLE : m_cacheServiceUrl));
        }

        // setup timer to check for cache server availability
        final MutableLong lastCheckEndMillis = new MutableLong(Long.valueOf(0));

        // start timer to check for server availability and recover period exceeded conditions
        m_serverCheckTimerExecutor.scheduleWithFixedDelay(() -> {
            final long checkStartMillis = System.currentTimeMillis();

            // Check for server availability as long as recover period status is ok.
            // At first run or in case we're within the recover exceeded detection period, do server check
            // with current run, otherwise do server checks with the CACHESERVER_STANDARD_CHECK_PERIOD_MILLIS
            if (!isCacheServerRecoverPeriodNotReached() &&
                    ((0 == lastCheckEndMillis.getValue()) ||
                            (m_recoverPeriodStartTimeMillis.get() > 0) ||
                            ((checkStartMillis - lastCheckEndMillis.getValue()) >= CACHESERVER_STANDARD_CHECK_PERIOD_MILLIS))) {

                m_probeUpdateRunning.set(true);

                try {
                    final boolean serverAvailable = m_versionValidator.isConnectionAvailable();
                    final long checkEndMillis = System.currentTimeMillis();
                    final long checkDurationMillis = checkEndMillis - checkStartMillis;

                    lastCheckEndMillis.setValue(checkEndMillis);
                    m_cacheServerAvailable.set(serverAvailable);

                    // trace long times needed for server detection
                    if (ServerManager.isLogTrace() && (checkDurationMillis >= CACHESERVER_WARNING_DURATION_MILLIS)) {
                        ServerManager.logTrace("DC Server CacheService runtime unexpectedly high while waiting for cache server answer: " + checkDurationMillis + "ms",
                                new LogData("cacheServerAvailable", Boolean.toString(serverAvailable)));
                    }

                    // check if recover period has been reached and reset appropriate vars
                    final long recoverPeriodStartTimeMillis = m_recoverPeriodStartTimeMillis.get();

                    if (!isCacheServerRecoverPeriodNotReached() && (recoverPeriodStartTimeMillis > 0)) {
                        final long recoverPeriodDurationMillis = checkEndMillis - recoverPeriodStartTimeMillis;

                        if (recoverPeriodDurationMillis >= m_recoverPeriodMillis) {
                            m_recoverPeriodStartTimeMillis.set(0);

                            ServerManager.logInfo("DC Server CacheService recover period sucessfully finished",
                                    new LogData("recoverPeriodRuntime", recoverPeriodDurationMillis + "ms"));
                        }
                    }
                } finally {
                    m_probeUpdateRunning.set(false);
                }
            }
        }, 0L, CACHESERVER_RECOVER_CHECK_PERIOD_MILLIS, TimeUnit.MILLISECONDS);

        ServerManager.logInfo("DC Server CacheServiceClient started server availability timer with a period of " + CACHESERVER_STANDARD_CHECK_PERIOD_MILLIS + "ms");

    }

    // - public API ------------------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            m_serverCheckTimerExecutor.shutdownNow();
        }
    }

    /**
     * @return
     */
    public boolean isCacheServerEnabled() {
        return m_versionValidator.isConnectionEnabled();
    }

    /**
     * @return
     */
    public boolean isCacheServerAvailable() {
        return m_cacheServerAvailable.get();
    }

    /**
     * @return
     */
    public boolean isCacheServerRecoverPeriodNotReached() {
        return m_recoveryPeriodNotReached.get();
    }

    /**
     * @param hash
     * @return
     */
    public boolean hasCacheServiceEntry(@NonNull final String hash, @Nullable String filename) {
        if (isCacheServerEnabled()) {
            final String hasObjectsPath = new StringBuilder(256).append(PATH_CACHESERVICE_SERVICE_HAS_OBJECTS).append(PATH_SEPARATOR).append(implEncodeHttpValue(hash)).toString();
            try {
                final JSONObject jsonResponse = implExecuteJsonRequest(implCreateJsonGetRequest(hasObjectsPath));
                return (((null != jsonResponse) && (200 == jsonResponse.optInt(JSON_KEY_CODE))));
            } catch (Exception e) {
                ServerManager.logWarn("DC Server CacheService client is currently not able check for existing cache entry: " + Throwables.getRootCause(e), new LogData("hash", hash), new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
            }
        }

        return false;

    }

    @Nullable
    public CacheServiceEntry getCacheServiceEntry(@NonNull final String hash, @Nullable String filename) {
        CacheServiceEntry ret = null;
        final boolean trace = ServerManager.isLogTrace();
        final String infoFileName = filename != null ? filename : "unknown";
        long traceStartTimeMillis = 0;
        if (trace) {
            traceStartTimeMillis = System.currentTimeMillis();
        }
        if (trace) {
            traceStartTimeMillis = System.currentTimeMillis();
            ServerManager.logTrace("DC Server get cache result initiated",
                    new LogData("cachehash", hash),
                    new LogData("filename", infoFileName));
        }
        if (isCacheServerEnabled()) {
            final String getObjectsPath = new StringBuilder(256).append(PATH_CACHESERVICE_SERVICE_GET_OBJECTS).append(PATH_SEPARATOR).append(implEncodeHttpValue(hash)).append(QUERY_SEPARATOR)/*.append(STR_REMOVE_KEY_ON_ERROR_QUERY_PARAM).append(QUERY_COMBINER).append(STR_OBJECT_PROPERTIES_QUERY_PARAM).append(QUERY_COMBINER)*/.append(STR_OBJECT_RESULT_PARAM).toString();
            try {
                ret = implExecuteGetCacheServiceEntryRequest(getObjectsPath, hash);
            } catch (Exception e) {
                ServerManager.logWarn("DC Server CacheService client is currently not able to get cache entry from CacheService: " + Throwables.getRootCause(e), new LogData("hash", hash), new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
            }
        }
        if (trace) {
            ServerManager.logTrace("DC Server get cache result " + (ret != null ? "succeeded" : "failed"),
                    new LogData("cachehash", hash),
                    new LogData("filename", infoFileName),
                    new LogData("exectime", Long.toString(System.currentTimeMillis() - traceStartTimeMillis) + "ms"));
        }
        return ret;
    }

    @Nullable
    public InputStream getCacheResultStream(@NonNull final String hash, @Nullable String filename) {
        final CacheServiceEntry cacheResult = getCacheServiceEntry(hash, filename);
        return cacheResult != null ? cacheResult.getResultStream() : null;
    }

    /**
     * @param CacheServiceEntry
     * @return
     */
    public boolean addCacheServiceEntry(@NonNull final CacheServiceEntry CacheServiceEntry, @Nullable String filename) {
        if (isCacheServerEnabled()) {
            final String hash = CacheServiceEntry.getHash();
            final String addObjectsPath = new StringBuilder(256).append(PATH_CACHESERVICE_SERVICE_ADD_OBJECTS).append(PATH_SEPARATOR).append(implEncodeHttpValue(hash)).toString();

            try {
                final HttpPost httpRequest = implCreateJsonPostRequest(addObjectsPath);
                if (httpRequest!=null) {
                    final MultipartEntityBuilder multipartBuilder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                    for(Entry<String, byte[]> entry : CacheServiceEntry.getObjectMap().entrySet()) {
                        multipartBuilder.addBinaryBody(entry.getKey(), entry.getValue());
                    }
                    final HttpEntity entity = multipartBuilder.build();
                    httpRequest.setEntity(entity);
                    httpRequest.addHeader(entity.getContentType());

                    final JSONObject jsonResponse = implExecuteJsonRequest(httpRequest);
                    return (((null != jsonResponse) && (200 == jsonResponse.optInt(JSON_KEY_CODE))));
                    }
            }
            catch (Exception e) {
                ServerManager.logWarn("DC Server CacheService client is currently not able to upload cache entry to CacheService: " + Throwables.getRootCause(e), new LogData("hash", hash), new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
            }
        }
        return false;
    }

    /**
     * @return
     */
    public CacheInfo getCacheInfo() {
        if (isCacheServerEnabled()) {
            final String getGroupInfoPath = new StringBuilder(256).append(PATH_CACHESERVICE_SERVICE_GET_GROUP_INFO).append(QUERY_SEPARATOR).append(STR_METRICS_TRUE_QUERY_PARAM).toString();

            try {
                final JSONObject jsonGroupInfoResponse = implExecuteJsonRequest(implCreateJsonGetRequest(getGroupInfoPath));
                if (null != jsonGroupInfoResponse) {
                    final JSONArray jsonGroupInfoArray = jsonGroupInfoResponse.optJSONArray(JSON_KEY_GROUPS);

                    if ((null != jsonGroupInfoArray) && (jsonGroupInfoArray.length() > 0)) {
                        final JSONObject jsonGroupInfo = jsonGroupInfoArray.optJSONObject(0);

                        if (null != jsonGroupInfo) {
                            final JSONObject jsonCacheInfo = jsonGroupInfo.optJSONObject(JSON_KEY_CACHE);

                            return (null != jsonCacheInfo) ? new CacheInfo(System.currentTimeMillis(), jsonCacheInfo.optLong(JSON_KEY_CACHE_KEYCOUNT), jsonCacheInfo.optLong(JSON_KEY_CACHE_MAXKEYAGE_MILLIS, 0), jsonCacheInfo.optLong(JSON_KEY_CACHE_GROUPLENGTH)) : null;
                        }
                    }
                }
            } catch (Exception e) {
                ServerManager.logWarn("DC Server CacheService client is currently not able get OX_DC GroupInfo from CacheService server: " + Throwables.getRootCause(e));
            }
        }

        return null;
    }

    /**
     * @return
     */
    public long getCacheTotalSize() {
        if (isCacheServerEnabled()) {
            final String getGroupInfoPath = new StringBuilder(256).append(PATH_CACHESERVICE_SERVICE_GET_GROUP_INFO).append(QUERY_SEPARATOR).append(STR_METRICS_TRUE_QUERY_PARAM).toString();

            try {
                final JSONObject jsonGroupInfoResponse = implExecuteJsonRequest(implCreateJsonGetRequest(getGroupInfoPath));
                if (null != jsonGroupInfoResponse) {
                    final JSONObject jsonCacheInfo = jsonGroupInfoResponse.optJSONObject(JSON_KEY_CACHE);

                    if (null != jsonCacheInfo) {
                        return jsonCacheInfo.optLong(JSON_KEY_CACHE_KEYCOUNT);
                    }
                }
            } catch (Exception e) {
                ServerManager.logWarn("DC Server CacheService client is currently not able to get cache total size from  CacheService server: " + Throwables.getRootCause(e));
            }
        }

        return 0;
    }

    /**
     * @return
     */
    public String getCacheServiceURL() {
        return m_cacheServiceUrl;
    }

    // - Implementation --------------------------------------------------------

    @Nullable
    private HttpGet implCreateJsonGetRequest(@NonNull final String path) throws Exception {
        return (HttpGet)implCreateRequest(path, false, MIME_TYPE_JSON);
    }

    @Nullable
    private HttpPost implCreateJsonPostRequest(@NonNull final String path) throws Exception {
        return (HttpPost)implCreateRequest(path, true, MIME_TYPE_JSON);
    }

    @Nullable
    private HttpRequestBase implCreateRequest(@NonNull final String path, boolean createPostRequest, String mimeTypeAccept) throws Exception {
        HttpRequestBase httpRequest = null;
        try {
            final URI targetURI = new URIBuilder(m_cacheServiceUrl + (path.startsWith("/") ? path : ('/' + path))).build();
            final RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(CONNECT_TIMEOUT_MILLIS).setConnectTimeout(CONNECT_TIMEOUT_MILLIS).setSocketTimeout(READ_TIMEOUT_MILLIS).build();
            httpRequest = createPostRequest ? new HttpPost(targetURI) : new HttpGet(targetURI);
            httpRequest.setConfig(requestConfig);
            httpRequest.addHeader(HttpHeaders.ACCEPT, mimeTypeAccept);
        } catch (Exception e) {
            ServerManager.logError("DC Server CacheService client received exception during server request: " + Throwables.getRootCause(e));
        }
        return httpRequest;
    }

    @Nullable
    private JSONObject implExecuteJsonRequest(final HttpRequestBase httpRequest) throws Exception {
        if (httpRequest == null) {
            return null;
        }
        final CloseableHttpClient managedHttpClient = HttpClients.custom().setSSLContext(m_sslContext).build();
        if (null == managedHttpClient) {
            throw new Exception("DC Server CacheService client could not get managed HttpClient instance");
        }
        HttpResponse httpResponse = null;

        try {
            httpResponse = managedHttpClient.execute(httpRequest, HttpClientContext.create());

            final StatusLine statusLine = httpResponse.getStatusLine();
            final int statusCode = statusLine.getStatusCode();

            if ((HttpStatus.SC_OK == statusCode) || (HttpStatus.SC_NOT_FOUND == statusCode)) {
                return new JSONObject(EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8));
            } else if (HttpStatus.SC_REQUEST_TIMEOUT == statusCode) {
                ServerManager.logError("DC Server CacheService client received 'Request timeout' server error response: " + statusCode);
            } else {
                ServerManager.logWarn("DC Server CacheService client received invalid server response: " + statusCode);
            }

            EntityUtils.consume(httpResponse.getEntity());
        } catch (IOException e) {
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC Server CacheService client received IO exception during server request: " + Throwables.getRootCause(e));
            }

            m_versionValidator.setConnectionInvalid();
        } catch (Exception e) {
            ServerManager.logError("DC Server CacheService client received exception during server request: " + Throwables.getRootCause(e));
        } finally {
            implCloseHttp(httpRequest, httpResponse);

            try {
                managedHttpClient.close();
            } catch (IOException e) {
                ServerManager.logError("DC Server CacheService client received exception when closing HttpClient: " + Throwables.getRootCause(e));
            }
        }
        return null;
    }

    @Nullable
    private CacheServiceEntry implExecuteGetCacheServiceEntryRequest(@NonNull final String path, @NonNull final String hash) throws Exception {

        final CloseableHttpClient managedHttpClient = HttpClients.custom().setSSLContext(m_sslContext).build();

        if (null == managedHttpClient) {
            throw new Exception("DC Server CacheService client could not get managed HttpClient instance");
        }

        HttpGet httpRequest = null;
        HttpResponse httpResponse = null;

        try {
            httpRequest = (HttpGet)implCreateRequest(path, false, MIME_TYPE_MULTIPART_FORM_DATA);

            // in case of valid optionalJsonRequestData use a POST request type
            httpResponse = managedHttpClient.execute(httpRequest, HttpClientContext.create());

            final StatusLine statusLine = httpResponse.getStatusLine();
            final int statusCode = statusLine.getStatusCode();

            if (HttpStatus.SC_OK == statusCode) {
                final HttpEntity httpEntity = httpResponse.getEntity();

                if (null != httpEntity) {
                    final ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(httpEntity.getContent(), MIME_TYPE_MULTIPART_FORM_DATA);
                    final MimeMultipart mimeMultipart = new MimeMultipart(byteArrayDataSource);
                    final CacheServiceEntry CacheServiceEntryResult = new CacheServiceEntry(hash);

                    for (int i = 0, count = mimeMultipart.getCount(); i < count; ++i) {
                        final BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                        final String[] contentDispositions = bodyPart.getHeader(HEADER_CONTENT_DISPOSITION);

                        if (MIME_TYPE_APPLICATION_OCTET_STREAM.equals(bodyPart.getContentType()) && ArrayUtils.isNotEmpty(contentDispositions)) {
                            final String name = implGetParamValue(contentDispositions[0], HEADER_KEY_NAME);

                            try (final InputStream inputStm = bodyPart.getInputStream()) {
                                CacheServiceEntryResult.getObjectMap().put(name, IOUtils.toByteArray(inputStm));
                            }
                        }
                    }
                    return CacheServiceEntryResult;
                }
                return null;
            } else if (HttpStatus.SC_NOT_FOUND == statusCode) {
                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace("DC Server CacheService client could not find cache entry in CacheService: " + hash);
                }

                return null;
            } else if (HttpStatus.SC_REQUEST_TIMEOUT == statusCode) {
                ServerManager.logError("DC Server CacheService client received 'Request timeout' server error response: " + statusCode);
            } else {
                ServerManager.logWarn("DC Server CacheService client received invalid server response: " + statusCode);
            }

            EntityUtils.consume(httpResponse.getEntity());
        } catch (IOException e) {
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC Server CacheService client received exception during server request: " + Throwables.getRootCause(e));
            }

            m_versionValidator.setConnectionInvalid();
        } catch (Exception e) {
            ServerManager.logError("DC Server CacheService client received exception during server request: " + Throwables.getRootCause(e));
        } finally {
            implCloseHttp(httpRequest, httpResponse);

            try {
                managedHttpClient.close();
            } catch (IOException e) {
                ServerManager.logError("DC Server CacheService client received exception when closing HttpClient: " + Throwables.getRootCause(e));
            }
        }

        return null;
    }

    /**
     * @param request
     * @param response
     */
    private void implCloseHttp(@Nullable final HttpRequestBase request, @Nullable final HttpResponse response) {
        // close optional response
        if (null != response) {
            final HttpEntity entity = response.getEntity();

            if (null != entity) {
                try {
                    EntityUtils.consumeQuietly(entity);
                } catch (Exception e) {
                    ServerManager.logTrace("DC Server CacheService client failed to ensure that the entity content is fully consumed and the content stream, if exists, is closed: " + Throwables.getRootCause(e));
                }
            }

            if (response instanceof CloseableHttpResponse) {
                try {
                    ((CloseableHttpResponse) response).close();
                } catch (Exception e) {
                    ServerManager.logTrace("DC Server CacheService client error when closing HTTP response: " + Throwables.getRootCause(e));
                }
            }
        }

        // close optional request
        if (null != request) {
            try {
                request.reset();
            } catch (Exception e) {
                ServerManager.logTrace("DC Server CacheService client failed to reset request for making it reusable: " + Throwables.getRootCause(e));
            }
        }
    }

    /**
     * @param value
     * @return
     */
    private static String implEncodeHttpValue(@NonNull final String value) {
        try {
            return URLEncoder.encode(value, CHARSET_UTF8.toString());
        } catch (UnsupportedEncodingException e) {
            ServerManager.logExcp(e);
        }

        return StringUtils.EMPTY;
    }

    /**
     * @param str
     * @param paramName
     * @return
     */
    private static String implGetParamValue(@NonNull final String str, @NonNull final String paramName) {
        final String paramNameToUse = paramName.endsWith("=") ? paramName : (paramName + "=");
        final int nameStartPos = str.lastIndexOf(paramNameToUse);

        if (nameStartPos > -1) {
            final int namePos = nameStartPos + paramNameToUse.length();
            final int endNamePos = str.indexOf(';', namePos);

            return StringUtils.strip(str.substring(namePos, (-1 == endNamePos) ? str.length() : endNamePos).trim(), "\"").trim();
        }

        return StringUtils.EMPTY;
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicBoolean m_probeUpdateRunning = new AtomicBoolean(false);

    final private AtomicBoolean m_cacheServerAvailable = new AtomicBoolean(false);

    final private AtomicLong m_recoverPeriodStartTimeMillis = new AtomicLong(0);

    final private AtomicBoolean m_recoveryPeriodNotReached= new AtomicBoolean(false);

    final private long m_recoverPeriodMillis;

    final private SSLContext m_sslContext;

    final private String m_cacheServiceUrl;

    final private VersionValidator m_versionValidator;

    final private ScheduledExecutorService m_serverCheckTimerExecutor = Executors.newScheduledThreadPool(1);

    // - static members --------------------------------------------------------

    final private static Charset CHARSET_UTF8 = Charset.forName("UTF-8");

    final private static char PATH_SEPARATOR = '/';
    final private static char QUERY_SEPARATOR = '?';

    final private static int CONNECT_TIMEOUT_MILLIS = 1000;
    final private static int READ_TIMEOUT_MILLIS = 30000;

    final private static String STR_METRICS_TRUE_QUERY_PARAM = "metrics=true";

    final private static String STR_RESULT = "result";
    final private static String STR_OBJECT_RESULT_PARAM = "objects=" + STR_RESULT;

    final private static String HEADER_CONTENT_DISPOSITION = "Content-Disposition";

    final private static String HEADER_KEY_NAME = "name=";

    final private static String MIME_TYPE_JSON = "application/json";
    final private static String MIME_TYPE_APPLICATION_OCTET_STREAM = "application/octet-stream";
    final private static String MIME_TYPE_MULTIPART_FORM_DATA = "multipart/form-data";

    final private static String OX_DC_GROUP = "OX_DC";

    final private static String PATH_CACHESERVICE_SERVICE_ADD_OBJECTS = "/addObjects/" + OX_DC_GROUP;
    final private static String PATH_CACHESERVICE_SERVICE_GET_GROUP_INFO = "/getGroupInfo/" + OX_DC_GROUP;
    final private static String PATH_CACHESERVICE_SERVICE_GET_OBJECTS = "/getObjects/" + OX_DC_GROUP;
    final private static String PATH_CACHESERVICE_SERVICE_HAS_OBJECTS = "/hasObjects/" + OX_DC_GROUP;
    final private static String PATH_CACHESERVICE_SERVICE_HEALTH = "/health";
    final private static String PATH_CACHESERVICE_SERVICE_REGISTER_GROUP = "/registerGroup/" + OX_DC_GROUP;

    final private static String JSON_KEY_API = "api";
    final private static String JSON_KEY_CACHE = "cache";
    final private static String JSON_KEY_CACHE_KEYCOUNT = "keyCount";
    final private static String JSON_KEY_CACHE_MAXKEYAGE_MILLIS = "maxKeyAgeMillis";
    final private static String JSON_KEY_CACHE_GROUPLENGTH = "groupLength";
    final private static String JSON_KEY_CODE = "code";
    final private static String JSON_KEY_GROUPS = "groups";
    final private static String JSON_KEY_SERVERID = "serverId";
    final private static String JSON_KEY_MAXGROUPSIZE = "maxGroupSize";
    final private static String JSON_KEY_MAXKEYCOUNT = "maxKeyCount";
    final private static String JSON_KEY_TIMEOUT_MILLIS = "keyTimeout";
    final private static String JSON_KEY_CLEANUP_PERIOD_MILLIS = "cleanupPeriod";
}
