/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.executor;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.http.TransferObject;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.tika.Tika;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class Executor {

    protected record FileFilenameResult(File file, String filename) {}

    public static final String FORMAT_FILE = "file";

    private static final Pattern ALLOWED_HASH_CHARS_PATTERN = Pattern.compile("^[a-zA-Z0-9_\\-.,@#]+$");

    private static final Pattern ALLOWED_HASH_START_CHARS_PATTERN = Pattern.compile("^[a-zA-Z0-9]+");

    private static final Pattern ALLOWED_PAGERANGE_CHARS_PATTERN = Pattern.compile("^[0-9\\-,]+$");

    public static final int MAX_FORM_DATA_PART_COUNT = 32;

    protected final TransferObject<Serializable> transferObject = new TransferObject<>();

    protected RConvertRequestModel rConvertRequestModel;

    protected ServerConfig serverConfig;

    public Executor(@NonNull ServerConfig serverConfig, @NonNull RConvertRequestModel rConvertRequestModel) throws InvalidParameterException {
        super();
        this.serverConfig = serverConfig;
        this.rConvertRequestModel = rConvertRequestModel;
        extractFormData();
    }

    protected abstract Map<String, Object> execute();

    // TODO NEXT refactor boolean array isPDFSource
    protected File createTempInputFile(boolean @NonNull [] isPDFSource, @NonNull Map<String, Object> jobProperties) {
        File tempInputFile = null;
        var url = rConvertRequestModel.getUrl();

        if (url == null) {
            tempInputFile = createTempInputFileFromSerializedObject(isPDFSource, jobProperties);
        } else {
            var httpURL = url.trim();
            var infoFileName= (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            if (url.indexOf("data:") == 0) {
                tempInputFile = createTempInputFileFromBase64(url, isPDFSource, jobProperties);
            } else if (httpURL.startsWith("http://") || httpURL.startsWith("https://")) {
                tempInputFile = createTempInputFileFromHttpUrl(httpURL, isPDFSource, jobProperties);
            } else if (serverConfig.isAllowLocalFileUrls() && url.startsWith("file:///")) {
                var result = createTempInputFileFromLocalFile(url, isPDFSource, jobProperties);
                tempInputFile = result.file();
                infoFileName = result.filename();
            }

            if (tempInputFile == null && ServerManager.isLogDebug()) {
                ServerManager.logDebug("DC WebService didn't receive a valid soure URL or could not write source content to temp. file");
            } else if (StringUtils.isNotEmpty(infoFileName)) {
                jobProperties.put(Properties.PROP_INFO_FILENAME, infoFileName);
            } else {
                jobProperties.remove(Properties.PROP_INFO_FILENAME);
            }
        }

        return tempInputFile;
    }

    protected File createTempInputFileFromSerializedObject(boolean @NonNull [] isPDFSource, @NonNull Map<String, Object> jobProperties) {
        File tempInputFile = null;
        Serializable contentSerializable = transferObject.getSerializedObject();

        if (contentSerializable instanceof File) {
            tempInputFile = (File) contentSerializable;
            transferObject.setSerializedObject(null);

            if (isPDFSource.length > 0) {
                var serialMimeType = transferObject.getMimeType();

                // check for PDF mime type
                isPDFSource[0] = "application/pdf".equals(serialMimeType);
            }
        }

        if (tempInputFile == null && ServerManager.isLogDebug()) {
            ServerManager.logDebug("DC WebService did not received a valid input buffer with data");
        }

        return tempInputFile;
    }

    protected File createTempInputFileFromBase64(@NonNull String url, boolean @NonNull [] isPDFSource, @NonNull Map<String, Object> jobProperties) {
        File tempInputFile = null;

        var base64SearchPattern = "base64,";
        var pos = url.indexOf(base64SearchPattern);

        if (isPDFSource.length > 0 && pos > 5) {
            // check for PDF mime type
            isPDFSource[0] = url.substring(5, 5 + StringUtils.length("application/pdf")).equals("application/pdf");
        }

        if (pos != -1 && (pos += base64SearchPattern.length()) < (url.length() - 1)) {
            var data = Base64.decodeBase64(url.substring(pos));

            if (ArrayUtils.isNotEmpty(data)) {
                tempInputFile = implCreateTempFile();

                if (tempInputFile != null) {
                    try {
                        FileUtils.writeByteArrayToFile(tempInputFile, data);
                    } catch (IOException e) {
                        FileUtils.deleteQuietly(tempInputFile);
                        tempInputFile = null;
                        ServerManager.logExcp(e);
                    }
                }
            }
        }

        return tempInputFile;
    }

    protected File createTempInputFileFromHttpUrl(@NonNull String httpURL, boolean @NonNull [] isPDFSource, @NonNull Map<String, Object> jobProperties) {
        File tempInputFile;
        var tempFile = implCreateTempFile();
        var extensionWrapper = new MutableObject<String>(null);

        if ((tempInputFile = ServerManager.resolveURL(httpURL, tempFile, extensionWrapper)) == null) {
            FileUtils.deleteQuietly(tempFile);
        } else if ("pdf".equals(extensionWrapper.getValue()) && (isPDFSource.length > 0)) {
            isPDFSource[0] = true;
        }

        return tempInputFile;
    }
    /**
     *
     * @param filePath FilePath must start with 'file://'
     * @param isPDFSource can be empty
     * @param jobProperties can contain the Properties.PROP_INFO_FILENAME to overwrite the filename of the local file
     * @return pair with the temp file and the filename
     */
    protected FileFilenameResult createTempInputFileFromLocalFile(@NonNull String filePath, boolean @NonNull [] isPDFSource, @NonNull Map<String, Object> jobProperties) {
        File tempInputFile = null;
        var infoFileName= (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        var fileName = filePath.substring(7);
        var localFile = new File(fileName);

        if (fileName.toLowerCase().trim().endsWith(".pdf") && (isPDFSource.length > 0)) {
            isPDFSource[0] = true;
        }

        if (localFile.canRead()) {
            tempInputFile = implCreateTempFile();

            if (tempInputFile != null) {
                try {
                    Files.copy(localFile.toPath(), FileUtils.openOutputStream(tempInputFile));

                    if (infoFileName == null) {
                        infoFileName = fileName;
                    }
                } catch (IOException e) {
                    FileUtils.deleteQuietly(tempInputFile);
                    tempInputFile = null;
                    ServerManager.logExcp(e);
                }
            }
        }

        return new FileFilenameResult(tempInputFile, infoFileName);
    }

    public static boolean isValidHash(String hash) {
       var validHash = hash != null &&
                ALLOWED_HASH_CHARS_PATTERN.matcher(hash).matches() &&
                ALLOWED_HASH_START_CHARS_PATTERN.matcher(hash).find() &&
                Cache.isValidHash(hash);

        if (!validHash && hash != null) {
            ServerManager.logError("DC rejected service request due to invalid pattern of hash parameter: " + hash);
        }

        return validHash;
    }

    protected static boolean isValidPageRange(String pageRange) {
        return pageRange != null && ALLOWED_PAGERANGE_CHARS_PATTERN.matcher(pageRange).matches();
    }

    protected void extractFormData() throws InvalidParameterException {
        File serializedObject = null;
        try {
            var sourceFile = rConvertRequestModel.getSourcefile();
            if (sourceFile != null) {
                var itemInputStm = sourceFile.getInputStream();
                serializedObject = implCreateTempFile();
                FileUtils.copyInputStreamToFile(itemInputStm, serializedObject);

                transferObject.setSerializedObject(serializedObject);
                // TODO NEXT Use Tika via Service?
                transferObject.setMimeType(new Tika().detect(serializedObject));
            }
        } catch (IOException e) {
            FileUtils.deleteQuietly(serializedObject);
            throw new InvalidParameterException(Throwables.getRootCause(e).getMessage());
        }
    }

    private File implCreateTempFile() {
        return ServerManager.createTempFile("oxcs", serverConfig.getScratchDir());
    }
}
