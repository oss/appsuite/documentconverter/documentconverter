/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.executor;

import com.openexchange.documentconverter.server.CacheServiceClient;
import com.openexchange.documentconverter.server.CacheServiceEntry;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.job.JobPriority;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import lombok.NonNull;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class JobExecutor extends Executor {

    // RESPONSE_SIZE_WARN_THRESHOLD_BYTES set to 100MB
    private static final long RESPONSE_SIZE_WARN_THRESHOLD_BYTES = 100 * 1024 *1024;

    private final Cache cache;
    private final ManagedJobFactory managedJobFactory;
    private final AsyncServerExecutor asyncServerExecutor;
    @NonNull private final ServerManager serverManager;

    public JobExecutor(@NonNull ServerManager serverManager, @NonNull ServerConfig serverConfig, @NonNull Cache cache, @NonNull RConvertRequestModel rConvertRequestModel, @NonNull ManagedJobFactory managedJobFactory, @NonNull AsyncServerExecutor asyncServerExecutor) throws InvalidParameterException {
        super(serverConfig, rConvertRequestModel);
        this.serverManager = serverManager;
        this.cache = cache;
        this.managedJobFactory = managedJobFactory;
        this.asyncServerExecutor = asyncServerExecutor;
    }

    @Override
    public @NonNull Map<String, Object> execute() {
        var resultProperties = new HashMap<String, Object>();
        var method = rConvertRequestModel.getMethod();

        if (isNotEmpty(method)) {
            var jobProperties = getJobProperties();
            var filename = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            switch (method) {
                case "convert" -> processConvert(filename, jobProperties, resultProperties);
                default -> ServerManager.logWarn("Process method not supported: " + method);
            }
        }

        // Add error if for example method is empty or not valid
        if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.GENERAL.getErrorCode());
        }
        return resultProperties;
    }

    protected void processConvert(String filename, Map<String, Object> jobProperties, Map<String, Object> resultProperties) {
        byte[] resultBuffer = null;

        // try to get the result based on the given remote hash from the cache
        var cacheHash = (String)jobProperties.get(Properties.PROP_CACHE_HASH);
        if (isValidHash(cacheHash)) {
            try (var resultStm = cache.getCachedResult(cacheHash, filename, resultProperties, true)) {
                resultBuffer = ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);
            } catch (IOException e) {
                ServerManager.logExcp(e);
            }

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC server cache entry " + ((null != resultBuffer) ? "found" : "not found"),
                        new LogData("hash", cacheHash),
                        new LogData("filename", (null != filename) ? filename : "unkown"));
            }

            //
            if (resultBuffer != null && resultBuffer.length > 0) {
                final CacheServiceClient cacheServiceClient = serverManager.getCacheServiceClient();
                if (cacheServiceClient != null) {
                    final CacheServiceEntry cacheServiceEntry = new CacheServiceEntry(cacheHash, new ByteArrayInputStream(resultBuffer));
                    cacheServiceClient.addCacheServiceEntry(cacheServiceEntry, filename);
                }
            }
        }

        // if we we could not get the result from the cache, do the real conversion
        if (resultBuffer == null) {
            // if no result is found inside the cache, perform a real conversion
            var jobType = (String) jobProperties.get(Properties.PROP_JOBTYPE);

            resultProperties.clear();

            if (isNotEmpty(jobType)) {
                boolean[] isPDFSource = { false };
                var tmpInputFile = createTempInputFile(isPDFSource, jobProperties);

                if (tmpInputFile != null) {
                    var featuresId = rConvertRequestModel.getFeaturesId();

                    // if a 'featuresid' param is set, containing a valid combination (ORed)
                    // of feature ids, add this featuresId to the current job properties
                    if (featuresId != null && featuresId > 0) {
                        jobProperties.put(Properties.PROP_FEATURES_ID, featuresId);
                    }

                    // autoscale
                    if (jobType.startsWith("html") && rConvertRequestModel.getAutoScale()) {
                        jobProperties.put(Properties.PROP_AUTOSCALE, true);
                    }

                    // do the conversion with the given input data
                    jobProperties.put(Properties.PROP_INPUT_FILE, tmpInputFile);

                    // finally convert (either synchronously or asynchronously)
                    try (var resultStm = executeDocumentConverterConversion(jobType, cacheHash, jobProperties, resultProperties)) {
                        ensureValidResultPropertiesAndGetBuffer(resultStm, resultProperties);
                    } catch (IOException e) {
                        ServerManager.logExcp(e);
                    }

                    // remove the temp. input file
                    FileUtils.deleteQuietly(tmpInputFile);
                }
            }
        }
    }

    protected InputStream executeDocumentConverterConversion(@NonNull String jobType, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, @NonNull Map<String, Object> resultProperties) {
        InputStream result = null;
        var isAsyncConversion = MapUtils.getBooleanValue(jobProperties, Properties.PROP_ASYNC);

        if (isAsyncConversion) {
            asyncServerExecutor.triggerExecution(jobType, cacheHash, jobProperties, resultProperties);
            resultProperties.put(Properties.PROP_RESULT_ASYNC, true);
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.NONE.getErrorCode());
        } else {
            result = managedJobFactory.createManagedJob().process(jobType, cacheHash, jobProperties, resultProperties);
        }

        return result;
    }

    protected void addToMapIfNotNull(@NonNull String key, Object value, @NonNull Map<String, Object> map) {
        if (value != null) {
            map.put(key, value);
        }
    }

    protected @NonNull Map<String, Object> getJobProperties() {
        var properties = new HashMap<String, Object>();
        String curParam;

        properties.put(Properties.PROP_REMOTE_METHOD, rConvertRequestModel.getMethod());
        properties.put(Properties.PROP_JOBTYPE, rConvertRequestModel.getJobType());
        properties.put(Properties.PROP_PRIORITY, JobPriority.fromString(rConvertRequestModel.getPriority()));
        properties.put(Properties.PROP_IMAGE_SCALE_TYPE, rConvertRequestModel.getImageScaleType());

        addToMapIfNotNull(Properties.PROP_JOBID, rConvertRequestModel.getJobId(), properties);
        addToMapIfNotNull(Properties.PROP_LOCALE, rConvertRequestModel.getLocale(), properties);
        addToMapIfNotNull(Properties.PROP_FILTER_SHORT_NAME, rConvertRequestModel.getFilterShortName(), properties);
        addToMapIfNotNull(Properties.PROP_INPUT_TYPE, rConvertRequestModel.getInputType(), properties);
        addToMapIfNotNull(Properties.PROP_PIXEL_WIDTH, rConvertRequestModel.getPixelWidth(), properties);
        addToMapIfNotNull(Properties.PROP_PIXEL_HEIGHT, rConvertRequestModel.getPixelHeight(), properties);
        addToMapIfNotNull(Properties.PROP_MIME_TYPE, rConvertRequestModel.getMimeType(), properties);
        addToMapIfNotNull(Properties.PROP_PAGE_NUMBER, rConvertRequestModel.getPageNumber(), properties);
        addToMapIfNotNull(Properties.PROP_SHAPE_NUMBER, rConvertRequestModel.getShapeNumber(), properties);
        addToMapIfNotNull(Properties.PROP_ZIP_ARCHIVE, rConvertRequestModel.getZipArchive(), properties);
        addToMapIfNotNull(Properties.PROP_IMAGE_RESOLUTION, rConvertRequestModel.getImageResolution(), properties);
        addToMapIfNotNull(Properties.PROP_ASYNC, rConvertRequestModel.getAsync(), properties);
        addToMapIfNotNull(Properties.PROP_HIDE_CHANGES, rConvertRequestModel.getHideChanges(), properties);
        addToMapIfNotNull(Properties.PROP_HIDE_COMMENTS, rConvertRequestModel.getHideComments(), properties);
        addToMapIfNotNull(Properties.PROP_USER_REQUEST, rConvertRequestModel.getUserRequest(), properties);


        if (isValidHash(rConvertRequestModel.getCacheHash())) {
            properties.put(Properties.PROP_CACHE_HASH, rConvertRequestModel.getCacheHash());
        }

        // InputUrl
        if ((curParam = rConvertRequestModel.getInputUrl()) != null) {
            try {
                // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
                // get decoded as this will throw a not declared IllegalArgumentException.
                // In such cases, we replace the illegal hex value with a space char (%25).
                curParam = URLDecoder.decode(curParam.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), StandardCharsets.UTF_8);
            } catch (Exception e) {
                // TODO NEXT Is that a good idea to try to remove invalid hex values and if that produces an Exception only log this error and use the unchecked inputUrl
                ServerManager.logTrace("DC cannot decode given URL, using as is: " + curParam);
            }

            properties.put(Properties.PROP_INPUT_URL, curParam);
        }

        // PageRange
        if ((curParam = rConvertRequestModel.getPageRange()) != null) {
            properties.put(Properties.PROP_PAGE_RANGE, isValidPageRange(curParam) ? curParam : "1");
        }

        // InfoFilename
        if ((curParam = rConvertRequestModel.getInfoFileName()) != null) {
            try {
                // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
                // get decoded as this will throw a not declared IllegalArgumentException.
                // In such cases, we replace the illegal hex value with a space char (%25).
                curParam = URLDecoder.decode(curParam.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), StandardCharsets.UTF_8);
            } catch (Exception e) {
                // TODO NEXT Is that a good idea to try to remove invalid hex values and if that produces an Exception only log this error and use the unchecked infofilename
                ServerManager.logTrace("DC cannot decode given URL, using as is: " + curParam);
            }
            properties.put(Properties.PROP_INFO_FILENAME, curParam);
        }



        if ((curParam = rConvertRequestModel.getShapeReplacements()) != null) {
            properties.put(Properties.PROP_SHAPE_REPLACEMENTS, URLDecoder.decode(curParam, StandardCharsets.UTF_8));
        }

        ServerManager.ensureInputTypeSet(properties);

        return properties;
    }

    protected static byte[] ensureValidResultPropertiesAndGetBuffer(InputStream resultStm, @NonNull Map<String, Object> resultProperties) throws IOException {
        byte[] resultBuffer = null;

        // ensure that the property PROP _RESULT_BUFFER is set in success case
        if (resultStm != null) {
            resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

            if (resultBuffer == null) {
                resultBuffer = IOUtils.toByteArray(resultStm);
                resultProperties.put(Properties.PROP_RESULT_BUFFER, resultBuffer);
            }
        }

        // ensure, that the property PROP_RESULT_ERROR_CODE is always set
        var resultErrorCode = (Integer) resultProperties.get(Properties.PROP_RESULT_ERROR_CODE);

        if (resultBuffer == null) {
            // set JobError.GENERAL as default, if no jobError is set by now
            if (resultErrorCode == null) {
                resultErrorCode = JobError.GENERAL.getErrorCode();
            }
        } else if (resultBuffer.length < 1) {
            // set JobError.GENERAL as default, if no jobError is set by now
            if (resultErrorCode == null) {
                resultErrorCode = JobError.NO_CONTENT.getErrorCode();
            }

            resultBuffer = null;
        } else {
            resultErrorCode = JobError.NONE.getErrorCode();
        }

        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, resultErrorCode);

        return resultBuffer;
    }

    public static void implHandleResponseSize(@NonNull String msg, long size) {
        var sizeMB = size >> 20;

        if (size >= RESPONSE_SIZE_WARN_THRESHOLD_BYTES) {
            ServerManager.logWarn("DC webservice is returning large request response with content of size: " +
                    size + " Bytes (" +
                    sizeMB + " MegaBytes)");

        }

        if (ServerManager.isLogTrace()) {
            ServerManager.logTrace(msg + ": " +
                    size + " Bytes (" +
                    sizeMB + " MegaBytes)");
        }
    }
}
