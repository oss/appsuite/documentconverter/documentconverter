/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.sun.star.beans.XPropertySet;
import com.sun.star.uno.UnoRuntime;
import jakarta.annotation.Nullable;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@link REThread}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * @author ka
 *
 */
class REThread extends JobThread {

    private static final AtomicInteger RE_THREAD_NUMBER = new AtomicInteger(0);

    private static final Map<JobError, Integer> RE_ERROR_RUN_COUNT_MAP = new HashMap<>();

    static {
        // initialize with default runs of 1
        RE_ERROR_RUN_COUNT_MAP.putAll(ServerManager.getDefaultSupportedBackendRuns());

        // set run count to 2 for JobError.GENERAL, JobError.DISPOSED
        RE_ERROR_RUN_COUNT_MAP.put(JobError.GENERAL, 2);
        RE_ERROR_RUN_COUNT_MAP.put(JobError.DISPOSED, 2);
    }

    private final int reThreadNumber = RE_THREAD_NUMBER.incrementAndGet();

    private final REInstanceProvider instanceProvider;

    private REInstance instance = null;

    private String locale = null;

    private long connectTimeout = REInstance.CONNECT_TIMEOUT_MS;

    /**
     * Initializes a new {@link REThread}.
     *
     * @param jobProcessor
     * @param cache
     * @param watchdog
     */
    REThread(
        @NonNull ServerConfig serverConfig,
        @NonNull ServerManager serverManager,
        @NonNull JobProcessor jobProcessor,
        @NonNull REInstanceProvider instanceProvider,
        @NonNull Cache cache,
        @NonNull ErrorCache errorCache,
        @NonNull Watchdog watchdog,
        @NonNull Statistics statistics) {
        super(serverConfig, serverManager, jobProcessor, BackendType.READERENGINE, cache, errorCache, watchdog, statistics);

        setName(getLogBuilder().append("#").append(reThreadNumber).toString());

        this.instanceProvider = instanceProvider;

        synchronized (RE_ERROR_RUN_COUNT_MAP) {
            RE_ERROR_RUN_COUNT_MAP.put(JobError.TIMEOUT, serverConfig.getJobRestartAfterTimeout() ? 2 : 1);
        }
    }

    // - JobThread -------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#getSupportedBackendRuns()
     */
    @Override
    Map<JobError, Integer> getSupportedBackendRuns() {
        return RE_ERROR_RUN_COUNT_MAP;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#isDisposed()
     */
    @Override
    public boolean isDisposed() {
        final REInstance currentInstance = getCurrentInstance();

        return (null == currentInstance) || currentInstance.isDisposed();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#createCopy()
     */
    @Override
    protected JobThread createCopy() {
        return new REThread(serverConfig, serverManager, jobProcessor, instanceProvider, cache, errorCache, watchdog, statistics);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#shutdownCurrentInstance()
     */
    @Override
    protected synchronized void shutdownCurrentInstance() {
        if (null != instance) {
            instance.kill();
            instance = null;
        }

        super.shutdownCurrentInstance();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#prepareProcessingJob()
     */
    @Override
    protected boolean prepareProcessingJob() {
        var currentInstance = getOrCreateInstance();
        var ret = (null != currentInstance);

        if (ret) {
            // reset the set instance local to null (=> use default locale)
            locale = null;

            // set readerEngine instance as execution data,
            // if we have a valid readerEngine instance
            setJobExecutionData(currentInstance);
        }

        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#handlePrepareProcessingJobFailed()
     */
    @Override
    protected void handlePrepareProcessingJobFailed() {
        if (isRunning()) {
            if (ServerManager.isLogError()) {
                ServerManager.logError(getLogBuilder().
                    append("Engine#").append(reThreadNumber).append(" could not be launched => trying again in ").
                    append(connectTimeout / 1000).append('s').toString());
            }

            ServerManager.sleepThread(connectTimeout, true);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#setLocale(java.lang.String)
     */
    @Override
    protected String setLocale(String jobLocale) {
        var curLocale = super.setLocale(jobLocale);

        if ((null == locale) || !locale.equalsIgnoreCase(curLocale)) {
            var currentInstance = getCurrentInstance();

            if (null != currentInstance) {
                var serviceFactory = currentInstance.getComponentFactory();
                var serviceContext = currentInstance.getDefaultContext();

                if ((null != serviceFactory) && (null != serviceContext)) {
                    try {
                        var xLocaleService = UnoRuntime.queryInterface(
                            XPropertySet.class,
                            serviceFactory.createInstanceWithContext(
                                "com.sun.star.config.OxLocaleService", serviceContext));

                        if (null != xLocaleService) {
                            xLocaleService.setPropertyValue("OxLocale", curLocale.replace('_', '-'));
                            locale = curLocale;
                        }
                    } catch (final Exception e) {
                        ServerManager.logExcp(e);
                    }
                }
            }

            if (null == locale) {
                locale = "en_US";
            }
        }

        return locale;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#endProcessingJob(boolean)
     */
    @Override
    protected synchronized void endProcessingJob(JobEntry curJobEntry) {
        if (null != curJobEntry) {
            var currentInstance = getCurrentInstance();

            if (null != currentInstance) {

                // always restart instance in error case or
                // if max. number of conversions are finished
                if (curJobEntry.getJobErrorEx().hasError() ||
                    (currentInstance.getRestartCount() < 2) ||
                    (0 == (getInstanceJobsProcessed() % currentInstance.getRestartCount()))) {

                    shutdownCurrentInstance();
                }
            }
        }

        setJobExecutionData(null);

        super.endProcessingJob(curJobEntry);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.server.impl.JobThread#getRestartCount()
     */
    @Override
    protected int getRestartCount() {
        var currentInstance = getCurrentInstance();

        return (null != currentInstance) ?
            currentInstance.getRestartCount() :
            super.getRestartCount();

    }

    /**
     * @return
     */
    @Nullable
    private synchronized REInstance getOrCreateInstance() {
        return isRunning() ?
            (null != instance) ? instance : (instance = instanceProvider.getREInstance()) :
            null;
    }

    /**
     * @return
     */
    @Nullable
    private synchronized REInstance getCurrentInstance() {
        return instance;
    }

}
