/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.DocumentConverterUtil;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.openexchange.documentconverter.server.watchdog.WatchdogHandler;
import com.openexchange.documentconverter.server.watchdog.WatchdogMode;
import lombok.NonNull;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

// TODO NEXT Implement it
/**
 * {@link JobMonitor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.3
 */
public class JobMonitor {

    // JobMonitor timer name
    private static final String DC_JOBMONITOR_TIMER_NAME = "DC JobMonitor timer";

    // JobMonitor default timer period
    private static final long DC_MONITOR_TIMER_PERIOD_MILLIS =  60 * 1000L;

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    private final ScheduledExecutorService logTimerExecutor = Executors.newScheduledThreadPool(1);

    private final JobProcessor jobProcessor;

    private final Watchdog queueWatchdog;

    private final Map<String, JobEntry> scheduledJobEntryMap = new ConcurrentHashMap<>();

    private final Map<String, WatchdogHandler> watchdogHandlerMap = new ConcurrentHashMap<>();

    private final AtomicBoolean queueLimitReached = new AtomicBoolean(false);

    private final Map<JobPriority, AtomicLong> removedJobsPriorityMap = new HashMap<>();

    private final int queueCountLimitHigh;

    private final int queueCountLimitLow;

    private final long queueTimeoutMillis;

    private long lastLogTimestampMillis = 0;

    private Timer logTimer = null;

    // JobMonitor timer period - member is needed for unit-testing
    private long monitorTimerPeriodMillis;

    private final Statistics statistics;

    /**
     * Initializes a new {@link JobMonitor}.
     * @param jobProcessor the job processor instance that created this job monitor
     * @param queueCountLimitHigh the high limit for the queue count
     * @param queueCountLimitLow the low limit for the queue count
     * @param queueTimeoutSeconds the timeout in seconds for the queue
     */
    public JobMonitor(
            @NonNull JobProcessor jobProcessor,
            int queueCountLimitHigh,
            int queueCountLimitLow,
            int queueTimeoutSeconds,
            @NonNull Statistics statistics) {
        this(jobProcessor, queueCountLimitHigh, queueCountLimitLow, queueTimeoutSeconds, DC_MONITOR_TIMER_PERIOD_MILLIS, statistics);
    }

    /**
     * For unit-testing only.
     * @param jobProcessor the job processor instance that created this job monitor
     * @param queueCountLimitHigh the high limit for the queue count
     * @param queueCountLimitLow the low limit for the queue count
     * @param queueTimeoutSeconds the timeout in seconds for the queue
     * @param monitorTimerPeriodMillis the period of the monitor timer
     */
    protected JobMonitor(
        @NonNull JobProcessor jobProcessor,
        int queueCountLimitHigh,
        int queueCountLimitLow,
        int queueTimeoutSeconds,
        long monitorTimerPeriodMillis,
        @NonNull Statistics statistics) {

        this.jobProcessor = jobProcessor;

        this.queueCountLimitHigh = queueCountLimitHigh;
        this.queueCountLimitLow = queueCountLimitLow;

        this.queueTimeoutMillis = queueTimeoutSeconds * 1000L;
        this.monitorTimerPeriodMillis = monitorTimerPeriodMillis;

        this.statistics = statistics;
        // initialize time based jobs removed priority map
        for (var curJobPriority : JobPriority.values()) {
            removedJobsPriorityMap.put(curJobPriority, new AtomicLong(0));
        }

        if (queueTimeoutMillis > 0) {
            logTimerExecutor.scheduleWithFixedDelay(this::logQueueTimeoutLimitSummary,
                this.monitorTimerPeriodMillis, this.monitorTimerPeriodMillis, TimeUnit.MILLISECONDS);
        }

        queueWatchdog = new Watchdog("DC JobMonitor watchdog", queueTimeoutMillis, WatchdogMode.TIMEOUT);
        queueWatchdog.start();

        startTimer(this.monitorTimerPeriodMillis);

        // log status of job monitoring at startup
        if (ServerManager.isLogInfo()) {
            // count limit
            var logStrBuilder = new StringBuilder(256).
                append("DC JobMonitor count based queue limiting").
                append(' ').append(isCountLimitEnabled() ? "enabled" : "disabled").
                append(" (").
                append("queueCountLimitHigh: ").append(queueCountLimitHigh).
                append(", ").
                append("queueCountLimitLow: ").append(queueCountLimitLow).
                append(")");

            ServerManager.logInfo(logStrBuilder.toString());

            // count limit
            logStrBuilder.setLength(0);
            logStrBuilder.append("DC JobMonitor timeout based queue limiting").
                append(' ').append(isTimeoutLimitEnabled() ? "enabled" : "disabled").
                append(" (").
                append("queueTimeoutSeconds: ").append(queueTimeoutSeconds).
                append(")");

            ServerManager.logInfo(logStrBuilder.toString());
        }
    }

    public synchronized void jobScheduled(@NonNull final JobEntry jobEntry) {
        var jobId = jobEntry.getJobId();

        scheduledJobEntryMap.put(jobId, jobEntry);

        // remove old watchdog handler, if still set
        implRemoveWatchdogHandler(jobId);

        if (isCountLimitEnabled() &&
            (statistics.incrementScheduledJobCountInQueue() == queueCountLimitHigh) &&
            queueLimitReached.compareAndSet(false, true)) {

            // TODO: Reminder we removed lt statistics
            // statistics.incrementQueueCountLimitHighReached();

            // log
            logIfQueueLimitReached(System.currentTimeMillis(), true);
        }

        if (isTimeoutLimitEnabled()) {
            // add new watchdog handler for jobId to map in order to identify the
            // handler for this jobId, e.g. if scheduling of this job has been
            // finished and the appropriate watchdogHandler needs to be removed
            WatchdogHandler watchdogHandler = (curTimeMillis, startTimeMillis, jobError) -> removeScheduledJob(jobId, curTimeMillis, startTimeMillis);

            watchdogHandlerMap.put(jobId, watchdogHandler);
            queueWatchdog.addWatchdogHandler(watchdogHandler);
        }
    }

    public synchronized void jobSchedulingFinished(@NonNull final JobEntry jobEntry) {
        var jobId = jobEntry.getJobId();

        // remove WatchdogHandler for this id from a previous scheduling
        implRemoveWatchdogHandler(jobId);

        if (isCountLimitEnabled() &&
            (statistics.decrementScheduledJobCountInQueue() == queueCountLimitLow) &&
            queueLimitReached.compareAndSet(true, false)) {

            // TODO: Reminder we removed lt statistics
            // statistics.incrementQueueCountLimitLowReached();

            // log
            ServerManager.logInfo("DC JobMonitor JobCountInQueueLimitLow reached => requests are accepted again " +
                '(' + statistics.getScheduledJobCountInQueue() + " entries currently scheduled)");
        }

        scheduledJobEntryMap.remove(jobId);
    }

    /**
     *
     */
    public boolean isQueueLimitReached() {
        return queueLimitReached.get() || !isRunning.get();
    }

    // - Public API ------------------------------------------------------------

    /**
     * terminate
     *
     */
    public void terminate() {
        if (isRunning.compareAndSet(true, false)) {
            var shutdownStartTime = 0L;
            var trace = ServerManager.isLogTrace();

            if (trace) {
                shutdownStartTime = System.currentTimeMillis();
                ServerManager.logTrace("DC JobMonitor starting shutdown...");
            }

            logTimerExecutor.shutdownNow();

            synchronized (this) {
                // cancel and destroy running timer
                if (null != logTimer) {
                    logTimer.cancel();
                    logTimer = null;
                }

                // we only remove all leftover watchdog handlers here from the
                // watchdog since complete removal of the scheduled jobs would
                // interfere with the current iteration when the corresponding
                // #jobSchedulingFinished method is indirectly called within this loop
                watchdogHandlerMap.forEach((curJobId, curWatchdogHandler) -> queueWatchdog.removeWatchdogHandler(curWatchdogHandler));

                watchdogHandlerMap.clear();
            }

            // terminate queue watchdog
            queueWatchdog.terminate();

            scheduledJobEntryMap.clear();

            if (trace) {
                ServerManager.logTrace("DC JobMonitor finished shutdown: " +
                    (System.currentTimeMillis() - shutdownStartTime) + "ms!");
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * Schedule new single shot timer with given timeout value
     *
     * @param timeoutMillis the timeout in milliseconds
     */
    private void startTimer(long timeoutMillis) {
        var timerTask = new TimerTask() {
            @Override
            public void run() {
                if (isRunning.get()) {
                    var curTimeMillis = System.currentTimeMillis();

                    // log warning if queue limit is currently reached
                    logIfQueueLimitReached(curTimeMillis, false);

                    // restart timer with a new, calculated timeout
                    startTimer(Math.max(DC_MONITOR_TIMER_PERIOD_MILLIS - (curTimeMillis - lastLogTimestampMillis), 0));
                }
            }
        };

        synchronized (this) {
            if (null != logTimer) {
                logTimer.cancel();
            }

            logTimer = new Timer(DC_JOBMONITOR_TIMER_NAME, true);
            logTimer.schedule(timerTask, timeoutMillis);
        }
    }

    /**
     * @param jobId the job id of the job to be removed from the watchdog map
     */
    private void implRemoveWatchdogHandler(@NonNull final String jobId) {
        var watchdogHandler = watchdogHandlerMap.remove(jobId);

        if (null != watchdogHandler) {
            queueWatchdog.removeWatchdogHandler(watchdogHandler);
        }
    }

    /**
     * !!! Method needs to by synchronized <=
     * !!! it is called from within a lambda expression and would give deadlock otherwise !!!
     *
     * @param jobId the job id of the job to be removed from the scheduled job map
     */
    private synchronized void removeScheduledJob(@NonNull String jobId,long curTimeMillis, long startTimeMillis) {
        var jobEntry = scheduledJobEntryMap.get(jobId);
        var jobPriority = (null != jobEntry) ? jobEntry.getJobPriority() : null;
        var filename = (null != jobEntry) ? (String) jobEntry.getJob().getJobProperties().get(Properties.PROP_INFO_FILENAME) : DocumentConverterUtil.STR_NOT_AVAILABLE;

        if (null != jobPriority) {
            removedJobsPriorityMap.get(jobPriority).incrementAndGet();
        }

        // The watchdog handler is called from a different thread than the one
        // that scheduled the job, so that calling JobProcessor#removeScheduledJob
        // is ok and  no additional async. removal is necessary
        if (jobProcessor.removeScheduledJob(jobId)) {
            // TODO: Reminder we removed lt statistics
            // statistics.incrementQueueTimeoutCount();

            // log single removal
            ServerManager.logInfo("DC JobMonitor removed scheduled entry from qeue after " + ((curTimeMillis - startTimeMillis) / 1000L) + "s",
                new LogData("jobId", jobId),
                new LogData("filename", filename));
        }

        // remove entry from scheduled job entry map
        scheduledJobEntryMap.remove(jobId);
    }

    /**
     *
     * @return return true if the count limit is enabled
     */
    private boolean isCountLimitEnabled() {
        return (queueCountLimitHigh > 0);
    }

    /**
     *
     * @return return true if the timeout limit is enabled
     */
    private boolean isTimeoutLimitEnabled() {
        return (queueTimeoutMillis > 0);
    }

    /**
     *
     * @param curTimeMillis the current time in milliseconds
     * @param forceLogging if true, the log will be written regardless of the last log timestamp
     */
    private synchronized void logIfQueueLimitReached(final long curTimeMillis, final boolean forceLogging) {
        if (forceLogging || ((curTimeMillis - lastLogTimestampMillis) >= this.monitorTimerPeriodMillis)) {
            if (queueLimitReached.get()) {
                ServerManager.logWarn("DC JobMonitor JobCountInQueueLimitHigh reached => requests are rejected " +
                    '(' + statistics.getScheduledJobCountInQueue() + " entries currently scheduled)");
            }

            lastLogTimestampMillis = curTimeMillis;
        }
    }

    /**
     *
     */
    private void logQueueTimeoutLimitSummary() {
        var logDataList = new ArrayList<>(removedJobsPriorityMap.size() + 1);
        var summedUpCount = 0L;

        for (var curJobPriority : JobPriority.values()) {
            var curCounter = removedJobsPriorityMap.get(curJobPriority);
            var curCount = curCounter.get();

            if (curCount > 0) {
                curCounter.set(0);
                summedUpCount += curCount;
            }

            logDataList.add(new LogData(curJobPriority.toString().toLowerCase(), Long.toString(curCount)));
        }

        if (summedUpCount > 0) {
            logDataList.add(new LogData("total", Long.toString(summedUpCount)));

            ServerManager.logWarn(
                "DC JobMonitor removed scheduled entries from queue after " + (queueTimeoutMillis / 1000L) +
                    "s during last observation period of " + (DC_MONITOR_TIMER_PERIOD_MILLIS / 1000L) + "s",
                logDataList.toArray(new LogData[0]));
        }
    }

}
