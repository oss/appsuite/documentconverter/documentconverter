/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import lombok.Getter;
import lombok.NonNull;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

// TODO NEXT check implementation - may be something missing?
public class JobEntry {

    @Getter
    private final String jobId;

    private JobPriority jobPriority = JobPriority.MEDIUM;

    @Getter
    private final JobStatusListener jobStatusListener;

    @Getter
    private final Job job;

    private JobStatus jobStatus = JobStatus.INVALID;

    private JobErrorEx jobErrorEx = new JobErrorEx();

    private final long[] statusTimeMillis = new long[JobStatus.values().length];

    private final AtomicBoolean cacheHit = new AtomicBoolean(false);

    private final AtomicBoolean errorCacheHit = new AtomicBoolean(false);

    private final AtomicBoolean multipleSchedule = new AtomicBoolean(false);

    public JobEntry(@NonNull String jobId, @NonNull Job serverJob, @NonNull JobPriority jobPriority, @NonNull JobStatusListener jobStatusListener) {
        this.jobId = jobId;
        this.jobPriority = jobPriority;
        this.job = serverJob;
        this.jobStatusListener = jobStatusListener;
        setJobStatus(JobStatus.NEW);
        serverJob.getJobProperties().put(Properties.PROP_JOBID, jobId);
    }

    /**
     * @return The hash of the Job
     */
    public String getHash() {
        return job.getHash();
    }

    /**
     * @return The identifier of the Job for the queue
     */
    public String getQueueIdentifier() {
        final String hash = getHash();

        return (Cache.isValidHash(hash) ? hash : getJobId());
    }

    /**
     * @return The properties of the Job
     */
    public Map<String, Object> getJobProperties() {
        return job.getJobProperties();
    }

    /**
     * @return The properties of the result of the processed job
     */
    public Map<String, Object> getResultProperties() {
        return job.getResultProperties();
    }

    /**
     * @return the current status of the Job
     */
    @NonNull
    public synchronized JobStatus getJobStatus() {
        return jobStatus;
    }

    public synchronized void setJobStatus(JobStatus status) {
        if (status != jobStatus) {
            jobStatus = status;

            // the timestamp of a new status will only be tracked once
            if (0 == statusTimeMillis[status.ordinal()]) {
                statusTimeMillis[status.ordinal()] = System.currentTimeMillis();
            } else if (JobStatus.SCHEDULED == status) {
                // set flag that SCHEDULE status has been set more than once
                multipleSchedule.compareAndSet(false, true);
            }
        }
    }

    /**
     * @return the current status of the Job
     */
    public synchronized JobErrorEx getJobErrorEx() {
        return jobErrorEx;
    }

    public synchronized void setJobErrorEx(JobErrorEx jobErrorEx) {
        this.jobErrorEx = jobErrorEx;
    }

    @NonNull
    public JobPriority getJobPriority() {
        return jobPriority;
    }

    /**
     * @return The point of time, at which this entry has been scheduled for the first time
     */
    public long getScheduledAtTimeMillis() {
        return statusTimeMillis[JobStatus.SCHEDULED.ordinal()];
    }

    /**
     * @return The duration of the job in queue before running, if job has been finished; 0 otherwise
     */
    public long getQueueTimeMillis() {
        final long finishedTimeMillis = Math.max(statusTimeMillis[JobStatus.FINISHED.ordinal()], statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ?  (statusTimeMillis[JobStatus.RUNNING.ordinal()] - statusTimeMillis[JobStatus.SCHEDULED.ordinal()]) : 0);
    }

    /**
     * @return The pure conversion duration of the job, if job has been finished; 0 otherwise
     */
    public long getConversionTimeMillis() {
        final long finishedTimeMillis = Math.max(statusTimeMillis[JobStatus.FINISHED.ordinal()], statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ? (finishedTimeMillis -  statusTimeMillis[JobStatus.RUNNING.ordinal()]) : 0);
    }

    /**
     * @return The complete processing duration of the job from adding to removing, if job has been finished; 0 otherwise
     */
    public long getJobTimeMillis() {
        final long finishedTimeMillis = Math.max(statusTimeMillis[JobStatus.FINISHED.ordinal()], statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ? (finishedTimeMillis -  statusTimeMillis[JobStatus.SCHEDULED.ordinal()]) : 0);
    }

    public void setCacheHit() {
        cacheHit.compareAndSet(false, true);
    }

    /**
     * @return True if the job could be resolved from cache
     */
    public boolean isCacheHit() {
        return cacheHit.get();
    }

    public synchronized void setErrorCacheHit(JobErrorEx jobErrorExToSet) {
        if (jobErrorExToSet.hasError()) {
            this.jobErrorEx = jobErrorExToSet;
            errorCacheHit.compareAndSet(false, true);
        }
    }

    /**
     * @return True if the job could be resolved from errorcache
     */
    public boolean isErrorCacheHit() {
        return errorCacheHit.get();
    }

    /**
     * @return True if the job has been scheduled more than once
     */
    public boolean isMultipleSchedule() {
        return multipleSchedule.get();
    }

    /**
     * @return The locale to be used to execute the job or the standard locale
     */
    public String getJobLocale() {
        var jobProperties = job.getJobProperties();
        String locale = null;

        if (null != jobProperties) {
            locale = (String) jobProperties.get(Properties.PROP_LOCALE);
        }

        return ((null != locale) && (!locale.isEmpty())) ? locale : "en_US";
    }

}
