/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Vector;

@Slf4j
@Service
@Scope("singleton")
public class EngineGroupFactory {

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ServerManager serverManager;

    @Autowired
    private Cache cache;

    @Autowired
    private ErrorCache errorCache;

    @Autowired
    private Statistics statistics;

    public EngineGroup createEngineGroup(@NonNull BackendType engineType, int engineCount, @NonNull JobProcessor jobProcessor, @NonNull REInstanceProvider instanceProvider, @NonNull Watchdog jobWatchdog) {
        Vector<JobThread> engineVector = new Vector<>(engineCount);

        for (int i = 0; i < engineVector.capacity(); ++i) {
            engineVector.add(createJobThread(engineType, jobProcessor, instanceProvider, cache, errorCache, jobWatchdog, statistics));
        }

        return new EngineGroup(engineVector, new JobEntryQueue(jobProcessor));
    }

    private JobThread createJobThread(BackendType engineType, JobProcessor jobProcessor, REInstanceProvider instanceProvider, Cache cache, ErrorCache errorCache, Watchdog jobWatchdog, Statistics statistics) {
        if (BackendType.PDFTOOL.equals(engineType)) {
            return new PDFToolThread(serverConfig, serverManager, jobProcessor, cache, errorCache, jobWatchdog, statistics);
        } else if (BackendType.READERENGINE.equals(engineType)) {
            return new REThread(serverConfig, serverManager, jobProcessor, instanceProvider, cache, errorCache, jobWatchdog, statistics);
        } else {
            throw new IllegalArgumentException("Unknown engine type: " + engineType);
        }
    }

}
