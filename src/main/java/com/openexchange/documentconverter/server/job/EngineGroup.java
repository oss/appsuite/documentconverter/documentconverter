/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.error.ExceptionUtils;
import lombok.NonNull;
import com.openexchange.documentconverter.server.ServerManager;

import java.util.Vector;

public class EngineGroup {

    protected Vector<JobThread> engineVector;

    protected JobEntryQueue entryQueue;

    /**
     * Initializes a new {@link EngineGroup}.
     *
     * @param engineVector
     */
    EngineGroup(@NonNull Vector<JobThread> engineVector, @NonNull JobEntryQueue entryQueue) {
        super();

        this.engineVector = engineVector;
        this.entryQueue = entryQueue;
    }

    /**
     * @return The name of the supported backend engine type
     */
    String getGroupName() {
        return !engineVector.isEmpty() ? engineVector.get(0).getBackendType().toString().toUpperCase() : "Unknown";
    }

    /**
     * Retrieve the job entry queue
     *
     * @return the job entry queue
     */
    JobEntryQueue getJobEntryQueue() {
        return entryQueue;
    }

    /**
     * @return
     */
    @NonNull
    Vector<JobThread> getJobThreads() {
        return engineVector;
    }

    /**
     *
     */
    void start() {
        for (var curThread : engineVector) {
            curThread.start();
        }
    }

    /**
     *
     */
    void terminate() {
        var groupName = getGroupName();
        var trace = ServerManager.isLogTrace();
        var traceStartTimeMillis = 0L;

        if (trace) {
            traceStartTimeMillis = System.currentTimeMillis();
            ServerManager.logTrace("DC starting shutdown of EngineGroup " + groupName + ": ...");
        }

        entryQueue.terminate();

        for (var curThread : engineVector) {
            try {
                curThread.terminate();
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);
                ServerManager.logExcp(new Exception(e));
            }

            try {
                curThread.join();
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);
            }
        }

        if (trace) {
            ServerManager.logTrace("DC finished shutdown of EngineGroup " + groupName + ": " +
                (System.currentTimeMillis() - traceStartTimeMillis) + "ms!");
        }

    }

}
