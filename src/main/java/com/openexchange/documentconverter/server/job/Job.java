/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import jakarta.annotation.Nullable;
import lombok.NonNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

public interface Job {
    /**
     * @return The type of engine, the current job is assigned to
     */
    @NonNull BackendType backendTypeNeeded();

    /**
     * This method will be called in order to let the job do the real work
     *
     * @param jobExecutionData the necessary execution data for the job
     * @return The converter status of the job
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @NonNull ConverterStatus execute(@Nullable Object jobExecutionData) throws InterruptedException, IOException, MalformedURLException, Exception;

    /**
     * Will be called, if the engine, used by the implementation should abort the current job
     */
    void kill() throws Exception;

    /**
     * @return The hash value, upon which this job (-result) can be uniquely identified
     */
    @NonNull String getHash();

    /**
     * @param jobProperties Additional job properties to be added to this job; may be called more than once
     */
    void addJobProperties(@NonNull Map<String, Object> jobProperties);

    /**
     * @return The current job properties
     */
    @NonNull Map<String, Object> getJobProperties();

    /**
     * @return The current result properties
     */
    @NonNull Map<String, Object> getResultProperties();

    /**
     * @return true if the job does produce something that is cacheable. A async shape2png job for instance already
     * has its results cached.
     */
    boolean producesCacheableResult();
}
