/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.cache.Cache;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// TODO JobEntryQueue a component or not? Looks more like a "normal" class
/**
 * {@link JobEntryQueue}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public class JobEntryQueue {

    private final JobProcessor jobProcessor;

    private final ReentrantLock lock = new ReentrantLock(true);

    private final Condition notEmptyCondition = lock.newCondition();

    private final List<JobEntry> scheduled = new ArrayList<>();

    private final List<JobEntry> processing = new ArrayList<>();

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    /**
     * Initializes a new {@link JobEntryQueue}.
     */
    public JobEntryQueue(JobProcessor jobProcessor) {
        super();
        this.jobProcessor = jobProcessor;
    }

    // - Public interface ------------------------------------------------------

    /**
     * Terminate the queue
     */
    public void terminate() {
        if (isRunning.compareAndSet(true, false)) {
            try {
                lock.lock();
                notEmptyCondition.signal();
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * @return true, if the queue is running, otherwise false
     */
    public boolean isRunning() {
        return isRunning.get();
    }

    /**
     * @return The number of entries in the queue (scheduled and processing)
     */
    public int size() {
        try {
            lock.lock();
            return (scheduled.size() + processing.size());
        } finally {
            lock.unlock();
        }
    }

    /**
     * @param jobEntry The job to be scheduled for execution
     */
    public void schedule(@NonNull JobEntry jobEntry) {
        if (isRunning()) {
            final JobPriority jobPriority = jobEntry.getJobPriority();

            try {
                lock.lock();

                boolean entryAdded = false;

                // Entries with the lowest priority will always be added to end of list,
                // other entries need to be inserted at correct, priority based position
                if (JobPriority.lowest() != jobPriority) {
                    final ListIterator<JobEntry> listIter = scheduled.listIterator();

                    // insert entry at priority based position
                    while (listIter.hasNext()) {
                        final JobEntry curEntry = listIter.next();

                        // If the given jobPriority is higher than current jobPriority,
                        // insert given entry just before the current entry. This
                        // ensures, that entries are always added in a priority based
                        // order, beginning from the highest order to the lowest order and FIFO
                        if (jobPriority.compareTo(curEntry.getJobPriority()) > 0) {
                            listIter.previous();
                            listIter.add(jobEntry);
                            entryAdded = true;
                            break;
                        }
                    }
                }

                // add entry to end of list, if it was not already added in a prioritized way before
                if (!entryAdded) {
                    scheduled.add(jobEntry);
                }
            } finally {
                lock.unlock();
            }

            // status update SCHEDULED needs to be called out of sync block
            jobProcessor.updateServerJobStatus(jobEntry, JobStatus.SCHEDULED);

            // signal notEmptyCondition for threads, waiting for an entry to become available
            try {
                lock.lock();
                notEmptyCondition.signal();
            } finally {
                lock.unlock();
            }

        }
    }

    /**
     * @param jobId The job's id
     * @return The job entry with the given id or null, if no such entry exists
     */
    public JobEntry get(@NonNull String jobId) {
        try {
            lock.lock();
            return find(other -> jobId.compareTo(other.getJobId()), scheduled, processing);
        } finally {
            lock.unlock();
        }
    }

    /**
     * @param jobId The job's id
     * @return the removed job entry or null, if no such entry exists
     */
    public JobEntry remove(@NonNull String jobId, boolean scheduledOnly) {
        JobEntry ret;

        try {
            lock.lock();

            // check jobEntry process list first, since we can
            // assume, that most of all entries are removed, when they
            // will have reached the processing state and this
            // #remove method is eventually called after processing
            ret = scheduledOnly ?
                doRemove(jobId, scheduled) :
                doRemove(jobId, processing, scheduled);
        } finally {
            lock.unlock();
        }

        // perform status update for each entry out of sync block, since
        // entries might be removed from their original lists during the update
        if (null != ret) {
            jobProcessor.updateServerJobStatus(ret, JobStatus.REMOVED);
        }

        return ret;
    }


    /**
     * @param jobEntry The job entry to receive the updated status
     * @param jobStatus The job's status
     */
    public void finishedJobEntry(@NonNull JobEntry jobEntry, @NonNull JobStatus jobStatus) {
        jobProcessor.updateServerJobStatus(jobEntry, jobStatus);
    }

    /**
     * Retrieves the next job entry to process and blocks until the scheduled queue is not empty
     * @return The next job entry to process or null, if no such entry exists
     */
    public JobEntry getNextJobEntryToProcess() {
        JobEntry nextJobEntry = null;

        try {
            lock.lock();

            while (isRunning() && (scheduled.isEmpty())) {
                try {
                    notEmptyCondition.await();
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted
                    // TODO does this makes sense - should we not return from this method?
                }
            }

            final ListIterator<JobEntry> listIter = scheduled.listIterator();

            while(listIter.hasNext()) {
                var curEntry = listIter.next();

                // use entry and move entry from scheduled list to processing list,
                // if the job's hash is not currently processed otherwise;
                if ((null != curEntry) && !isProcessing(curEntry.getHash())) {
                    listIter.remove();
                    processing.add(curEntry);
                    nextJobEntry = curEntry;
                    break;
                }
            }
        } finally {
            lock.unlock();
        }

        // perform status update for each entry out of sync block, since
        // entries might be removed from their original lists during the update
        if (null != nextJobEntry) {
            jobProcessor.updateServerJobStatus(nextJobEntry, JobStatus.RUNNING);
        }

        return nextJobEntry;
    }

    // - Implementation ----------------------------------------------------

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param hash the hash of the JobEntry to check
     * @return true, if the job entry with the given hash is currently being
     * processed, otherwise false
     */
    private boolean isProcessing(@NonNull String hash) {
        return (Cache.isValidHash(hash) && (null != find(compareJobEntry -> {
            var compareHash = compareJobEntry.getHash();
            return (Cache.isValidHash(compareHash) ? hash.compareTo(compareHash) : 1);
        }, processing)));
    }

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param findPredicate the predicate to find the job entry
     * @param lists the lists to search for the job entry
     * @return the found job entry or null, if no such entry exists
     */
    @SafeVarargs
    private JobEntry find(@NonNull Comparable<JobEntry> findPredicate, @NonNull List<JobEntry>... lists) {
        for (var curList : lists) {
            for (var curEntry : curList) {
                if (0 == findPredicate.compareTo(curEntry)) {
                    return curEntry;
                }
            }
        }

        return null;
    }

    /**
     * This private method is not synchronized, so that the caller needs
     * to ensure correct synchronization for given lists
     *
     * @param jobId the job id of the JobEntry to be removed
     * @param lists the lists to remove the job entry from
     * @return the removed job entry or null, if no such entry exists
     */
    @SafeVarargs
    private JobEntry doRemove(@NonNull String jobId, @NonNull List<JobEntry>... lists) {
        for (var curList : lists) {
            var listIter = curList.listIterator();

            while (listIter.hasNext()) {
                var curEntry = listIter.next();

                if (jobId.equals(curEntry.getJobId())) {
                    listIter.remove();
                    return curEntry;
                }
            }
        }

        return null;
    }
}
