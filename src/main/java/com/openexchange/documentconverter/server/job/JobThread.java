/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.cache.CacheEntry;
import com.openexchange.documentconverter.server.concurrency.IdLocker;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.openexchange.documentconverter.server.watchdog.WatchdogHandler;
import com.sun.star.lang.DisposedException;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

// TODO NEXT not yet finished - there are some parts which must be implemented or refactored
abstract class JobThread extends Thread implements WatchdogHandler {

    private static final Set<JobErrorEx> SAVE_JOBERROR_EXCLUDE_LIST = new HashSet<>(Arrays.asList(
                                                                            new JobErrorEx(JobError.PASSWORD),
                                                                            new JobErrorEx(JobError.NO_CONTENT)));

    protected final ServerConfig serverConfig;

    protected final ServerManager serverManager;

    protected final JobProcessor jobProcessor;

    protected final BackendType backendType;

    protected final Cache cache;

    protected final ErrorCache errorCache;

    protected final Watchdog watchdog;

    protected  final Statistics statistics;

    protected final AtomicLong currentStartExecutionTimeMillis = new AtomicLong(0);

    protected final AtomicBoolean isRunning = new AtomicBoolean(true);

    protected volatile JobEntry activeJobEntry = null;

    private Object jobExecutionData = null;

    private long instanceJobsProcessed = 0;

    private int curBackendRun = 0;

    /**
     * Initializes a new {@link JobThread}.
     *
     * @param jobProcessor The {@link JobProcessor} to be used for processing jobs
     * @param serverManager The {@link ServerManager} to be used for logging
     * @param backendType The type of backend to be supported by this thread
     * @param cache The {@link Cache} to be used for caching results
     * @param errorCache The {@link ErrorCache} to be used for caching errors
     * @param watchdog The {@link Watchdog} to be used for monitoring the thread
     * @param statistics The {@link Statistics} to be used for counting processed jobs
     */
    JobThread(
        @NonNull ServerConfig serverConfig,
        @NonNull ServerManager serverManager,
        @NonNull JobProcessor jobProcessor,
        @NonNull BackendType backendType,
        @NonNull Cache cache,
        @NonNull ErrorCache errorCache,
        @NonNull Watchdog watchdog,
        @NonNull Statistics statistics) {

        super();

        this.serverConfig = serverConfig;
        this.serverManager = serverManager;
        this.jobProcessor = jobProcessor;
        this.backendType = backendType;
        this.cache = cache;
        this.errorCache = errorCache;
        this.watchdog = watchdog;
        this.statistics = statistics;
    }

    /**
     * The number of backend runs, the
     * actual thread is supporting. If this number is
     * greater than 1 (default), the backend processing
     * loop will run for this number of times
     * in total when a {@link JobError#GENERAL} or
     * {@link JobError#DISPOSED} happened during
     * the last run. In this case, the
     * {@link JobThread#endProcessingJob(JobEntry)}
     * method will not be called before the total
     * run count has been reached.
     *
     * @return The number of supported runs
     */
    Map<JobError, Integer> getSupportedBackendRuns() {
        return ServerManager.getDefaultSupportedBackendRuns();
    }

    /**
     * @return a copy of the current instance
     */
    abstract JobThread createCopy();

    /**
     * @return The type of currently supported backend
     */
    BackendType getBackendType() {
        return backendType;
    }

    /**
     * set the termination flag and interrupt the thread to terminate ASAP
     */
    void terminate() {
        if (isRunning.compareAndSet(true, false)) {
            var trace = ServerManager.isLogTrace();
            var threadName = getName();
            var traceStartTimeMillis = 0L;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace(getLogBuilder().
                    append("starting shutdown ").append(threadName).
                    append("...").toString());
            }

            shutdownCurrentInstance();
            interrupt();

            if (trace) {
                ServerManager.logTrace(getLogBuilder().
                    append("finished shutdown ").append(threadName).append(": ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * Determines whether the thread is running or not
     */
    boolean isRunning() {
        return isRunning.get();
    }

    /**
     * Shutdown internal backend
     */
    void shutdownCurrentInstance() {
        instanceJobsProcessed = 0;
        setJobExecutionData(null);
    }

    /**
     * @return The current JobEntry, that is processed or null if there is no active job
     */
    JobEntry getCurrentJobEntry() {
        return activeJobEntry;
    }

    /**
     * @return The current start execution time in milliseconds
     */
    long getCurrentStartExecutionTimeMillis() {
        return currentStartExecutionTimeMillis.get();
    }

    /*
     * The core processing loop of this JobThread
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        var jobErrorExWrapper = new MutableObject<>(new JobErrorEx());
        JobEntry curJobEntry;

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getLogBuilder().append("running").toString());
        }

        while (isRunning()) {
            try {
                // prepare processing job
                if (!prepareProcessingJob()) {
                    handlePrepareProcessingJobFailed();
                    continue;
                }

                // start processing job
                if (startProcessingJob() && (null != (curJobEntry = activeJobEntry))) {
                    final String jobLocale = curJobEntry.getJobLocale();
                    final boolean debug = ServerManager.isLogDebug();
                    final boolean trace = ServerManager.isLogTrace();

                    currentStartExecutionTimeMillis.set(watchdog.addWatchdogHandler(this));
                    jobErrorExWrapper.setValue(new JobErrorEx());

                    if (trace) {
                        ServerManager.logTrace(
                            getLogBuilder().append("processing job").toString(),
                            curJobEntry.getJobProperties(),
                            new LogData("jobid", curJobEntry.getJobId()),
                            new LogData("hash", curJobEntry.getHash()),
                            !StringUtils.isEmpty(jobLocale) ? new LogData("logtext", jobLocale) : null);
                    }

                    try {
                        // set locale for current job
                        setLocale(jobLocale);

                        if (trace) {
                            ServerManager.logTrace(
                                getLogBuilder().append("started processing job").toString());
                        }

                        if (!executeJob(curJobEntry, jobExecutionData)) {
                            jobErrorExWrapper.setValue(curJobEntry.getJobErrorEx());

                            if (jobErrorExWrapper.getValue().hasNoError()) {
                                jobErrorExWrapper.setValue(new JobErrorEx(JobError.GENERAL));
                                curJobEntry.setJobErrorEx(jobErrorExWrapper.getValue());
                            }

                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                    append("error processing job (Execute path): ").
                                    append(System.currentTimeMillis() - currentStartExecutionTimeMillis.get()).append("ms").toString());
                            }
                        } else if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                append("finished processing job: ").
                                append(System.currentTimeMillis() - currentStartExecutionTimeMillis.get()).append("ms").toString());
                        }
                    } catch (Throwable e) {
                        ExceptionUtils.handleThrowable(e);

                        jobErrorExWrapper.setValue(curJobEntry.getJobErrorEx());

                        if (jobErrorExWrapper.getValue().hasNoError()) {
                            final JobErrorEx exceptionJobErrorEx = new JobErrorEx((e instanceof DisposedException) ? JobError.DISPOSED : JobError.GENERAL);

                            jobErrorExWrapper.setValue(exceptionJobErrorEx);
                            curJobEntry.setJobErrorEx(exceptionJobErrorEx);
                        }

                        if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                    append("error processing job (Exception path): ").
                                    append(System.currentTimeMillis() - currentStartExecutionTimeMillis.get()).append("ms").toString(),
                                new LogData("exception", e.getMessage()));
                        }
                    }

                    // immediately remove watchdog handler
                    watchdog.removeWatchdogHandler(this);

                    // increase number of processed jobs for the currently used ReaderEngine instance
                    ++instanceJobsProcessed;

                    // check all locations for a possibly set error code
                    JobErrorEx resultingJobErrorEx = jobErrorExWrapper.getValue();

                    if (resultingJobErrorEx.hasNoError() && curJobEntry.getJobErrorEx().hasError()) {
                        resultingJobErrorEx = curJobEntry.getJobErrorEx();
                    }

                    // check, if the used thread supports more than one run in case of an
                    // error => if yes, reset job error and backend instance and perform the
                    // next run until the maximum count of runs (error dependent) is reached
                    if (resultingJobErrorEx.hasError()) {
                        final int maxRuns = getSupportedBackendRuns().get(resultingJobErrorEx.getJobError());

                        if ((curBackendRun < maxRuns) && isRunning()) {
                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                        append("will reuse current job for followup run #").append(curBackendRun + 1).toString(),
                                    curJobEntry.getJobProperties(),
                                    new LogData("joberror", resultingJobErrorEx.getJobError().getErrorText()),
                                    new LogData("jobid", curJobEntry.getJobId()),
                                    new LogData("hash", curJobEntry.getHash()));
                            }

                            shutdownCurrentInstance();
                            curJobEntry.setJobErrorEx(new JobErrorEx());
                        } else {
                            curJobEntry.setJobErrorEx(resultingJobErrorEx);
                            curBackendRun = 0;

                            // job error, but no more runs to complete  => cleanup, notifying, cache writing etc.
                            endProcessingJob(curJobEntry);
                        }
                    } else {
                        // job ok with no error => cleanup, notifying, cache writing etc.
                        endProcessingJob(curJobEntry);
                    }

                    if (debug) {
                        ServerManager.logDebug(getLogBuilder().
                                append("released job").toString(),
                            new LogData("jobid", curJobEntry.getJobId()),
                            new LogData("hash", curJobEntry.getHash()));
                    }

                    if (trace) {
                        ServerManager.logTrace(getLogBuilder().
                            append("finished job run: ").
                            append(System.currentTimeMillis() - currentStartExecutionTimeMillis.get()).append("ms").toString());
                    }

                    currentStartExecutionTimeMillis.set(0);
                }
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }
        }

        curJobEntry = null;
        shutdownCurrentInstance();

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getLogBuilder().append("finished").toString());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.tools.WatchdogHandler#handleWatchdogNotification(long, long, com.openexchange.documentconverter.JobError)
     */
    @Override
    public final void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
        var curJobEntry = activeJobEntry;

        if (null != curJobEntry) {
            curJobEntry.setJobErrorEx(notificationJobErrorEx);

            try {
                // notify processor that an error has happened
                shutdownCurrentInstance();

                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace(getLogBuilder().
                            append((JobError.TIMEOUT == notificationJobErrorEx.getJobError()) ? "TIMEOUT reached" :"disposed").
                            append(" => job has been killed").toString(),
                        new LogData("jobid", curJobEntry.getJobId()),
                        new LogData("hash", curJobEntry.getHash()),
                        new LogData("exectime", curTimeMillis - startTimeMillis + "ms"));
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                ServerManager.logExcp(new Exception(getLogBuilder().
                    append("exception caught during Watchdog abort notification: ").
                    append(notificationJobErrorEx.getJobError().getErrorText()).toString()));
            }
        }
    }

    /**
     * @return the number of processed jobs for the currently used ReaderEngine instance
     */
    protected long getInstanceJobsProcessed() {
        return instanceJobsProcessed;
    }

    /**
     * @return the number of restarts for the currently used ReaderEngine instance
     */
    protected int getRestartCount() {
        return 1;
    }

    /**
     * @return TRUE in case the preparation of the processing job was successful, FALSE otherwise
     */
    protected boolean prepareProcessingJob() {
        return true;
    }

    /**
     * Handle the case that the preparation of the processing job failed
     */
    protected void handlePrepareProcessingJobFailed() {
        // nothing to be done here
    }

    /**
     * @return true if a job has been started, false if no job is available
     */
    protected boolean startProcessingJob() {
        boolean ret = false;

        // just use the currently set job entry in case of a retry
        // or wait until we get a next job from the queue via calling
        // the appropriate, blocking JobProcessor method
        if (null == activeJobEntry) {
            curBackendRun = 1;
            activeJobEntry = jobProcessor.getNextJobEntryToProcess(getBackendType());

            if (null != activeJobEntry) {
                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug(getLogBuilder().
                            append("acquired job for run #1").toString(),
                        new LogData("jobid", activeJobEntry.getJobId()),
                        new LogData("hash", activeJobEntry.getHash()));
                }

                // increment number of totally processed jobs for appropriate BackendType before first run only;
                // subsequent backend runs for the same job will not increment this number
                statistics.incrementConversionCount(backendType);

                // always try to process a result from cache or
                // errorcache first, if this is the first backend run
                if (JobStatus.INVALID != processCachedJobEntry(jobProcessor, activeJobEntry)) {
                    endProcessingJob(activeJobEntry);
                    activeJobEntry = null;
                } else {
                    ret = true;
                }
            }
        } else {
            ret = true;

            // increment run count and increment statistics counter, if run count reached '2'
            if (2 == ++curBackendRun) {
                statistics.incrementConversionWithMultipleRunsCount();
            }

            if (ServerManager.isLogDebug()) {
                ServerManager.logDebug(getLogBuilder().
                        append("reusing job for run #").append(curBackendRun).toString(),
                    new LogData("jobid", activeJobEntry.getJobId()),
                    new LogData("hash", activeJobEntry.getHash()));
            }
        }

        return ret;
    }

    /**
     * @param jobLocale The new job locale to be set as instance locale for the upcoming processing of jobs. The instance locale is set to a
     *            valid locale in any case after this call.
     */
    // FIXME This method looks odd - it does NOT set the locale (and it cannot as there
    // is no member which contains locale information. It just returns the given locale
    // en-US is cas jobLocale is null or empty
    protected String setLocale(String jobLocale) {
        return ((null != jobLocale) && (jobLocale.length() > 1)) ? jobLocale : "en_US";
    }

    /**
     * @param executionData User data to be set before the job is executed.
     *            The data is given to the various execute calls as parameter
     */
    protected void setJobExecutionData(Object executionData) {
        jobExecutionData = executionData;
    }

    /**
     *
     */
    protected synchronized void endProcessingJob(JobEntry curJobEntry) {
        if (null != curJobEntry) {
            final ErrorCache errorCache = jobProcessor.getErrorCache();
            final JobErrorEx jobErrorEx = curJobEntry.getJobErrorEx();
            final boolean hasError = jobErrorEx.hasError();
            final JobStatus jobStatus = hasError ? JobStatus.ERROR : JobStatus.FINISHED;

            // a possible cache entry has been written by the JobEntryExecutor,
            // just add an error to the errorcache here if not already contained
            if (1 == errorCache.notifyJobFinish(curJobEntry) || (hasError && !errorCache.isEnabled())) {
                saveErrorFile(curJobEntry);
            }

            // update Statistics in case of error
            if (hasError) {
                // Increment conversion error statistics (<= increment conversion count for specific JobError)
                statistics.incrementConversionErrorCount(jobErrorEx.getJobError());
            }

            jobProcessor.finishedJobEntry(getBackendType(), curJobEntry, jobStatus);
        }

        activeJobEntry = null;
    }

    /**
     * @param jobEntry The job entry to be executed
     * @param jobExecutionData Job context data to be set before the job is executed.
     * @return TRUE if the job has been executed successfully, FALSE otherwise
     * @throws com.sun.star.lang.DisposedException
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @SuppressWarnings("RedundantThrows")
    protected boolean executeJob(@NonNull JobEntry jobEntry, Object jobExecutionData) throws com.sun.star.lang.DisposedException, InterruptedException, IOException, MalformedURLException, Exception {
        var job = jobEntry.getJob();
        var ret = false;

        if (null != job) {
            String hash = job.getHash();
            final boolean serverJobCachable = Cache.isValidHash(hash) && cache.isEnabled();

            if (!serverJobCachable) {
                // if the cache can't handle new cache entry with given hash
                // (e.g. invalid hash or cache is still filled up in the
                // background and thus currently disabled) =>
                // use unique hash id and don't cache at end of job execution
                hash = jobEntry.getJobId();
            }

            if (!Cache.isValidHash(hash)) {
                ServerManager.logWarn(getLogBuilder().
                    append("executeJob call with invalid hash not possible").toString());

                return false;
            }

            CacheEntry newCacheEntry = null;

            IdLocker.lock(hash);

            try {
                var addedToCache = false;

                if (null != (newCacheEntry = cache.startNewEntry(hash))) {
                    Map<String, Object> additionalJobProperties = new HashMap<>(4);
                    var trace = ServerManager.isLogTrace();
                    var traceStartTimeMillis = trace ? System.currentTimeMillis() : 0L;

                    // set property to not close document, since this
                    // is the last conversion for the current instance
                    if ((instanceJobsProcessed + 1) >= getRestartCount()) {
                        additionalJobProperties.put(Properties.PROP_CLOSE_DOCUMENT, Boolean.FALSE);
                    }

                    // finally, set output file property at job
                    additionalJobProperties.put(Properties.PROP_OUTPUT_FILE, newCacheEntry.getResultFile());

                    job.addJobProperties(additionalJobProperties);

                    var jobInputFile = (File) job.getJobProperties().get(Properties.PROP_INPUT_FILE);

                    if (trace) {
                        ServerManager.logTrace(getLogBuilder().append("started executing job").toString());
                    }

                    var converterStatus = ConverterStatus.ERROR;

                    try {
                        // execute the job, but don't try a job execution at all,
                        // if an input file is set, but has a file length of 0 bytes
                        if ((null == jobInputFile) || (jobInputFile.length() > 0)) {
                            converterStatus = job.execute(jobExecutionData);

                            ret = (ConverterStatus.ERROR != converterStatus);

                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                    append("finished executing job: ").
                                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                            }
                        }
                    } catch (Exception e) {
                        if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                append("error executing job: ").
                                append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                        }

                        // rethrow Exception in case something bad happened
                        throw e;
                    } finally {
                        var jobResultProperties = job.getResultProperties();

                        // if succeeded job has a valid cache set, persistently write result to cache;
                        // if not, the result property already contains all information
                        if (ret) {
                            if (job.producesCacheableResult() && Cache.isValidHash(job.getHash())) {
                                if (cache.isEnabled()) {
                                    newCacheEntry.getResultProperties().clear();
                                    newCacheEntry.getResultProperties().putAll(job.getResultProperties());
                                    addedToCache = cache.addEntry(newCacheEntry, true, null);
                                }
                            }
                        } else if (jobResultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                            jobEntry.setJobErrorEx(JobErrorEx.fromResultProperties(jobResultProperties));
                        }

                        if (!addedToCache) {
                            if (null != jobInputFile) {
                                // remove lock file from just killed conversion process
                                FileUtils.deleteQuietly(new File(jobInputFile.getParentFile(), ".~lock." + jobInputFile.getName() + "#"));
                            }

                            // clear just created cache file
                            newCacheEntry.clear();
                        }
                    }
                }
            } finally {
                if (null != newCacheEntry) {
                    cache.endNewEntry(newCacheEntry);
                }

                IdLocker.unlock(hash);
            }
        }

        return ret;
    }

    // - Statics interface -----------------------------------------------------

    /**
     * @param jobEntry
     * @return The <code>JobStatus</code> from a cached entry or {@link JobStatus.INVALID}
     *         if result could not be retrieved from cache
     */
    protected JobStatus processCachedJobEntry(JobProcessor jobProcessor, JobEntry jobEntry) {
        var ret = JobStatus.INVALID;

        // always try to retrieve a result from result or errorcache first
        if (null != jobEntry) {
            var job = jobEntry.getJob();
            var hash = job.getHash();
            var filename = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);
            var cachedJobErrorEx = jobProcessor.getErrorCache().getJobErrorExForHash(hash);

            if (null == cachedJobErrorEx) {
                try (var cachedResultStm = cache.getCachedResult(hash, filename, job.getResultProperties(), false)) {
                    if (null != cachedResultStm) {
                        jobEntry.setCacheHit();
                        ret = JobStatus.FINISHED;

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace(getLogBuilder().
                                    append("cache hit in job thread").toString(),
                                new LogData("jobid", jobEntry.getJobId()),
                                new LogData("hash", hash),
                                new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                        }
                    }
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            } else {
                jobEntry.setErrorCacheHit(cachedJobErrorEx);
                ret = JobStatus.ERROR;

                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace(getLogBuilder().
                            append("errorcache hit in job thread").toString(),
                        new LogData("jobid", jobEntry.getJobId()),
                        new LogData("hash", hash),
                        new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                }
            }
        }

        return ret;
    }

    /**
     * @param entry
     */
    protected void saveErrorFile(@NonNull JobEntry entry) {
        var errorDir = new File(serverConfig.getErrorDir(), "error");
        var jobErrorEx = entry.getJobErrorEx();

        if (errorDir.canWrite() && !SAVE_JOBERROR_EXCLUDE_LIST.contains(jobErrorEx)) {
            var job = entry.getJob();
            var jobProperties = (null != job) ? job.getJobProperties() : null;

            if ((null != jobProperties) && (null != entry.getJobId()) && jobProperties.containsKey(Properties.PROP_INPUT_FILE)) {
                var jobHash = entry.getHash();
                var fileNameBuilder = new StringBuilder(Cache.isValidHash(jobHash) ? jobHash : entry.getJobId());

                // add suffix to jobId, if known at all
                if (jobProperties.containsKey(Properties.PROP_INFO_FILENAME)) {
                    var suffix = FilenameUtils.getExtension((String) jobProperties.get(Properties.PROP_INFO_FILENAME));

                    if (null != suffix) {
                        fileNameBuilder.append('.').append(suffix);
                    }
                }

                var timeoutDir = new File(serverConfig.getErrorDir(), "timeout");
                var destFile = new File((JobError.TIMEOUT == jobErrorEx.getJobError()) ? timeoutDir : errorDir, fileNameBuilder.toString());

                try {
                    Files.copy(((File) jobProperties.get(Properties.PROP_INPUT_FILE)).toPath(), FileUtils.openOutputStream(destFile));
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }

    /**
     * @return
     */
    protected StringBuilder getLogBuilder() {
        return new StringBuilder(256).append("DC ").append(backendType.toString()).append(" JobThread ");
    }

}

