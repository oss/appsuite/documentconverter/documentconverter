/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import lombok.NonNull;

import java.util.concurrent.atomic.AtomicInteger;

public class PDFToolThread extends JobThread {

    private static final AtomicInteger PDF_THREAD_NUMBER = new AtomicInteger(0);

    private final int pdfToolThreadNumber = PDF_THREAD_NUMBER.incrementAndGet();

    /**
     * @param jobProcessor
     * @param cache
     * @param watchdog
     */
    public PDFToolThread(
        @NonNull ServerConfig serverConfig,
        @NonNull ServerManager serverManager,
        @NonNull JobProcessor jobProcessor,
        @NonNull Cache cache,
        @NonNull ErrorCache errorCache,
        @NonNull Watchdog watchdog,
        @NonNull Statistics statistics) {

        super(serverConfig, serverManager, jobProcessor, BackendType.PDFTOOL, cache, errorCache, watchdog, statistics);

        setName(getLogBuilder().append("#").append(pdfToolThreadNumber).toString());
    }

    // - Overrides -------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#createCopy()
     */
    @Override
    protected JobThread createCopy() {
        return new PDFToolThread(serverConfig, serverManager, jobProcessor, cache, errorCache, watchdog, statistics);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#shutdownCurrentInstance()
     */
    @Override
    protected void shutdownCurrentInstance() {
        final JobEntry curJobEntry = getCurrentJobEntry();

        if (null != curJobEntry) {
            var job = curJobEntry.getJob();

            if (null != job) {
                try {
                    job.kill();
                } catch (Exception e) {
                    ServerManager.logExcp(e);
                }
            }
        }

        super.shutdownCurrentInstance();
    }

}
