/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.job;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.error.ErrorCache;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.readerengine.REDescriptor;
import com.openexchange.documentconverter.server.readerengine.REInstanceProvider;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.openexchange.documentconverter.server.watchdog.WatchdogHandler;
import com.openexchange.documentconverter.server.watchdog.WatchdogMode;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

// TODO: test it
@Component
public class JobProcessor implements WatchdogHandler {

    protected static final long NO_RESPONSE_TIMEOUT_PERCENTAGE = 125L;
    protected static final String GENERIC_JOB = "generic job";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ConverterJobFactory converterJobFactory;

    @Autowired
    private ServerManager serverManager;

    @Autowired
    private REDescriptor readerEngineDescriptor;

    @Getter
    @Autowired
    private Cache cache;

    @Getter
    @Autowired
    private ErrorCache errorCache;

    @Autowired
    private Statistics statistics;

    @Autowired
    private EngineGroupFactory engineGroupFactory;

    private REInstanceProvider instanceProvider;

    @Getter
    private ExecutorService readerEngineExecutorService;

    private Watchdog jobWatchdog;

    @Getter
    private JobMonitor jobMonitor;

    private final Map<BackendType, EngineGroup> engineGroups = new HashMap<>();

    private final List<JobStatusListener> jobStatusListeners = Collections.synchronizedList(new LinkedList<>());

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    private long noResponseTimeoutMillis;

    @PostConstruct
    public void init() {
        var jobTimeoutMilliSeconds = serverConfig.getJobExecutionTimeoutMilliseconds();
        var engineCount = serverConfig.getJobProcessorCount();

        instanceProvider = new REInstanceProvider(serverManager, this, readerEngineDescriptor, serverConfig.getJobProcessorPoolSize());

        // Timeout Watchdog
        jobWatchdog = new Watchdog("DC JobProcessor watchdog", jobTimeoutMilliSeconds, WatchdogMode.TIMEOUT_AND_DISPOSED);

        // NoResponse timeout
        noResponseTimeoutMillis = jobTimeoutMilliSeconds * NO_RESPONSE_TIMEOUT_PERCENTAGE;

        // initialize and start all engines for Documentconverter
        readerEngineExecutorService = Executors.newFixedThreadPool(engineCount);

        // create engine groups
        initEngineGroups(engineCount);

        // start all created engine groups
        for (var curGroup : engineGroups.values()) {
            curGroup.start();
        }

        jobMonitor = new JobMonitor(this, serverConfig.getJobQueueCountLimitHigh(), serverConfig.getJobQueueCountLimitLow(), serverConfig.getJobQueueTimeoutSeconds(), statistics);

        // Start job watchdog
        jobWatchdog.start();
    }

    public boolean isRunning() {
        return isRunning.get();
    }

    public Job createServerJob(@NonNull String jobType, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, Map<String, Object> resultProperties) {
        return converterJobFactory.createJob(jobType, cacheHash, jobProperties, resultProperties);
    }

    public String scheduleJob(Job job, JobPriority jobPriority, JobStatusListener jobStatusListener, String jobDescription) {
        String jobId = null;

        if ((null != job) && isRunning()) {
            jobId = UUID.randomUUID().toString();
            var usedJobPriority = (null != jobPriority) ? jobPriority : JobPriority.MEDIUM;
            var newJobEntry = new JobEntry(jobId, job, usedJobPriority, jobStatusListener);
            var finalJobDescription = (StringUtils.isNotEmpty(jobDescription)) ? jobDescription : GENERIC_JOB;

            if (ServerManager.isLogTrace()) {
                var infoFilename = (String) job.getJobProperties().get(Properties.PROP_INFO_FILENAME);
                ServerManager.logTrace("DC scheduling " + finalJobDescription, new LogData("jobid", jobId), new LogData("doctype", (String) job.getJobProperties().get(Properties.PROP_INPUT_TYPE)), new LogData("filename", (null != infoFilename) ? infoFilename : "unkown"));
            }

            var engineGroup = engineGroups.get(job.backendTypeNeeded());

            if (null != engineGroup) {
                // schedule the new JobEntry at the appropriate queue
                engineGroup.entryQueue.schedule(newJobEntry);

                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug("DC job added to " + engineGroup.getGroupName() + " queue", new LogData("jobid", jobId), new LogData("jobs_remaining", Integer.toString(engineGroup.entryQueue.size())));
                }
            }
        }

        return jobId;
    }

    /**
     * @param jobEntry
     * @param newJobStatus
     */
    protected void updateServerJobStatus(final JobEntry jobEntry, final JobStatus newJobStatus) {
        if (null != jobEntry) {
            var oldJobStatus = jobEntry.getJobStatus();

            if (newJobStatus != oldJobStatus) {
                var jobId = jobEntry.getJobId();

                // immediately set new job status, so that all
                // following calls will work with the correct, new
                // JobStatus set at the currently updated JobEntry
                jobEntry.setJobStatus(newJobStatus);

                if (JobStatus.SCHEDULED == newJobStatus) {
                    jobMonitor.jobScheduled(jobEntry);
                } else if (JobStatus.SCHEDULED == oldJobStatus) {
                    jobMonitor.jobSchedulingFinished(jobEntry);
                }

                // notify all registered listeners
                synchronized (jobStatusListeners) {
                    for (final JobStatusListener listener : jobStatusListeners) {
                        if (null != listener) {
                            listener.statusChanged(jobId, jobEntry);
                        }
                    }
                }

                statistics.statusChanged(jobId, jobEntry);

                // notify listener, set at JobEntry itself
                var jobStatusListener = jobEntry.getJobStatusListener();

                if (null != jobStatusListener) {
                    jobStatusListener.statusChanged(jobId, jobEntry);
                }
            }
        }
    }

    /**
     * Blocking (!) call to retrieve the next available job with
     * JobStatus.SCHEDULED
     *
     * @return the next available jobEntry or null in case of termination
     */
    protected JobEntry getNextJobEntryToProcess(@NonNull BackendType backendType) {
        final EngineGroup engineGroup = engineGroups.get(backendType);

        return ((null != engineGroup) ? engineGroup.entryQueue.getNextJobEntryToProcess() : null);
    }

    /**
     *
     */
    protected void finishedJobEntry(@NonNull BackendType backendType,
                                    @NonNull JobEntry jobEntry,
                                    @NonNull JobStatus jobStatus) {

        var engineGroup = engineGroups.get(backendType);

        if (null != engineGroup) {
            engineGroup.entryQueue.finishedJobEntry(jobEntry, jobStatus);
        }
    }

    public void killJob(@NonNull String jobId, @NonNull final JobErrorEx jobErrorEx) {
        var curTimeMillis = System.currentTimeMillis();

        if (StringUtils.isNotBlank(jobId)) {
            for (var curGroup : engineGroups.values()) {
                for (var curThread : curGroup.engineVector) {
                    var curEntry = curThread.getCurrentJobEntry();

                    if ((null != curEntry) && jobId.equals(curEntry.getJobId())) {
                        curThread.handleWatchdogNotification(curTimeMillis, curThread.getCurrentStartExecutionTimeMillis(), jobErrorEx);
                        break;
                    }
                }
            }
        }
    }

    public boolean removeJob(final String jobId) {
        return (StringUtils.isNotBlank(jobId)) ? doRemoveJob(jobId, false) : false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#removeJob(long)
     */
    public boolean removeScheduledJob(final String jobId) {
        return (StringUtils.isNotBlank(jobId)) ? doRemoveJob(jobId, true) : false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.openexchange.documentconverter.IJobProcessor#addJobStatusChangeListener
     * (com.openexchange.documentconverter.IJobStatusListener)
     */
    public void addJobStatusChangeListener(JobStatusListener listener) {
        jobStatusListeners.add(listener);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#
     * removeJobStatusChangeListener
     * (com.openexchange.documentconverter.IJobStatusListener)
     */
    public void removeJobStatusChangeListener(JobStatusListener listener) {
        if (null != listener) {
            jobStatusListeners.remove(listener);
        }
    }

    /**
     * notifies the handler that the watchdog detected an event for this handler
     *
     * @param curTimeMillis          The current time, given to avoid multiple querying for current time in handler method
     * @param startTimeMillis        The point of time, this handler has been started to watch a timeout
     * @param notificationJobErrorEx The reason for notification
     */
    @Override
    public void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
        if (JobError.TIMEOUT == notificationJobErrorEx.getJobError()) {
            // this handler is called, whenever a scheduled (Ready) entry
            // has reached its 'no response' timeout, which means, that
            // no instance thread has requested the next job within this timeout;
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC received 'NO RESPONSE' timeout => replacing job threads");
            }

            // check all readerEngine instances against the last time,
            // they took a job from the queue; kill and replace thread
            // with a new copy, if this timeout has been reached
            var readerEngineGroup = engineGroups.get(BackendType.READERENGINE);

            for (int i = 0; i < readerEngineGroup.engineVector.size(); ++i) {
                var curJobThread = readerEngineGroup.engineVector.get(i);
                var curJobEntry = curJobThread.getCurrentJobEntry();

                if ((null != curJobEntry) && (curTimeMillis - startTimeMillis) >= noResponseTimeoutMillis) {
                    var newJobThread = curJobThread.createCopy();

                    if (null != newJobThread) {
                        curJobThread.terminate();
                        readerEngineGroup.engineVector.set(i, newJobThread);
                        newJobThread.start();
                    }
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#terminate()
     */
    public synchronized void shutdown() {
        if (isRunning.compareAndSet(true, false)) {
            var jobTimeoutMilliSeconds = jobWatchdog.getTimeoutMilliSeconds();
            var trace = ServerManager.isLogTrace();
            var traceStartTimeMillis = 0L;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC JobProcessor starting shutdown...");
            }

            logStatistics();

            // terminate JobMonitor in order to not accept new jobs anymore
            jobMonitor.terminate();

            // terminate RE instance provider
            instanceProvider.terminate();

            // shutdown all engine groups
            for (final EngineGroup curGroup : engineGroups.values()) {
                curGroup.terminate();
            }

            // interrupt all pending jobs with the help of the Watchdog
            jobWatchdog.terminate();

            try {
                jobWatchdog.join();
            } catch (@SuppressWarnings("unused") final InterruptedException e) {
                // ok
            }

            // shutdown thread pool
            try {
                readerEngineExecutorService.shutdownNow();
                readerEngineExecutorService.awaitTermination(jobTimeoutMilliSeconds, TimeUnit.MILLISECONDS);
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }

            // shudown errorcache
            errorCache.terminate();

            // cleanup of all still living OSL_PIPE_* files located
            // in LO/AOO hard coded /tmp or /var/tmp directories
            try {
                var tmpDir = new File("/tmp");

                if (!tmpDir.canWrite()) {
                    tmpDir = new File("/var/tmp");
                }

                if (tmpDir.canWrite()) {
                    var tmpDirEntries = tmpDir.list();

                    if (null != tmpDirEntries) {
                        for (var curEntry : tmpDirEntries) {
                            if (curEntry.startsWith("OSL_PIPE_")) {
                                FileUtils.deleteQuietly(new File(tmpDir, curEntry));
                            }
                        }
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }

            if (trace) {
                ServerManager.logTrace("DC JobProcessor finished shutdown: " +
                    (System.currentTimeMillis() - traceStartTimeMillis) + "ms" +
                    '!');
            }
        }
    }

    public @NonNull Map<BackendType, EngineStatus> getEngineStatus() {
        Map<BackendType, EngineStatus> engineStatusMap = new HashMap<>();

        for (var curBackendType : BackendType.values()) {
            engineStatusMap.put(curBackendType, getEngineStatus(curBackendType));
        }

        return engineStatusMap;
    }

    EngineStatus getEngineStatus(@NonNull BackendType backendType) {
        final var engineGroup = engineGroups.get(backendType);

        return new EngineStatus() {

            @Override
            public int totalCount() {
                return engineGroup == null ? 0 : engineGroup.getJobThreads().size();
            }

            @Override
            public int runningCount() {
                int runningCount = 0;

                if (engineGroup != null) {
                    var jobThreads = engineGroup.getJobThreads();

                    for (var curJobThread : jobThreads) {
                        if (curJobThread.isAlive() && curJobThread.isRunning()) {
                            ++runningCount;
                        }
                    }
                }

                return runningCount;
            }
        };
    }

    private void initEngineGroups(int engineCount) {
        var readerEngineGroup = engineGroupFactory.createEngineGroup(BackendType.READERENGINE, engineCount, this, instanceProvider, jobWatchdog);
        engineGroups.put(BackendType.READERENGINE, readerEngineGroup);
        var pdfToolEngineGroup = engineGroupFactory.createEngineGroup(BackendType.PDFTOOL, engineCount, this, instanceProvider, jobWatchdog);
        engineGroups.put(BackendType.PDFTOOL, pdfToolEngineGroup);
    }

    private boolean doRemoveJob(@NonNull final String jobId, boolean scheduledOnly) {
        var trace = ServerManager.isLogTrace();

        for (var curGroup : engineGroups.values()) {
            if ((null != curGroup.entryQueue.remove(jobId, scheduledOnly))) {
                if (trace) {
                    ServerManager.logTrace("DC removed " + (scheduledOnly ? "scheduled " : "") + "job from " +
                            curGroup.getGroupName() + " queue",
                        new LogData("jobid", jobId),
                        new LogData("jobs_remaining", Integer.toString(curGroup.entryQueue.size())));
                }

                return true;
            }
        }

        return false;
    }

    /**
     * write out some fundamental statistics when processor is going to be shut down
     */
    private void logStatistics() {
        if (ServerManager.isLogInfo()) {
            var cacheHitRatio = (Math.floor(statistics.getCacheHitRatio() * 10000.0) / 100.0);
            var errorCacheHitRatio = (Math.floor(statistics.getErrorCacheHitRatio() * 10000.0) / 100.0);
            var statisticsBuilder = new StringBuilder(1024).append("DC statistics");

            statisticsBuilder.
                append("; jobsProcessed: ").append(statistics.getJobsProcessed()).
                append("; jobErrors: ").append(statistics.getJobErrorsTotal()).
                append("; conversions: ").append(statistics.getConversionCount(null)).
                append("; conversionsReaderEngine: ").append(statistics.getConversionCount(BackendType.READERENGINE)).
                append("; conversionsPDFTool: ").append(statistics.getConversionCount(BackendType.PDFTOOL)).
                append("; conversionsMultiRun: ").append(statistics.getConversionCountWithMultipleRuns()).
                append("; conversionErrorsGeneral: ").append(statistics.getConversionErrorsGeneral()).
                append("; conversionErrorsTimeout: ").append(statistics.getConversionErrorsTimeout()).
                append("; conversionErrorsDisposed: ").append(statistics.getConversionErrorsDisposed()).
                append("; conversionErrorsPassword: ").append(statistics.getConversionErrorsPassword()).
                append("; conversionErrorsNoContent: ").append(statistics.getConversionErrorsNoContent()).
                append("; cacheHits: ").append(statistics.getCacheHits()).
                append("; cacheRatio: ").append(cacheHitRatio).append("%").
                append("; errorCacheHits: ").append(statistics.getErrorCacheHits()).
                append("; errorCacheRatio: ").append(errorCacheHitRatio).append("%");

            ServerManager.logInfo(statisticsBuilder.toString());
        }
    }

}
