/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.job.Job;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class XRechnungToPDFConverterJob implements Job {

    protected static String XRechnungProgramPath = "/opt/open-xchange/xrechnung-visualization";

    protected volatile Process process = null;

    final private static int ERRORCODE_TIMEOUT = -1;

    final private static int ERRORCODE_NONE = 0;

    /**
     * Initializes a new {@link XRechnungToPDFConverterJob}.
     *
     * @param jobProperties the job properties to be used for the conversion job
     * @param resultProperties the result properties being used to providing information about the resulted conversion
     */
    public XRechnungToPDFConverterJob(@NonNull ServerConfig serverConfig, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, @Nullable Map<String, Object> resultProperties) {
        this.serverConfig = serverConfig;
        this.cacheHash = cacheHash;
        this.jobProperties = jobProperties;
        this.resultProperties = resultProperties == null ? new HashMap<>(8) : resultProperties;
        this.inputFile = (File)jobProperties.get(Properties.PROP_INPUT_FILE);
    }

    // TODO: new BackendType Needed
    @Override
    @NonNull
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    @Override
    @NonNull
    public ConverterStatus execute(@Nullable Object jobExecutionData) throws Exception {
        ConverterStatus status = ConverterStatus.ERROR;
        if (isInstalled()) {
            try {
                final File outFile = (File)jobProperties.get(Properties.PROP_OUTPUT_FILE);
                if (outFile != null) {
                    process = Runtime.getRuntime().exec(getCommandLine(outFile.getAbsolutePath(), getConfig()), null, new File(XRechnungProgramPath));

                    var exitValue = ServerManager.waitForProcess(process, serverConfig.getJobExecutionTimeoutMilliseconds());
                    if (exitValue == ERRORCODE_NONE) {
                        // TODO: cache is taken from (File)jobProperties.get(Properties.PROP_OUTPUT_FILE) but resultProperties.put(Properties.PROP_RESULT_BUFFER, result) is still required
                        status = ConverterStatus.OK;
                        final byte[] result = Files.readAllBytes(outFile.toPath());
                        resultProperties.put(Properties.PROP_RESULT_BUFFER, result);
                    } else {
                        final Process usedProcess = process;
                        if (usedProcess != null) {
                            if (ServerManager.isLogTrace()) {
                                final InputStream inputStream = usedProcess.getInputStream();
                                if (inputStream != null) {
                                    final String input = new String(inputStream.readNBytes(1024), StandardCharsets.UTF_8);
                                    if (StringUtils.isNotEmpty(input)) {
                                        ServerManager.logTrace("DC XRechnung execute (input stream):" + input, jobProperties);
                                    }
                                }
                            }
                            if (ServerManager.isLogError()) {
                                final InputStream errorStream = usedProcess.getErrorStream();
                                if (errorStream != null) {
                                    final String error = new String(errorStream.readAllBytes(), StandardCharsets.UTF_8);
                                    if (StringUtils.isNotEmpty(error)) {
                                        ServerManager.logError("DC XRechnung execute (error stream):" + error, jobProperties);
                                    }
                                }
                            }
                            if (ERRORCODE_TIMEOUT == exitValue) {
                                // kill process ultimately in case of a timeout error to get rid of looping processes
                                usedProcess.destroyForcibly();
                            }
                        }
                    }
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug(e.getMessage());
                }
            }
        }
        return status;
    }

    @Override
    public void kill() {
        var usedProcess = process;

        if (null != usedProcess) {
            usedProcess.destroyForcibly();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC XRechnung killed");
            }
        }

    }

    @Override
    public void addJobProperties(Map<String, Object> additionalJobProperties) {
        jobProperties.putAll(additionalJobProperties);
    }

    @Override
    @NonNull public String getHash() {
        return cacheHash;
    };

    @Override
    @NonNull
    public Map<String, Object> getJobProperties() {
        return jobProperties;
    }

    @Override
    @NonNull
    public Map<String, Object> getResultProperties() {
        return resultProperties;
    }

    @Override
    public boolean producesCacheableResult() {
        return true;
    }

    // - Implementatation ------------------------------------------------------


    private String[] getCommandLine(String outputFile, String config) {
        return new String[]{
            "java",
            "-Xms256m", "-Xmx512m",
            "-jar", "OpenXRechnungToolbox.jar",
            "-c", config,
            "-viz",
            "-i", inputFile.getAbsolutePath(),
            "-o", outputFile,
            "-p"
        };
    }

    private String getConfig() {
        return XRechnungProgramPath + "/resources/app.config_" + (cacheHash.contains("-lang.de") ? "de" : "en");
    }

    private synchronized boolean isInstalled() {
        return Files.exists(Paths.get(XRechnungProgramPath + "/OpenXRechnungToolbox.jar"));
    }

    public static boolean isXRechnung(@NonNull String cacheHash, File inputFile) {
        if (cacheHash.contains("-cap.xr") && inputFile != null) {
            final byte[] data = new byte[4096];
            try (RandomAccessFile accessFile = new RandomAccessFile(inputFile, "r")) {
                accessFile.readFully(data);
            }
            catch (IOException e) {
                ServerManager.logExcp("DC XRechnungToPDFConverterJob::isXRechnung:" + cacheHash, e);
            }
            return pattern.matcher(new String(data, StandardCharsets.UTF_8)).find();
        }
        return false;
    }

    // possible xml root elements for xrechnung are: Invoice, CrossIndustryInvoice or CreditNote
    final private static Pattern pattern = Pattern.compile("(?=.*Invoice|.*CreditNote)", Pattern.DOTALL);

    final private ServerConfig serverConfig;

    final private String cacheHash;

    final private Map<String, Object> jobProperties;

    final private Map<String, Object> resultProperties;

    final private File inputFile;
}
