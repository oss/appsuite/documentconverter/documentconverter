/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.job.Job;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

@Slf4j
@Service
@Scope("singleton")
public class ConverterJobFactory {

    @Autowired
    ServerConfig serverConfig;

    @Autowired
    private Cache cache;

    @Autowired
    private ServerManager serverManager;

    public Job createJob(@NonNull String jobType, @NonNull String cacheHash, @NonNull Map<String, Object> jobProperties, Map<String, Object> resultProperties) {
        Job job = null;

        if ("pdf".equals(jobType) || "pdfa".equals(jobType)) {
            if(XRechnungToPDFConverterJob.isXRechnung(cacheHash, (File)jobProperties.get(Properties.PROP_INPUT_FILE))) {
                job = new XRechnungToPDFConverterJob(serverConfig, cacheHash, jobProperties, resultProperties);
            } else {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "pdf");
                job = createDocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);
            }
        } else if ("ooxml".equals(jobType)) {
            jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
            job = createDocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);
        } else if ("odf".equals(jobType)) {
            jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "odf");
            job = createDocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);
        } else if ("graphic".equals(jobType)) {
            job = new GraphicConverterJob(jobProperties, resultProperties);
        } else if ("shape2png".equals(jobType)) {
            job = new Shape2PNGConverterJob(cache, jobProperties, resultProperties);
        }

        return job;
    }

    private static Job createDocumentConverterJob(ServerConfig serverConfig, Map<String, Object> jobProperties, Map<String, Object> resultProperties, String jobType) {
        final boolean useCoolConversion = DocumentConverterCoolJob.checkAvailability(serverConfig, false) && DocumentConverterCoolJob.isJobTypeSupported(jobType);
        return useCoolConversion ? new DocumentConverterCoolJob(serverConfig, jobProperties, resultProperties, jobType) : new DocumentConverterJob(serverConfig, jobProperties, resultProperties, jobType);
    }
}
