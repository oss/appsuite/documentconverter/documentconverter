/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.Job;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XNameContainer;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XModel;
import com.sun.star.lang.DisposedException;
import com.sun.star.lang.XComponent;
import com.sun.star.style.XStyleFamiliesSupplier;
import com.sun.star.task.*;
import com.sun.star.ucb.XInteractionSupplyAuthentication;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.XCloseable;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.ByteArrayBuffer;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipFile;

@Slf4j
abstract public class ConverterJob implements Job {

    public static final String ALL_PAGES = "1-99999";

    private static final Charset CHARSET_UTF8 = StandardCharsets.UTF_8;
    private static final String INPUT_TYPE_TXT = "txt";
    private static final String INPUT_TYPE_TEXT = "text";
    private static final int TEXT_DECODER_BUFFER_SIZE = 4096;

    final protected String cacheHash;
    protected Map<String, Object> jobProperties;
    protected Map<String, Object> resultProperties;
    protected String locale = null;
    protected File inputFile = null;
    protected String inputType = null;
    protected String inputURL = null;
    protected File outputFile = null;
    protected JobErrorEx resultJobErrorEx = new JobErrorEx();
    protected String resultMimeType = "";
    protected String resultExtension = "";
    protected int resultPageCount = -1;
    protected int originalPageCount = -1;
    protected int pixelWidth = -1;
    protected int pixelHeight = -1;
    protected boolean resultZipArchive = false;
    protected String pageRange = ALL_PAGES;
    protected boolean hideChanges = true;
    protected boolean hideComments = true;
    protected boolean isCSVImport = false;
    protected boolean closeDocument = true;

    /**
     * {@link ConverterJobInteractionHandler}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     */
    private static class ConverterJobInteractionHandler implements XInteractionHandler {

        private static final int PDF_ERRORCODE_NO_DOCUMENT_CONTENT = 0x01000001;

        private final ConverterJob converterJob;

        /**
         * Initializes a new {@link ConverterJobInteractionHandler}.
         */
        ConverterJobInteractionHandler(ConverterJob converterJob) {
            super();
            this.converterJob = converterJob;
        }

        /* (non-Javadoc)
         * @see com.sun.star.task.XInteractionHandler#handle(com.sun.star.task.XInteractionRequest)
         */
        @Override
        public void handle(@Nullable XInteractionRequest xRequest) {
            if ((null != xRequest) && (null != converterJob)) {
                var interactionRequest = xRequest.getRequest();
                var iaContinuations = xRequest.getContinuations();

                if (interactionRequest instanceof PDFExportException pdfExportException) {
                    var errorCodes = pdfExportException.ErrorCodes;

                    if (null != errorCodes) {
                        for (var errorCode : errorCodes) {
                            if (PDF_ERRORCODE_NO_DOCUMENT_CONTENT == errorCode) {
                                converterJob.setJobErrorEx(new JobErrorEx(JobError.NO_CONTENT));
                            }
                        }
                    }
                } else if (null != iaContinuations) {
                    for (var curContinuation : iaContinuations) {
                        if (null != curContinuation) {
                            final XInteractionPassword xPassword = UnoRuntime.queryInterface(XInteractionPassword.class, curContinuation);
                            final XInteractionPassword2 xPassword2 = UnoRuntime.queryInterface(XInteractionPassword2.class, curContinuation);
                            final XInteractionSupplyAuthentication xSupplyAuth = UnoRuntime.queryInterface(XInteractionSupplyAuthentication.class, curContinuation);

                            if ((null != xPassword) || (null != xPassword2) || (null != xSupplyAuth)) {
                                converterJob.setJobErrorEx(new JobErrorEx(JobError.PASSWORD));
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * Initializes a new {@link ConverterJob}.
     *
     * @param jobProperties The job properties to be used for the job processing.
     * @param resultProperties The properties related to the result of the conversion job done.
     */
    ConverterJob(@NonNull Map<String, Object> jobProperties, @Nullable Map<String, Object> resultProperties) {
        super();

        this.cacheHash = (String)jobProperties.getOrDefault(Properties.PROP_CACHE_HASH, "0");
        this.jobProperties = (null == jobProperties) ? new HashMap<>(12) : jobProperties;
        this.resultProperties = (null == resultProperties) ? new HashMap<>(8) : resultProperties;
    }

    // - IJob ------------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.ICachable#getHash()
     */
    @Override
    public String getHash() {
        return cacheHash;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#addJobProperties(java.util.Map )
     */
    @Override
    public void addJobProperties(@NonNull Map<String, Object> additionalJobProperties) {
        jobProperties.putAll(additionalJobProperties);
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#getJobProperties()
     */
    @Override
    public @NonNull Map<String, Object> getJobProperties() {
        return jobProperties;
    }

    /**
     * @return The current job properties
     */
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#getJobResultProperties()
     */
    @Override
    public @NonNull Map<String, Object> getResultProperties() {
        return resultProperties;
    }

    @Override
    public boolean producesCacheableResult() {
        return true;
    }

    /**
     * @return the status of the conversion job
     * @throws DisposedException
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @Override
    @NonNull
    public ConverterStatus execute(@Nullable Object jobExecutionData) throws Exception {
        var ret = ConverterStatus.ERROR;

        resultJobErrorEx = new JobErrorEx();

        if (beginExecute(jobExecutionData)) {
            doExecute(jobExecutionData);
        }

        ret = endExecute();

        return ret;
    }

    @Override
    public void kill() {
        // default implementation does nothing
        log.trace("DC ConverterJob#kill called but implementation missing");
    }

    // - instance methods ------------------------------------------------------

    /**
     * @param jobExecutionData TODO
     * @return true, if the job execution can be started, false otherwise
     * @throws com.sun.star.lang.DisposedException
     */
    protected boolean beginExecute(@Nullable Object jobExecutionData) throws com.sun.star.lang.DisposedException, IOException {
        resultPageCount = -1;
        originalPageCount = -1;
        resultMimeType = "";
        resultExtension = "";
        resultZipArchive = false;
        pixelWidth = -1;
        pixelHeight = -1;

        applyJobProperties();

        if ((null == inputFile) || !inputFile.canRead() || (null == outputFile)) {
            resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        }

        return resultJobErrorEx.hasNoError();
    }

    /**
     * @param jobExecutionData TODO
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @NonNull
    protected abstract ConverterStatus doExecute(@Nullable Object jobExecutionData) throws Exception;

    /**
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    @NonNull
    protected ConverterStatus endExecute() throws com.sun.star.lang.DisposedException, IOException, Exception {
        if (resultJobErrorEx.hasNoError() && (outputFile.length() > 0) && !resultProperties.containsKey(Properties.PROP_RESULT_BUFFER)) {
            if (null == resultProperties.get(Properties.PROP_RESULT_BUFFER)) {
                resultProperties.put(Properties.PROP_RESULT_BUFFER, FileUtils.readFileToByteArray(outputFile));
            }

            // multiple result pages are stored within a zip archive =>
            // if page count has not been set previously, calculate it
            // from the number of contained zip entries;
            if ((resultPageCount <= 0) && resultZipArchive) {
                try (var zipFile = new ZipFile(outputFile)) {
                    resultPageCount = zipFile.size();
                }
            }
        }

        // set result parameters at job result property set
        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, resultJobErrorEx.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, resultJobErrorEx.getErrorData().toString());

        if (resultJobErrorEx.hasNoError()) {

            if (locale != null) {
                resultProperties.put(Properties.PROP_RESULT_LOCALE, locale);
            }

            resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, resultPageCount);

            // in case of a possible single page conversion, set the
            // 'OriginalPageCount' result property
            if (originalPageCount > 1) {
                resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, originalPageCount);
            }

            if (resultMimeType == null) {
                resultProperties.remove(Properties.PROP_RESULT_MIME_TYPE);
            } else {
                resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, resultMimeType);
            }

            if (resultExtension == null) {
                resultProperties.remove(Properties.PROP_RESULT_EXTENSION);
            } else {
                resultProperties.put(Properties.PROP_RESULT_EXTENSION, resultExtension);
            }

            resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, resultZipArchive);
        }

        return resultJobErrorEx.hasNoError() ? ConverterStatus.OK : ConverterStatus.ERROR;
    }

    protected @Nullable XComponent loadComponent(@Nullable REInstance instance, String pageRange) throws Exception {
        var trace = log.isTraceEnabled();
        XComponent ret = null;

        // always read document from a local (temp) file URL, since LIBO
        // doesn't handle loadComponentFromURL very well due to
        // reading just 4 Byte chunks per each InputStream read call;
        // AOO doesn't have such problems at all, but for consistency reasons,
        // this behaviour is valid for all types of backend

        if ((null != inputFile) && (null != instance) && instance.isConnected()) {
            implEnsureUTF8TextInputFile();


            var inputFileUrlString = ServerManager.getAdjustedFileURL(inputFile);
            final PropertyValue[][] mediaDesc = { getMediaDescriptor(inputFileUrlString, pageRange) };
            var loader = UnoRuntime.queryInterface(XComponentLoader.class, instance.getDesktop());

            long loadStartTime = 0;

            if (trace) {
                loadStartTime = System.currentTimeMillis();
                log.trace("DC ReaderEngine started loading source document");
            }

            if (FileUtils.sizeOf(inputFile) < 1) {
                setJobErrorEx(new JobErrorEx(JobError.NO_CONTENT));
            }

            if (resultJobErrorEx.hasNoError() && (null != loader)) {
                ret = loader.loadComponentFromURL(inputFileUrlString, "_blank", 0, mediaDesc[0]);

                if (trace) {
                    log.trace("DC ReaderEngine finished loading source document: " + (System.currentTimeMillis() - loadStartTime) + "ms");
                }

                if (resultJobErrorEx.hasError()) {
                    ret = null;
                } else {
                    var xModel = (null != ret) ? UnoRuntime.queryInterface(XModel.class, ret) : null;

                    if (xModel != null) {
                        var mediaDescriptor = xModel.getArgs();

                        if (mediaDescriptor != null) {
                            for (var mediaValue : mediaDescriptor) {
                                if ("FilterData".equals(mediaValue.Name)) {
                                    var filterDataValues = (PropertyValue[]) mediaValue.Value;
                                    for (var filterDataValue : filterDataValues) {
                                        if (filterDataValue.Name.equals(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT)) {
                                            originalPageCount = ((Integer) filterDataValue.Value);
                                        }
                                    }
                                }
                            }
                        }

                        doXModelSetup(xModel);
                    }
                }
            } else if (trace) {
                log.trace("DC ReaderEngine error loading source document: " + (System.currentTimeMillis() - loadStartTime) + "ms");
            }
        }

        return ret;
    }

    /**
     * @param inputFileUrlString
     * @param pageRange
     * @return The properly filled media descriptor
     */
    @NonNull
    protected PropertyValue[] getMediaDescriptor(@NonNull String inputFileUrlString, @Nullable String pageRange) {
        var propertyCount = 4;
        var curPropertyPos = 0;
        String filterName = null;

        // add page range, if necessary
        var pageRangeUsed = StringUtils.isNotEmpty(pageRange) && !ALL_PAGES.equals(pageRange);

        if (pageRangeUsed) {
            ++propertyCount;
        }

        // check, if a special filter needs to be set
        if (null != inputType) {
            var documentInformation= DocumentInformation.createFromExtension(inputType);

            if (null != documentInformation) {
                filterName = documentInformation.getFilterName();
                ++propertyCount;
            }

            if ("csv".equals(inputType)) {
                isCSVImport = true;
            }
        }

        // setup media descriptor
        var mediaDesc = new PropertyValue[propertyCount];

        // URL
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "URL";
        mediaDesc[curPropertyPos++].Value = inputFileUrlString;

        // InteractionHandler
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "InteractionHandler";
        mediaDesc[curPropertyPos++].Value = new ConverterJobInteractionHandler(this);

        // Hidden
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "Hidden";
        mediaDesc[curPropertyPos++].Value = Boolean.TRUE;

        // UserFileName
        mediaDesc[curPropertyPos] = new PropertyValue();
        mediaDesc[curPropertyPos].Name = "UserFileName";
        mediaDesc[curPropertyPos++].Value = implGetUserFileURL();

        // PageRange
        if (pageRangeUsed) {
            final PropertyValue[] filterData = new PropertyValue[1];
            filterData[0] = new PropertyValue();
            filterData[0].Name = "PageRange";
            filterData[0].Value = pageRange;

            // FilterData
            mediaDesc[curPropertyPos] = new PropertyValue();
            mediaDesc[curPropertyPos].Name = "FilterData";
            mediaDesc[curPropertyPos++].Value = filterData;
        }

        // FilterName
        if (null != filterName) {
            mediaDesc[curPropertyPos] = new PropertyValue();
            mediaDesc[curPropertyPos].Name = "FilterName";
            mediaDesc[curPropertyPos++].Value = filterName;
        }

        return mediaDesc;
    }

    /**
     * @param xModel
     */
    protected void doXModelSetup(@NonNull XModel xModel) throws DisposedException {
        var trace = log.isTraceEnabled();
        var setupStartTime = System.currentTimeMillis();

        if (trace) {
            log.trace("DC ReaderEngine started setting up model");
        }

        try {
            if (hideChanges || hideComments) {
                final XPropertySet xPropSet = UnoRuntime.queryInterface(XPropertySet.class, xModel);

                if (null != xPropSet) {
                    if (xPropSet.getPropertySetInfo().hasPropertyByName("RedlineDisplayType")) {
                        xPropSet.setPropertyValue("RedlineDisplayType", (short)0);
                    }
                }
            }

            if (isCSVImport) {
                // set HeaderIsOn/HeaderOn to false for CSV imports
                var xPageStyles = UnoRuntime.queryInterface(XNameContainer.class, UnoRuntime.queryInterface(XStyleFamiliesSupplier.class, xModel).getStyleFamilies().getByName("PageStyles"));

                if ((null != xPageStyles) && xPageStyles.hasByName("Default")) {
                    var xPagePropSet = UnoRuntime.queryInterface(XPropertySet.class, xPageStyles.getByName("Default"));

                    if (null != xPagePropSet) {
                        if (xPagePropSet.getPropertySetInfo().hasPropertyByName("HeaderIsOn")) {
                            xPagePropSet.setPropertyValue("HeaderIsOn", Boolean.FALSE);
                        }

                        if (xPagePropSet.getPropertySetInfo().hasPropertyByName("HeaderOn")) {
                            xPagePropSet.setPropertyValue("HeaderOn", Boolean.FALSE);
                        }
                    }
                }
            }
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            throw new DisposedException(e);
        }

        if (trace) {
            log.trace("DC ReaderEngine finished setting up model: " + (System.currentTimeMillis() - setupStartTime) + "ms");
        }
    }

    /**
     * @param component
     * @throws com.sun.star.lang.DisposedException
     * @throws Exception
     */
    protected void closeComponent(@Nullable XComponent component) throws com.sun.star.lang.DisposedException, Exception {
        var trace = log.isTraceEnabled();

        if (closeDocument) {
            var closeStartTime = 0L;

            if (trace) {
                closeStartTime = System.currentTimeMillis();
                log.trace("DC ReaderEngine started closing document");
            }

            if (null != component) {
                var closeable = UnoRuntime.queryInterface(XCloseable.class, component);

                if (null != closeable) {
                    closeable.close(true);
                }
            }

            if (trace) {
                log.trace("DC ReaderEngine finished closing document: " + (System.currentTimeMillis() - closeStartTime) + "ms");
            }
        } else if (trace) {
            log.trace("DC ReaderEngine is not closing document (as requested)");
        }
    }

    /**
     *
     */
    protected void applyJobProperties() {
        Object obj;

        locale = getValueOrDefaultFromJobProperties(Properties.PROP_LOCALE, String.class, locale);
        inputFile = getValueOrDefaultFromJobProperties(Properties.PROP_INPUT_FILE, File.class, inputFile);
        if (null != (obj = jobProperties.get(Properties.PROP_INPUT_TYPE))) {
            inputType = ((String) obj).toLowerCase();
        }
        inputURL = getValueOrDefaultFromJobProperties(Properties.PROP_INPUT_URL, String.class, inputURL);
        outputFile = getValueOrDefaultFromJobProperties(Properties.PROP_OUTPUT_FILE, File.class, outputFile);
        resultZipArchive = getBooleanValueOrDefaultFromJobProperties(Properties.PROP_ZIP_ARCHIVE, resultZipArchive);
        pixelWidth = getIntegerValueOrDefaultFromJobProperties(Properties.PROP_PIXEL_WIDTH, pixelWidth);
        pixelHeight = getIntegerValueOrDefaultFromJobProperties(Properties.PROP_PIXEL_HEIGHT, pixelHeight);
        pageRange = getValueOrDefaultFromJobProperties(Properties.PROP_PAGE_RANGE, String.class, pageRange);
        hideChanges = getBooleanValueOrDefaultFromJobProperties(Properties.PROP_HIDE_CHANGES, hideChanges);
        hideComments = getBooleanValueOrDefaultFromJobProperties(Properties.PROP_HIDE_COMMENTS, hideComments);
        closeDocument = getBooleanValueOrDefaultFromJobProperties(Properties.PROP_CLOSE_DOCUMENT, closeDocument);
    }

    @Nullable protected <T> T getValueOrDefaultFromJobProperties(@NonNull String key, @NonNull Class<T> expectedType, @Nullable T defaultValue) {
        var obj = jobProperties.get(key);
        return (obj != null) ? expectedType.cast(obj) : defaultValue;
    }

    @Nullable protected boolean getBooleanValueOrDefaultFromJobProperties(@NonNull String key, boolean defaultValue) {
        var obj = jobProperties.get(key);
        return (obj != null) ? (Boolean)obj : defaultValue;
    }

    @Nullable protected int getIntegerValueOrDefaultFromJobProperties(@NonNull String key, int defaultValue) {
        var obj = jobProperties.get(key);
        return (obj != null) ? (Integer)obj : defaultValue;
    }

    /**
     *
     */
    protected void setJobErrorEx(@NonNull JobErrorEx jobErrorEx) {
        if ((JobError.PASSWORD == jobErrorEx.getJobError()) && (null != inputType) && inputType.endsWith(Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX)) {
            resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        } else {
            resultJobErrorEx = jobErrorEx;
        }

        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, resultJobErrorEx.getErrorCode());
        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, resultJobErrorEx.getErrorData().toString());
    }

    /**
     * @return The user file name as URI, constructed with file://localhost/${INFO_FILENAME} schema.
     *  If the file name ${INFO_FILENAME} is not given, the default file name 'unknown.document' is used instead.
     */
    @NonNull
    private String implGetUserFileURL() {
        // try to use 'InputUrl' property first
        var userFileURL = inputURL;

        // if no 'InputUrl' property is set, try to use
        // the info filename property to construct the URL
        if (StringUtils.isEmpty(userFileURL)) {
            var fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            // if none user filename is set up to now, create a default one
            if (StringUtils.isEmpty(fileName)) {
                fileName = "unknown." + (StringUtils.isBlank(inputType) ? "document" : inputType);
            }

            userFileURL = "file:///" + fileName;
        }

        return userFileURL;
    }

    private void implEnsureUTF8TextInputFile() {
        if (INPUT_TYPE_TXT.equalsIgnoreCase(inputType) ||
            INPUT_TYPE_TEXT.equalsIgnoreCase(inputType)) {

            var byteArrayBuffer = new ByteArrayBuffer(TEXT_DECODER_BUFFER_SIZE);
            var charsetDetector = new UniversalDetector(null);
            String detectedCharset;

            try (var inputStm = FileUtils.openInputStream(inputFile)) {
                var readBuffer = new byte[TEXT_DECODER_BUFFER_SIZE];

                for (int curReadCount;  0 < (curReadCount = inputStm.read(readBuffer)) && !charsetDetector.isDone();) {
                    charsetDetector.handleData(readBuffer, 0, curReadCount);
                    byteArrayBuffer.append(readBuffer, 0, curReadCount);
                }
            } catch (Throwable e) {
                log.trace("DC server not able to detect charset from text file, assuming UTF-8: " + Throwables.getRootCause(e).getMessage());
            } finally {
                charsetDetector.dataEnd();
                detectedCharset = charsetDetector.getDetectedCharset();
            }

            if ((null != detectedCharset) && (!Constants.CHARSET_UTF_8.equals(detectedCharset))) {
                try {
                    var sourceCharSet = Charset.forName(detectedCharset);

                    // convert byte array with detected source file encoding into a
                    // UTF-8 encoded byte array to be used by ReaderEngine in every case
                    FileUtils.writeByteArrayToFile(inputFile, new String(byteArrayBuffer.toByteArray(), sourceCharSet).getBytes(CHARSET_UTF8));
                } catch (IOException e) {
                    log.trace("DC server not able to write UTF-8 converted source file: " + Throwables.getRootCause(e).getMessage());
                }
            }
        }
    }
}
