/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.sun.star.beans.PropertyValue;
import com.sun.star.container.XIndexAccess;
import com.sun.star.drawing.*;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.uno.UnoRuntime;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Shape2PNGConverterJob extends ConverterJob {

    private static final JobErrorEx GENERAL_ERROR = new JobErrorEx(JobError.GENERAL);

    private String shapeReplacements;

    // represents the page number (starting at 1) of presentation or the sheet number of spreadsheet documents, for text documents this member is 0
    private int pageNumber;

    // shape number (starting at 1) that has to be exported
    private int shapeNumber;

    private final Cache cache;

    private final boolean producesCacheableResult;

    /**
     * Initializes a new {@link Shape2PNGConverterJob}.
     *
     * @param jobProperties the job properties to be used for the conversion job
     * @param resultProperties the result properties being used to providing information about the resulted conversion
     */
    public Shape2PNGConverterJob(@NonNull Cache cache, @NonNull Map<String, Object> jobProperties, @Nullable Map<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
        this.cache = cache;
        this.producesCacheableResult = shapeNumber > 0;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    @NonNull
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /* (non-Javadoc)
     * Executes the conversion job using the provided job execution data
     *
     * @param jobExecutionData the necessary execution data for the job
     * @return The status of the processed job
     */
    @Override
    @NonNull
    protected ConverterStatus doExecute(@Nullable Object jobExecutionData) throws Exception {
        var converterStatus = ConverterStatus.ERROR;
        var instance = (jobExecutionData instanceof REInstance reInstance) ? reInstance : null;

        XComponent component = null;
        if (shapeNumber > 0) {
            // if shape number is given then this job is saving the replacement graphic into the output file
            component = super.loadComponent(instance, pageRange);
            resultJobErrorEx = getShape(instance, component, outputFile, pageNumber, shapeNumber);
            resultMimeType = "image/png";
        } else {
            // this job tries to get the list of replacements from the input document and is adding each replacement into the cache
            converterStatus = ConverterStatus.OK;
            resultJobErrorEx = new JobErrorEx(JobError.NONE);
            var replacements = implGetReplacements(cache);
            if (replacements != null && !replacements.isEmpty()) {
                component = super.loadComponent(instance, pageRange);
                cacheReplacements(instance, component, cache, replacements);
            }
        }

        if (component != null) {
            try {
                closeComponent(component);
                converterStatus = ConverterStatus.OK;
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (resultJobErrorEx.hasNoError()) {
                    converterStatus = ConverterStatus.OK_WITH_ENGINE_ERROR;
                } else {
                    throw e;
                }
            }
        }
        return converterStatus;
    }

    @NonNull
    private static JobErrorEx getShape(@Nullable REInstance instance, @Nullable XComponent component, @Nullable File outputFile, int pageNumber, int shapeNumber) throws Exception {
        XDrawPage drawPage = (null != component) ? retrieveDrawPage(component, pageNumber) : null;

        if ((drawPage != null) && (null != instance)) {
            var componentFactory = instance.getComponentFactory();
            if (componentFactory == null) return GENERAL_ERROR;

            var exportFilter = UnoRuntime.queryInterface(XGraphicExportFilter.class, componentFactory.createInstanceWithContext(
                "com.sun.star.drawing.GraphicExportFilter", instance.getDefaultContext()));
            if (exportFilter == null) return GENERAL_ERROR;

            var shapes = UnoRuntime.queryInterface(XShapes.class, drawPage);
            if (shapes == null) return GENERAL_ERROR;

            var shape = UnoRuntime.queryInterface(XShape.class, shapes.getByIndex(shapeNumber - 1));
            if (shape == null) return GENERAL_ERROR;

            var shapeComponent = UnoRuntime.queryInterface(XComponent.class, shape);
            if (shapeComponent != null) {
                exportFilter.setSourceDocument(shapeComponent);

                var filterData = new PropertyValue[1];

                // TODO: fixed resolution needed ? m_pixelHeight, m_pixelWidth
                filterData[0] = new PropertyValue();
                filterData[0].Name = "ImageResolution";
                filterData[0].Value = 300;

                // set store properties
                var outputArgs = new PropertyValue[4];

                outputArgs[0] = new PropertyValue();
                outputArgs[0].Name = "FilterName";
                outputArgs[0].Value = "PNG";

                outputArgs[1] = new PropertyValue();
                outputArgs[1].Name = "URL";
                outputArgs[1].Value = ServerManager.getAdjustedFileURL(outputFile);

                outputArgs[2] = new PropertyValue();
                outputArgs[2].Name = "Overwrite";
                outputArgs[2].Value = Boolean.TRUE;

                outputArgs[3] = new PropertyValue();
                outputArgs[3].Name = "FilterData";
                outputArgs[3].Value = filterData;

                exportFilter.filter(outputArgs);
            }
        }

        return new JobErrorEx(((outputFile != null) && (outputFile.length() > 0)) ? JobError.NONE : JobError.GENERAL);
    }

    @Nullable
    private static XDrawPage retrieveDrawPage(@NonNull XComponent component, int pageNumber) throws IndexOutOfBoundsException, WrappedTargetException {
        XDrawPage drawPage = null;

        var info = UnoRuntime.queryInterface(XServiceInfo.class, component);
        if (null == info) return null;

        if (info.supportsService("com.sun.star.text.TextDocument")) {
            var drawPageSupplier = UnoRuntime.queryInterface(XDrawPageSupplier.class, component);
            drawPage = (drawPageSupplier != null) ? drawPageSupplier.getDrawPage() : null;
        } else if (pageNumber > 0 && info.supportsService("com.sun.star.sheet.SpreadsheetDocument")) {
            var spreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, component);
            if (spreadsheetDocument != null) {
                var sheets = UnoRuntime.queryInterface(XIndexAccess.class, spreadsheetDocument.getSheets());
                if (sheets != null) {
                    var drawPageSupplier = UnoRuntime.queryInterface(XDrawPageSupplier.class, sheets.getByIndex(pageNumber - 1));
                    drawPage = (drawPageSupplier != null) ? drawPageSupplier.getDrawPage() : null;
                }
            }
        } else if (pageNumber > 0 && info.supportsService("com.sun.star.presentation.PresentationDocument") || info.supportsService("com.sun.star.drawing.DrawingDocument")) {
            var drawPagesSupplier = UnoRuntime.queryInterface(XDrawPagesSupplier.class, component);
            if (drawPagesSupplier != null) {
                var drawPages = drawPagesSupplier.getDrawPages();
                drawPage = (drawPages != null) ? UnoRuntime.queryInterface(XDrawPage.class, drawPages.getByIndex(pageNumber - 1)) : null;
            }
        }

        return drawPage;
    }

    private void cacheReplacements(@Nullable REInstance instance, @Nullable XComponent component, @Nullable Cache cache, @NonNull Map<String, String> replacements) throws Exception {
        if (cache == null) return;

        for (var entry : replacements.entrySet()) {
            var hash = entry.getKey();
            var resourceName = entry.getValue();

            var params = resourceName.split("\\.", -1);
            try {
                var l = params.length;
                if (params[l - 5].startsWith("h") && params[l - 4].startsWith("p") && params[l - 3].startsWith("s") && params[l - 2].startsWith("i")) {
                    var pageIndex = Integer.parseInt(params[l - 4].substring(1));
                    var shapeIndex = Integer.parseInt(params[l - 3].substring(1));
                    var newCacheEntry = cache.startNewEntry(hash);
                    if (newCacheEntry == null) return;

                    try {
                        var jobError = getShape(instance, component, newCacheEntry.getResultFile(), pageIndex + 1, shapeIndex + 1);
                        if (jobError.hasNoError()) {
                            if (!cache.addEntry(newCacheEntry, true, null)) {
                                ServerManager.logTrace("DC server, shape2png, cache entry could not been added.");
                            }
                        }
                    } finally {
                        cache.endNewEntry(newCacheEntry);
                    }
                }
            } catch(RuntimeException e) {
                ServerManager.logTrace("DC server, shape2png, wrong syntax in replacement map: " +  Throwables.getRootCause(e).getMessage());
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        shapeReplacements = getValueOrDefaultFromJobProperties(Properties.PROP_SHAPE_REPLACEMENTS, String.class, shapeReplacements);
        pageNumber = getIntegerValueOrDefaultFromJobProperties(Properties.PROP_PAGE_NUMBER, pageNumber);
        shapeNumber = getIntegerValueOrDefaultFromJobProperties(Properties.PROP_SHAPE_NUMBER, shapeNumber);
    }

    /**
     * Retrieves a map of replacements (which are not cached already)
     *
     * @return a map of replacements or null
     */
    @Nullable
    private Map<String, String> implGetReplacements(@NonNull Cache cache) {
        if (null == shapeReplacements) return null;

        try {
            Map<String, String> replacementMap = new HashMap<>();
            var shapeReplacementsAsJson = new JSONObject(this.shapeReplacements);
            shapeReplacementsAsJson.toMap().forEach((key, value) -> {
                final String hash = key + "-shape2png";
                if (cache.getEntry(hash, null) == null) {
                    replacementMap.put(hash, value.toString());
                }
            });

            return replacementMap;
        } catch (JSONException e) {
            ServerManager.logExcp(e);
        }
        return null;
    }

    @Override
    public boolean producesCacheableResult() {
        return producesCacheableResult;
    }
}
