/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.sun.star.beans.PropertyState;
import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.uno.UnoRuntime;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DocumentConverterJob extends ConverterJob {

    private final ServerConfig serverConfig;

    private final String jobType;

    // The short name of the export filter to be used (html, pdf, odf or ooxml)
    protected String filterShortName = null;

    // The UNO filtername to be used for the export
    protected String filterName = null;

    /**
     * Initializes a new {@link DocumentConverterJob}.
     *
     * @param jobProperties the job properties to be used for the conversion job
     * @param resultProperties the result properties being used to providing information about the resulted conversion
     * @param jobType
     */
    public DocumentConverterJob(@NonNull ServerConfig serverConfig, @NonNull Map<String, Object> jobProperties, @Nullable Map<String, Object> resultProperties, String jobType) {
        super(jobProperties, resultProperties);
        this.serverConfig = serverConfig;
        this.jobType = jobType;
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    @NonNull
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @NotNull
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws Exception {
        var instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;
        var component = super.loadComponent(instance, pageRange);
        var storable = (null != component) ? UnoRuntime.queryInterface(XStorable.class, component) : null;
        var appType = "writer";
        var converterStatus = ConverterStatus.ERROR;
        var trace = ServerManager.isLogTrace();
        var traceStartTimeMillis = 0L;

        if (null != component) {
            final XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, component);

            if (null != info) {
                if (info.supportsService("com.sun.star.text.TextDocument")) {
                    appType = "writer";
                } else if (info.supportsService("com.sun.star.sheet.SpreadsheetDocument")) {
                    appType = "calc";
                } else if (info.supportsService("com.sun.star.presentation.PresentationDocument")) {
                    appType = "impress";
                } else if (info.supportsService("com.sun.star.drawing.DrawingDocument")) {
                    appType = "draw";
                }
            }
        }

        if (null != storable) {
            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine started storing document");
            }

            // set filter name, mimetype and extension according to
            // filter shortname and detected input document type
            setFilterParams(component, appType);

            // always store document to the outputFile, since LIBO
            // doesn't handle storeToURL very well regarding non-seekable output
            // streams with all kinds of export filters
            var isPDFExport = "pdf".equals(resultExtension);
            var outputFileUrlString = ServerManager.getAdjustedFileURL(outputFile);

            // preparing FilterData
            List<PropertyValue> filterData = new ArrayList<>(6);

            filterData.add(new PropertyValue("PixelWidth", -1, pixelWidth, PropertyState.DIRECT_VALUE));
            filterData.add(new PropertyValue("PixelHeight", -1, pixelHeight, PropertyState.DIRECT_VALUE));
            filterData.add(new PropertyValue("UseNativeTextDecoration", -1, Boolean.FALSE, PropertyState.DIRECT_VALUE));

            if (resultZipArchive) {
                filterData.add(new PropertyValue("ContainerType", -1, "ZipArchive", PropertyState.DIRECT_VALUE));
            }

            if (isPDFExport) {
                filterData.add(new PropertyValue("Quality", -1, serverConfig.getPdfJpegQualityPercentage(), PropertyState.DIRECT_VALUE));

                if("pdfa".equals(jobType)) {
                    filterData.add(new PropertyValue("SelectPdfVersion", -1, 1, PropertyState.DIRECT_VALUE));
                }
            }

            // initialize media descriptor
            final List<PropertyValue> mediaDesc = new ArrayList<>(4);

            mediaDesc.add(new PropertyValue("URL", -1, outputFileUrlString, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("FilterName", -1, filterName, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("Overwrite", -1, Boolean.TRUE, PropertyState.DIRECT_VALUE));
            mediaDesc.add(new PropertyValue("FilterData", -1, filterData.toArray(new PropertyValue[0]), PropertyState.DIRECT_VALUE));

            try {
                // store loaded component content with correctly setup filter
                storable.storeToURL(outputFileUrlString, mediaDesc.toArray(new PropertyValue[0]));
                resultJobErrorEx = new JobErrorEx((outputFile.length() > 0) ? JobError.NONE : JobError.GENERAL);

                if (trace) {
                    ServerManager.logTrace("DC ReaderEngine finished storing document: " + (System.currentTimeMillis() - traceStartTimeMillis) + "ms");
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (resultJobErrorEx.hasNoError()) {
                    resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
                }

                if (trace) {
                    final String excpMessage = e.getMessage();

                    ServerManager.logTrace(((null != excpMessage) ?
                        ("DC ReaderEngine error storing document with exception: " + excpMessage + ":") :
                        "DC ReaderEngine killed while storing document.") +
                        (System.currentTimeMillis() - traceStartTimeMillis) + "ms");
                }

                throw e;
            }
        } else if (resultJobErrorEx.hasNoError()) {
            resultJobErrorEx = new JobErrorEx(JobError.GENERAL);
        }

        if (null != component) {
            try {
                closeComponent(component);
                converterStatus = ConverterStatus.OK;
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (resultJobErrorEx.hasNoError()) {
                    converterStatus = ConverterStatus.OK_WITH_ENGINE_ERROR;
                } else {
                    throw e;
                }
            }
        }

        return converterStatus;
    }

    /**
     * @param component The component which contains the document to export.
     * @param appType The application type of the component.
     */
    private void setFilterParams(XComponent component, String appType) {
        boolean calc = false, impress = false, draw = false;

        if ((null == filterShortName) || !(filterShortName.equalsIgnoreCase("pdf") || filterShortName.equalsIgnoreCase("odf") || filterShortName.equalsIgnoreCase("ooxml"))) {
            filterShortName = "html";
        } else {
            filterShortName = filterShortName.toLowerCase();
        }

        // get type of loaded component
        if ((null != component) && (null != appType)) {
            // standard application detection did not work => try it via the detected application type
            if ((appType.contains("calc")) || (appType.contains("excel")) || (appType.contains("spreadsheet"))) {
                calc = true;
            } else if ((appType.contains("impress")) || (appType.contains("powerpoint")) || appType.contains("presentation")) {
                impress = true;
            } else if ((appType.contains("draw")) || (appType.contains("image")) || (appType.contains("svg"))) {
                draw = true;
            }
        }

        // get corresponding filtername, mimetype and extension for export filter in conjunction with application
        switch (filterShortName) {
            case "html" -> {
                filterName = "writer_ox_multi_exporter_filter";
                resultMimeType = "text/html;charset=UTF-8";
                resultExtension = "html";
            }
            case "pdf" -> {
                filterName = "writer_pdf_Export";
                resultMimeType = "application/pdf";
                resultExtension = "pdf";
            }
            case "odf" -> {
                if (calc) {
                    filterName = "calc8";
                    resultMimeType = "application/vnd.oasis.opendocument.spreadsheet";
                    resultExtension = "ods";
                } else if (impress) {
                    filterName = "impress8";
                    resultMimeType = "application/vnd.oasis.opendocument.presentation";
                    resultExtension = "odp";
                } else if (draw) {
                    filterName = "draw8";
                    resultMimeType = "application/vnd.oasis.opendocument.graphics";
                    resultExtension = "odg";
                } else {
                    filterName = "writer8";
                    resultMimeType = "application/vnd.oasis.opendocument.text";
                    resultExtension = "odt";
                }
            }
            case "ooxml" -> {
                if (calc) {
                    filterName = "Calc MS Excel 2007 XML";
                    resultMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    resultExtension = "xlsx";
                } else if (impress || draw) {
                    filterName = "Impress MS PowerPoint 2007 XML";
                    resultMimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    resultExtension = "pptx";
                } else {
                    filterName = "MS Word 2007 XML";
                    resultMimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    resultExtension = "docx";
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();
        filterShortName = getValueOrDefaultFromJobProperties(Properties.PROP_FILTER_SHORT_NAME, String.class, filterShortName);
    }

}
