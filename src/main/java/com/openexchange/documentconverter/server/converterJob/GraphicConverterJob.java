/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.readerengine.REInstance;
import com.sun.star.beans.PropertyValue;
import com.sun.star.graphic.XGraphicProvider;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MimeTypeUtils;

import java.util.Map;

/**
 * {@link GraphicConverterJob}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
public class GraphicConverterJob extends ConverterJob {

    /**
     * Initializes a new {@link GraphicConverterJob}.
     * @param jobProperties
     * @param resultProperties
     */
    GraphicConverterJob(@NonNull Map<String, Object> jobProperties, @Nullable Map<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    @NonNull
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @NotNull
    @Override
    @NonNull
    protected ConverterStatus doExecute(@Nullable Object jobExecutionData) throws Exception {
        var instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;
        XMultiComponentFactory componentFactory;
        var inputFileUrlString = ServerManager.getAdjustedFileURL(inputFile);

        if (ServerManager.isLogTrace()) {
            ServerManager.logTrace("DC converting graphic", new LogData("graphic_url", inputFileUrlString), new LogData("target_mimetype", resultMimeType));
        }

        if ((null != instance) && (null != (componentFactory = instance.getComponentFactory()))) {
            var graphicProvider = UnoRuntime.queryInterface(XGraphicProvider.class, componentFactory.createInstanceWithContext(
                "com.sun.star.graphic.GraphicProvider",
                instance.getDefaultContext()));

            var inputArgs = new PropertyValue[1];
            inputArgs[0] = new PropertyValue();
            inputArgs[0].Name = "URL";
            inputArgs[0].Value = inputFileUrlString;

            var graphic = graphicProvider.queryGraphic(inputArgs);

            if (null != graphic) {
                var widthOrHeightSet = (pixelWidth > 0) || (pixelHeight > 0);
                var filterDataCount = widthOrHeightSet ? 2 : 1;

                var filterData = new PropertyValue[filterDataCount];
                filterData[--filterDataCount] = new PropertyValue();

                if (widthOrHeightSet) {
                    filterData[filterDataCount].Name = "PixelHeight";
                    filterData[filterDataCount].Value = (pixelHeight > 0) ? pixelHeight : -1;

                    filterData[--filterDataCount] = new PropertyValue();
                    filterData[filterDataCount].Name = "PixelWidth";
                    filterData[filterDataCount].Value = (pixelWidth > 0) ? pixelWidth : -1;
                } else {
                    // set FilterData (ImageResolution of 300 DPI);
                    // a dynamic setting of the logical size to be
                    // displayed would be preferred here (KA: #45144 - 2017-03-29)
                    // ---
                    // Update 2017-11-1: set fixed resolution only, if
                    // width/height properties are not set or set to -1x-1
                    filterData[filterDataCount].Name = "ImageResolution";
                    filterData[filterDataCount].Value = 300;
                }

                // set store properties
                var outputArgs = new PropertyValue[4];

                outputArgs[0] = new PropertyValue();
                outputArgs[0].Name = "MimeType";
                outputArgs[0].Value = resultMimeType;

                outputArgs[1] = new PropertyValue();
                outputArgs[1].Name = "URL";
                outputArgs[1].Value = ServerManager.getAdjustedFileURL(outputFile);

                outputArgs[2] = new PropertyValue();
                outputArgs[2].Name = "Overwrite";
                outputArgs[2].Value = Boolean.TRUE;

                outputArgs[3] = new PropertyValue();
                outputArgs[3].Name = "FilterData";
                outputArgs[3].Value = filterData;


                graphicProvider.storeGraphic(graphic, outputArgs);

                resultJobErrorEx = new JobErrorEx();
            }
        }

        return resultJobErrorEx.hasNoError() ? ConverterStatus.OK : ConverterStatus.ERROR;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        resultMimeType = (String)jobProperties.get(Properties.PROP_MIME_TYPE);
        resultMimeType = StringUtils.isEmpty(resultMimeType) ? MimeTypeUtils.IMAGE_PNG_VALUE : resultMimeType.toLowerCase();
        resultExtension = MimeTypeUtils.IMAGE_JPEG_VALUE.equals(resultMimeType) ? "jpg" : (MimeTypeUtils.IMAGE_PNG_VALUE.equals(resultMimeType) ? "png" : "img");
    }
}
