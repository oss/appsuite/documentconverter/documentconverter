/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converterJob;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.config.ServerConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import com.openexchange.documentconverter.server.job.Job;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.converter.ConverterStatus;
import com.openexchange.documentconverter.server.logging.LogData;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import jakarta.annotation.Nullable;
import lombok.NonNull;

public class DocumentConverterCoolJob implements Job {

    /**
     * Initializes a new {@link DocumentConverterCoolJob}.
     *
     * @param _jobProperties the job properties to be used for the conversion job
     * @param _resultProperties the result properties being used to providing information about the resulted conversion
     * @param _jobType
     */
    public DocumentConverterCoolJob(@NonNull ServerConfig serverConfig, @NonNull Map<String, Object> _jobProperties, @Nullable Map<String, Object> _resultProperties, String _jobType) {
            this.serverConfig = serverConfig;
            jobType = _jobType;
            cacheHash = (String)_jobProperties.getOrDefault(Properties.PROP_CACHE_HASH, "0");
            jobProperties = _jobProperties;
            resultProperties = _resultProperties == null ? new HashMap<>(8) : _resultProperties;
        }

        // TODO: new BackendType Needed
        @Override
        @NonNull
        public BackendType backendTypeNeeded() {
            return BackendType.READERENGINE;
        }

        @Override
        @NonNull
        public ConverterStatus execute(Object jobExecutionData) throws Exception {
            ByteArrayInputStream result = convertTo(FileUtils.readFileToByteArray((File)jobProperties.get(Properties.PROP_INPUT_FILE)), getFormatFromJobType(jobType));
            if (result != null) {
                resultProperties.put(Properties.PROP_RESULT_BUFFER, result.readAllBytes());
            }
            return result != null ? ConverterStatus.OK : ConverterStatus.ERROR;
        }

        private ByteArrayInputStream convertTo(byte[] sourceDocument, String destinationFormat) throws Exception {
            final HttpPost uploadFile = new HttpPost(serverConfig.getCoolUrl() + "/cool/convert-to");
            try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                final HttpEntity reqEntity = MultipartEntityBuilder.create().setCharset(Charset.forName("UTF-8"))
                        .addPart("data", new ByteArrayBody(sourceDocument, "testfile"))
                        .addPart("format", new StringBody(destinationFormat, ContentType.TEXT_PLAIN))
                        .build();

                uploadFile.setEntity(reqEntity);
                try(CloseableHttpResponse response = httpClient.execute(uploadFile)) {
                    final HttpEntity responseEntity = response.getEntity();
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC Requesting Collabora Conversion",
                            new LogData("hash", (String)jobProperties.get(Properties.PROP_CACHE_HASH)));
                    }
                    return new ByteArrayInputStream(IOUtils.toByteArray(responseEntity.getContent()));
                }
            }
        }

        /**
         * checkAvailability
         *
         * @return true if COOL_URL is set and if the cool server is responding with "OK"
         */
        public static boolean checkAvailability(@NonNull ServerConfig serverConfig, boolean deepCheck) {
            final String coolUrl = serverConfig.getCoolUrl();
            final boolean useCool = serverConfig.isUseCool();

            if (!useCool || StringUtils.isBlank(coolUrl)) {
                return false;
            }
            if(!deepCheck) {
                return true;
            }
            final HttpGet checkAvailability = new HttpGet(coolUrl);
            try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
                try(CloseableHttpResponse response = httpClient.execute(checkAvailability)) {
                    final HttpEntity responseEntity = response.getEntity();
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC Checking Cool Availability: " + checkAvailability.getRequestLine());
                    }
                    final String availability = IOUtils.toString(responseEntity.getContent(), StandardCharsets.UTF_8);
                    if("OK".equals(availability)) {
                        return true;
                    }
                }
            } catch (Exception e) {
                if (ServerManager.isLogError()) {
                    ServerManager.logError("DC Error Checking Cool Availability: " + checkAvailability.getRequestLine());
                }
            }
            return false;
        }

        @Override
        public void kill() throws Exception {
            //
        }

        @Override
        public void addJobProperties(Map<String, Object> additionalJobProperties) {
            jobProperties.putAll(additionalJobProperties);
        }

        @Override
        @NonNull public String getHash() { return cacheHash; };

        @Override
        @NonNull
        public Map<String, Object> getJobProperties() {
            return jobProperties;
        }

        @Override
        @NonNull
        public Map<String, Object> getResultProperties() {
            return resultProperties;
        }

        @Override
        public boolean producesCacheableResult() {
            return true;
        }

        /**
         * supportsJobType
         *
         * @param jobType
         *
         * @return true if the jobType is supported
         */
        public static boolean isJobTypeSupported(String jobType) {
            return getFormatFromJobType(jobType) != null;
        }

        private static String getFormatFromJobType(String jobType) {
            switch(jobType) {
                case "pdf" : return "pdf";
            }
            return null;
        }

        final String jobType;

        final String cacheHash;

        final Map<String, Object> jobProperties;

        final Map<String, Object> resultProperties;

        private final ServerConfig serverConfig;
}
