/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import jakarta.annotation.Nullable;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

public class DocumentInformation {

    @SuppressWarnings("unused")
    private DocumentInformation() {
        // not to be used
        super();

        m_filterName = null;
        m_documentType = null;
    }

    /**
     * Initializes a new {@link DocumentInformation}.
     * @param filterName
     * @param documentType
     */
    public DocumentInformation(@NonNull String filterName, @NonNull final DocumentType documentType) {
        m_filterName = filterName;
        m_documentType = documentType;
    }

    /**
     * @return
     */
    public @NonNull String getFilterName() {
        return m_filterName;
    }

    /**
     * @return
     */
    public @NonNull DocumentType getDocumentType() {
        return m_documentType;
    }

    /**
     * @param extension
     * @return
     */
    public static boolean isExtensionSupported(final String extension) {
        return (null != extension) && SUPPORTED_TYPE_MAP.containsKey(extension.toLowerCase());
    }

    /**
     * @param extension
     * @return
     */
    public static @Nullable DocumentInformation createFromExtension(@Nullable final String extension) {
        return (null != extension) ? SUPPORTED_TYPE_MAP.get(extension.toLowerCase()) : null;
    }

    // - Members -----------------------------------------------------------

    private final String m_filterName;
    private final DocumentType m_documentType;

    // - Static  Members -------------------------------------------------------

    private static Map<String, DocumentInformation> SUPPORTED_TYPE_MAP = new HashMap<>();

    // Create map of all supported types
    static {
        SUPPORTED_TYPE_MAP.put("csv", new DocumentInformation("Text - txt - csv (StarCalc)", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("doc", new DocumentInformation("MS Word 97", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("docm", new DocumentInformation("MS Word 2007 XML", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("docx", new DocumentInformation("MS Word 2007 XML", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("docxm", new DocumentInformation("MS Word 2007 XML", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("dot", new DocumentInformation("MS Word 97 Vorlage", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("dotm", new DocumentInformation("MS Word 2007 XML Template", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("dotx", new DocumentInformation("MS Word 2007 XML Template", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("dotxm", new DocumentInformation("MS Word 2007 XML Template", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("fodp", new DocumentInformation("OpenDocument Presentation Flat XML", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("fods", new DocumentInformation("OpenDocument Spreadsheet Flat XML", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("fodt", new DocumentInformation("OpenDocument Text Flat XML", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("html", new DocumentInformation("HTML (StarWriter)", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odg", new DocumentInformation("impress8_draw", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odgm", new DocumentInformation("impress8_draw", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odp", new DocumentInformation("impress8", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odpm", new DocumentInformation("impress8", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("ods", new DocumentInformation("calc8", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odsm", new DocumentInformation("calc8", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odt", new DocumentInformation("writer8", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("odtm", new DocumentInformation("writer8", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otg", new DocumentInformation("draw8_template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otgm", new DocumentInformation("draw8_template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otm", new DocumentInformation("writerglobal8", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("ots", new DocumentInformation("calc8_template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otsm", new DocumentInformation("calc8_template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otp", new DocumentInformation("impress8_template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("otpm", new DocumentInformation("impress8_template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("ott", new DocumentInformation("writer8_template", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("ottm", new DocumentInformation("writer8_template", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("pot", new DocumentInformation("MS PowerPoint 97 Vorlage", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("potm", new DocumentInformation("Impress MS PowerPoint 2007 XML Template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("potx", new DocumentInformation("Impress MS PowerPoint 2007 XML Template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("potxm", new DocumentInformation("Impress MS PowerPoint 2007 XML Template", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("pps", new DocumentInformation("MS PowerPoint 97", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("ppt", new DocumentInformation("MS PowerPoint 97", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("pptm", new DocumentInformation("Impress MS PowerPoint 2007 XML", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("pptx", new DocumentInformation("Impress MS PowerPoint 2007 XML", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("pptxm", new DocumentInformation("Impress MS PowerPoint 2007 XML", DocumentType.PRESENTATION_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("rtf", new DocumentInformation("Rich Text Format", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("txt", new DocumentInformation("Text", DocumentType.TEXT_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlm", new DocumentInformation("Calc MS Excel 2007 XML", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xls", new DocumentInformation("MS Excel 97", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlsm", new DocumentInformation("Calc MS Excel 2007 XML", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlst", new DocumentInformation("MS Excel 97 Vorlage/Template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlstm", new DocumentInformation("Calc MS Excel 2007 XML Template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlt", new DocumentInformation("MS Excel 97 Vorlage/Template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xltm", new DocumentInformation("Calc MS Excel 2007 XML Template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlsb", new DocumentInformation("Calc MS Excel 2007 Binary", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlsx", new DocumentInformation("Calc MS Excel 2007 XML", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xlsxm", new DocumentInformation("Calc MS Excel 2007 XML", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xltx", new DocumentInformation("Calc MS Excel 2007 XML Template", DocumentType.SPREADSHEET_DOCUMENT));
        SUPPORTED_TYPE_MAP.put("xltxm", new DocumentInformation("Calc MS Excel 2007 XML Template", DocumentType.SPREADSHEET_DOCUMENT));
    }
}

