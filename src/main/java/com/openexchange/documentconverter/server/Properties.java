/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import lombok.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Properties {
    // TODO NEXT Change the Name_Jsonname Properties to BidiMap?
    public static final String INFO_PREFIX = "info_";

    public static final int MAX_ERROR_MESSAGE_TOTAL_LENGTH = 2048;

    public static final int MAX_ERROR_MESSAGE_END_LENGTH = 256;

    public static final String CACHE_PROPERTIES_FILENAME = "properties"; // String

    public static final String CACHE_RESULT_FILENAME = "result"; // String

    public static final String PROP_CACHE_PERSIST_ENTRY_NAME = "EntryName"; // String

    public static final String PROP_CACHE_HASH = "CacheHash"; // String

    public static final String PROP_LOCALE = "Locale"; // String

    public static final String PROP_FILTER_SHORT_NAME = "FilterShortName"; // String

    public static final String PROP_PIXEL_WIDTH = "PixelWidth"; // Integer

    public static final String PROP_PIXEL_HEIGHT = "PixelHeight"; // Integer

    public static final String PROP_PIXEL_ZOOM= "PixelZoom"; // Double

    public static final String PROP_INPUT_FILE = "InputFile"; // File

    public static final String PROP_INPUT_STREAM = "InputStream"; // InputStream

    public static final String PROP_INPUT_TYPE = "InputType"; // String

    public static final String PROP_INPUT_URL = "InputUrl"; // String

    public static final String PROP_JOBID = "JobId"; // String

    public static final String PROP_JOBTYPE = "JobType"; // String

    public static final String PROP_MIME_TYPE = "MimeType"; // String

    public static final String PROP_OUTPUT_FILE = "OutputFile"; // File

    public static final String PROP_PAGE_NUMBER = "PageNumber"; // Integer

    public static final String PROP_SHAPE_NUMBER = "ShapeNumber"; // Integer

    public static final String PROP_PAGE_RANGE = "PageRange"; // String

    public static final String PROP_ZIP_ARCHIVE = "ZipArchive"; // Boolean

    public static final String PROP_PRIORITY = "Priority"; // JobPriority

    public static final String PROP_READERENGINE_ROOT = "ReaderEngineRoot"; // String

    public static final String PROP_REMOTE_METHOD = "Method"; // String

    public static final String PROP_INFO_FILENAME = INFO_PREFIX + "Filename"; // String

    public static final String PROP_FEATURES_ID = "FeaturesId"; // Integer

    public static final String PROP_AUTOSCALE = "AutoScale"; // Boolean

    public static final String PROP_HIDE_CHANGES = "HideChanges"; // Boolean

    public static final String PROP_HIDE_COMMENTS = "HideComments"; // Boolean

    public static final String PROP_ASYNC = "Async"; // Boolean

    public static final String PROP_IMAGE_SCALE_TYPE = "ImageScaleType"; // String

    public static final String PROP_IMAGE_RESOLUTION = "ImageResolution"; // Integer

    public static final String PROP_USER_REQUEST = "UserRequest"; // Boolean

    public static final String PROP_CLOSE_DOCUMENT = "CloseDocument"; // Boolean

    public static final String PROP_SHAPE_REPLACEMENTS = "ShapeReplacements"; // String

    public static final String PROP_RESULT_LOCALE = PROP_LOCALE; // String

    public static final String PROP_RESULT_ERROR_CODE = "ErrorCode"; // Integer

    public static final String PROP_RESULT_ERROR_DATA = "ErrorData"; // String

    public static final String PROP_RESULT_EXTENSION = "Extension"; // String

    public static final String PROP_RESULT_BUFFER = "ResultBuffer"; // byte[]

    public static final String PROP_RESULT_JOBID = PROP_JOBID; // String

    public static final String PROP_RESULT_MIME_TYPE = PROP_MIME_TYPE; // String

    public static final String PROP_RESULT_PAGE_COUNT = "PageCount"; // Integer

    public static final String PROP_RESULT_ORIGINAL_PAGE_COUNT = "OriginalPageCount"; // Integer

    public static final String PROP_RESULT_PAGE_NUMBER = PROP_PAGE_NUMBER; // Integer

    public static final String PROP_RESULT_SHAPE_NUMBER = PROP_SHAPE_NUMBER; // Integer

    public static final String PROP_RESULT_TEMP_INPUT_FILE = "TempInputFile"; // File

    public static final String PROP_RESULT_TEMP_INFO_FILENAME = PROP_INFO_FILENAME; // String

    public static final String PROP_RESULT_TEMP_INPUT_TYPE = PROP_INPUT_TYPE; // String

    public static final String PROP_RESULT_SINGLE_PAGE_JOBTYPE = "SinglePageJobType"; // String

    public static final String PROP_RESULT_ZIP_ARCHIVE = PROP_ZIP_ARCHIVE; // Boolean

    public static final String PROP_RESULT_ASYNC = PROP_ASYNC; // Boolean

    public static final String OX_RESCUEDOCUMENT_EXTENSION_APPENDIX = "_ox";

    public static final String OX_DOCUMENTCONVERTER_TEMPDIR_NAME = "oxdc.tmp";

    public static final String OX_DOCUMENTCONVERTER_LTSDIR_NAME = "oxdc.lts";

    public static final String JSON_KEY_LOCALE = "locale";

    public static final String JSON_KEY_ERRORCODE = "errorCode";

    public static final String JSON_KEY_ERRORDATA = "errorData";

    public static final String JSON_KEY_EXTENSION = "extension";

    public static final String JSON_KEY_JOBID = "jobId";

    public static final String JSON_KEY_MIMETYPE = "mimeType";

    public static final String JSON_KEY_PAGECOUNT = "pageCount";

    public static final String JSON_KEY_ORIGINALPAGECOUNT = "originalPageCount";

    public static final String JSON_KEY_PAGENUMBER = "pageNumber";

    public static final String JSON_KEY_SHAPENUMBER = "shapeNumber";

    public static final String JSON_KEY_INPUTFILE = "inputFile";

    public static final String JSON_KEY_INPUTTYPE = "inputType";

    public static final String JSON_KEY_INFOFILENAME = "infoFileName";

    public static final String JSON_KEY_SINGLEPAGEJOBTYPE = "singlePageJobType";

    public static final String JSON_KEY_ZIPARCHIVE = "zipArchive";

    public static final String JSON_KEY_ASYNC = "async";

    public static JSONObject toJSONResult(@NonNull Map<String, Object> resultProperties) {
        final JSONObject jsonResult = new JSONObject();

        try {
            Object curObj = null;

            if (null != (curObj = resultProperties.get(PROP_RESULT_LOCALE))) {
                jsonResult.put(JSON_KEY_LOCALE, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_ERROR_CODE))) {
                jsonResult.put(JSON_KEY_ERRORCODE, ((Integer) curObj).intValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_ERROR_DATA))) {
                jsonResult.put(JSON_KEY_ERRORDATA, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_EXTENSION))) {
                jsonResult.put(JSON_KEY_EXTENSION, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_JOBID))) {
                jsonResult.put(JSON_KEY_JOBID, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_MIME_TYPE))) {
                jsonResult.put(JSON_KEY_MIMETYPE, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_PAGE_COUNT))) {
                jsonResult.put(JSON_KEY_PAGECOUNT, ((Integer) curObj).intValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_ORIGINAL_PAGE_COUNT))) {
                jsonResult.put(JSON_KEY_ORIGINALPAGECOUNT, ((Integer) curObj).intValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_PAGE_NUMBER))) {
                jsonResult.put(JSON_KEY_PAGENUMBER, ((Integer) curObj).intValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_SHAPE_NUMBER))) {
                jsonResult.put(JSON_KEY_SHAPENUMBER, ((Integer) curObj).intValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INPUT_FILE))) {
                jsonResult.put(JSON_KEY_INPUTFILE, ((File) curObj).getPath());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INFO_FILENAME))) {
                jsonResult.put(JSON_KEY_INFOFILENAME, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INPUT_TYPE))) {
                jsonResult.put(JSON_KEY_INPUTTYPE, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_SINGLE_PAGE_JOBTYPE))) {
                jsonResult.put(JSON_KEY_SINGLEPAGEJOBTYPE, curObj);
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_ZIP_ARCHIVE))) {
                jsonResult.put(JSON_KEY_ZIPARCHIVE, ((Boolean) curObj).booleanValue());
            }

            if (null != (curObj = resultProperties.get(PROP_RESULT_ASYNC))) {
                jsonResult.put(JSON_KEY_ASYNC, ((Boolean) curObj).booleanValue());
            }
        } catch (JSONException e) {
            // TODO NEXT implement it. Exception Handling!
            // DocumentConverterManager.logExcp(e);
        }

        return jsonResult;
    }
}
