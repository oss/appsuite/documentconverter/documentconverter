/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.config;

import com.openexchange.documentconverter.server.Properties;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@Slf4j
@Getter
@Component
public class ServerConfig {

    public static final int MAX_CACHE_ENTRYCOUNT = 15000000;
    private static final String HTTP_SCHEMA = "http://";
    private static final String HTTPS_SCHEMA = "https://";

    // to be used for client connections to Http servers
    private SSLContext SSL_CONTEXT = null;

    // the directory, containing the ./program directory of the underlying readerEngine
    @Value("${com.openexchange.documentconverter.installDir:/opt/readerengine}")
    private String reInstallDir;

    // the directory, containing the cache for persistent, huge job data at runtime
    @Value("${com.openexchange.documentconverter.cacheDir:/var/spool/open-xchange/documentconverter/readerengine.cache}")
    private String cacheDir;

    @Value("${com.openexchange.documentconverter.cacheDirDepth:0}")
    private int cacheDirDepth;

    // a temp. directory, containing the runtime data for each started engine process
    @Value("${com.openexchange.documentconverter.scratchDir:/var/spool/open-xchange/documentconverter/readerengine.scratch}")
    private String scratchDirPath;

    // a temp. directory, containing files that could not be loaded due to an error condition or due to a timeout
    @Value("${com.openexchange.documentconverter.errorDir:#{null}}")
    private String errorDir;

    // USE_COOL
    @Value("${com.openexchange.documentconverter.useCool:false}")
    private boolean useCool;

    // specifies if Collabora Online should be used for document conversion, then the url to
    // the server is needed, e.g: "http://localhost:9980"
    @Value("${com.openexchange.documentconverter.coolUrl:#{null}}")
    private String coolUrl;

    // external URL blacklist file
    @Value("${com.openexchange.documentconverter.blacklistFile:/opt/open-xchange/etc/readerengine.blacklist}")
    private String urlBlacklistFile;

    // external URL whitelist file
    @Value("${com.openexchange.documentconverter.whitelistFile:/opt/open-xchange/etc/readerengine.whitelist}")
    private String urlWhitelistFile;

    // external URL maximum link count
    @Value("${com.openexchange.documentconverter.urlLinkLimit:200}")
    private int urlLinkLimit;

    // proxy server for external readerengine URLs
    @Value("${com.openexchange.documentconverter.urlLinkProxy:#{null}}")
    private String urlLinkProxy;

    // Cache URL(s) to be used for remote conversions; the local backend will handle caching in every case, but a cache speedup can be
    // achieved by adding additional cache servers
    @Value("${com.openexchange.documentconverter.cacheServiceUrl:#{null}}")
    private String cacheServiceUrl;

    // the number of engines, working in parallel to execute jobs;
    @Value("${com.openexchange.documentconverter.jobProcessorCount:3}")
    private int jobProcessorCount;

    // the number of engines, held within an instance pool;
    @Value("${com.openexchange.documentconverter.processorPoolSize:0}")
    private int jobProcessorPoolSize;

    // the maximum number of executed jobs, after which a single engine is automatically restarted;
    @Value("${com.openexchange.documentconverter.jobRestartCount:50}")
    private int jobRestartCount;

    // Restart a job after a TIMEOUT error occurs
    // JOB_RESTART_AFTER_TIMEOUT
    @Value("${com.openexchange.documentconverter.jobRestartAfterTimeout:false}")
    private boolean jobRestartAfterTimeout;

    // the timeout in milliseconds, after which the execution of a single job is terminated
    @Value("${com.openexchange.documentconverter.jobExecutionTimeoutMilliseconds:60000}")
    private long jobExecutionTimeoutMilliseconds;

    // the maximum size in MegaBytes of virtual memory that a readerengine instance can allocate; -1 for no upper limit
    @Value("${com.openexchange.documentconverter.maxVMemMB:2048}")
    private long maxVMemMB;

    // the maximum size in MegaBytes for source files to be converted; -1 for no upper limit
    @Value("${com.openexchange.documentconverter.maxSourceFileSizeMB:128}")
    private long maxSourceFileSizeMB;

    // the minimum size in MegaBytes of the volume, the cache is located on, that will not be used by the cache;
    @Value("${com.openexchange.documentconverter.minFreeVolumeSizeMB:1024}")
    private long minFreeVolumeSizeMB;

    // the maximum size in MegaBytes of all persistently cached converter job entries at runtime; the total cache size will also take into
    // account the value for the minimum free volume size, so that at least this free volume size will not be occupied by the cache -1 for
    // no upper limit
    @Value("${com.openexchange.documentconverter.maxCacheSizeMB:-1}")
    private long maxCacheSizeMB;

    // the maximum count of converter jobs cached at runtime
    @Value("${com.openexchange.documentconverter.maxCacheEntries:4000000}")
    private long maxCacheEntries;

    // The percentage of MAX_CACHEENTRIES when the cache clean cleanup thread starts to clean-up cache entries down to the number
    // of CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE of MAX_CACHEENTRIES.
    // Internally limited to 1 <= CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE <= 100.
    @Value("${com.openexchange.documentconverter.cacheCleanupThresholdUpperPercentage:95}")
    private long cacheCleanupThresholdUpperPercentage;

    //  The percentage of MAX_CACHEENTRIES when the cache clean stops to clean-up cache entries.
    // Internally limited to 0 <= CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE <= CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE.
    // CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE
    @Value("${com.openexchange.documentconverter.cacheCleanupThresholdLowerPercentage:95}")
    private long cacheCleanupThresholdLowerPercentage;

    // the timeout in seconds, after which a cached job execution result is automatically removed from the cache 0 for disabling the timeout
    // based removal
    @Value("${com.openexchange.documentconverter.cacheEntryTimeoutSeconds:2592000}")
    private long cacheEntryTimeoutSeconds;

    // the period in seconds, after which the next cache cleanup should happen
    @Value("${com.openexchange.documentconverter.cacheCleanupPeriodSeconds:300}")
    private long cacheCleanupPeriodSeconds;

    // the period in seconds for the which the cache tries to reestablish a connection to remote cache server
    // READINESS_UP_RECOVER_PERIOD_SECONDS
    @Value("${com.openexchange.documentconverter.probe.readiness.upRecoveryPeriodSeconds:20}")
    private long upRecoveryPeriodSeconds;

    // the maximum size in MegaBytes of virtual memory that a PDFTool instance can allocate; -1 for no upper limit
    @Value("${com.openexchange.documentconverter.pdftoolMaxVMemMB:1024}")
    private long pdftoolMaxVMemMB;

    // Flag to forbid/allow the servlet engine to handle local file:// URLS
    @Value("${com.openexchange.documentconverter.servletLocalFileUrls:false}")
    private boolean allowLocalFileUrls;

    // the timeout in milliseconds, after which an entry is removed from the errorcache
    @Value("${com.openexchange.documentconverter.errorCacheTimeoutSeconds:600}")
    private long errorCacheTimeoutSeconds;

    @Value("${com.openexchange.documentconverter.autoCleanupTimeoutSeconds:900}")
    private long autoCleanupTimeoutSeconds;

    // the max. number of cycles, a hash is added to the errorcache,
    // until it won't be removed from the errorcache anymore
    @Value("${com.openexchange.documentconverter.errorCacheMaxCycleCount:5}")
    private int errorCacheMaxCycleCount;

    // JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH
    @Value("${com.openexchange.documentconverter.jobAsyncQueueCountLimitHigh:2048}")
    private int jobAsyncQueueCountLimitHigh;

    // JOB_QUEUE_COUNT_LIMIT_HIGH
    @Value("${com.openexchange.documentconverter.jobQueueCountLimitHigh:40}")
    private int jobQueueCountLimitHigh;

    // JOB_QUEUE_COUNT_LIMIT_LOW
    @Value("${com.openexchange.documentconverter.jobQueueCountLimitLow:30}")
    private int jobQueueCountLimitLow;

    // JOB_QUEUE_TIMEOUT_SECONDS
    @Value("${com.openexchange.documentconverter.jobQueueTimeoutSeconds:300}")
    private int jobQueueTimeoutSeconds;

    // The JPEG image quality percentage value when exporting documents to PDF
    @Value("${com.openexchange.documentconverter.pdf.jpegQualityPercentage:75}")
    private int pdfJpegQualityPercentage;

    // LIVENESS_PERIOD_SECONDS
    @Value("${com.openexchange.documentconverter.probe.liveness.periodSeconds:10}")
    private long livenessPeriodSeconds;

    // LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS
    @Value("${com.openexchange.documentconverter.probe.liveness.downAfterReadinessDownSeconds:15}")
    private long livenessDownAfterReadinessDownSeconds;

    // READINESS_PERIOD_SECONDS
    @Value("${com.openexchange.documentconverter.probe.readiness.periodSeconds:5}")
    private long readinessPeriodSeconds;

    // READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS
    @Value("${com.openexchange.documentconverter.probe.readiness.downAfterUsedServiceUnavailabilitySeconds:300}")
    private long readinessDownAfterUsedServiceUnavailabilitySeconds;

    private File scratchDir;

    /**
     * Initializes a new {@link ServerConfig}.
     */
    public ServerConfig() {
    }

    /**
     * Initializes a new {@link ServerConfig}.
     * @param sslContext The SSL context to be used for client connections to http servers
     */
    public ServerConfig(@Nullable SSLContext sslContext) {
        SSL_CONTEXT = sslContext;
    }

    /**
     * Initializes the configuration instance with the values from the configuration settings.
     */
    @PostConstruct
    public void initConfig() throws IOException {

        createScratchDir();
        createErrorDir();

        // TODO what's the purpose of this?
        new File(reInstallDir).isDirectory();
        if (StringUtils.isEmpty(reInstallDir) || !new File(reInstallDir).canRead()) {
            // TODO NEXT Exception Handling
            throw new IOException("DC ReaderEngine install path is not readable => DC will not be available: " + reInstallDir);
        }

        jobProcessorCount = Math.max(1, jobProcessorCount);
        jobRestartCount = Math.max(0, jobRestartCount);
        jobProcessorPoolSize = Math.max(0, jobProcessorPoolSize);
        jobExecutionTimeoutMilliseconds = Math.max(1, jobExecutionTimeoutMilliseconds);
        maxVMemMB = Math.max(1, maxVMemMB);
        pdftoolMaxVMemMB = Math.max(1, pdftoolMaxVMemMB);
        maxSourceFileSizeMB = Math.max(1, maxSourceFileSizeMB);
        minFreeVolumeSizeMB = Math.max(0, minFreeVolumeSizeMB);
        maxCacheSizeMB = Math.max(-1, maxCacheSizeMB);
        maxCacheEntries = ((maxCacheEntries < 0) || (maxCacheEntries > MAX_CACHE_ENTRYCOUNT)) ? MAX_CACHE_ENTRYCOUNT : maxCacheEntries;
        cacheCleanupThresholdUpperPercentage = Math.min(100, Math.max(1, cacheCleanupThresholdUpperPercentage));
        cacheCleanupThresholdLowerPercentage = Math.min(cacheCleanupThresholdLowerPercentage, cacheCleanupThresholdUpperPercentage);
        cacheEntryTimeoutSeconds = Math.max(0, cacheEntryTimeoutSeconds);
        cacheCleanupPeriodSeconds = Math.max(0, cacheCleanupPeriodSeconds);
        upRecoveryPeriodSeconds = Math.max(1, upRecoveryPeriodSeconds);
        errorCacheTimeoutSeconds = Math.max(0, errorCacheTimeoutSeconds);
        autoCleanupTimeoutSeconds = Math.max(0, autoCleanupTimeoutSeconds);
        errorCacheMaxCycleCount = Math.max(0, errorCacheMaxCycleCount);
        jobQueueCountLimitHigh = Math.max(1, jobQueueCountLimitHigh);
        jobQueueCountLimitLow = Math.max(0, jobQueueCountLimitLow);
        if (jobQueueCountLimitLow >= jobQueueCountLimitHigh) {
            // lower limit higher or equal than higher limit does not make any sense
            jobQueueCountLimitLow = jobQueueCountLimitHigh - 1;
        }
        jobQueueTimeoutSeconds = Math.max(1, jobQueueTimeoutSeconds);
        jobAsyncQueueCountLimitHigh = Math.max(1, jobAsyncQueueCountLimitHigh);
        pdfJpegQualityPercentage = Math.min(100, Math.max(1, pdfJpegQualityPercentage));
        livenessPeriodSeconds = Math.max(1, livenessPeriodSeconds);
        readinessPeriodSeconds = Math.max(1, readinessPeriodSeconds);
    }

    private void createScratchDir() throws IOException {
        if (StringUtils.isEmpty(scratchDirPath)) {
            // TODO NEXT Exception handling
            throw new NullPointerException("scratchDir is empty");
        }

        scratchDirPath = new File(scratchDirPath, Properties.OX_DOCUMENTCONVERTER_TEMPDIR_NAME).toString();

        scratchDir = new File(scratchDirPath);

        try {
            // try to delete existing one
            FileUtils.deleteDirectory(scratchDir);
        } catch (@SuppressWarnings("unused") final IOException e) {
            // nothing to do
        }

        try {
            FileUtils.forceMkdir(scratchDir);
        } catch (@SuppressWarnings("unused") final IOException e) {
            // nothing to do
        }

        if (!scratchDir.canWrite()) {
            // TODO NEXT Exception handling
            throw  new IOException("DC scratch directory could not be created or is write protected => Installation is broken: " + scratchDirPath);
        }
    }

    private static final String ERROR_FILE_SUBDIR = "error";
    private static final String TIMEOUT_FILE_SUBDIR = "timeout";

    private void createErrorDir() throws IOException {
        String engineErrorDir = errorDir;

        // create error and directory in case the entry is set within the config
        if (StringUtils.isNotEmpty(engineErrorDir)) {
            // Error dir
            final File errorDir = new File(engineErrorDir, ERROR_FILE_SUBDIR);

            try {
                FileUtils.forceMkdir(errorDir);
            } catch (@SuppressWarnings("unused") final IOException e) {
                //
            }

            if (!errorDir.canWrite()) {
                throw  new IOException("DC error directory could not be created or is write protected => Error files can't be be saved to: " + errorDir);

            } else {
                // Timeout dir
                final File timeoutDir = new File(engineErrorDir, TIMEOUT_FILE_SUBDIR);

                try {
                    FileUtils.forceMkdir(timeoutDir);
                } catch (@SuppressWarnings("unused") final IOException e) {
                    //fromJson
                }

                if (!timeoutDir.canWrite()) {
                    throw  new IOException("DC timeout directory could not be created or is write protected => Timeout files can't be saved to: " + timeoutDir);
                }
            }
        }
    }

    public boolean getJobRestartAfterTimeout() {
        return jobRestartAfterTimeout;
    }

}
