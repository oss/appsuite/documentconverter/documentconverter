/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.cache.CacheDescriptor;
import com.openexchange.documentconverter.server.cache.CacheEntry;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.ox.error.DataExceptionCodes;
import com.openexchange.documentconverter.server.ox.error.OXException;
import com.openexchange.documentconverter.server.ox.tools.URITools;
import com.openexchange.documentconverter.server.readerengine.REDescriptor;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Slf4j
@Service
@Scope("singleton")
public class ServerManager {

    public static final int IMAGE_RESOLUTION_ORIGINAL = -1;
    public static final int HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS=60000;
    public static final int HTTP_DOCUMENTCONVERTER_READ_TIMEOUT_MS=(int) ((HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS * 210L) / 100L);
    public static final int HTTP_CACHESERVER_CONNECT_TIMEOUT_MS=5000;
    public static final int HTTP_CACHESERVER_READ_TIMEOUT_MS=60000;
    public static final String[] BASH_SEARCH_PATHS_STR = {
            "/bin/bash",
            "/sbin/bash",
            "/usr/bin/bash",
            "/usr/local/bin/bash",
            "/usr/local/sbin/bash"
    };

    private static final Map<JobError, Integer> DEFAULT_ERROR_RUNCOUNT_MAP = new HashMap<>();

    private static String BASH_EXECUTABLE_PATH_STR = null;

    static {
        for (final JobError curJobError : JobError.values()) {
            DEFAULT_ERROR_RUNCOUNT_MAP.put(curJobError, 1);
        }

        for (final var curBashSearchPath : BASH_SEARCH_PATHS_STR) {
            for (var curBashPathStr : BASH_SEARCH_PATHS_STR) {
                if (new File(curBashPathStr).canExecute()) {
                    BASH_EXECUTABLE_PATH_STR = curBashPathStr;
                    break;
                }
            }
        }

        if (null == BASH_EXECUTABLE_PATH_STR) {
            BASH_EXECUTABLE_PATH_STR = "bash";
        }
    }

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    private final ServerConfig serverConfig;

    private CacheServiceClient cacheServiceClient = null;

    private final List<String> serverErrors = new ArrayList<>();

    private static final List<Pattern> URL_BLACKLIST_PATTERNS = new ArrayList<>();

    private static final List<Pattern> URL_WHITELIST_PATTERNS = new ArrayList<>();

    // TODO NEXT only used in old dc tests
    private static final Function<URL, Optional<OXException>> URL_VALIDATOR_FUNC = (hopURL) -> {
        final String hopURLString = hopURL.toString().trim();
        Optional<OXException> ret = Optional.empty();

        if ((StringUtils.isBlank(hopURLString)) ||
                ((true == implIsMatchingURLPattern(URL_BLACKLIST_PATTERNS, hopURLString)) &&
                        (false == implIsMatchingURLPattern(URL_WHITELIST_PATTERNS, hopURLString)))) {

            ret = Optional.of(DataExceptionCodes.INVALID_ARGUMENT.create("url", hopURLString));
        }

        return ret;
    };

    private final ScheduledExecutorService urlResolvedFilesCleanUpTimerExecutor = Executors.newScheduledThreadPool(1);

    private static final List<File> URL_RESOLVED_FILE_LIST = new LinkedList<>();

    // set cleanup timer to 5 minutes
    private static final long URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS = 5L * 60L * 1000L;

    @Autowired
    REDescriptor readerEngineDescriptor;

    public ServerManager(@NonNull ServerConfig serverConfig) {
        this.serverConfig = serverConfig;

        // set cache parameters
        final CacheDescriptor cacheDescriptor = new CacheDescriptor(serverConfig);
        cacheDescriptor.cacheServiceUrl = serverConfig.getCacheServiceUrl();
        try {
            cacheServiceClient = new CacheServiceClient(cacheDescriptor);
        } catch (Exception e) {
            logError("DC CacheServiceClient could not be initiaized correctly");
        }

        // initialize timer to clean up temp. files created by resolved URLs
        urlResolvedFilesCleanUpTimerExecutor.scheduleAtFixedRate(() -> {
            var curTimeMillis = System.currentTimeMillis();

            synchronized (URL_RESOLVED_FILE_LIST) {
                var fileIterator = URL_RESOLVED_FILE_LIST.iterator();
                while (fileIterator.hasNext()) {
                    var curFile = fileIterator.next();

                    if (curFile != null) {
                        if ((curTimeMillis - curFile.lastModified()) > URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS) {
                            fileIterator.remove();
                            FileUtils.deleteQuietly(curFile);
                        } else {
                            // leave loop as items are ordered by time
                            break;
                        }
                    }
                }
            }
        }, URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS, URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
    }

    // TODO NEXT is it possible to remove @PostConstruct
    @PostConstruct
    private void init() {

        // register class name for compatibility streaming puposes
        DataSourceFileInputStream.registerDCDataSourceClassName(CacheEntry.class.getName());
        DataSourceFileInputStream.registerDCDataSourceClassName(DataSourceFile.class.getName());

        fillPatternList(URL_BLACKLIST_PATTERNS, new File(readerEngineDescriptor.getBlacklistFile()));
        fillPatternList(URL_WHITELIST_PATTERNS, new File(readerEngineDescriptor.getWhitelistFile()));

        if (isLogWarn()) {
            var blacklistFile = readerEngineDescriptor.getBlacklistFile();
            var whitelistFile = readerEngineDescriptor.getWhitelistFile();

            if ((serverConfig.getMaxVMemMB() < 1500)) {
                logWarn("com.openexchange.documentconverter.maxVMemMB is set to a very low value of " + serverConfig.getMaxVMemMB() + " (MB). Some conversions of large documents may give an error. Please consider setting a higher value of at least 1500 (MB)");
            }

            if ((serverConfig.getPdftoolMaxVMemMB() < 50)) {
                logWarn("com.openexchange.documentconverter.pdftoolMaxVMemMB is set to a very low value of " + serverConfig.getPdftoolMaxVMemMB() + " (MB). Some processing of large PDF documents may give an error. Please consider setting a higher value of at least 50 (MB)");
            }

            if ((null == blacklistFile) || !new File(blacklistFile).canRead()) {
                logWarn("DC URL blacklist file is not readable", new LogData("filename", (null != blacklistFile) ? blacklistFile : "null"));
            }

            if ((null == whitelistFile) || !new File(whitelistFile).canRead()) {
                logWarn("DC URL whitelist file is not readable", new LogData("filename", (null != whitelistFile) ? whitelistFile : "null"));
            }
        }

        if (isLogInfo()) {
            logInfo("DC manager launched");
        }
    }

    @Nullable public CacheServiceClient getCacheServiceClient() {
        return cacheServiceClient;
    }

    // TODO NEXT Is moved to MangedJobFactory
//    public InputStream convert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
//        return null;
//    }

    public boolean isRunning() {
        return isRunning.get();
    }

    // TODO NEXT moved to documentConverterService.getMetrics()
//    public DocumentConverterInformation getMBean() {
//        return m_dcMBean;
//    }

    // TODO NEXT moved to AsyncServerExecutor.triggerExecution
    // public void triggerAsyncConvert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {}

    // TODO NEXT used from ManagedJob / JobExecutor
    public static void ensureInputTypeSet(Map<String, Object> jobProperties) {
        var inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);

        // try to get a valid input type of the source file
        if (StringUtils.isEmpty(inputType)) {
            var fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            if (fileName != null) {
                jobProperties.put(Properties.PROP_INPUT_TYPE, FilenameUtils.getExtension(fileName).trim().toLowerCase());
            }
        }
    }

    public static String getAdjustedFileURL(File file) {
        String fileUrl = null;

        if (file != null) {
            try {
                fileUrl = "file://" + file.toURI().toURL().getPath();
            } catch (final MalformedURLException e) {
                ServerManager.logExcp(e);
            }
        }

        return fileUrl;
    }

    public static void sleepThread(long sleepTimeMillis, boolean force) {
        var curTimeMillis = System.currentTimeMillis();
        var endTimeMillis =  curTimeMillis + Math.max(sleepTimeMillis, 1);

        do {
            var curWaitTimeMillis = endTimeMillis - curTimeMillis;

            if (curWaitTimeMillis > 0) {
                try {
                    Thread.sleep(curWaitTimeMillis);
                } catch (@SuppressWarnings("unused") final InterruptedException e) {
                    // TODO: Must be reviewed by Kai if this does not lead to side effects. Nevertheless, it is not
                    // a good practice to ignore an InterruptedException.
                    Thread.currentThread().interrupt();
                    force = false; // stop forced sleep as we were interrupted
                }
            }
        } while (force && ((curTimeMillis = System.currentTimeMillis()) < endTimeMillis));
    }

    public static Map<JobError, Integer> getDefaultSupportedBackendRuns() {
        return DEFAULT_ERROR_RUNCOUNT_MAP;
    }

    @Nullable public static URLConnection getValidatedFinalURLConnection(final String urlString, final Optional<Function<URLConnection, Optional<OXException>>> httpGetConnectionDecorator) {
        try {
            // use URITools.getFinalURL call with provided URL_VALIDATOR_FUNC to
            // validate each hop URL of possibly redirected URLs and return final URL
            return URITools.getTerminalConnection(urlString.trim(), Optional.of(URL_VALIDATOR_FUNC), httpGetConnectionDecorator);
        } catch (Exception e) {
            logWarn("DC detected invalid URL (" + e.getMessage() + ")");
        }

        return null;
    }

    @Nullable
    public static File resolveURL(String url, File targetFile, MutableObject<String> extensionWrapper) {
        File resultFile = null;

        URLConnection urlConnection;
        if (StringUtils.isNotBlank(url) && (null != targetFile) &&
                ((urlConnection = ServerManager.getValidatedFinalURLConnection(url.replace(" ", "%20"), Optional.of(HTTP_GET_CONNECTION_DECORATOR)))
                        instanceof HttpURLConnection)) {

            var httpURLConnection = (HttpURLConnection) urlConnection;

// TODO: is sslSocketFactoryProvider to be used ?
//            final com.openexchange.documentconverter.server.ox.net.SSLSocketFactoryProvider sslSocketFactoryProvider = null;
//            final javax.net.ssl.SSLSocketFactory sslSocketFactory = sslSocketFactoryProvider != null ? sslSocketFactoryProvider.getDefault() : null;
//            if ((httpURLConnection instanceof HttpsURLConnection) && sslSocketFactory != null) {
//                try {
//                    ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(sslSocketFactory);
//                } catch (@SuppressWarnings("unused") Exception e) {
//                    logWarn("DC is not able to establish Http(s) connection with URL: " + url);
//                }
//            }

            if (extensionWrapper != null) {
                extensionWrapper.setValue(StringUtils.EMPTY);

                final int extPos = url.lastIndexOf('.');

                if ((extPos > -1) && (extPos < (url.length() - 2))) {
                    extensionWrapper.setValue(url.substring(extPos + 1).toLowerCase());
                }
            }

            try {
                httpURLConnection.connect();

                try (InputStream httpInputStream = httpURLConnection.getInputStream()) {
                    if (httpInputStream != null) {
                        FileUtils.copyInputStreamToFile(httpInputStream, resultFile = targetFile);

                        if (ServerManager.isLogWarn() && (!resultFile.canRead() || (resultFile.length() <= 0))) {
                            ServerManager.logWarn("DC not able to write HTTP source file to local temp. file: " + url + " => " + resultFile.getCanonicalPath());

                            FileUtils.deleteQuietly(resultFile);
                            resultFile = null;
                        } else {
                            resultFile.setLastModified(System.currentTimeMillis());

                            synchronized (URL_RESOLVED_FILE_LIST) {
                                URL_RESOLVED_FILE_LIST.add(resultFile);
                            }
                        }
                    } else if (ServerManager.isLogWarn()) {
                        ServerManager.logWarn("DC temp. file to store downloaded HTTP source file could not be created: " + url);
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
                FileUtils.deleteQuietly(resultFile);
                resultFile = null;
            } finally {
                httpURLConnection.disconnect();
            }
        }

        return resultFile;
    }

    private static Function<URLConnection, Optional<OXException>> HTTP_GET_CONNECTION_DECORATOR = (urlConnection) -> {
        Optional<OXException> result = Optional.empty();

        if (urlConnection instanceof HttpURLConnection) {
            var httpConnection = (HttpURLConnection) urlConnection;

            try {
                httpConnection.setRequestMethod("GET");
                httpConnection.setConnectTimeout(ServerManager.getHTTPConnectTimeoutMillis(ServerType.DOCUMENTCONVERTER));
                httpConnection.setReadTimeout(ServerManager.getHTTPReadTimeoutMillis(ServerType.DOCUMENTCONVERTER));
            } catch (Exception e) {
                result = Optional.of(DataExceptionCodes.ERROR.create(e));
            }
        } else {
            result = Optional.of(DataExceptionCodes.INVALID_ARGUMENT.create("No valid HTTP(s) connection"));
        }

        return result;
    };

    /**
     * @param tmpFilePrefix The mandatory tmp file prefix
     * @return The tmp file, that was just created of <code>null</code> ij case of an error
     */
    @Nullable
    public File createTempFile(@NonNull String tmpFilePrefix) {
        return createTempFile(tmpFilePrefix, serverConfig.getScratchDir());
    }

    @Nullable
    public static File createTempFile(@NonNull String tmpFilePrefix, File scratchDir) {
        File tempFile = null;

        if (validateOrMkdir(scratchDir)) {
            // create a temp. file with the name 'prefix*.tmp' within the configured documentconverter cache directory

            try {
                tempFile = File.createTempFile(tmpFilePrefix, ".tmp", scratchDir);
            } catch (IOException e) {
                logExcp(e);
            }
        }

        return tempFile;
    }

    /**
     * @param tmpDirName The mandatory first directory to create within the tmp root directory
     * @param tmpDirNameN Optional subdirectory names to append to all previews subdirectories
     * @return The tmp directory, that was just created or <code>null</code> in case of an error
     */
    @Nullable
    public File createTempDir(@NonNull String tmpDirName, String... tmpDirNameN) {
        var tempDir = createTempFile(tmpDirName);

        // FIXME createTempFile() can return null and next line will throw NPE
        if ((tempDir != null) && tempDir.canWrite()) {
            FileUtils.deleteQuietly(tempDir);

            if (ArrayUtils.isNotEmpty(tmpDirNameN)) {
                for (final String curDirToAdd : tmpDirNameN) {
                    tempDir = new File(tempDir, curDirToAdd);
                }
            }

            try {
                FileUtils.forceMkdir(tempDir);
            } catch (IOException e) {
                logExcp(e);
            }

            if (!tempDir.exists()) {
                tempDir = null;
            }
        }

        return tempDir;
    }

    public static boolean validateOrMkdir(File directoryFile) {
        if (null != directoryFile) {
            if (!directoryFile.exists()) {
                try {
                    FileUtils.forceMkdir(directoryFile);
                } catch (final IOException e) {
                    // TODO: logExcp(e);
                }

                FileUtils.waitFor(directoryFile, 3);
            }

            return (directoryFile.isDirectory() && directoryFile.canWrite());
        }

        return false;
    }

    // TODO NEXT Only used from ManagedJob
    public File getJobInputFile(@NonNull Map<String, Object> jobProperties, @NonNull MutableBoolean deleteInputFile) {
        var inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

        deleteInputFile.setValue(Boolean.FALSE);

        // if no input file is given, write content of possible InputStream
        // property to a temp file; the conversion is always done using a real
        // file as input source due to very bad performance with InputStream
        // reads via the UNO bridge (at least with latest LO ReaderEngine)
        if (inputFile == null) {
            try (var inputStm = (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM)) {
                if (null != inputStm) {
                    inputFile = createTempFile("oxcs");
                    if (inputFile != null) {
                        try {
                            FileUtils.copyInputStreamToFile(inputStm, inputFile);

                            // set the just filled inputFile as new source at our
                            // job
                            // and remove the previously set input stream;
                            jobProperties.put(Properties.PROP_INPUT_FILE, inputFile);
                            jobProperties.remove(Properties.PROP_INPUT_STREAM);

                            // the temporarily created file needs to be deleted by
                            // the caller of this method later in time
                            deleteInputFile.setValue(Boolean.TRUE);
                        } catch (final IOException e) {
                            logExcp(e);

                            FileUtils.deleteQuietly(inputFile);
                            inputFile = null;
                        }
                    }
                }
            } catch (final IOException e) {
                logExcp(e);
            }
        }

        // check for input type %PDF-x.x, since this input
        // type is explicitly checked at various locations
        if (inputFile != null) {
            try (var inputStm = FileUtils.openInputStream(inputFile)) {
                var buffer = new byte[256];

                if ((inputStm.read(buffer) > 0) && new String(buffer, "UTF-8").trim().toLowerCase().startsWith("%pdf-")) {
                    jobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
                }
            } catch (final IOException e) {
                logExcp(e);
            }
        }

        return inputFile;
    }

    public static int getHTTPConnectTimeoutMillis(@NonNull ServerType serverType) {
        if (serverType.equals(ServerType.CACHESERVER)) {
            return HTTP_CACHESERVER_CONNECT_TIMEOUT_MS;
        }
        return HTTP_DOCUMENTCONVERTER_CONNECT_TIMEOUT_MS;
    }

    public static int getHTTPReadTimeoutMillis(@NonNull ServerType serverType) {
        if (serverType.equals(ServerType.CACHESERVER)) {
            return HTTP_CACHESERVER_READ_TIMEOUT_MS;
        }
        return HTTP_DOCUMENTCONVERTER_READ_TIMEOUT_MS;
    }

    public static String getFilenameForLog(String filename) {
        try {
            // #DOCS-3981: ensure that no invalid hex values (e.g. %Xy)
            // get decoded as this will throw a not declared IllegalArgumentException.
            // In such cases, we replace the illegal hex value with a space char (%25).
            return (filename == null) ? DocumentConverterUtil.STR_NOT_AVAILABLE :
                    URLDecoder.decode(filename.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8");
        } catch (@SuppressWarnings("unused") final Exception e) {
            logTrace("DC cannot decode given URL, using as is: " + filename);
        }

        return filename;
    }

    /**
     * @param maxVMemMB
     * @return The bash process command array, containing possible
     *  max VMem settings within the last element.
     *  If parameter is set to 0, no ulimits will be set.
     *  The last element needs to be appended with the real command to be finally usable
     */
    public static String[] getMaxVMemBashCommandLine(final long maxVMemMB) {
        if (maxVMemMB > 0) {
            final var uLimitVMemKbStr = Long.toString(1024L * maxVMemMB);

            return new String[] { BASH_EXECUTABLE_PATH_STR, "-c", new StringBuilder(256).
                    append("ulimit -v ").append(uLimitVMemKbStr).append("; ").
                    append("ulimit -m ").append(uLimitVMemKbStr).append("; ").toString() };
        }

        return new String[] { BASH_EXECUTABLE_PATH_STR, "-c" };
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                logExcp(e);
            }
        }
    }

    /**
     * Method impl. is copied from Process#waitFor(long timeout, TimeUnit unit) method
     * with the difference that this method returns the exit code of the process in case
     * of termination in time or -1 in case of timeout or invalid process.
     * This method works on milliseconds only as well.
     * @param process The process to wait for.
     * @param timeoutMillis The milliseconds to wait for the process before -1 is returned.
     * @return The exit code of the process or -1 if process has reached its timeout or process is <code>null</code>.
     */
    public static int waitForProcess(Process process, long timeoutMillis) {
        if (process != null) {
            var startTimeMillis = System.currentTimeMillis();
            var remMillis = timeoutMillis;

            Thread.yield();
            do {
                try {
                    // if process has not yet terminated, an IllegalThreadStateException is thrown
                    return process.exitValue();
                } catch(@SuppressWarnings("unused") IllegalThreadStateException e) {
                    if (remMillis > 0) {
                        try {
                            Thread.sleep(Math.min(remMillis + 1, 50));
                        } catch (@SuppressWarnings("unused") InterruptedException e1) {
                            break;
                        }
                    }
                }

                remMillis = timeoutMillis - (System.currentTimeMillis() - startTimeMillis);
            } while (remMillis > 0);
        }

        return -1;
    }

    public static boolean isLogTrace() {
        return log.isTraceEnabled();
    }

    public static boolean isLogDebug() {
        return log.isDebugEnabled();
    }

    public static boolean isLogInfo() {
        return log.isInfoEnabled();
    }

    public static boolean isLogWarn() {
        return log.isWarnEnabled();
    }

    public static boolean isLogError() {
        return log.isErrorEnabled();
    }

    public static void logInfo(String info, LogData... extraData) {
        logInfo(info, null, extraData);
    }

    public static void logInfo(String info, Map<String, Object> jobProperties, LogData... extraData) {
        log.info(implGetLogMessage(info, jobProperties, extraData));
    }

    public static void logWarn(String warning, LogData... extraData) {
        logWarn(warning, null, extraData);
    }

    public static void logWarn(String warning, Map<String, Object> jobProperties, LogData... extraData) {
        log.warn(implGetLogMessage(warning, jobProperties, extraData));
    }

    public static void logError(String error, LogData... extraData) {
        logError(error, null, extraData);
    }

     public static void logError(String error, Map<String, Object> jobProperties, LogData... extraData) {
        log.error(implGetLogMessage(error, jobProperties, extraData));
    }

    public static void logDebug(String debugMsg, LogData... extraData) {
        logDebug(debugMsg, null, extraData);
    }

    public static void logDebug(String debugMsg, Map<String, Object> jobProperties, LogData... extraData) {
        log.debug(implGetLogMessage(debugMsg, jobProperties, extraData));
    }

    public static void logTrace(String traceMsg, LogData... extraData) {
        logTrace(traceMsg, null, extraData);
    }

    public static void logTrace(String traceMsg, Map<String, Object> jobProperties, LogData... extraData) {
        log.trace(implGetLogMessage(traceMsg, jobProperties, extraData));
    }

    /**
     * @param e
     */
    public static void logExcp(Exception e) {
        var rootCause = Throwables.getRootCause(e);
        var rootCauseMessage = rootCause.getMessage();
        var lowerCaseRootCauseMessage = (null != rootCauseMessage) ? rootCauseMessage.toLowerCase() : DocumentConverterUtil.STR_NOT_AVAILABLE;
        String logMessage;

        if (StringUtils.isNotEmpty(rootCauseMessage)) {
            if (rootCauseMessage.length() > Properties.MAX_ERROR_MESSAGE_TOTAL_LENGTH) {
                var fillStr = "...";
                var beginLength = Properties.MAX_ERROR_MESSAGE_TOTAL_LENGTH - Properties.MAX_ERROR_MESSAGE_END_LENGTH - fillStr.length();
                var endLength = Properties.MAX_ERROR_MESSAGE_END_LENGTH;

                logMessage = rootCauseMessage.substring(0, beginLength) + "..." + rootCauseMessage.substring(rootCauseMessage.length() - endLength);
            } else {
                logMessage = rootCauseMessage;
            }
        } else {
            logMessage = rootCause.getClass().getName();
        }

        if (StringUtils.isEmpty(logMessage)) {
            logMessage = DocumentConverterUtil.STR_NOT_AVAILABLE;
        }

        if ((rootCause instanceof SocketException) ||
                (rootCause instanceof SocketTimeoutException) ||
                (rootCause instanceof EOFException) ||
                ((rootCause instanceof IOException) &&
                        ((lowerCaseRootCauseMessage.contains("broken pipe") ||
                                lowerCaseRootCauseMessage.contains(("connection reset")) ||
                                lowerCaseRootCauseMessage.contains(("invalid stream")))))) {

            // ignore but trace socket exceptions; these may occur, when a connection timeout has happened
            log.trace("DC caught an ignorable connection exception: {}", logMessage);
        }
    }

    public static void logExcp(String logMessage, Exception e) {
        if (null != log) {
            log.error(logMessage, e);
        }
    }

    public static String implGetLogMessage(String message, Map<String, Object> jobProperties, LogData... extraData) {
        var logMessageBuilder = (new StringBuilder(message.length() << 1)).append(message);
        StringBuilder infoStrBuilder = null;
        String fileName = null;

        if (jobProperties != null) {
            fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        }

        var addClosingBrace = (extraData.length > 0) || (null != fileName);

        if (extraData.length > 0) {
            logMessageBuilder.append(" (");

            for (var i = 0; i < extraData.length; ++i) {
                var logData = extraData[i];

                if (logData != null) {
                    if (i > 0) {
                        logMessageBuilder.append(", ");
                    }

                    logMessageBuilder.append(logData.getKey());
                    logMessageBuilder.append('=');

                    var value = logData.getValue();
                    logMessageBuilder.append(value == null ? "null" : value);
                }
            }
        }

        // add decoded file name
        if (fileName != null) {
            logMessageBuilder.
                    append((extraData.length > 0) ? ", " : " (").append("filename=").
                    append(getFilenameForLog(fileName));
        }

        if (jobProperties != null && !jobProperties.isEmpty()) {
            for (var key : jobProperties.keySet()) {
                // log all info_ properties but info_Filename only in case of an error
                if (key.startsWith(Properties.INFO_PREFIX) && !key.equals(Properties.PROP_INFO_FILENAME)) {

                    if (infoStrBuilder == null) {
                        infoStrBuilder = new StringBuilder(" (jobproperties: ");
                    } else {
                        infoStrBuilder.append(", ");
                    }
                    var propObj = jobProperties.get(key);
                    var propValue = propObj == null ? "null" : propObj.toString();

                    infoStrBuilder.append(key.substring(Properties.INFO_PREFIX.length())).append('=').append(propValue);
                }
            }
        }

        if (infoStrBuilder != null) {
            logMessageBuilder.append(infoStrBuilder);
        }

        if (addClosingBrace) {
            logMessageBuilder.append(')');
        }

        return logMessageBuilder.toString();
    }

    public @NonNull List<String> getServerErrors() {
        return serverErrors;
    }

    private static void fillPatternList(@NonNull List<Pattern> patternList, File file) {
        if (file != null && file.canRead()) {
            String readLine;
            try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
                while ((readLine = reader.readLine()) != null) {
                    if (StringUtils.isNotBlank(readLine = readLine.trim()) && !readLine.startsWith("#")) {
                        try {
                            patternList.add(Pattern.compile(readLine));
                        } catch (@SuppressWarnings("unused") PatternSyntaxException e) {
                            logError("DC could not compile provided RegExp pattern:" + readLine + " (file: " + file.getAbsolutePath() + ")");
                        }
                    }
                }
            } catch (@SuppressWarnings("unused") Exception e) {
                logError("DC could not read RegExp pattern file: " + file.getAbsolutePath());
            }
        }
    }

    private static boolean implIsMatchingURLPattern(@NonNull List<Pattern> patternList, @NonNull String testString) {
        for (var curPattern : patternList) {
            if (curPattern.matcher(testString).matches()) {
                return true;
            }
        }

        return false;
    }
    // TODO NEXT
//    public void shutdown() {
//        if ((null != m_jobProcessor) && m_isRunning.compareAndSet(true, false)) {
//            // terminate everything
//            long shutdownStartTime = 0;
//
//            if (isLogInfo()) {
//                shutdownStartTime = System.currentTimeMillis();
//                logInfo("DC ServerManager starting shutdown...");
//            }
//
//            if (null != m_timerExecutor) {
//                m_timerExecutor.shutdownNow();
//            }
//
//            if (null != m_pageConverter) {
//                m_pageConverter.shutdown();
//            }
//
//            if (null != m_asyncExecutor) {
//                m_asyncExecutor.shutdown();
//                m_asyncExecutor = null;
//            }
//
//            if (null != m_jobProcessor) {
//                m_jobProcessor.shutdown();
//                m_jobProcessor = null;
//            }
//
//            if (null != m_cache) {
//                m_cache.shutdown();
//                m_cache = null;
//            }
//
//            // shutdown statistics related resources (e.g. timer threads)
//            if (null != m_statistics) {
//                m_statistics.terminate();
//                m_statistics = null;
//            }
//
//            if (isLogInfo()) {
//                logInfo(new StringBuilder(256).
//                        append("DC ServerManager finished shutdown: ").
//                        append(System.currentTimeMillis() - shutdownStartTime).append("ms").
//                        append('!').toString());
//            }
//        }
//
//        // delete scratch directory
//        synchronized (this) {
//            if (null != m_engineScratchDir) {
//                try {
//                    FileUtils.deleteDirectory(m_engineScratchDir);
//                } catch (final IOException e) {
//                    logExcp(e);
//                } finally {
//                    m_engineScratchDir = null;
//                }
//            }
//        }
//    }

    //  TODO NEXT moved to Cache
    //  public static InputStream getCachedResult();

    // TODO NEXT only used in Statistic. Use @Autowired AsyncExecutor in Statistic
    //    public int getAsyncJobCountScheduled() {}
    //    public long getAsyncJobCountProcessed() {}
    //    public long getAsyncJobCountDropped() {}

    // TODO NEXT moved to Cache
    // @NonNull public static List<String> executeBashCommand(final String command) {}

    // TODO NEXT moved to PageConverter
    // public void endPageConversion(String jobId, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {}

    // TODO NEXT moved to AsyncExecutor getLogData()
    // public LogData[] getAsyncLogData() {}

    // TODO NEXT moved to PageConverter
    // public String beginPageConversion(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {}

    // TODO NEXT moved to PageConverter
    //    public InputStream getConversionPage(String jobId, int pageNumber, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {}

    // TODO NEXT moved JobProcessor
    // public IJob createServerJob(String _jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {}

    // TODO NEXT moved to JobProcessor
    // public @NonNull Map<BackendType, EngineStatus> getEngineStatus() {}
}
