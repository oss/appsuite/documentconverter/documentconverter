/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.concurrency;

import java.util.HashMap;
import java.util.Map;

// TODO NEXT Refactor
public class IdLocker {

    // m_elementLockMap needs to be synchronized with every access (intended)
    private static final Map<String, IdLock> elementLockMap = new HashMap<>();

    public enum Mode {
        STANDARD,
        TRY_LOCK
    }

    public enum UnlockMode {
        STANDARD
    }

    public static boolean lock(String element) {
        return lock(element, Mode.STANDARD);
    }

    public static boolean lock(String element, Mode mode) {
        if (element == null) {
            return false;
        }

        boolean locked;
        IdLock idLock;

        synchronized (elementLockMap) {
            idLock = elementLockMap.get(element);

            if (null == idLock) {
                elementLockMap.put(element, idLock = new IdLock());
            } else {
                idLock.incrementUseCount();
            }
        }

        locked = idLock.lock(mode);

        // cleaning up in case of unsuccessful try lock
        if (!locked) {
            synchronized (elementLockMap) {
                if (0 == idLock.decrementUseCount()) {
                    elementLockMap.remove(element);
                }
            }
        }

        return locked;
    }

    public static void unlock(String element) {
        if (element == null) {
            return;
        }
        synchronized (elementLockMap) {
            var idLock = elementLockMap.get(element);

            if (null != idLock) {
                idLock.unlock();

                if (0 == idLock.decrementUseCount()) {
                    elementLockMap.remove(element);
                }
            }
        }
    }
}
