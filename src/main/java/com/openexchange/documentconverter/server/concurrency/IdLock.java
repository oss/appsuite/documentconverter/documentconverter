/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.concurrency;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class IdLock extends ReentrantLock {

    private final AtomicInteger m_useCount = new AtomicInteger(1);

    public IdLock() {
        super();
    }

    public int incrementUseCount() {
        return m_useCount.incrementAndGet();
    }

    public int decrementUseCount() {
        return m_useCount.decrementAndGet();
    }

    public boolean lock(IdLocker.Mode mode) {
        if (IdLocker.Mode.TRY_LOCK == mode) {
            return super.tryLock();
        }

        super.lock();

        return true;
    }

    /**
     * Calling this method needs to be synchronized by the caller
     */
    public void unlock(IdLocker.UnlockMode unlockMode) {
        super.unlock();
    }

}
