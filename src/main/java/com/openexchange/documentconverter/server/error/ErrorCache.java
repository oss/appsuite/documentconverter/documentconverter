/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import com.openexchange.documentconverter.server.DocumentConverterUtil;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.error.JobErrorEx.StatusErrorCache;
import com.openexchange.documentconverter.server.job.JobEntry;
import com.openexchange.documentconverter.server.watchdog.Watchdog;
import com.openexchange.documentconverter.server.watchdog.WatchdogMode;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ErrorCache {

    @Autowired
    private ServerConfig serverConfig;

    private final Map<String, ErrorCacheEntry> errorHashMap = new HashMap<>();

    private Watchdog errorCacheWatchdog = null;

    private int errorCacheMaxCycleCount = 0;

    @PostConstruct
    public void init() {
        var errorCacheEnabled = (serverConfig.getErrorCacheTimeoutSeconds() > 0) && (serverConfig.getErrorCacheMaxCycleCount() > 0);

        if (errorCacheEnabled) {
            errorCacheMaxCycleCount = serverConfig.getErrorCacheMaxCycleCount();

            errorCacheWatchdog = new Watchdog("DC ErrorCache watchdog", serverConfig.getErrorCacheTimeoutSeconds() * 1000L, WatchdogMode.TIMEOUT);
            errorCacheWatchdog.start();

            if (ServerManager.isLogInfo()) {
                ServerManager.logInfo("DC ErrorCache enabled",
                    new LogData("timeout", serverConfig.getErrorCacheTimeoutSeconds() + "s"),
                    new LogData("cycles", Integer.toString(serverConfig.getErrorCacheMaxCycleCount())));
            }
        } else if (ServerManager.isLogInfo()) {
            JobErrorEx.setErrorCacheDisabled();
            ServerManager.logInfo("DC ErrorCache disabled");
        }
    }

    /**
     * @return true, if this cache is configured to collect job errors based on their hash value
     */
    public boolean isEnabled() {
        return errorCacheWatchdog != null;
    }

    public void terminate() {
        if (errorCacheWatchdog != null) {
            errorCacheWatchdog.terminate();

            try {
                errorCacheWatchdog.join();
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                // ok
            }
        }
    }

    /**
     * @return The number of errorcache activations for the job entry
     */
    public long notifyJobFinish(@NonNull JobEntry jobEntry) {
        var hash = jobEntry.getHash();
        var jobErrorEx = jobEntry.getJobErrorEx();
        var activationCount = 0L;

        if (isEnabled() && Cache.isValidHash(hash) && jobErrorEx != null) {
            var fileName = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);

            synchronized (errorHashMap) {
                var errorCacheEntry = errorHashMap.get(hash);

                if (null != errorCacheEntry) {
                    if (jobErrorEx.hasNoError()) {
                        errorCacheWatchdog.removeWatchdogHandler(errorCacheEntry);
                        errorHashMap.remove(hash);

                        errorCacheEntry = null;

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace("DC ErrorCache removed entry after successful conversion",
                                    new LogData("hash", hash),
                                    new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
                        }
                    } else if (errorCacheEntry.isActive()) {
                        errorCacheEntry = null;
                    } else {
                        jobErrorEx.setStatusErrorCache(StatusErrorCache.NEW);
                    }
                } else if (jobErrorEx.hasError()) {
                    jobErrorEx.setStatusErrorCache(StatusErrorCache.NEW);
                    errorHashMap.put(hash, errorCacheEntry = new ErrorCacheEntry(jobEntry, errorCacheMaxCycleCount));
                }

                if (null != errorCacheEntry) {
                    activationCount = errorCacheEntry.activate();

                    errorCacheWatchdog.addWatchdogHandler(errorCacheEntry);

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC ErrorCache activated entry",
                                new LogData("hash", hash),
                                new LogData("joberror", jobErrorEx.getJobError().getErrorText()),
                                new LogData("activations", Long.toString(errorCacheEntry.getActivationCount())),
                                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }
            }
        }

        return activationCount;
    }

    /**
     * @return The error for the job with the given hash or <code>null</code>, if the corresponding job is not flagged as an error job at
     *         all
     */
    public JobErrorEx getJobErrorExForHash(String hash) {
        if (isEnabled() && Cache.isValidHash(hash)) {
            synchronized (errorHashMap) {
                var foundEntry = errorHashMap.get(hash);

                if (foundEntry != null && foundEntry.isActive()) {
                    var jobErrorEx = new JobErrorEx(JobError.fromErrorCode(foundEntry.getJobErrorCode()));

                    jobErrorEx.setStatusErrorCache((foundEntry.getActivationCount() < errorCacheMaxCycleCount) ? StatusErrorCache.ACTIVE : StatusErrorCache.LOCKED);

                    return jobErrorEx;
                }
            }
        }

        return null;
    }

}
