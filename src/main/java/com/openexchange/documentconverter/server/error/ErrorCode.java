package com.openexchange.documentconverter.server.error;

public enum ErrorCode {

    NO_ERROR,
    ERROR_UNKNOWN,
    ERROR_INVALID_PARAM;
}
