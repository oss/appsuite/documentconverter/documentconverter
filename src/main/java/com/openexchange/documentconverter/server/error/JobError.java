/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import java.util.Arrays;
import java.util.function.Consumer;

public enum JobError {

    NONE(0x00000000, "none", "ERROR_NONE"),
    GENERAL(0x00000001, "filterError", "ERROR_GENERAL_ERROR"),
    TIMEOUT(0x00000002, "timeout", "ERROR_TIMEOUT"),
    PASSWORD(0x00000004, "passwordProtected", "ERROR_PASSWORD_PROTECTED"),
    NO_CONTENT(0x00000008, "noContent","ERROR_NO_CONTENT"),
    MAX_SOURCESIZE(0x00000010, "maxSourceSizeExceeded", "ERROR_MAX_SOURCE_SIZE_EXCEEDED"),
    DISPOSED(0x00000020, "engineDisposed", "ERROR_CONVERSION_ENGINE_DISPOSED"),
    UNSUPPORTED(0x00000040, "unsupportedType", "ERROR_UNSUPPORTED_INPUT_TYPE"),
    OUT_OF_MEMORY(0x00000080, "outOfMemory", "ERROR_OUT_OF_MEMORY"),
    ABORT(0x00000100, "abort", "ERROR_ABORT"),
    PDFTOOL(0x00000200, "pdfTool", "ERROR_PDFTOOL"),
    MAX_QUEUE_COUNT(0x00000400, "maxQueueCount", "ERROR_MAX_QUEUE_COUNT"),
    QUEUE_TIMEOUT(0x00000800, "queueTimeout", "ERROR_QUEUE_TIMEOUT"),
    SERVER_CONNECTION_ERROR(0x00001000, "serverConnection", "ERROR_SERVER_CONNECTION_ERROR"),
    SOURCE_NOT_ACCESSIBLE(0x00002000, "sourceNotAccessible", "ERROR_SOURCE_NOT_ACCESSIBLE");

    private int errorCode;
    private String errorText;
    private String errorCodeRepresentation;

    JobError(int errorCode, String errorText, String errorCodeRepresentation) {
        this.errorCode = errorCode;
        this.errorText = errorText;
        this.errorCodeRepresentation = errorCodeRepresentation;
    }

    final public int getErrorCode() {
        return errorCode;
    }

    public boolean equalsErrorCode(int errorCode) {
        return this.errorCode == errorCode;
    }

    final public String getErrorText() {
        return errorText;
    }

    final public String getErrorCodeRepresentation() {
        return errorCodeRepresentation;
    }

    public static JobError fromErrorCode(int errorCode) {
        JobError result = GENERAL;

        for (var jobError : JobError.values()) {
            if (jobError.getErrorCode() == errorCode) {
                result = jobError;
                break;
            }
        }

        return result;
    }

    // TODO NEXT REmove it?
    public static void forEach(Consumer<? super JobError> action) {
        Arrays.stream(values()).forEach(action);
    }
}
