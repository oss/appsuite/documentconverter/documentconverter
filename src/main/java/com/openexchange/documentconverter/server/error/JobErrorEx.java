/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import com.openexchange.documentconverter.server.Properties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;

public class JobErrorEx {

    public static final String JSON_KEY_ERROR_CODE = "errorCode";
    public static final String JSON_KEY_STATUS_ERROR_CACHE = "statusErrorCache";
    public static final String JSON_KEY_DESCRIPTION = "description";
    public static final String JSON_KEY_DESCRIPTION_DATA = "descriptionData";


    private final JobError jobError;

    private String errorDescription = null;

    private JSONObject errorData = null;

    private StatusErrorCache statusErrorCache = DEFAULT_STATUS_ERROR_CACHE;

    private static StatusErrorCache DEFAULT_STATUS_ERROR_CACHE = StatusErrorCache.NONE;

    public enum StatusErrorCache {

        NONE(),
        DISABLED(),
        NEW(),
        ACTIVE(),
        LOCKED();

        /**
         * @return
         */
        public String getStatusText() {
            return name().toLowerCase();
        }

        /**
         * @param statusText
         * @return
         */
        public static StatusErrorCache fromStatusText(String statusText) {

            try {
                return StatusErrorCache.valueOf(StringUtils.upperCase(statusText));
            } catch (IllegalArgumentException e) {
                return StatusErrorCache.NONE;
            }
        }

    }

    public JobErrorEx() {
        jobError = JobError.NONE;
    }

    public JobErrorEx(JobError jobError) {
        this.jobError = jobError == null ? JobError.NONE : jobError;
    }

    // TODO NEXT What???
    public synchronized static void setErrorCacheDisabled() {
        DEFAULT_STATUS_ERROR_CACHE = StatusErrorCache.DISABLED;
    }

    public boolean hasError() {
        return JobError.NONE != jobError;
    }

    public boolean hasNoError() {
        return JobError.NONE == jobError;
    }

    public JobError getJobError() {
        return jobError;
    }

    /**
     * @return The error code id of the enum
     */
    public int getErrorCode() {
        return jobError.getErrorCode();
    }

    /**
     * @return The textual representation of the current error
     */
    public String getErrorText() {
        return jobError.getErrorText();
    }

    public synchronized void setStatusErrorCache(final StatusErrorCache statusErrorCache) {
        if (statusErrorCache != null) {
            this.statusErrorCache = statusErrorCache;
        }
    }

    public synchronized StatusErrorCache getStatusErrorCache() {
        return statusErrorCache;
    }

    public synchronized void setErrorData(final String errorDescription) {
        this.errorDescription = errorDescription;
        errorData = null;
    }

    public synchronized void setErrorData(final String errorDescription, final JSONObject errorData) {
        this.errorDescription = errorDescription;
        this.errorData = errorData;
    }

    /**
     * @return The extended JSON description of this error
     */
    public synchronized JSONObject getErrorData() {
        var jsonErrorData = new JSONObject();

        try {
            jsonErrorData.put(JSON_KEY_ERROR_CODE, jobError.getErrorCodeRepresentation());

            if (StatusErrorCache.NONE != statusErrorCache) {
                jsonErrorData.put(JSON_KEY_STATUS_ERROR_CACHE, statusErrorCache.getStatusText());
            }

            if (null != errorDescription) {
                jsonErrorData.put(JSON_KEY_DESCRIPTION, errorDescription);
            }

            if (null != errorData) {
                jsonErrorData.put(JSON_KEY_DESCRIPTION_DATA, errorData);
            }
        } catch (JSONException e) {
            // TODO NEXT implement it. Exception Handling!
            //DocumentConverterManager.logExcp(e);
        }

        return jsonErrorData;
    }

    public static JobErrorEx fromResultProperties(final Map<String, Object> resultProperties) {
        var jobErrorEx = new JobErrorEx();

        if (null != resultProperties) {
            var resultCodeNumberObj = resultProperties.get(Properties.PROP_RESULT_ERROR_CODE);

            if (resultCodeNumberObj instanceof Number) {
                jobErrorEx = new JobErrorEx(JobError.fromErrorCode(((Number) resultCodeNumberObj).intValue()));

                if (jobErrorEx.hasError()) {
                    var errorData = (String) resultProperties.get(Properties.PROP_RESULT_ERROR_DATA);

                    if (StringUtils.isNotBlank(errorData)) {
                        try {
                            var jsonErrorData = new JSONObject(errorData);

                            if (jsonErrorData.has(JSON_KEY_STATUS_ERROR_CACHE)) {
                                jobErrorEx.setStatusErrorCache(StatusErrorCache.fromStatusText(jsonErrorData.getString(JSON_KEY_STATUS_ERROR_CACHE)));
                            }

                            if (jsonErrorData.has(JSON_KEY_DESCRIPTION)) {
                                jobErrorEx.setErrorData(
                                        jsonErrorData.getString(JSON_KEY_DESCRIPTION),
                                        jsonErrorData.optJSONObject(JSON_KEY_DESCRIPTION_DATA));
                            }

                        } catch (JSONException e) {
                            // TODO NEXT implement it. Exception Handling!
                            // DocumentConverterManager.logExcp(e);
                            jobErrorEx = new JobErrorEx(JobError.GENERAL);
                        }
                    }
                }
            }
        }

        return jobErrorEx;
    }

    // TODO NEXT Why? Same Method in JobError
    public static void forEach(Consumer<? super JobError> action) {
        Arrays.stream(JobError.values()).forEach(action);
    }

}
