/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.error;

import com.openexchange.documentconverter.server.DocumentConverterUtil;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.job.JobEntry;
import com.openexchange.documentconverter.server.watchdog.WatchdogHandler;
import lombok.Getter;
import lombok.NonNull;

import java.util.concurrent.atomic.AtomicLong;

class ErrorCacheEntry implements WatchdogHandler {

    @Getter
    private final String hash;

    @Getter
    private final int jobErrorCode;

    private final String fileName;

    private final long errorCacheMaxCycleCount;

    private boolean isActive = false;

    private AtomicLong activationCount = new AtomicLong(0);

    protected ErrorCacheEntry(@NonNull JobEntry jobEntry, int errorCacheMaxCycleCount) {
        super();

        var fileName = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);

        hash = jobEntry.getHash();
        jobErrorCode = jobEntry.getJobErrorEx().getErrorCode();
        this.fileName = fileName != null ? fileName : DocumentConverterUtil.STR_UNKNOWN;
        this.errorCacheMaxCycleCount = errorCacheMaxCycleCount;
    }

    @Override
    public void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
        if (activationCount.get() < errorCacheMaxCycleCount) {
            isActive = false;

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace(
                    "DC ErrorCache deactivated entry after timeout",
                    new LogData("hash", hash),
                    new LogData("joberror", JobError.fromErrorCode(jobErrorCode).toString()),
                    new LogData("activations", Long.toString(activationCount.get())),
                    new LogData("filename", fileName));
            }
        } else {
            ServerManager.logInfo(
                "DC ErrorCache permanently added entry after " + Long.toString(activationCount.get()) + " activations",
                new LogData("hash", hash),
                new LogData("joberror", JobError.fromErrorCode(jobErrorCode).toString()),
                new LogData("filename", fileName));
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public long activate() {
        isActive = true;

        return activationCount.incrementAndGet();
    }

    public long getActivationCount() {
        return activationCount.get();
    }
}
