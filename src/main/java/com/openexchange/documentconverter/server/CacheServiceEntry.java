/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link CacheServiceEntry}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
public class CacheServiceEntry {

    // - Statics ---------------------------------------------------------------

    // - Public API ------------------------------------------------------------

    public CacheServiceEntry(String hash) {
        this.hash = hash;
    }

    public CacheServiceEntry(String hash, InputStream result) {
        this.hash = hash;

        byte[] resultArray = null;
        try {
            resultArray = IOUtils.toByteArray(result);
        }
        catch (IOException e) {
            LOG.error("DC CacheEntry could not create result buffer");
        }
        if (resultArray != null && resultArray.length > 0) {
            this.objectMap.put("result", resultArray);
        }
    }

    public String getHash() {
        return hash;
    }

    /*
     * returns the result input stream or null
     */
    public InputStream getResultStream() {
        final byte[] result = objectMap.get("result");
        return result != null && result.length > 0 ? new ByteArrayInputStream(result) : null;
    }

    // - Members ----------------------------------------------------------

    public HashMap<String, byte[]> getObjectMap() {
        return objectMap;
    }

    private HashMap<String, byte[]> objectMap = new HashMap<String, byte[]>(2);

    private final String hash;

    private static Logger LOG = LoggerFactory.getLogger(CacheServiceEntry.class);
}
