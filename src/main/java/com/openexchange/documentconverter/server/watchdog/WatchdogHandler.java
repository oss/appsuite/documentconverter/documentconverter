/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.watchdog;

import com.openexchange.documentconverter.server.error.JobErrorEx;

// TODO NEXT remove Interface?
public interface WatchdogHandler {
    /**
     * notifies the handler that the watchdog detected an event for this handler
     *
     * @param curTimeMillis The current time, given to avoid multiple querying for current time in handler method
     * @param startTimeMillis The point of time, this handler has been started to watch a timeout
     * @param notificationJobErrorEx The reason for notification
     */
    void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx);


    /**
     * @return The handler implementation needs to return <code>false</code>,
     *  if it is still working on an internal job or <code>false</code>,
     *  if the {@link Watchdog} should send a {@link JobError.DISPOSED} notification
     *  to the handler. Typically used to detec SIGKILL based aborts by the handler
     *  with the goal to receive a {@link JobError.DISPOSED} notification to finally terminate
     *  the current work.
     *  If no such mechanism is needed, the handler should return <code>false</code>
     *  in any case.
     */
    default boolean isDisposed() {
        return false;
    }
}
