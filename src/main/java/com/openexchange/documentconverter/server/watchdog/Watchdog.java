/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.watchdog;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import lombok.NonNull;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

// TODO NEXT Refactor
public class Watchdog extends Thread {

    private final long DEFAULT_CHECK_TIMEOUT_MILLIS = 1000;

    private final Queue<WatchdogEntry> watchdogEntries = new ConcurrentLinkedQueue<>();

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    private final WatchdogMode watchdogMode;

    private final long timeoutMilliSeconds;

    public Watchdog(@NonNull String name, long timeoutMilliSeconds, WatchdogMode watchdogMode) {
        super(name);

        this.timeoutMilliSeconds = timeoutMilliSeconds;
        this.watchdogMode = watchdogMode;
    }

    public long getTimeoutMilliSeconds() {
        return timeoutMilliSeconds;
    }

    /**
     * @return The time in milliseconds, the handler was reset or added
     */
    public long addWatchdogHandler(@NonNull WatchdogHandler watchdogHandler) {
        WatchdogEntry affectedEntry = null;

        // don't add handler if we're already terminated
        if (!isRunning.get()) {
            return 0;
        }
        // TODO NEXT Refactor? Not easy to read
        synchronized (watchdogEntries) {
            var watchdogEntryIterator = watchdogEntries.iterator();

            while (watchdogEntryIterator.hasNext()) {
                var curEntry = watchdogEntryIterator.next();

                if (curEntry.getWatchdogHandler() == watchdogHandler) {
                    // remove found entry from current position if
                    // found, reset startTime and put entry to the end
                    watchdogEntryIterator.remove();

                    (affectedEntry = curEntry).resetStartTimeMillis();
                    watchdogEntries.add(affectedEntry);

                    break;
                }
            }

            if (null == affectedEntry) {
                watchdogEntries.add(affectedEntry = new WatchdogEntry(watchdogHandler));
            }
        }

        return affectedEntry.getStartTimeMillis();
    }

    public void removeWatchdogHandler(@NonNull WatchdogHandler watchdogHandler) {
        synchronized (watchdogEntries) {
            var watchdogEntryIterator = watchdogEntries.iterator();

            while (watchdogEntryIterator.hasNext()) {
                if (watchdogEntryIterator.next().getWatchdogHandler() == watchdogHandler) {
                    watchdogEntryIterator.remove();
                    break;
                }
            }
        }
    }

    public void terminate() {
        if (isRunning.compareAndSet(true, false) && ServerManager.isLogTrace()) {
            var threadName = getName();
            var traceStartTimeMillis = System.currentTimeMillis();
            // TODO NEXT I don't understand this to logs, nothing happens between
            ServerManager.logTrace(new StringBuilder(256).
                    append("DC starting shutdown ").append(threadName).
                    append("...").toString());

            ServerManager.logTrace(new StringBuilder(256).
                    append("DC finished shutdown ").append(threadName).append(": ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
        }
    }

    boolean isRunning() {
        return isRunning.get();
    }

    @Override
    public void run() {
        var curTimeMillis = 0L;

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getName() + " running");
        }

        while (isRunning()) {
            var timeoutOnly = isTimeoutOnly();
            var nextTimeoutMillis = DEFAULT_CHECK_TIMEOUT_MILLIS;

            if (watchdogEntries.peek() != null) {
                curTimeMillis = System.currentTimeMillis();

                synchronized (watchdogEntries) {
                    // check for disposed  and timeout processes in this order
                    var watchdogEntryIterator = watchdogEntries.iterator();

                    while (watchdogEntryIterator.hasNext()) {
                        var watchdogEntry = watchdogEntryIterator.next();

                        // check for TIMEOUT or DISPOSED status, with
                        // TIMEOUT having  higher priority than DISPOSED
                        var notificationJobErrorEx = ((curTimeMillis - watchdogEntry.getStartTimeMillis()) >= timeoutMilliSeconds) ?
                                new JobErrorEx(JobError.TIMEOUT) :
                                (!isTimeoutOnly() && watchdogEntry.getWatchdogHandler().isDisposed()) ?
                                        new JobErrorEx(JobError.DISPOSED) :
                                        new JobErrorEx(JobError.NONE);

                        // remove handler from queue and notify handler
                        // in case of found watchdog condition
                        if (notificationJobErrorEx.hasError()) {
                            watchdogEntryIterator.remove();

                            watchdogEntry.getWatchdogHandler().handleWatchdogNotification(
                                    curTimeMillis,
                                    watchdogEntry.getStartTimeMillis(),
                                    notificationJobErrorEx);
                        } else if (timeoutOnly) {
                            // leave loop if we only check for timeouts (sorted moded)
                            // and the first entry didn't reach its timeout
                            break;
                        }
                    }

                    // calculate next timeout value in case of TIMEOUT only mode
                    // with an entry queue having its entries in sorted order
                    if (timeoutOnly) {
                        var oldestSortedEntry = watchdogEntries.peek();
                        nextTimeoutMillis = oldestSortedEntry == null ? timeoutMilliSeconds : Math.max(0, timeoutMilliSeconds - (curTimeMillis - oldestSortedEntry.getStartTimeMillis()));
                    }
                }
            }

            // sleep thread for the calculated nextTimeoutMillis time until next test
            ServerManager.sleepThread(nextTimeoutMillis, false);
        }

        // when thread has been terminated, notify
        // all pending entries of a timeout
        synchronized (watchdogEntries) {
            WatchdogEntry curEntry;

            curTimeMillis = System.currentTimeMillis();

            while ((curEntry = watchdogEntries.poll()) != null) {
                curEntry.getWatchdogHandler().handleWatchdogNotification(
                        curTimeMillis,
                        curEntry.getStartTimeMillis(),
                        new JobErrorEx(JobError.TIMEOUT));
            }
        }

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getName() +  " finished");
        }
    }

    /**
     * @return <code>true</code> if it is ensured, that all entries are sorted
     *  in a descending mode, so that iterating over all entries within the watchdog
     *  worker can be stopped when the first entry did not reach its timeout.
     */
    boolean isTimeoutOnly() {
        return WatchdogMode.TIMEOUT == watchdogMode;
    }

}
