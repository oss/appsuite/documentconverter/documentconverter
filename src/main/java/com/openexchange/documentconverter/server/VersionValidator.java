/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import java.util.concurrent.atomic.AtomicBoolean;
import lombok.NonNull;
import jakarta.annotation.Nullable;

/**
 * {@link VersionValidator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class VersionValidator {

    final public  static int VERSION_INVALID = -1;

    /**
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public interface GetVersionHandler {
        int getVersion(VersionValidator versionValidator);
    }

    /**
     * VersionChangedHandler
     */
    public interface VersionChangedHandler {
        void versionChanged(VersionValidator versionValidator, int oldVersion, int newVersion);
    }

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private VersionValidator() {
        // unused
        m_getVersionHandler = null;
        m_gotValidVersionHandler = null;
        m_lostValidVersionHandler = null;
    }

    /**
     * Initializes a new {@link VersionValidator}.
     */
    public VersionValidator(
            @NonNull final GetVersionHandler getVersionHandler,
            @Nullable final VersionChangedHandler gotValidVersionHandler,
            @Nullable final VersionChangedHandler lostValidVersionHandler) {

        super();

        m_getVersionHandler = getVersionHandler;
        m_gotValidVersionHandler = gotValidVersionHandler;
        m_lostValidVersionHandler = lostValidVersionHandler;
    }

    /**
     *
     */
    synchronized public void setConnectionInvalid() {
        final int oldVersion = m_serverVersion;

        if (oldVersion > VERSION_INVALID) {
            m_serverVersion = VERSION_INVALID;
            m_enabled.compareAndSet(true, false);

            if (null != m_lostValidVersionHandler) {
                m_lostValidVersionHandler.versionChanged(this, oldVersion, VERSION_INVALID);
            }
        }
    }

    /**
     * Lazy validation of server connection.
     * Connection might be already down.
     * @return
     */
    public boolean isConnectionEnabled() {
        return m_enabled.get();
    }

    /**
     /**
     * Forced, physical validation of server connection.
     * Connection status reflects the current status of the server.
     * @return
     */
    public boolean isConnectionAvailable() {
        return (implGetServerVersion() > VERSION_INVALID);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    private int implGetServerVersion() {
        synchronized (this) {
            final int oldVersion = m_serverVersion;
            final int currentVersion = Math.max(m_getVersionHandler.getVersion(this), VERSION_INVALID);
            final boolean versionValid = currentVersion > VERSION_INVALID;

            if (versionValid) {
                if ((oldVersion != currentVersion) && (null != m_gotValidVersionHandler)) {
                    m_gotValidVersionHandler.versionChanged(this, oldVersion, currentVersion);
                }
            }

            m_enabled.set(versionValid);
            m_serverVersion = currentVersion;
        }

        return m_serverVersion;
    }


    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_enabled = new AtomicBoolean(false);

    final private GetVersionHandler m_getVersionHandler;

    final private VersionChangedHandler m_gotValidVersionHandler;

    final private VersionChangedHandler m_lostValidVersionHandler;

    private int m_serverVersion = VERSION_INVALID;
}
