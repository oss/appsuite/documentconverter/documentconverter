/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converter;

import com.openexchange.documentconverter.server.BackendType;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.error.ConverterException;
import com.openexchange.documentconverter.server.error.ExceptionUtils;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.*;
import com.openexchange.documentconverter.server.metrics.Statistics;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// TODO NEXT Refactor
public class ManagedJob implements JobStatusListener {

    public static final String GENERIC_JOB = "Generic job";
    public static final String STANDARD_GRAPHIC_JOB = "ReaderEngine Graphic job";

    private static final long MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE = 225;
    private static final long MAX_QUEUETIME_TO_JOBEXEC_TIME_PERCENTAGE = MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE * 10;
    private static final int JOBSTATUS_ORDINAL_RUNNING = JobStatus.RUNNING.ordinal();
    private static final String STR_PDF = "pdf";
    private static final String STR_PDF_A = "pdfa";

    private final ConverterJobFactory jobFactory;
    private final ServerManager serverManager;
    private final JobProcessor jobProcessor;
    private final JobMonitor jobMonitor;
    private final ServerConfig serverConfig;
    private final Cache cache;
    private final Statistics statistics;
    private final ReentrantLock lock = new ReentrantLock(true);
    private final Condition startedExecutionCondition = lock.newCondition();
    private final Condition finishedExecutionCondition = lock.newCondition();
    private volatile JobStatus jobStatus = JobStatus.NEW;
    private volatile JobErrorEx jobErrorEx = new JobErrorEx();

    ManagedJob(@NonNull ConverterJobFactory jobFactory, @NonNull ServerManager serverManager, @NonNull JobProcessor jobProcessor, JobMonitor jobMonitor, @NonNull ServerConfig serverConfig, @NonNull Cache cache, @NonNull Statistics statistics) {
        this.jobFactory = jobFactory;
        this.serverManager = serverManager;
        this.jobProcessor = jobProcessor;
        this.jobMonitor = jobMonitor;
        this.serverConfig = serverConfig;
        this.cache = cache;
        this.statistics = statistics;
    }

    public InputStream process(@NonNull String jobType, @NonNull String cacheHash, Map<String, Object> jobProperties, Map<String, Object> resultProperties) {
        String usedJobType = StringUtils.isBlank(jobType) ? null : jobType.toLowerCase();
        InputStream resultInputStream = null;

        if (usedJobType != null && jobProcessor.isRunning()) {
            var curJobProperties = new HashMap<String, Object>(12);
            var curResultProperties = new HashMap<String, Object>(8);

            ServerManager.ensureInputTypeSet(jobProperties);

            // create a temporary properties map
            curJobProperties.putAll(jobProperties);

            var deleteInputFile = new MutableBoolean(Boolean.FALSE);
            var jobInputFile = serverManager.getJobInputFile(jobProperties, deleteInputFile);
            if (jobInputFile != null) {
                var inputType = (String) curJobProperties.get(Properties.PROP_INPUT_TYPE);
                var isPDFInput = STR_PDF.equals(inputType);

                if ((usedJobType.equals(STR_PDF) || usedJobType.equals(STR_PDF_A)) && isPDFInput) {
                    resultInputStream = getPDFInputStream(jobInputFile, resultProperties, curResultProperties);
                } else {
                    var job = jobProcessor.createServerJob(usedJobType, cacheHash, curJobProperties, resultProperties);
                    if (job == null) {
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.GENERAL.getErrorCode());
                    } else {
                        resultInputStream = processJob(jobProcessor, job, curJobProperties, resultProperties, GENERIC_JOB);
                    }
                }
            }

            // cleanup input file, if flagged
            deleteFileIfFlagged((File) curJobProperties.remove(Properties.PROP_INPUT_FILE), deleteInputFile);
        } else  {
            // set GENERAL error in case of a missing job processor or job type
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.GENERAL.getErrorCode());
        }

        if ((null != resultInputStream) && !resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.NONE.getErrorCode());
        }
        return resultInputStream;
    }

    public void statusChanged(String jobId, Object data) {
        if (data instanceof JobEntry changedEntry) {
            var oldJobStatus = jobStatus;

            try {
                lock.lock();

                jobStatus = changedEntry.getJobStatus();

                // scheduled entry has been removed from queue =>
                // change JobStatus to JobStatus.ERROR and signal waiting thread for this job
                if (JobStatus.SCHEDULED == oldJobStatus && JobStatus.REMOVED == jobStatus) {
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job was removed from queue => setting QUEUE_TIMEOUT error at job and signaling startedExecutionCondition",
                                new LogData("jobid", jobId));
                    }

                    jobErrorEx = new JobErrorEx(JobError.QUEUE_TIMEOUT);

                    changedEntry.setJobStatus(jobStatus = JobStatus.ERROR);
                    changedEntry.setJobErrorEx(jobErrorEx);
                    changedEntry.getResultProperties().put(Properties.PROP_RESULT_ERROR_CODE, jobErrorEx.getErrorCode());
                    changedEntry.getResultProperties().put(Properties.PROP_RESULT_ERROR_DATA, jobErrorEx.getErrorData().toString());

                    // signal started condition, followed by signaling finished condition
                    startedExecutionCondition.signal();
                } else if ((JobStatus.FINISHED == jobStatus) || (JobStatus.ERROR == jobStatus)) {
                    jobErrorEx = changedEntry.getJobErrorEx();

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job finished RUNNING state => signaling finishedExecutionCondition",
                                new LogData("jobid", jobId));
                    }

                    // notify waiting thread that this job has just finished
                    finishedExecutionCondition.signal();
                } else if (JobStatus.RUNNING == jobStatus) {
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC job reached RUNNING state => signaling startedExecutionCondition",
                                new LogData("jobid", jobId));
                    }

                    // notify waiting thread that this job has just begun running
                    startedExecutionCondition.signal();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    InputStream processJob(@NonNull JobProcessor jobProcessor, @NonNull Job job, @NonNull Map<String, Object> jobProperties, @NonNull Map<String, Object> resultProperties, String jobDescription) {
        var hash = job.getHash();
        var jobStartTimeMillis = System.currentTimeMillis();
        var filename = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);
        var cachedJobErrorEx = jobProcessor.getErrorCache().getJobErrorExForHash(hash);
        var logTrace = ServerManager.isLogTrace();
        var logDebug = ServerManager.isLogDebug();
        InputStream resultInputStream = null;

        if (null == cachedJobErrorEx) {
            // create a job id in every case
            var jobId = UUID.randomUUID().toString();
            // lookup into result cache => if not found and no cache only flag is set => schedule job and process
            // TODO NEXT hash can be null! Add a comment to getCachedResult if it's needed that hash must not be null
            resultInputStream = cache.getCachedResult(hash, filename, resultProperties, true);
            if (resultInputStream == null) {
                // First check if configured max. queue count has been reached for Readerengine
                // jobs only => return immediately with appropriate error code set
                if (jobMonitor != null
                        && jobMonitor.isQueueLimitReached()
                        && BackendType.READERENGINE == job.backendTypeNeeded()) {

                    jobErrorEx = new JobErrorEx(JobError.MAX_QUEUE_COUNT);

                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, jobErrorEx.getErrorCode());
                    resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, jobErrorEx.getErrorData().toString());

                    if (logDebug) {
                        ServerManager.logDebug("DC job convert error",
                                jobProperties,
                                new LogData("jobid", jobId),
                                new LogData("hash", hash),
                                new LogData("joberror", jobErrorEx.getJobError().getErrorText()));
                    }

                    return null;
                }

                try {
                    // process job via queue scheduling
                    jobId = jobProcessor.scheduleJob(job, (JobPriority) jobProperties.get(Properties.PROP_PRIORITY), this, jobDescription);

                    if (null != jobId) {
                        var jobExecutionTimeoutMS = serverConfig.getJobExecutionTimeoutMilliseconds();
                        var maxQueueTimeForJob = (jobExecutionTimeoutMS * MAX_QUEUETIME_TO_JOBEXEC_TIME_PERCENTAGE) / 100;
                        var maxRunTimeForJob = (jobExecutionTimeoutMS * MAX_RUNNING_TO_JOBEXEC_TIME_PERCENTAGE) / 100;

                        try {
                            // waiting, until the job is really executed or processed in any way
                            lock.lock();

                            if (jobStatus.ordinal() < JOBSTATUS_ORDINAL_RUNNING) {
                                startedExecutionCondition.await(maxQueueTimeForJob, TimeUnit.MILLISECONDS);
                            }
                        } finally {
                            lock.unlock();
                        }

                        var jobStartExecTimeMillis = System.currentTimeMillis();

                        try {
                            // waiting, until the job is finished, but no more than upper time limit for execution (beside watchdog)
                            lock.lock();

                            if (jobStatus.ordinal() <= JOBSTATUS_ORDINAL_RUNNING) {
                                finishedExecutionCondition.await(maxRunTimeForJob, TimeUnit.MILLISECONDS);
                            }
                        } finally {
                            lock.unlock();
                        }

                        // If job has been started and is currently still executing,
                        // check the current total exec time and force a kill of the
                        // current job via the JobProcessor.
                        // In general, the Watchdog should have killed
                        // the job already and the job status should be > RUNNING;
                        // for yet unknown reasons, we don't get notified for some
                        // rare jobs once in a while.
                        // So, this is our last chance to really finish the job the
                        // hard way and cleanup resources accordingly
                        var jobEndExecTimeMillis = System.currentTimeMillis();

                        if (jobStatus.ordinal() <= JOBSTATUS_ORDINAL_RUNNING) {
                            if (ServerManager.isLogWarn()) {
                                ServerManager.logWarn("DC job will be aborted due to final TIMEOUT detection without job being finished up to now",
                                        new LogData("jobid", jobId),
                                        new LogData("hash", hash),
                                        new LogData("exectime", jobEndExecTimeMillis - jobStartExecTimeMillis + "ms"));
                            }

                            var notificationErrorEx = new JobErrorEx(JobError.TIMEOUT);

                            jobProcessor.killJob(jobId, notificationErrorEx);
                            jobErrorEx = notificationErrorEx;
                        }

                        // error and success handling
                        if (JobStatus.FINISHED == jobStatus) {
                            var resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

                            if (null != resultBuffer) {
                                resultInputStream = new ByteArrayInputStream(resultBuffer);
                                jobErrorEx = new JobErrorEx();
                            } else {
                                jobErrorEx = new JobErrorEx(JobError.NO_CONTENT);
                            }
                        }

                        // set final jobError at result properties
                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, jobErrorEx.getErrorCode());
                        resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, jobErrorEx.getErrorData().toString());

                        if (logTrace && (null != jobDescription)) {
                            ServerManager.logTrace(
                                    "DC finished " + jobDescription,
                                    new LogData("jobid", jobId),
                                    new LogData("hash", hash),
                                    new LogData("doctype", (String) jobProperties.get(Properties.PROP_INPUT_TYPE)),
                                    new LogData("jobtime", jobEndExecTimeMillis - jobStartTimeMillis + "ms"),
                                    new LogData("queuetime", jobStartExecTimeMillis - jobStartTimeMillis + "ms"),
                                    new LogData("exectime", jobEndExecTimeMillis - jobStartExecTimeMillis + "ms"));
                        }
                    } else if (logTrace && jobProcessor.isRunning()) {
                        ServerManager.logTrace("DC job not added to queue => please check ReaderEngine installation!",
                                jobProperties,
                                new LogData("jobid", jobId));
                    }
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                    ServerManager.logExcp(new ConverterException(e));
                } finally {
                    if (null != jobId) {
                        // remove job from JobProcessor
                        jobProcessor.removeJob(jobId);
                    }
                }
            }

            // add jobId to result properties
            resultProperties.put(Properties.PROP_RESULT_JOBID, jobId);

            // Logging (async Shape2PNGConverterJob does not produce result content)
            if (null == resultInputStream && job.producesCacheableResult()) {
                if (jobProcessor.isRunning()) {
                    var errorMsg = "DC job convert error";
                    var jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
                    LogData[] logDatas = {
                            new LogData("jobid", jobId),
                            new LogData("hash", hash),
                            new LogData("joberror", jobErrorEx.getJobError().toString())
                    };

                    // special treatment for queue limit related errors => log
                    // on DEBUG level only, not on WARN level since the current
                    // queue limit status has already been logged as a WARNING
                    if ((JobError.MAX_QUEUE_COUNT == jobErrorEx.getJobError()) || (JobError.QUEUE_TIMEOUT == jobErrorEx.getJobError())) {
                        if (logDebug) {
                            ServerManager.logDebug(errorMsg, jobProperties, logDatas);
                        }
                    } else {
                        ServerManager.logWarn(errorMsg, jobProperties, logDatas);
                    }
                }
            } else if (logTrace) {
                ServerManager.logTrace("DC job converted",
                        new LogData("jobid", jobId),
                        new LogData("hash", hash),
                        new LogData(
                                "mimetype",
                                resultProperties.containsKey(Properties.PROP_RESULT_MIME_TYPE) ? (String) resultProperties.get(Properties.PROP_RESULT_MIME_TYPE) : ""),
                        new LogData(
                                "extension",
                                resultProperties.containsKey(Properties.PROP_RESULT_EXTENSION) ? (String) resultProperties.get(Properties.PROP_RESULT_EXTENSION) : ""),
                        new LogData(
                                "pagecount",
                                resultProperties.containsKey(Properties.PROP_RESULT_PAGE_COUNT) ? ((Integer) resultProperties.get(Properties.PROP_RESULT_PAGE_COUNT)).toString() : "-1"),
                        new LogData(
                                "zip",
                                resultProperties.containsKey(Properties.PROP_RESULT_ZIP_ARCHIVE) ? ((Boolean) resultProperties.get(Properties.PROP_RESULT_ZIP_ARCHIVE)).toString() : "false"),
                        resultProperties.containsKey(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT) ? new LogData(
                                "originalpagecount",
                                ((Integer) resultProperties.get(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT)).toString()) : null);
            }
        } else {
            if (!resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, cachedJobErrorEx.getErrorCode());
                resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, cachedJobErrorEx.getErrorData().toString());
            }

            // update the statistics errorcache hit counter
            statistics.incrementProcessedJobCount(cachedJobErrorEx, true);

            if (logTrace && jobProcessor.isRunning()) {
                ServerManager.logTrace("DC error returned from errorcache",
                        jobProperties,
                        new LogData("hash", hash),
                        new LogData("joberror", cachedJobErrorEx.getJobError().toString()));
            }
        }

        return resultInputStream;
    }

    void deleteFileIfFlagged(File file, @NonNull MutableBoolean deleteFile) {
        if (deleteFile.booleanValue()) {
            deleteFile.setValue(Boolean.valueOf(!FileUtils.deleteQuietly(file)));
        }
    }

    InputStream getPDFInputStream(File jobInputFile, Map<String, Object> resultProperties, Map<String, Object> curResultProperties) {
        InputStream resultInputStream = null;
        // just return the given input document content,
        // if a PDF conversion of a PDF file is requested
        try {
            var resultBuffer = FileUtils.readFileToByteArray(jobInputFile);

            resultInputStream = new ByteArrayInputStream(resultBuffer);
            resultProperties.put(Properties.PROP_RESULT_BUFFER, resultBuffer);
            resultProperties.put(Properties.PROP_CACHE_HASH, curResultProperties.get(Properties.PROP_CACHE_HASH));
        } catch (final IOException e) {
            ServerManager.logExcp(e);
        }

        return resultInputStream;
    }
}
