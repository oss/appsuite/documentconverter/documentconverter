/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converter;

import lombok.NonNull;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

// TODO NEXT Done
public class JobData {

    private final String pageJobType;

    private final Map<String, Object> pageJobProperties;

    private File pdfInputFile;

    private final AtomicLong timeStampMillis = new AtomicLong(System.currentTimeMillis());

    JobData(@NonNull File pdfInputFile, @NonNull String pageJobType, @NonNull Map<String, Object> pageJobProperties) {
        this.pageJobType = pageJobType;
        this.pageJobProperties = pageJobProperties;
        this.pdfInputFile = pdfInputFile;
    }

    void clear() {
        FileUtils.deleteQuietly(pdfInputFile);
        pdfInputFile = null;
    }

    @NonNull Map<String, Object> getPageJobProperties() {
        return pageJobProperties;
    }

    @NonNull String getPageJobType() {
        return pageJobType;
    }

    long getTimeStampMillis() {
        return timeStampMillis.get();
    }

    void updateTimestamp() {
        timeStampMillis.set(System.currentTimeMillis());
    }
}
