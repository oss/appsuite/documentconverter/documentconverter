/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.converter;

import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converterJob.ConverterJobFactory;
import com.openexchange.documentconverter.server.job.JobMonitor;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.metrics.Statistics;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.HashMap;

@Slf4j
@Service
@Scope("singleton")
@AllArgsConstructor
@NoArgsConstructor
public class ManagedJobFactory {

    @Autowired
    private ConverterJobFactory jobFactory;

    @Autowired
    private ServerManager serverManager;

    @Autowired
    private JobProcessor jobProcessor;

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private Statistics statistics;

    @Autowired
    private Cache cache;

    public ManagedJob createManagedJob() {
        return new ManagedJob(jobFactory, serverManager, jobProcessor, jobProcessor.getJobMonitor(), serverConfig, cache, statistics);
    }
}
