/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

// TODO NEXT Complete
public class CacheHashFileMapper {

    private static final int MAX_DEPTH = 4;
    private static final char PATH_DELIMITER = File.separatorChar;

    private static final String[] BYTE_TO_HEX_MAP = new String[256];

    // TODO NEXT What is the benefit of reusing StringBuilder. The list has no limit and no method to remove unused StringBuilders.
    private static final LinkedList<StringBuilder> STRING_BUILDERS = new LinkedList<>();

    @Getter
    private final File rootDir;

    private final String rootDirString;

    @Getter
    private final int depth;

    // static init. of BYTE_TO_HEX_MAP entries and STRING_BUILDERS
    static {
        for (int i = 0, count = BYTE_TO_HEX_MAP.length; i < count; ++i) {
            var curHexValue = Integer.toHexString(i);
            BYTE_TO_HEX_MAP[i] = (curHexValue.length() < 2) ? ("0" + curHexValue) : curHexValue;
        }
    }

    public CacheHashFileMapper(@NonNull File rootDir, int depth) {
        super();

        this.rootDir = rootDir;
        this.depth = Math.min(Math.max(depth, 0), MAX_DEPTH);

        this.rootDirString = getRootDirAsString(rootDir);
    }

    public File mapHashToFile(@NonNull String hash) {
        var cacheFileNameBuilder = acquireStringBuilder(rootDirString);

        try {
            // always treat hash code as unsigned (int) value
            var hashCode = Integer.toUnsignedLong(hash.hashCode());

            // create entry directory tree by using 1 byte each (LSB to MSB) for every
            // depth level and mapping via Hex string LUT for each possble byte
            for (int curShift = 0, maxShift = (depth * 8); curShift < maxShift; curShift += 8) {
                cacheFileNameBuilder.append(PATH_DELIMITER).append(BYTE_TO_HEX_MAP[(int)((hashCode >> curShift) & 0xFF)]);
            }

            return new File(cacheFileNameBuilder.append(PATH_DELIMITER).append(hash).toString());
        } finally {
            releaseStringBuilder(cacheFileNameBuilder);
        }
    }


    @NonNull private StringBuilder acquireStringBuilder(@NonNull final String rootDir) {
        StringBuilder result;

        synchronized (STRING_BUILDERS) {
            result = STRING_BUILDERS.poll();
        }

        // if the list is empty create a new StringBuffer
        if (result == null) {
            result = new StringBuilder(512);
        }

        return result.append(rootDir);
    }

    private void releaseStringBuilder(@NonNull final StringBuilder stringBuilder) {
        stringBuilder.setLength(0);

        synchronized (STRING_BUILDERS) {
            STRING_BUILDERS.add(stringBuilder);
        }
    }

    private String getRootDirAsString(@NonNull File rootDir) {
        String rootDirString;

        // obtain root dir as string and remove possible ending path delimiters
        try {
            rootDirString = rootDir.getCanonicalPath();
        } catch (IOException e) {
            rootDirString = rootDir.getAbsolutePath();
        }

        while (StringUtils.isNotEmpty(rootDirString) && (rootDirString.charAt(rootDirString.length() - 1) == PATH_DELIMITER)) {
            rootDirString = rootDirString.substring(0, rootDirString.length() - 1);
        }

        return rootDirString;
    }
}
