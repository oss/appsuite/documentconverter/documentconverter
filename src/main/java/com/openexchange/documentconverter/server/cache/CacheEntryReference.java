/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import lombok.NonNull;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.time.Month;
import java.util.Arrays;

// TODO NEXT Complete
public class CacheEntryReference implements Comparable<CacheEntryReference> {
    private static final Charset ASCII_CHARSET = Charset.forName("US-ASCII");

    private final byte[] asciiHashBytes;

    private long lastAccessTimeMillis = 0;

    private int persistentSize = 0;

    public CacheEntryReference(@NonNull String asciiHash) {
        super();
        asciiHashBytes = asciiHash.getBytes(ASCII_CHARSET);
    }

    public CacheEntryReference(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String asciiHash, long lastAccessTime) {
        this(asciiHash);

        var persistentDir = getPersistentDir(hashFileMapper);

        lastAccessTimeMillis = lastAccessTime;
        persistentSize = (int) Cache.calculateDirectorySize(persistentDir);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(asciiHashBytes);
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (obj instanceof CacheEntryReference) {
            return Arrays.equals(asciiHashBytes, ((CacheEntryReference) obj).asciiHashBytes);
        }

        return false;
    }

    @Override
    public int compareTo(CacheEntryReference compareCacheHashEntry) {
        // only the timestamps are compared in natural sort order (olderDate < newerData),
        return Long.compare(lastAccessTimeMillis, compareCacheHashEntry.lastAccessTimeMillis);
    }

    @NonNull public String getHash() {
        return new String(asciiHashBytes, ASCII_CHARSET);
    }

    public boolean isValidTimestamp(long curTimeMillis, long timeoutMillis) {
        return ((curTimeMillis - lastAccessTimeMillis) < timeoutMillis);
    }

    /**
     * @return  The full path to this objects cache entry directory
     */
    File getPersistentDir(@NonNull CacheHashFileMapper hashFileMapper) {
        return hashFileMapper.mapHashToFile(getHash());
    }

    public long getLastAccessTimeMillis() {
        return lastAccessTimeMillis;
    }

    public long getPersistentSize() {
        return persistentSize;
    }

    public void clear(@NonNull CacheHashFileMapper hashFileMapper) {
        FileUtils.deleteQuietly(getPersistentDir(hashFileMapper));
    }

    public void touch(@NonNull CacheHashFileMapper hashFileMapper) {
        getPersistentDir(hashFileMapper).setLastModified(lastAccessTimeMillis = System.currentTimeMillis());
    }

}
