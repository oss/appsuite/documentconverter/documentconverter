/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.concurrency.IdLocker;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// TODO NEXT Refactor needed!
public class CacheEntry {

    private static final long serialVersionUID = 1L;

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private String hash = null;

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private final HashMap<String, Object> resultProperties = new HashMap<>(8);

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private byte[] resultContent = null;

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private Date accessTime = new Date();

    // !!!  DO NOT CHANGE TYPE, ORDER OR NAME OF THIS VARIABLE FOR STREAMING COMPATIBILITY !!!
    private long persistentSize = 0;

    private transient File entryDir = null;

    private CacheEntry() {
        super();

        implInitTransients();
    }

    private CacheEntry(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String hash, @NonNull Map<String, Object> properties, byte[] content) {
        this();

        entryDir = getCacheEntryDir(hashFileMapper, hash);
        this.hash = hash;

        resultProperties.clear();
        resultProperties.putAll(properties);

        resultContent = content;
        accessTime = new Date(entryDir.lastModified());
        persistentSize = Cache.calculateDirectorySize(entryDir);
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entryDir == null) ? 0 : entryDir.hashCode());
        result = prime * result + ((hash == null) ? 0 : hash.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CacheEntry other = (CacheEntry) obj;
        if (entryDir == null) {
            if (other.entryDir != null) {
                return false;
            }
        } else if (!entryDir.equals(other.entryDir)) {
            return false;
        }
        if (hash == null) {
            if (other.hash != null) {
                return false;
            }
        } else if (!hash.equals(other.hash)) {
            return false;
        }
        return true;
    }

    public boolean isValid() {
        return Cache.isValidHash(hash);
    }

    public void clear() {
        hash = null;
        persistentSize = 0;
        resultProperties.clear();

        FileUtils.deleteQuietly(entryDir);
        entryDir = null;
    }

    public String getHash() {
        return hash;
    }

    public HashMap<String, Object> getResultProperties() {
        return resultProperties;
    }

    public byte[] getResultContent() {
        try {
            // TODO NEXT No clue when resultContent is not null
            return isValid() ?
                    (null != resultContent) ? resultContent :
                            FileUtils.readFileToByteArray(getResultFile()) :
                    null;
        } catch (IOException e) {
            if (ServerManager.isLogWarn()) {
                ServerManager.logWarn(new StringBuilder(1024).
                        append("DC cache entry result content could not be read: ").
                        append(getHash()).
                        append('(').
                        append(Throwables.getRootCause(e)).
                        append(')').toString());
            }
        }

        return null;
    }

    public File getResultFile() {
        return new File(getPersistentDirectory(), Properties.CACHE_RESULT_FILENAME);
    }

    public File getPersistentDirectory() {
        return entryDir;
    }

    public Date getAccessTime() {
        return accessTime;
    }

    public boolean updateAndMakePersistent() {
        if (resultProperties !=  null && ServerManager.validateOrMkdir(entryDir)) {
            return implMakePersistent();
        }

        return false;
    }

    /**
     * @return true If result properties could be retrieved from cacheEntry and set at the current serverJob
     */
    public Map<String, Object> getJobResult() {
        Map<String, Object> jobResult = null;

        if (isValid()) {
            final File resultFile = getResultFile();

            if (resultFile.canRead() && (resultFile.length() > 0)) {
                (jobResult = new HashMap<>()).putAll(getResultProperties());

                try {
                    jobResult.put(Properties.PROP_RESULT_BUFFER, FileUtils.readFileToByteArray(resultFile));
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                    jobResult = null;
                }
            }
        }

        return jobResult;
    }

    private void implInitTransients() {
        entryDir = null;
    }

    private boolean implMakePersistent() {
        var persisted = false;
        if (isValid() && entryDir != null) {
            // prepare the property set for persistence
            resultProperties.remove(Properties.PROP_RESULT_BUFFER);

            // take name of the result directory as the entries persistent name
            resultProperties.put(Properties.PROP_CACHE_PERSIST_ENTRY_NAME, entryDir.getName());

            // write adjusted properties map
            try (ObjectOutputStream objStm = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(entryDir, Properties.CACHE_PROPERTIES_FILENAME))))) {
                objStm.writeObject(resultProperties);
                objStm.flush();

                // write content, if available at runtime
                // and set to null afterward since it can
                // be restored from persistent data
                if (resultContent != null) {
                    FileUtils.writeByteArrayToFile(new File(entryDir, Properties.CACHE_RESULT_FILENAME), resultContent);
                    resultContent = null;
                }

                // update persistent members
                accessTime = new Date(entryDir.lastModified());
                persistentSize = Cache.calculateDirectorySize(entryDir);

                persisted = true;
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }
        }

        return persisted;
    }

    /**
     * York Richter: A cache entry must already exist or the folder structure and the files must have been created.
     */
    public static CacheEntry createFrom(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String hash) {

        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // read result properties first
                final File cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);
                final File resultPropertiesFile = new File(cacheEntryDir, Properties.CACHE_PROPERTIES_FILENAME);
                final File resultContentFile = new File(cacheEntryDir, Properties.CACHE_RESULT_FILENAME);

                if (resultPropertiesFile.canRead() && resultContentFile.canRead()) {
                    // try to read properties first and remove possibly
                    // contained result buffer in every case since
                    // content is/should be available via content file
                    try (ObjectInputStream objInputStm = new ObjectInputStream(new BufferedInputStream(new FileInputStream(resultPropertiesFile)))) {
                        final Object readObject = objInputStm.readObject();
                        if (readObject instanceof Map<?, ?>) {
                            return new CacheEntry(hashFileMapper, hash, (Map<String, Object>) readObject, null);
                        }
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    public static CacheEntry createAndMakePersistent(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String hash, @NonNull Map<String, Object> properties, @NonNull byte[] content) {

        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                var cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);

                if (ServerManager.validateOrMkdir(cacheEntryDir)) {
                    var cacheEntry = new CacheEntry(hashFileMapper, hash, properties, content);

                    if (cacheEntry.implMakePersistent()) {
                        return cacheEntry;
                    }
                }

                // cacheEntryDir is removed when previous calls
                // did not return a new CacheEntry up to now
                FileUtils.deleteQuietly(cacheEntryDir);
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    public static boolean isValid(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String hash) {
        if (Cache.isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // verify if result properties are reabable and a result buffer can be read
                var cacheEntryDir = getCacheEntryDir(hashFileMapper, hash);
                var resultPropertiesFile = new File(cacheEntryDir, Properties.CACHE_PROPERTIES_FILENAME);
                var resultContentFile = new File(cacheEntryDir, Properties.CACHE_RESULT_FILENAME);

                return resultPropertiesFile.canRead() && (resultPropertiesFile.length() > 0) && resultContentFile.canRead();
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return false;
    }

    public static File getCacheEntryDir(@NonNull CacheHashFileMapper hashFileMapper, @NonNull String hash) {
        return hashFileMapper.mapHashToFile(hash);
    }
}
