/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.concurrency.IdLocker;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.metrics.Statistics;
import com.openexchange.documentconverter.server.metrics.TagsType;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.mutable.MutableLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.regex.Pattern;

// TODO NEXT Complete
@Component
public class Cache {

    @Autowired
    private CacheDescriptor cacheDescriptor;

    @Autowired
    private Statistics statistics;

    @Autowired
    private MetricsRegistry metricsRegistry;

    @Autowired
    private ServerManager serverManager;

    private CacheHashFileMapper cacheHashFileMapper;

    private CacheWorker m_cacheWorker;

    // Removed static final for better testing
    File SYSTEM_TEMP_DIR = FileUtils.getTempDirectory();
    private static final String CACHE_LOG_PREFIX = "DC cache ";
    static final long BLOCK_SIZE_DEFAULT = 4096;
    private static long DIR_BLOCK_SIZE = BLOCK_SIZE_DEFAULT;
    private static long FILE_BLOCK_SIZE = BLOCK_SIZE_DEFAULT;
    static final int CACHE_DEFAULT_COLLECTION_CAPACITY = 262144;
    static final long CACHE_CLEANUP_PERIOD_MILLIS = 60000;
    private static final long CACHE_CLEAR_CHUNK_ENTRY_COUNT = 100000;
    private static final String STR_SECONDS = "s";
    private static final String STR_UNLIMITED = "unlimited";
    static final String STR_MIB = "MiB";
    static final Pattern BLOCK_SIZE_PATTERN = Pattern.compile("^\\s*([0-9]+)");

    private final AtomicBoolean enabledFlag = new AtomicBoolean(false);
    private final OrderedHashContainer orderedContainer = new OrderedHashContainer();
    private final Deque<CacheEntry> newEntryDeque = new LinkedList<>();
    private final String cacheUID = UUID.randomUUID().toString();
    private final AtomicBoolean cacheIsRunning = new AtomicBoolean(true);
    protected long localCacheEntryTimeoutMillis;
    protected long maxLocalCacheCount;
    private boolean usingFallbackCacheDir = false;
    private long maxLocalCacheSize;
    private long minLocalFreeSize;
    private long cacheThresholdUpperPercentage;
    private long cacheThresholdLowerPercentage;
    private boolean localConstraintLimitNotReached = true;

    @PostConstruct
    void init() {

        File cacheDir = null;

        // initialize basic cache
        if ((null != cacheDescriptor.cacheDirectory) && (!cacheDescriptor.cacheDirectory.isEmpty())) {
            cacheDir = new File(cacheDescriptor.cacheDirectory);
        }

        // try to create/use fallback directory in case of an error with the configured cache directory
        // FIXME why this strange loop, but just use a UUID to prevent clashes?
        if (!ServerManager.validateOrMkdir(cacheDir)) {
            for (int i = 1; i <= 100; ++i) {
                cacheDir = new File(SYSTEM_TEMP_DIR, "oxdc.cache" + i);

                if (ServerManager.validateOrMkdir(cacheDir)) {
                    usingFallbackCacheDir = true;
                    ServerManager.logWarn(getCacheStrBuilder("has no valid writeable local cache directory to work on: ").append(cacheDir).append(". Using cache fallback directory instead: ").append(cacheDir.getAbsolutePath()).toString());

                    break;
                }

                cacheDir = null;
            }
        }

        if (null == cacheDir) {
            cacheDir = SYSTEM_TEMP_DIR;
            ServerManager.logError(getCacheStrBuilder("has no valid writeable local cache directory to work on! Using default temp. directory as last effort: ").append(cacheDir.getAbsolutePath()).toString());
        }

        calculateBlockSizes(cacheDir);

        cacheHashFileMapper = new CacheHashFileMapper(cacheDir, cacheDescriptor.cacheDirectoryDepth);

        statistics.setLocalCacheFreeVolumeSize(getFileFreeVolumeSize(cacheHashFileMapper.getRootDir()));
        metricsRegistry.addMetric("cache.local.oldest.seconds", this, (v) -> getLocalOldestEntrySeconds(), TagsType.CACHE, "The age of the oldest entry within the local cache in seconds.");
        metricsRegistry.addMetric("cache.local.entry.count", this, (v) -> getLocalEntryCount(), TagsType.CACHE, "The current count of entries, stored within the local persistent cache.");

        // using local cache with given cache constraints
        maxLocalCacheCount = cacheDescriptor.maximumEntryCount;
        maxLocalCacheSize = cacheDescriptor.maximumPersistSize;
        minLocalFreeSize = cacheDescriptor.minimumFreeVolumeSize;
        localCacheEntryTimeoutMillis = cacheDescriptor.timeoutMillis;

        cacheThresholdUpperPercentage = cacheDescriptor.cleanupThresholdUpperPercentage;
        cacheThresholdLowerPercentage = cacheDescriptor.cleanupThresholdLowerPercentage;

        m_cacheWorker = CacheWorker.createCacheWorker(this);
        m_cacheWorker.start();
    }

    public static boolean isValidHash(String hash) {
        return StringUtils.isNotBlank(hash);
    }

    // TODO NEXT Is it needed that hash be flagged as @NonNull because there is a test if the hash is valid and if it is null it is not valid
    public InputStream getCachedResult(@NonNull String hash, String filename, @NonNull Map<String, Object> resultProperties, boolean updateCacheHitStatistic) {

        if (!Cache.isValidHash(hash)) {
            return null;
        }

        CacheEntry cacheEntry;
        InputStream inputStreamResult = null;

        IdLocker.lock(hash);

        try {
            cacheEntry = getEntry(hash, filename);

            if (null != cacheEntry) {
                var jobResultProperties = cacheEntry.getJobResult();

                // try to read result from stored cache entry and assign
                // job result properties to given result properties
                if (null != jobResultProperties) {
                    resultProperties.putAll(jobResultProperties);

                    var resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);
                    if (null != resultBuffer) {
                        inputStreamResult = new ByteArrayInputStream(resultBuffer);
                    }

                    // Logging
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC cache hit => job result read from local cache.",
                                new LogData("hash", hash),
                                new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }

                if (null == inputStreamResult) {
                    // if a cache entry is found, but the result is not readable from the entry,
                    // remove the (invalid) entry from the cache; it will be recreated afterward
                    removeEntry(hash);

                    // Logging
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC cache hit, but job result not readable locally => removing entry!",
                                new LogData("hash", hash),
                                new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }
            }
        } finally {
            IdLocker.unlock(hash);
        }

        // update conversion cache hit counter in case of success and if requested
        if (updateCacheHitStatistic && (null != inputStreamResult)) {
            statistics.incrementProcessedJobCount(new JobErrorEx(), true);
        }

        return inputStreamResult;
    }

    public CacheEntry startNewEntry(String hash) {
        if (isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // ensure, that startEntry is not called twice for one hash
                if (!containsHash(hash)) {
                    synchronized (newEntryDeque) {

                        for (var cacheEntry : newEntryDeque) {
                            if (hash.equals(cacheEntry.getHash())) {
                                return null;
                            }
                        }
                    }
                }

                // newEntryDeque doesn't contain this hash => create new one and add to queue
                var newCacheEntry = CacheEntry.createAndMakePersistent(getHashFileMapper(), hash, new HashMap<>(8), new byte[] {});

                if (null != newCacheEntry) {
                    synchronized (newEntryDeque) {
                        newEntryDeque.push(newCacheEntry);
                    }

                    return newCacheEntry;
                }
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    public void endNewEntry(CacheEntry newCacheEntry) {
        if (null != newCacheEntry) {
            synchronized (newEntryDeque) {
                newEntryDeque.remove(newCacheEntry);
            }
        }
    }

    public CacheEntry getEntry(@NonNull String hash, String filename) {
        if (isEnabled() && isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // try to get from local cache first in every case
                CacheEntry cacheEntryResult = containsHash(hash) ? createCacheEntry(hash) : null;
                if (cacheEntryResult != null) {
                    synchronized (orderedContainer) {
                        // touch accessed entry
                        var cacheEntryReference = orderedContainer.remove(hash);

                        if (null != cacheEntryReference) {
                            cacheEntryReference.touch(getHashFileMapper());
                            orderedContainer.add(cacheEntryReference);
                        }
                    }

                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace(getCacheStrBuilder("read entry from local cache: ").append(hash).toString());
                    }
                }

                return cacheEntryResult;
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    // TODO NEXT remove parameter remoteTransfer and filename this parameters are only used in combination with cacheclientserver
    public boolean addEntry(CacheEntry entry, boolean remoteTransfer, String filename) {
        if (isEnabled() && (null != entry) && entry.isValid()) {
            var hash = entry.getHash();

            IdLocker.lock(hash);

            try {
                // add to local cache, if not already cached locally
                if (!containsHash(hash)) {
                    if (remoteTransfer) {
                        // caching via cache service, must be called before making the newCacheEntry persistent
                        final CacheServiceClient cacheServiceClient = serverManager.getCacheServiceClient();
                        if (cacheServiceClient != null) {
                            final byte[] resultContent = entry.getResultContent();
                            if (resultContent != null && resultContent.length > 0) {
                                final CacheServiceEntry cacheServiceEntry = new CacheServiceEntry(hash, new ByteArrayInputStream(resultContent));
                                final boolean addedToCache = cacheServiceClient.addCacheServiceEntry(cacheServiceEntry, filename);
                                if (ServerManager.isLogTrace()) {
                                    ServerManager.logTrace(getCacheStrBuilder("added entry to CacheService").append(addedToCache ? "(Successful)" : "(Failed)").append(": ").append(hash).toString());
                                }
                            }
                        }
                    }

                    // make result properties persistent at existing location and add to local cache
                    if (entry.updateAndMakePersistent()) {
                        // final String filename = (String) job.getJobProperties().get(Properties.PROP_INFO_FILENAME);
                        addCacheEntryRefToCache(new CacheEntryReference(getHashFileMapper(), hash, entry.getAccessTime().getTime()));
                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace(getCacheStrBuilder("added entry to local cache: ").append(hash).toString());
                        }
                    }
                }
                return true;
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return false;
    }

    public void removeEntry(String hash) {
        if (isEnabled() && isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                var cacheEntryReference = orderedContainer.get(hash);

                if (cacheEntryReference != null) {
                    synchronized (this) {
                        var entryToRemove = orderedContainer.remove(hash);

                        if (null != entryToRemove) {
                            removeCacheEntryRefFromCache(cacheEntryReference);
                        }
                    }
                }
            } finally {
                IdLocker.unlock(hash);
            }
        }
    }

    public boolean isEnabled() {
        return enabledFlag.get();
    }

    public static long calculateDirectorySize(File directory) {
        return calculateDirectorySize(directory, 0);
    }

    private static long calculateDirectorySize(File directory, long startSize) {
        var directorySize = startSize;

        if (FileUtils.isDirectory(directory)) {
            directorySize += DIR_BLOCK_SIZE;

            var files = directory.listFiles();

            if (files != null) {
                for (var curFile : files) {
                    if (curFile.isDirectory()) {
                        directorySize += calculateDirectorySize(curFile, directorySize);
                    } else {
                        var curFileSize = curFile.length();
                        directorySize += (((curFileSize / FILE_BLOCK_SIZE) + (((curFileSize % FILE_BLOCK_SIZE) > 0) ? 1 : 0)) * FILE_BLOCK_SIZE);
                    }
                }
            }
        }

        return directorySize;
    }

    boolean containsHash(@NonNull String hash) {
        return orderedContainer.contains(hash);
    }

    CacheEntry createCacheEntry(@NonNull String hash) {
        CacheEntry cacheEntryResult = null;
        var cacheEntry = CacheEntry.createFrom(getHashFileMapper(), hash);
        // check for null or invalid cache entry
        if (null == cacheEntry) {
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace(getCacheStrBuilder("could not find cache entry in local cache: ").append(hash).toString());
            }
        } else if (cacheEntry.isValid()) {
            cacheEntryResult = cacheEntry;
        } else {
            // TODO NEXT Remove: I think that can't happen because it's a new created CacheEntry and if the hash is not valid CacheEntry.createFrom returns null
            // try to find reason why cache cache entry is invalid
            var resultProperties = cacheEntry.getResultProperties();
            String removeReason;

            if (null == resultProperties) {
                removeReason = "cache entry properties file could not be read locally : " + hash;
            } else {
                var resultBuffer = cacheEntry.getResultContent();
                removeReason = (null == resultBuffer) ? "cache entry directory doesn't contain a readable local result file" : ("unkown: hash=" + hash);
            }

            cacheEntry.clear();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace(getCacheStrBuilder("found requested entry locally but removes it due to: ").append(removeReason).append(" (hash: ").append(hash).append(')').toString());
            }
        }

        return cacheEntryResult;
    }

    /**
     * @param logTitle The text after the prefix, no first space is needed.
     */
    protected static StringBuilder getCacheStrBuilder(@NonNull String logTitle) {
        return new StringBuilder(256).append(CACHE_LOG_PREFIX).append(logTitle);
    }

    public CacheHashFileMapper getHashFileMapper() {
        return cacheHashFileMapper;
    }

    /**
     * !!! Method is thread safe wrt. instance members but unsynchronized wrt. cacheEntry and needs synchronization for used hash by caller !!!
     */
    protected synchronized void removeCacheEntryRefFromCache(CacheEntryReference cacheEntryReference) {
        if (null != cacheEntryReference) {
            var cacheEntrySize = cacheEntryReference.getPersistentSize();

            statistics.subtractLocalCachePersistentSize(cacheEntrySize);
            statistics.addLocalCacheFreeVolumeSize(cacheEntrySize);

            cacheEntryReference.clear(getHashFileMapper());
        }
    }

    /**
     * !!! Method is thread safe wrt. instance members but unsynchronized wrt. cacheEntry and needs synchronization for used hash by caller !!!
     */
    protected synchronized void addCacheEntryRefToCache(@NonNull CacheEntryReference cacheEntryReference) {
        // put entry at the tail of the list, making it the newest entry
        orderedContainer.add(cacheEntryReference);

        // update cache relevant sizes
        var cacheEntrySize = cacheEntryReference.getPersistentSize();

        statistics.addLocalCachePersistentSize(cacheEntrySize);
        statistics.subtractLocalCacheFreeVolumeSize(cacheEntrySize);
    }

    public long getLocalEntryCount() {
        return orderedContainer.size();
    }

    protected static long getThroughputPerSecond(long count, long durationMillis) {
        return (long) ((double) (1000L * count) / (Math.max(durationMillis, 1L)));
    }

    /**
     * @return The size in Mib (1024KiB granularity)
     */
    static long getMiBSize(long sizeBytes) {
        return sizeBytes / 1048576;
    }

    public synchronized void cleanup() {
        // TODO NEXT implement it
        // use stack variables instead of member for faster access in loops
        var cleanupStartTimeMillis = System.currentTimeMillis();
        var hashFileMapper = getHashFileMapper();
        var cacheEntryTimeoutMillisToUse = localCacheEntryTimeoutMillis;
        var maxLocalCacheCountToUse = maxLocalCacheCount;
        var minLocalFreeSizeToUse = minLocalFreeSize;
        var maxLocalCacheSizeCleanupStart = (long) Math.ceil(maxLocalCacheSize * cacheThresholdUpperPercentage * 0.01);
        var maxLocalCacheSizeToUse = (maxLocalCacheSize > -1) ? (long) Math.ceil(maxLocalCacheSize * cacheThresholdLowerPercentage * 0.01) : -1;
        var cacheSizeLimited = (maxLocalCacheSizeToUse > -1);
        var entriesRemoved = false;

        statistics.setLocalCacheFreeVolumeSize(getFileFreeVolumeSize(hashFileMapper.getRootDir()));
        var startCacheSizeBasedCleanup =
                (statistics.getLocalCacheFreeVolumeSize() < minLocalFreeSizeToUse) ||
                        (cacheSizeLimited && (statistics.getLocalCachePersistentSize() >= maxLocalCacheSizeCleanupStart));


        // remove entries on a timeout basis, if timeout value is > 0
        if ((cacheEntryTimeoutMillisToUse > 0) && cleanup((curCashEntryReference) ->
            (cleanupStartTimeMillis - curCashEntryReference.getLastAccessTimeMillis()) >= cacheEntryTimeoutMillisToUse, "CacheTimeout") > 0) {
            entriesRemoved = true;
        }

        // remove entries on a count basis, if count value is > -1
        if ((maxLocalCacheCountToUse > -1) && cleanup((curCacheEntry) -> (orderedContainer.size() > maxLocalCacheCountToUse), "CacheCount") > 0) {
            entriesRemoved = true;
        }

        // remove cache entries on a free space available or maximum cache size basis, if max. local cache size is reached
        if (startCacheSizeBasedCleanup && cleanup((curCacheEntry) ->
            ((cacheSizeLimited && (statistics.getLocalCachePersistentSize() > maxLocalCacheSizeToUse)) || (statistics.getLocalCacheFreeVolumeSize() < minLocalFreeSizeToUse)), "CacheSize") > 0) {
            entriesRemoved = true;
        }

        if (entriesRemoved) {
            statistics.setLocalCacheFreeVolumeSize(getFileFreeVolumeSize(hashFileMapper.getRootDir()));

            var durationMillis = System.currentTimeMillis() - cleanupStartTimeMillis;

            if (localConstraintLimitNotReached) {
                logCacheGeometry("DC cache reached local constraint limit for the first time", durationMillis, true);
                localConstraintLimitNotReached = false;
            } else {
                var cacheEnabled = (maxLocalCacheSizeToUse != 0) && (maxLocalCacheCountToUse != 0);
                logCacheGeometry("DC cache " + (cacheEnabled ? "enabled " : "disabled " ) + "local cache instance", durationMillis, false);
            }
        }
    }

    void logCacheGeometry(String logText, long durationMillis, boolean isInfo) {
        if (isInfo || ServerManager.isLogTrace()) {
            var cacheRootDir = getHashFileMapper().getRootDir();
            var logData = ArrayUtils.addAll(new LogData[] {
                         new LogData("local cache dir", (null != cacheRootDir) ? cacheRootDir.toString() : DocumentConverterUtil.STR_NOT_AVAILABLE),
                    new LogData("local cur/max cache entries", orderedContainer.size() + " / " + ((-1 == maxLocalCacheCount) ? STR_UNLIMITED : Long.toString(maxLocalCacheCount))),
                    new LogData("local cur/max cache size", getMiBSize(statistics.getLocalCachePersistentSize()) + STR_MIB + " / " + ((-1 == maxLocalCacheSize) ? STR_UNLIMITED : getMiBSize(maxLocalCacheSize) + STR_MIB)),
                    new LogData("local cur/min free size", getMiBSize(statistics.getLocalCacheFreeVolumeSize()) + STR_MIB + " / " + getMiBSize(minLocalFreeSize) + STR_MIB),
                    new LogData("local oldest entry", getLocalOldestEntrySeconds() + STR_SECONDS),
                    new LogData("duration", durationMillis + "ms")
            }, statistics.getAsyncServerExcutorLogData());

            if (isInfo) {
                ServerManager.logInfo(logText, logData);
            } else {
                ServerManager.logTrace(logText, logData);
            }
        }
    }

    public void enable() {
        enabledFlag.compareAndSet(false, true);
    }

    private synchronized void calculateBlockSizes(@NonNull File cacheDir) {
        var testDir = new File(cacheDir, "testdirectory-" + UUID.randomUUID());

        try {
            if (ServerManager.validateOrMkdir(testDir)) {
                DIR_BLOCK_SIZE = calculateFileBlockSize(testDir.getAbsolutePath());

                var testFile = new File(testDir, "testfile.1");

                try (var oStm = new FileOutputStream(testFile)) {
                    oStm.write('x');
                    oStm.flush();
                }

                FILE_BLOCK_SIZE = calculateFileBlockSize(testFile.getAbsolutePath());
            }
        } catch (IOException e) {
            ServerManager.logExcp(e);
        } finally {
            FileUtils.deleteQuietly(testDir);
        }
    }

    long calculateFileBlockSize(@NonNull String filePath) {
        var outputList = executeBashCommand("du -s -L -B 1 " + filePath);

        if (!outputList.isEmpty()) {
            for (var curLine : outputList) {
                var matcher = BLOCK_SIZE_PATTERN.matcher(curLine);

                if (matcher.find()) {
                    final long fileBlockSize = NumberUtils.toLong(matcher.group(1));

                    if (fileBlockSize > 0) {
                        return fileBlockSize;
                    }
                }
            }
        }

        return BLOCK_SIZE_DEFAULT;
    }

        /**
     * @param command The command to execute
     * @return The array of output lines of the command.
     *  If command fails, the array of output lines is empty.
     */
    @NonNull private List<String> executeBashCommand(String command) {
        var outputLineList = new LinkedList<String>();

        if (null != command) {
            try {
                var processBuilder = new ProcessBuilder(ArrayUtils.add(ServerManager.getMaxVMemBashCommandLine(0), command) );
                var process = processBuilder.start();

                try (var reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                    for (String curLine; null != (curLine = reader.readLine()); ) {
                        outputLineList.add(curLine);
                    }
                }
            } catch (IOException e) {
                ServerManager.logExcp(e);
            }
        }

        return outputLineList;
    }

    long getFileFreeVolumeSize(@NonNull File file) {
        try {
            return Files.getFileStore(Paths.get(file.getCanonicalPath())).getUsableSpace();
        } catch (Exception e) {
            ServerManager.logExcp(e);
        }

        return 0;
    }

    @NonNull public String getUID() {
        return cacheUID;
    }

    public synchronized long clear(long count) {
        var clearStartTimeMillis = System.currentTimeMillis();
        var initialCount = getLocalEntryCount();
        var trace = ServerManager.isLogTrace();
        var ret = 0L;

        if (initialCount > 0) {
            var countToRemove = (count < 0) ? initialCount : Math.min(count, initialCount);

            if (trace) {
                ServerManager.logTrace(getCacheStrBuilder("is clearing local cache...: ").append(countToRemove).append(" / ").append(initialCount).toString());
            }

            var maxCountToRemoveInChunk = Math.min(countToRemove, CACHE_CLEAR_CHUNK_ENTRY_COUNT);
            var chunkCount = (int) (countToRemove / CACHE_CLEAR_CHUNK_ENTRY_COUNT + (((countToRemove % CACHE_CLEAR_CHUNK_ENTRY_COUNT) > 0) ? 1 : 0));

            var lastChunkStartTimeMillis = clearStartTimeMillis;

            for (int i = 0; i < chunkCount; ++i) {
                var curRemovedCounter = new MutableLong(0);
                var curRemovedCount = cleanup((curCacheEntry) -> (curRemovedCounter.incrementAndGet() <= maxCountToRemoveInChunk), "CacheClear");

                ret += curRemovedCount;

                var curNewTime = System.currentTimeMillis();
                var curLeftCount = countToRemove - ret;
                var curDurationMillis = curNewTime - lastChunkStartTimeMillis;
                    var curClearThroughput = getThroughputPerSecond(curRemovedCount, curDurationMillis);

                lastChunkStartTimeMillis = curNewTime;

                if (trace) {
                    ServerManager.logTrace(getCacheStrBuilder("cleared chunk of entries from local cache: ").append(curRemovedCount).append(", ").append(curLeftCount).append(" left to remove").append(" (clear chunk duration: ").append(curDurationMillis).append("ms").append(", clear chunk throughput: ").append(curClearThroughput).append("/s)").toString());
                }
            }
        }

        if (trace) {
            var leftCount = getLocalEntryCount();
            var durationMillis = System.currentTimeMillis() - clearStartTimeMillis;
            var clearThroughput = getThroughputPerSecond(ret, durationMillis);

            ServerManager.logTrace(getCacheStrBuilder("cleared local entries: ").append(ret).append(" / ").append(initialCount).append(", ").append(leftCount).append(" left").append(" (clear duration: ").append(durationMillis).append("ms").append(", clear throughput: ").append(clearThroughput).append("/s)").toString());
        }

        return ret;
    }

    /**
     * !!! Method needs synchronization for instance members if called!!!
     */
    private long cleanup(@NonNull Predicate<CacheEntryReference> removePredicate, @NonNull String cleanupReason) {
        var oldEntryCount = getLocalEntryCount();
        var oldCacheSize = statistics.getLocalCachePersistentSize();
        var removedEntryCount = 0L;

        synchronized (orderedContainer) {
            for (var orderedContainerIterator = orderedContainer.iterator(); orderedContainerIterator.hasNext();) {
                var curCashEntryReference = orderedContainerIterator.next();
                var curHash = curCashEntryReference.getHash();

                // !!! don't use a waiting lock here since we're in an instance synchronized block
                if (IdLocker.lock(curHash, IdLocker.Mode.TRY_LOCK)) {
                    try {
                        if (removePredicate.test(curCashEntryReference)) {
                            orderedContainerIterator.remove();


                            // subtract current entry data and clear persistent data
                            removeCacheEntryRefFromCache(curCashEntryReference);
                            ++removedEntryCount;
                        } else {
                            // we can stop, if the current entry didn't reach its
                            // timeout value, since all following entries are newer
                            break;
                        }
                    } finally {
                        IdLocker.unlock(curHash);
                    }
                }
            }
        }

        traceCacheRemoval(oldEntryCount, oldCacheSize, cleanupReason);

        return removedEntryCount;
    }

    /**
     * !!! Method needs synchronization for instance members if called!!!
     */
    private void traceCacheRemoval(long oldEntryCount, long oldCacheSize, String reason) {
        if (ServerManager.isLogTrace()) {
            var curEntryCount = orderedContainer.size();

            if ((oldEntryCount != curEntryCount) || (oldCacheSize != statistics.getLocalCachePersistentSize())) {
                ServerManager.logTrace("DC cache needed cleanup",
                        new LogData("local cur/old CacheEntries", curEntryCount + " / " + oldEntryCount),
                        new LogData("local cur/old CacheSize", getMiBSize(statistics.getLocalCachePersistentSize()) + "MiB / " + getMiBSize(oldCacheSize) + "MiB"),
                        new LogData("local cleanup reason", ((null != reason) && !reason.isEmpty()) ? reason : DocumentConverterUtil.STR_NOT_AVAILABLE));
            }
        }
    }

    public long getLocalOldestEntrySeconds() {
        var oldestEntry = orderedContainer.getFirstEntry();

        if (null != oldestEntry) {
            return (System.currentTimeMillis() - oldestEntry.getLastAccessTimeMillis()) / 1000L;
        }

        return 0;
    }

    public void shutdown() {
        if (cacheIsRunning.compareAndSet(true, false)) {
            enabledFlag.set(false);

            m_cacheWorker.shutdown();

            while (m_cacheWorker.isAlive()) {
                try {
                    m_cacheWorker.join();
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok
                }
            }

            // clear all new entries that were currently created
            synchronized (newEntryDeque) {
                newEntryDeque.clear();
            }

            orderedContainer.clear();

            // delete cache directory, if it is the fallback one
            var cacheDir = getHashFileMapper().getRootDir();

            if (usingFallbackCacheDir) {
                ServerManager.logInfo(getCacheStrBuilder("removing temporary local fallback cache directory: ").append(cacheDir.getAbsolutePath()).toString());

                try {
                    FileUtils.forceDelete(cacheDir);
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }
}
