/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import java.util.Iterator;
import java.util.LinkedHashMap;

import static com.openexchange.documentconverter.server.cache.Cache.CACHE_DEFAULT_COLLECTION_CAPACITY;

// TODO NEXT Clarify why not hash as key, but CacheEntryReference?
// FIXME Use ReadWriteLock instead of synchronized? Read a magnitude often than write?
class OrderedHashContainer {

    private final LinkedHashMap<CacheEntryReference, CacheEntryReference> orderedMap = new LinkedHashMap<>(CACHE_DEFAULT_COLLECTION_CAPACITY, 0.99F);

    private final Object sync = new Object();

    void add(CacheEntryReference cacheEntryReference) {
        if (cacheEntryReference != null) {
            synchronized (sync) {
                // TODO NEXT why not use the hash as key? cacheEntryReference.getHash()
                orderedMap.put(cacheEntryReference, cacheEntryReference);
            }
        }
    }

    public void clear() {
        synchronized (sync) {
            orderedMap.clear();
        }
    }

    boolean contains(String hash) {
        if (hash != null) {
            synchronized (sync) {
                return orderedMap.containsKey(new CacheEntryReference(hash));
            }
        }

        return false;
    }

    CacheEntryReference get(String hash) {
        if (hash != null) {
            synchronized (sync) {
                return orderedMap.get(new CacheEntryReference(hash));
            }
        }

        return null;
    }

    CacheEntryReference getFirstEntry() {
        synchronized (sync) {
            var iterator = orderedMap.keySet().iterator();

            return iterator.hasNext() ? iterator.next() : null;
        }
    }

    Iterator<CacheEntryReference> iterator() {
        synchronized (sync) {
            return orderedMap.keySet().iterator();
        }
    }

    CacheEntryReference remove(final String hash) {
        if (hash != null) {
            synchronized (sync) {
                return orderedMap.remove(new CacheEntryReference(hash));
            }
        }
        return null;
    }

    public long size() {
        synchronized (sync) {
            return orderedMap.size();
        }
    }
}
