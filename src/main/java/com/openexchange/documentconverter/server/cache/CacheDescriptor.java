/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.openexchange.documentconverter.server.config.ServerConfig;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;

// TODO NEXT Complete
@Component
@Getter
public class CacheDescriptor {

    @Autowired
    @Getter(AccessLevel.NONE)
    private ServerConfig serverConfig;

    /**
     * the path to the directory of the cache; if this directory does't exist, it will be created; the directory shouldn't contain any other
     * objects, since it is up to the implementation to remove this directory or change it in any way; the cache implementation is the owner
     * of this directory after successful initialization; the path to this directory and all contained files needs to have write permission
     * for the account of the user who owns the process
     */
    public String cacheDirectory = null;

    /**
     * The depth of target hash entry directories below the cache root directory,
     * with the maximum depth internally limited to 4
     */
    public int cacheDirectoryDepth = 0;

    /**
     * the lower limit for the size of the volume that should not be occupied by the cache
     */
    public long minimumFreeVolumeSize = 0;

    /**
     * the upper limit for the size of all persistent files within the cache directory; -1 for no upper limit
     */
    public long maximumPersistSize = -1;

    /**
     * the maximum number of objects to be cached at all; -1 for no upper limit
     */
    public long maximumEntryCount = -1;

    /**
     * the upper threshold of cache entry count percentage to start cleanup
     */
    public long cleanupThresholdUpperPercentage = 100;

    /**
     * the lower threshold of cache entry count percentage to finish cleanup
     */
    public long cleanupThresholdLowerPercentage = 100;


    /**
     * the timeout in milliseconds for the cached objects to be removed; 0 to disable the timeout based removal of cached objects
     */
    public long timeoutMillis = 2592000000L;

    /**
     * the period in milliseconds for the cache to perform a cleanup
     */
    public long cleanupPeriodMillis = 300000;

    /**
     * the period in milliseconds for the which the cache tries to reestablish a connection to remote cache server
     */
    public long recoverPeriodMillis = 20000;

    /**
     * the CacheService Url
     */
    public String cacheServiceUrl = null;

    /**
     * sslContext The SSLContext to be used for client => server requests of arbitrary type
     */
    public SSLContext sslContext = null;

    public CacheDescriptor(final ServerConfig serverConfig) {
        implInit(serverConfig);
    }

    @PostConstruct
    public void init() {
        implInit(this.serverConfig);
    }

    private void implInit(final ServerConfig serverConfig) {
        if (null != serverConfig) {
            cacheDirectory = serverConfig.getCacheDir();
            cacheDirectoryDepth = serverConfig.getCacheDirDepth();
            minimumFreeVolumeSize = serverConfig.getMinFreeVolumeSizeMB() * 1024 * 1024;
            cleanupThresholdUpperPercentage = serverConfig.getCacheCleanupThresholdUpperPercentage();
            cleanupThresholdLowerPercentage = serverConfig.getCacheCleanupThresholdLowerPercentage();
            recoverPeriodMillis = serverConfig.getUpRecoveryPeriodSeconds() * 1000;
            cacheServiceUrl = serverConfig.getCacheServiceUrl();
            sslContext = serverConfig.getSSL_CONTEXT();

            // following properties are used when group registering at the CacheService:
            maximumPersistSize = (serverConfig.getMaxCacheSizeMB() > 0) ? serverConfig.getMaxCacheSizeMB() * 1024 * 1024 : serverConfig.getMaxCacheSizeMB();
            maximumEntryCount = serverConfig.getMaxCacheEntries();
            timeoutMillis = serverConfig.getCacheEntryTimeoutSeconds() * 1000;
            cleanupPeriodMillis = serverConfig.getCacheCleanupPeriodSeconds() * 1000;
        }
    }
}
