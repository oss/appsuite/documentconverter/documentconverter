/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.cache;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.logging.LogData;
import com.openexchange.documentconverter.server.ServerManager;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.mutable.MutableLong;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.concurrent.atomic.AtomicBoolean;

// TODO NEXT check if cache worker is really needed for new documentconverter-next
class CacheWorker extends Thread {

    private final AtomicBoolean cacheWorkerRunning = new AtomicBoolean(true);

    private final Cache cache;

    CacheWorker(Cache cache) {
        super("DC Cache worker");
        this.cache = cache;
        // TODO NEXT remove comment if the CacheWorker is needed. Without the comment the tests are failing because the CacheWorker remove CacheEntries from the filesystem
//        start();
    }

    public static CacheWorker createCacheWorker(Cache cache) {
        return new CacheWorker(cache);
    }
    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        final boolean trace = ServerManager.isLogTrace();

        if (trace) {
            ServerManager.logTrace("DC cache worker started");
        }

        // read persistent cache entries and fill runtime cache
        implFillRuntimeCache();

        // start the cache worker loop
        implRunWorker();

        // worker thread inished
        if (trace) {
            ServerManager.logTrace("DC cache worker finished");
        }
    }

    /**
     *
     */
    protected void shutdown() {
        if (cacheWorkerRunning.compareAndSet(true, false)) {
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC cache worker started finishing");
            }

            interrupt();
        }
    }

    // - Implementation ---------------------------------------------------------

    private void implFillRuntimeCache() {
        // read persistent cache entries and fill runtime cache
        var runtime = Runtime.getRuntime();

        implGC();

        var VMMemMaxAvailableBytes = runtime.maxMemory();
        var VMMemUsedBeforeBytes = runtime.totalMemory() - runtime.freeMemory();

        try {
            var startFillMillis = System.currentTimeMillis();
            var trace = ServerManager.isLogTrace();

            // read all available persistent entries in a smart way
            if (trace) {
                ServerManager.logTrace(Cache.getCacheStrBuilder("worker finding, validating and adding local persistent entries to runtime cache...").toString());
            }

            // read valid CacheHashEntries into the runtime cache
            implReadCacheEntryReferences();

            implGC();

            var fillDurationMillis = System.currentTimeMillis() - startFillMillis;
            var fillDurationSeconds = fillDurationMillis / 1000L;
            var addedEntryCount = cache.getLocalEntryCount();
            var fillLogData = new LogData[] {
                    new LogData("local added entry count", Long.toString(addedEntryCount)),
                    new LogData("local add duration", fillDurationSeconds + "s"),
                    new LogData("local add throughput", Cache.getThroughputPerSecond(addedEntryCount, fillDurationMillis) + "/s")
            };

            // log initial cache overview after adding all entries
            if (trace) {
                ServerManager.logTrace("DC cache worker added valid entries to local runtime cache", fillLogData);
            }

            // log runtime memory metrics after adding all entries
            var VMMemCurAvailableBytes = runtime.totalMemory();
            var VMMemUsedAfterBytes = VMMemCurAvailableBytes - runtime.freeMemory();

            ServerManager.logInfo("DC runtime memory overview", new LogData[] {
                    new LogData("max available", Cache.getMiBSize(VMMemMaxAvailableBytes) + Cache.STR_MIB),
                    new LogData("cur available", Cache.getMiBSize(VMMemCurAvailableBytes) + Cache.STR_MIB),
                    new LogData("initial use", Cache.getMiBSize(VMMemUsedBeforeBytes) + Cache.STR_MIB),
                    new LogData("current use", Cache.getMiBSize(VMMemUsedAfterBytes) + Cache.STR_MIB)
            });

            cache.logCacheGeometry("DC cache local memory overview", fillDurationMillis, false);

            cache.cleanup();
        } catch (Exception e) {
            ServerManager.logExcp(e);
        } finally {
            // enable cache
            cache.enable();
        }
    }

    // TODO NEXT why? Ask Kai
    private void implGC() {
        var runtime = Runtime.getRuntime();

        runtime.runFinalization();
        runtime.gc();
    }

    /**
     *
     */
    private void implReadCacheEntryReferences() {
        var hashFileMapper = cache.getHashFileMapper();

        if (null == hashFileMapper) {
            return;
        }

        // if cacheDir is set, it has already been checked, that it is a readable directory
        var curTimeMillis = System.currentTimeMillis();
        var timeoutMillis = cache.localCacheEntryTimeoutMillis;
        var cacheRootDir = hashFileMapper.getRootDir();
        var trace = ServerManager.isLogTrace();
        CacheEntryReference[] cacheEntryReferenceArray = null;

        {
            // use own scope around foundFileList to destroy intermediate cacheEntryReference
            // list immediately after obtaining the cacheEntryReferenceArray from it
            var cacheEntryReferenceList = new ArrayList<>(Cache.CACHE_DEFAULT_COLLECTION_CAPACITY);
            var foundEntryCount = new MutableLong(0);

            try {
                Files.walkFileTree(cacheRootDir.toPath(), EnumSet.of(FileVisitOption.FOLLOW_LINKS), cache.getHashFileMapper().getDepth() + 1, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path curPath, BasicFileAttributes curPathAttr) throws IOException {
                        var ret = super.visitFile(curPath, curPathAttr);

                        if (curPathAttr.isDirectory()) {
                            var curDirectoryName = curPath.getFileName().toString();
                            var curCacheEntryReference = new CacheEntryReference(hashFileMapper, curDirectoryName, curPathAttr.lastModifiedTime().toMillis());

                            // add entry if timestamp is valid, else remove persistent entry completely
                            if (CacheEntry.isValid(hashFileMapper, curDirectoryName) && curCacheEntryReference.isValidTimestamp(curTimeMillis, timeoutMillis)) {
                                cacheEntryReferenceList.add(curCacheEntryReference);
                            } else {
                                curCacheEntryReference.clear(hashFileMapper);
                            }

                            if (trace && ((foundEntryCount.incrementAndGet() % 100000) == 0)) {
                                ServerManager.logTrace(cache.getCacheStrBuilder("found ").
                                        append(foundEntryCount.getValue()).
                                        append(" entries to add to cache. Continuing search...").toString());
                            }
                        }
                        return ret;
                    }
                });
            } catch (IOException e) {
                ServerManager.logExcp(new Exception("DC cache worker excepted while reading local cache entries: " + Throwables.getRootCause(e)));
            }

            cacheEntryReferenceArray = cacheEntryReferenceList.toArray(new CacheEntryReference[cacheEntryReferenceList.size()]);
        }

        // log and return if no entries were foundmaxLocalCacheSizeCleanupStart
        if (ArrayUtils.isEmpty(cacheEntryReferenceArray)) {
            if (trace) {
                ServerManager.logTrace(Cache.getCacheStrBuilder("did not find any local entries to add to cache: ").append(hashFileMapper.getRootDir().getAbsolutePath()).toString());
            }

            return;
        }

        // process each found entry by checking for valid entry and adding to cache container
        var foundCount = cacheEntryReferenceArray.length;

        if (trace) {
            ServerManager.logTrace(Cache.getCacheStrBuilder("found ").append(foundCount).append(" local entries. Sorting...").toString());
        }

        // sort found cacheEntryReferenceArray in ascending order (based on timestamp)
        Arrays.sort(cacheEntryReferenceArray);

        for (int processed = 0, startPos = (((cache.maxLocalCacheCount > 0) && (cache.maxLocalCacheCount < foundCount)) ? (int) (foundCount - cache.maxLocalCacheCount) : 0); processed < foundCount;) {
            var curCacheEntryReference = cacheEntryReferenceArray[processed];

            if (processed >= startPos) {
                cache.addCacheEntryRefToCache(curCacheEntryReference);
            } else {
                // clear entry in case of:
                // - outside of number of m_maxLocalCacheCount
                // - persistent entry invalid
                curCacheEntryReference.clear(hashFileMapper);
            }

            ++processed;

            // finally increment processed count and log every 100.000 processed entries
            if (trace && ((processed % 100000) == 0)) {
                ServerManager.logTrace(cache.getCacheStrBuilder("processed ").
                        append(processed).append(" / ").append(foundCount).
                        append(" local entries (").
                        append(String.format ("%.2f", processed * 100.0 / foundCount)).
                        append("%).").toString());
            }
        }
    }

    /**
     *
     */
    void implRunWorker() {
        var lastCleanupTimestampMillis = System.currentTimeMillis();

        while (cacheWorkerRunning.get()) {
            // cleanup cache every CACHE_CLEANUP_PERIOD_MILLIS
            var curTimestampMillis = System.currentTimeMillis();

            if (cache.isEnabled()) {
                if ((curTimestampMillis - lastCleanupTimestampMillis) >= Cache.CACHE_CLEANUP_PERIOD_MILLIS) {
                    cache.cleanup();
                    lastCleanupTimestampMillis = curTimestampMillis = System.currentTimeMillis();
                }
            } else {
                lastCleanupTimestampMillis = curTimestampMillis;
            }

            // sleep thread until next cache cleanup should happen

            ServerManager.sleepThread(Math.max(1L, (lastCleanupTimestampMillis + Cache.CACHE_CLEANUP_PERIOD_MILLIS) - curTimestampMillis), false);
        }
    }

}
