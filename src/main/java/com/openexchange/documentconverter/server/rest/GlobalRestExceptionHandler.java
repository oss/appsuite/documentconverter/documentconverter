package com.openexchange.documentconverter.server.rest;

import com.openexchange.documentconverter.server.error.ErrorCode;
import com.openexchange.documentconverter.server.rest.model.ErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.*;
import org.springframework.validation.FieldError;
import org.springframework.validation.method.ParameterValidationResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.HandlerMethodValidationException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;

@Slf4j
@RequiredArgsConstructor
@ControllerAdvice("com.openexchange.documentconverter.server.rest")
public class GlobalRestExceptionHandler  extends ResponseEntityExceptionHandler {

    // If, for example, a query parameter is Boolean and the query value is not Boolean
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        var error = new ErrorResponse();
        error.setErrorCode(ErrorCode.ERROR_INVALID_PARAM.ordinal());
        error.setErrorMsg("Failed to convert parameter");
        error.setDetails(ex.getPropertyName());
        return new ResponseEntity<>(new ErrorResponse(ErrorCode.ERROR_INVALID_PARAM.ordinal(), "Failed to convert parameter", ex.getPropertyName()), HttpStatus.BAD_REQUEST);
    }

    // If, for example, a request parameter @Pattern is not valid
    protected ResponseEntity<Object> handleHandlerMethodValidationException(HandlerMethodValidationException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        try {
            var validationResults = ex.getValueResults();
            var details = new ArrayList<String>();
            for (ParameterValidationResult validationResult : validationResults) {
                var methodParameter = validationResult.getMethodParameter();
                if (methodParameter != null) {
                    details.add(methodParameter.getParameterName());
                }
            }
            if (details.isEmpty()) {
                return this.handleExceptionInternal(ex, (Object)null, headers, status, request);
            }
            return new ResponseEntity<>(new ErrorResponse(ErrorCode.ERROR_INVALID_PARAM.ordinal(), "Failed to validate parameter", details), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return this.handleExceptionInternal(ex, (Object)null, headers, status, request);
        }
    }

    // If, for example, a request object with @Valid is not valid
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        try {
            var errors = ex.getBindingResult().getFieldErrors();
            var details = new ArrayList<String>();
            for (FieldError error : errors) {
               details.add(error.getField());
            }
            if (details.isEmpty()) {
                return this.handleExceptionInternal(ex, (Object)null, headers, status, request);
            }
            return new ResponseEntity<>(new ErrorResponse(ErrorCode.ERROR_INVALID_PARAM.ordinal(), "Failed to validate parameter", details), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return this.handleExceptionInternal(ex, (Object)null, headers, status, request);
        }
    }
}
