/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.rest.service;

import com.google.common.base.Throwables;
import com.openexchange.documentconverter.server.CacheServiceClient;
import com.openexchange.documentconverter.server.DocumentConverterApplication;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.async.AsyncServerExecutor;
import com.openexchange.documentconverter.server.cache.Cache;
import com.openexchange.documentconverter.server.config.ServerConfig;
import com.openexchange.documentconverter.server.converter.ManagedJobFactory;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.executor.*;
import com.openexchange.documentconverter.server.job.JobProcessor;
import com.openexchange.documentconverter.server.metrics.MetricsRegistry;
import com.openexchange.documentconverter.server.rest.model.*;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

import static com.openexchange.documentconverter.server.executor.Executor.FORMAT_FILE;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Slf4j
@Service
public class DocumentConverterService {

    @Autowired
    private ServerManager serverManager;

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private JobProcessor jobProcessor;

    @Autowired
    private Cache cache;

    @Autowired
    private ManagedJobFactory managedJobFactory;

    @Autowired
    private AsyncServerExecutor asyncServerExecutor;

    public @NonNull ServerStatusModel getServerStatus() {
        var serverStatus = new ServerStatusModel();
        serverStatus.setId(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_ID);
        serverStatus.setName(DocumentConverterApplication.DOCUMENTCONVERTER_NAME);
        serverStatus.setApi(DocumentConverterApplication.DOCUMENTCONVERTER_SERVER_API_VERSION);
        serverStatus.setStatus(serverManager.isRunning() ? "running" : "terminated");

        var engineStatusMap = jobProcessor.getEngineStatus();
        for (var backendType : engineStatusMap.keySet()) {
            var enginStatus = engineStatusMap.get(backendType);
            serverStatus.putEngineStatus(backendType.toString(),
                    new EngineStatusModel(enginStatus.totalCount(), enginStatus.runningCount())
            );

        }
        final CacheServiceClient cacheServiceClient = serverManager.getCacheServiceClient();
        if (cacheServiceClient != null) {
            serverStatus.setCacheServiceAvailable(cacheServiceClient.isCacheServerAvailable());
            serverStatus.setCacheServiceUrl(cacheServiceClient.getCacheServiceURL());
        }
        serverStatus.setErrorMessages(serverManager.getServerErrors());
        return serverStatus;
    }

    public @NonNull Map<String, Number> getMetrics() {
        var metrics = new TreeMap<String, Number>();

        for (var meter : Metrics.globalRegistry.getMeters()) {
            if (meter instanceof Gauge gauge) {
                var gaugeName = gauge.getId().getName();
                if (StringUtils.startsWith(gaugeName, MetricsRegistry.NAME_PREFIX)) {
                    StringBuilder gaugeNameBuilder = new StringBuilder(gaugeName.replace(MetricsRegistry.NAME_PREFIX, ""));

                    var isRatio = gaugeNameBuilder.toString().contains("ratio");

                    for (var tag : gauge.getId().getTags()) {
                        if (!"type".equals(tag.getKey())) {
                            gaugeNameBuilder.append(".").append(tag.getKey()).append(".").append(tag.getValue());
                        }
                    }

                    if (isRatio) {
                        metrics.put(gaugeNameBuilder.toString(), gauge.value());
                    } else {
                        metrics.put(gaugeNameBuilder.toString(), Double.valueOf(gauge.value()).longValue());
                    }
                }

            }
        }

        return metrics;
    }

    public @NonNull RConvertResult rconvert(RConvertRequestModel rConvertRequestModel) {

        var jobExecutor = new JobExecutor(serverManager, serverConfig, cache, rConvertRequestModel, managedJobFactory, asyncServerExecutor);
        var resultProperties = jobExecutor.execute();

        return getRConvertResult(resultProperties, rConvertRequestModel.getReturnType());
    }

    protected @NonNull RConvertResult getRConvertResult(Map<String, Object> resultProperties, String returnType) {

        var result = new RConvertResult();
        result.setResultProperties(resultProperties);
        try {

            var asyncConversion = MapUtils.getBooleanValue(resultProperties, Properties.PROP_RESULT_ASYNC, false);

            var resultBuffer = (byte[]) MapUtils.getObject(resultProperties, Properties.PROP_RESULT_BUFFER);

            if ("json".equals(returnType)) {
                if (asyncConversion) {
                    result.setJSONAsyncResult(getDefaultJSONResponse(true, resultProperties));
                } else {
                    result.setJSONSynchronResult(getSynchronJSONResponse(resultBuffer, resultProperties));
                }
            } else if (FORMAT_FILE.equals(returnType) && resultBuffer != null) {
                // TODO NEXT Why is there only a resultBuffer check here? For Json is no check!
                // What if the return type is "file" and async? Is then the resultBuffer null? If so why not check for !asyncConversion, because if the resultBuffer for sync conversation is null  it seems there are problems
                result.setFileResult(getFileResponse(resultBuffer, resultProperties));
            } else if (asyncConversion) {
                result.setDone(true);
            }

        } catch (OutOfMemoryError e) {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, JobError.OUT_OF_MEMORY.getErrorCode());

            ServerManager.logError("DC webservice has no memory left to send response for the last request: " +
                    Throwables.getRootCause(e));
        } catch (Exception e) {
            ServerManager.logExcp(e);
        }

        return result;
    }

    protected @NonNull RConvertJSONAsyncResponseModel getDefaultJSONResponse(boolean asyncConversion, @NonNull Map<String, Object> resultProperties) {
        var responseModel = new RConvertJSONAsyncResponseModel();

        var jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);

        responseModel.setAsync(asyncConversion);
        responseModel.setErrorCode(jobErrorEx.getErrorCode());
        responseModel.setErrorData(jobErrorEx.getErrorData().toString());

        return responseModel;
    }

    protected @NonNull RConvertJSONSynchronResponseModel getSynchronJSONResponse(byte[] resultBuffer, @NonNull Map<String, Object> resultProperties) {
        // get default json result as base
        var responseModel = new RConvertJSONSynchronResponseModel(getDefaultJSONResponse(false, resultProperties));

        // TODO NEXT REMOVE THAT its only a fix for missing mimeType
        var mimeType = MapUtils.getString(resultProperties, Properties.PROP_RESULT_MIME_TYPE, "application/pdf");
        // TODO NEXT REMOVE THAT its only a fix for missing extension
        var extension = MapUtils.getString(resultProperties, Properties.PROP_RESULT_EXTENSION, "pdf");
        var zipArchive = MapUtils.getBooleanValue(resultProperties, Properties.PROP_RESULT_ZIP_ARCHIVE, false);

        var jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
        var locale = MapUtils.getString(resultProperties, Properties.PROP_RESULT_LOCALE);
        var jobIDStr = MapUtils.getString(resultProperties, Properties.PROP_RESULT_JOBID);
        var pageJobType = MapUtils.getString(resultProperties, Properties.PROP_RESULT_SINGLE_PAGE_JOBTYPE);
        var pageCount = MapUtils.getInteger(resultProperties, Properties.PROP_RESULT_PAGE_COUNT);
        var originalPageCount = MapUtils.getInteger(resultProperties, Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT);
        var pageNumber = MapUtils.getInteger(resultProperties, Properties.PROP_RESULT_PAGE_NUMBER);
        var shapeNumber = MapUtils.getInteger(resultProperties, Properties.PROP_RESULT_SHAPE_NUMBER);

        if (isNotEmpty(locale)) {
            responseModel.setLocale(locale);
        }

        if (isNotEmpty(jobIDStr)) {
            responseModel.setJobId(jobIDStr);
        }

        if (isNotEmpty(pageJobType)) {
            responseModel.setPageJobType(pageJobType);
        }

        if ((null != pageCount) && (pageCount > 0)) {
            responseModel.setPageCount(pageCount);
        }

        if ((null != originalPageCount) && (originalPageCount > 0)) {
            responseModel.setOriginalPageCount(originalPageCount);
        }

        if ((null != pageNumber) && (pageNumber > 0)) {
            responseModel.setPageNumber(pageNumber);
        }

        if ((null != shapeNumber) && (shapeNumber > 0)) {
            responseModel.setShapeNumber(shapeNumber);
        }

        if (isNotEmpty(mimeType)) {
            responseModel.setMimeType(mimeType);
        }

        if (isNotEmpty(extension)) {
            responseModel.setExtension(extension);
        }

        switch (jobErrorEx.getJobError()) {
            case PASSWORD:
                responseModel.setPasswordProtected(true);
                break;
            case MAX_SOURCESIZE:
                responseModel.setMaxSourceSize(true);
                break;
            case UNSUPPORTED:
                responseModel.setUnsupported(true);
                break;
        }

        if (resultBuffer != null) {
            var base64Result = Base64.encodeBase64String(resultBuffer);
            // TODO NEXT can base64Result be null??
            if (base64Result != null) {
                JobExecutor.implHandleResponseSize(
                        "DC webservice returns JSON response with base64 encoded result string",
                        base64Result.length());

                responseModel.setResult("data:" +
                        (zipArchive ? "application/zip" : mimeType) +
                        ";base64," +
                        base64Result);
            }
        }

        return responseModel;
    }

    protected @NonNull FileResult getFileResponse(byte[] resultBuffer, @NonNull Map<String, Object> resultProperties) {
        // TODO NEXT REMOVE THAT its only a fix for missing mimeType
        var mimeType = MapUtils.getString(resultProperties, Properties.PROP_RESULT_MIME_TYPE, "application/pdf");
        // TODO NEXT REMOVE THAT its only a fix for missing extension
        var extension = MapUtils.getString(resultProperties, Properties.PROP_RESULT_EXTENSION, "pdf");
        var zipArchive = MapUtils.getBooleanValue(resultProperties, Properties.PROP_RESULT_ZIP_ARCHIVE, false);

        var fileResponse = new FileResult();
        fileResponse.setZip(zipArchive);
        fileResponse.setFilename("result");
        fileResponse.setMimeType(mimeType);
        fileResponse.setFile(resultBuffer);
        fileResponse.setExtension(extension);

        JobExecutor.implHandleResponseSize("DC webservice returns file response with file size", resultBuffer.length);

        return fileResponse;
    }
}
