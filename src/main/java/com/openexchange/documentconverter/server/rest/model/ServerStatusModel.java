package com.openexchange.documentconverter.server.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ServerStatusModel {

    @JsonProperty
    private String id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String api;

    @JsonProperty
    private String status;

    @JsonProperty
    private boolean cacheServiceAvailable = false;

    @JsonProperty
    private String cacheServiceUrl = null;

    @JsonProperty
    private Map<String, EngineStatusModel> engineStatus = new HashMap<>();

    @JsonProperty
    private List<String> errorMessages;

    public void putEngineStatus(@NonNull String engine, @NonNull EngineStatusModel status) {
        engineStatus.put(engine, status);
    }

}
