package com.openexchange.documentconverter.server.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EngineStatusModel {

    @JsonProperty
    private int totalCount;
    @JsonProperty
    private int runningCount;

    public EngineStatusModel(){}

    public EngineStatusModel(int totalCount, int runningCount) {
        this.totalCount = totalCount;
        this.runningCount = runningCount;
    }
}
