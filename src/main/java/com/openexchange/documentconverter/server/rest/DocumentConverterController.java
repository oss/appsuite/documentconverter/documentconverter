/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.rest;

import com.openexchange.documentconverter.server.DocumentConverterUtil;
import com.openexchange.documentconverter.server.Properties;
import com.openexchange.documentconverter.server.ServerManager;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.rest.model.ErrorResponse;
import com.openexchange.documentconverter.server.rest.model.RConvertRequestModel;
import com.openexchange.documentconverter.server.rest.model.StatusResponse;
import com.openexchange.documentconverter.server.rest.service.DocumentConverterService;
import com.openexchange.documentconverter.server.rest.service.FileResult;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/documentconverterws")
public class DocumentConverterController {

    // !!! Do not change string for compatibility reasons (KA, 2014-11-20) !!!
    protected static final String OX_DOCUMENTCONVERTER_TITLE_FIXED = "Open-Xchange DC";
    protected static final String DOCUMENTCONVERTER_SERVER_API_VERSION = "8";
    protected static final String OX_ERRORTEXT_GENERAL_ERROR = "General error";
    protected static final String[] OX_DEFAULT_RESPONSE_TEXT = { "WebService is running...", "Error Code: 0" };
    protected static final char LINE_SEPARATOR = '\n';

    @Autowired
    private DocumentConverterService documentConverterService;

    // API for rconvert
    public @PostMapping(
            consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE }
    ) ResponseEntity<Object> process(@Valid @ModelAttribute RConvertRequestModel processRequestModel) {

        var convertResult = documentConverterService.rconvert(processRequestModel);
        var errorCode = MapUtils.getInteger(convertResult.getResultProperties(), Properties.PROP_RESULT_ERROR_CODE);

        try {
            if (convertResult.isDone()) {
                // Default response for (binary) asynchronous conversion
                ResponseEntity<Object> responseEntity = new ResponseEntity<>(HttpStatus.OK);

                // JSON asynchronous response
                if (convertResult.getJsonAsyncResult() != null) {
                    responseEntity = new ResponseEntity<>(convertResult.getJsonAsyncResult(), HttpStatus.OK);
                }
                // JSON response
                else if (convertResult.getJsonSynchronResult() != null) {
                    responseEntity = new ResponseEntity<>(convertResult.getJsonSynchronResult(), HttpStatus.OK);
                }
                // File response
                else if (convertResult.getFileResult() != null) {
                    responseEntity = getFileResponse(convertResult.getFileResult());
                }
                return responseEntity;
            }
        } catch (Exception e) {
            ServerManager.logExcp(e);
            errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        }
        return getErrorResponse(errorCode, convertResult.getResultProperties());
    }

    public @GetMapping(
        value = "",
        produces = MediaType.TEXT_HTML_VALUE
    )
    String root(@RequestParam(value = "metrics", required = false) Boolean metrics) {

        ServerManager.logTrace("DC 'documentconverterws' was requested");

        return getResponsePage(OX_DEFAULT_RESPONSE_TEXT, (null != metrics) ? metrics : false);
    }

    public @GetMapping(
            value = "/status",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    ResponseEntity<Object> status(@RequestParam(value = "metrics", required = false) Boolean metrics) {

        ServerManager.logTrace("DC 'documentconverterws/status' was requested");

        var status = new StatusResponse();
        status.setServerStatus(documentConverterService.getServerStatus());

        if (BooleanUtils.toBooleanDefaultIfNull(metrics, false)) {
            status.setMetrics(documentConverterService.getMetrics());
        }

        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    protected ResponseEntity<Object> getFileResponse(@NonNull FileResult fileResult) {

        var mimeType = MimeType.valueOf(fileResult.isZip() ? "application/zip" : fileResult.getMimeType());
        var filename = fileResult.getFilename() + (fileResult.isZip() ? ".zip" : "." + fileResult.getExtension());

        return ResponseEntity.ok()
                .contentLength(fileResult.getFile().length)
                .contentType(new MediaType(mimeType))
                .header("Content-Disposition", "attachment; filename=" + filename)
                .body(fileResult.getFile());

    }

    protected ResponseEntity<Object> getErrorResponse(Integer errorCode, Map<String, Object> resultProperties) {

        HttpStatus httpStatus = null;
        String httpErrorMsg = null;

        if (errorCode == null) {
            errorCode = HttpStatus.BAD_REQUEST.value();
        }

        if (JobError.MAX_QUEUE_COUNT.equalsErrorCode(errorCode)) {
            httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
        } else if (JobError.QUEUE_TIMEOUT.equalsErrorCode(errorCode)) {
            httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
        } else {
            if (HttpStatus.BAD_REQUEST.value() == errorCode) {
                httpStatus = HttpStatus.BAD_REQUEST;
            } else {
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }
        return new ResponseEntity<>(new ErrorResponse(errorCode, JobErrorEx.fromResultProperties(resultProperties).getErrorData()), httpStatus);
    }

    protected String getResponsePage(@NonNull String[] defaultText, boolean metrics) {

        var responsePage = new StringBuilder();

        responsePage.append("<html>").append(LINE_SEPARATOR)
                .append("<head>").append(LINE_SEPARATOR)
                .append("<meta charset=\"UTF-8\">").append(LINE_SEPARATOR)
                .append("<title>" + OX_DOCUMENTCONVERTER_TITLE_FIXED + "</title>").append(LINE_SEPARATOR)
                .append("</head>").append(LINE_SEPARATOR)
                .append("<body>").append(LINE_SEPARATOR)
                .append("<h1 align=\"center\">OX Software GmbH DC</h1>").append(LINE_SEPARATOR);

        for (var text : defaultText) {
            responsePage.append("<p>")
                    .append(text)
                    .append("</p>")
                    .append(LINE_SEPARATOR);
        }

        // print server id
        responsePage.append("<p>Id: ").append(DocumentConverterUtil.DOCUMENTCONVERTER_SERVER_ID).append("</p>").append(LINE_SEPARATOR);

        // print server name for compatibility reasons (old clients might watch for this name)
        responsePage.append("<p>Name: documentconverter</p>").append(LINE_SEPARATOR);

        // print server API version
        responsePage.append("<p>API: v").append(DOCUMENTCONVERTER_SERVER_API_VERSION).append("</p>").append(LINE_SEPARATOR);

        if (metrics) {
            responsePage.append("<br />").append(LINE_SEPARATOR).append("<p><u>Metrics</u></p>").append(LINE_SEPARATOR);

            for (var metric : documentConverterService.getMetrics().entrySet()) {
                responsePage.append("<p>").append(metric.getKey()).append(": ").append(metric.getValue()).append("</p>").append(LINE_SEPARATOR);
            }
        }

        responsePage.append("</body></html>");

        return responsePage.toString();
    }
}
