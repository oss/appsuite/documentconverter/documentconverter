package com.openexchange.documentconverter.server.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import static com.openexchange.documentconverter.server.executor.Executor.MAX_FORM_DATA_PART_COUNT;

@Getter
@Setter
public class RConvertRequestModel {

    @Pattern(regexp = "^(convert)$")
    private String method = "convert";

    @Pattern(regexp = "^(pdf|pdfa|shape2png|graphic|odf|ooxml)$")
    private String jobType = "pdf";

    private String jobId;

    @Pattern(regexp = "^(background|low|medium|high|instant)$")
    private String priority = "background";

    // TODO NEXT add local validator
    private String locale;

    private String filterShortName;

    private String inputType;

    private String inputUrl;

    private Integer pixelWidth;

    private Integer pixelHeight;

    private String mimeType;

    private String pageRange;

    private Integer pageNumber;

    private Integer shapeNumber;

    private Boolean zipArchive = false;

    private String infoFileName;

    private Integer featuresId;

    private Boolean noCache = false;

    @NotBlank
    private String cacheHash;

    private Boolean cacheOnly = false;

    private Boolean async = false;

    private Integer imageResolution;

    @Pattern(regexp = "^(contain|cover)$")
    private String imageScaleType = "contain";

    private String remoteUrl;

    private String url;

    @Pattern(regexp = "^(json|file)$")
    private String returnType = "file";

    private Boolean autoScale = false;

    private Boolean hideChanges = false;

    private Boolean hideComments = false;

    private Boolean userRequest = false;

    private String shapeReplacements;

    // Test if more than one sourcefile is in the request
    // camel case is not allowed
    private MultipartFile sourcefile;
}
