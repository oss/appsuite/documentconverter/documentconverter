/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.rest.service;

import com.openexchange.documentconverter.server.rest.model.RConvertJSONAsyncResponseModel;
import com.openexchange.documentconverter.server.rest.model.RConvertJSONSynchronResponseModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter

public class RConvertResult {

    @Setter
    private boolean done = false;

    private RConvertJSONAsyncResponseModel jsonAsyncResult;

    private RConvertJSONSynchronResponseModel jsonSynchronResult;

    private FileResult fileResult;

    @Setter
    private Map<String, Object> resultProperties;

    public void setJSONAsyncResult(RConvertJSONAsyncResponseModel responseModel) {
        jsonAsyncResult = responseModel;
        done = true;
    }

    public void setJSONSynchronResult(RConvertJSONSynchronResponseModel responseModel) {
        jsonSynchronResult = responseModel;
        done = true;
    }

    public void setFileResult(FileResult fileResult) {
        this.fileResult = fileResult;
        done = true;
    }
}
