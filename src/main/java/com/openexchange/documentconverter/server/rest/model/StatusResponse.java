package com.openexchange.documentconverter.server.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
@Getter
@Setter
public class StatusResponse {

    @JsonProperty("server")
    private ServerStatusModel serverStatus;

    @JsonProperty
    private Map<String, Number> metrics;
}
