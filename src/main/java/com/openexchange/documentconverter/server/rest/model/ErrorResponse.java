package com.openexchange.documentconverter.server.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

@Getter
@Setter
public class ErrorResponse {

    public ErrorResponse() {
        //
    }

    public ErrorResponse(Integer errorCode, String errorMsg, Object details) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.details = details;
    }

    public ErrorResponse(Integer errorCode, JSONObject errorData) {
        this.errorCode = errorCode;
        this.errorData = errorData.toString();
    }

    @JsonProperty
    private Integer errorCode;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorMsg;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorData;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object details;
}
