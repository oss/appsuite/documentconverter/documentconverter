/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server;

import lombok.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DataSourceFileInputStream extends ObjectInputStream {

    private static final Set<String> DC_DATASOURCE_CLASS_NAMES = Collections.synchronizedSet(new HashSet<>());

    private static final Set<String> DC_DATASOURCE_ALLOWED_JRE_CLASSES = new HashSet<>();

    static {
        final String[] allowedJREClasses = {
                "[B",
                "java.lang.Boolean",
                "java.lang.Double",
                "java.lang.Integer",
                "java.lang.Long",
                "java.lang.Number",
                "java.lang.Short",
                "java.lang.String",
                "java.util.Date",
                "java.util.HashMap"
        };

        DC_DATASOURCE_ALLOWED_JRE_CLASSES.addAll(Arrays.asList(allowedJREClasses));
    }

    public static void registerDCDataSourceClassName(@NonNull final String className) {
        DC_DATASOURCE_CLASS_NAMES.add(className);
    }

    protected DataSourceFileInputStream() throws IOException, SecurityException {
        super();
    }

    public DataSourceFileInputStream(InputStream inputStm) throws IOException {
        super(inputStm);
    }

    /* (non-Javadoc)
     * @see java.io.ObjectInputStream#readClassDescriptor()
     */
    @Override
    public ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
        var objStmClass = super.readClassDescriptor();
        var className = (null != objStmClass) ? objStmClass.getName() : null;
        var isInternalClass = className != null && className.startsWith("com.openexchange.");

        // try to map unprefixed class name to actually available class (release compat.)
        if (className != null && isInternalClass && !DC_DATASOURCE_CLASS_NAMES.contains(className)) {
            var lastDotPos = className.lastIndexOf('.');
            // TODO NEXT wanted to check here whether the dot is at the end? If so, this must be changed.
            var unprefixedClassName = (lastDotPos > -1) && (lastDotPos < className.length()) ?
                    className.substring(lastDotPos + 1) :
                    null;

            if (unprefixedClassName != null) {
                synchronized (DC_DATASOURCE_CLASS_NAMES) {
                    for (var curClassName : DC_DATASOURCE_CLASS_NAMES) {
                        // use class, whose unprefixed class name matches
                        if (curClassName.endsWith(unprefixedClassName)) {
                            return ObjectStreamClass.lookup(Class.forName(curClassName));
                        }
                    }
                }
            }
        }

        if (!isInternalClass && className != null && !DC_DATASOURCE_ALLOWED_JRE_CLASSES.contains(className)) {
            throw new IOException("DC detected JRE class that is not allowed to be deserialized: " + className);
        }

        return objStmClass;
    }

}
