package com.openexchange.documentconverter.server.metrics;

import io.micrometer.core.instrument.Tags;
import lombok.NonNull;

public enum TagsType {
    ASYNC("happening", "async"),
    CACHE("type", "cache"),
    CONVERSIONS("type", "conversions"),
    ERROR_GENERAL("error", "general"),
    ERROR_TIMEOUT("error", "timeout"),
    ERROR_DISPOSED("error", "disposed"),
    ERROR_PASSWORD("error", "password"),
    ERROR_NO_CONTENT("error", "noContent"),
    ERROR_OUT_OF_MEMORY("error", "outOfMemory"),
    JOBS("type", "jobs"),
    JOBS_ASYNC(JOBS.getTags().and(ASYNC.getTags())),
    PRIO_BACKGROUND("priority", "background"),
    PRIO_LOW("priority", "low"),
    PRIO_MEDIUM("priority", "medium"),
    PRIO_HIGH("priority", "high"),
    PRIO_INSTANT("priority", "instant"),
    PRIO_ALL("priority", "all"),
    QUEUE("type", "queue"),
    QUEUE_ASYNC(QUEUE.getTags().and(ASYNC.getTags()));

    private Tags tags;

    TagsType(@NonNull String key, @NonNull String value) {
        this.tags = Tags.of(key, value);
    }

    TagsType(@NonNull Tags tags) {
        this.tags = tags;
    }

    @NonNull public Tags getTags() {
        return tags;
    }
}
