/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.metrics;

import com.openexchange.documentconverter.server.job.JobPriority;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class MetricsUtils {

    private MetricsUtils(){}

    public static long getMedianTimeMillis(@NonNull Map<JobPriority, ArrayList<AtomicLong>> timesMap, JobPriority jobPriority) {
        var result = 0.0;
        ArrayList<AtomicLong> workList;

        if (jobPriority == null) {
            workList = new ArrayList<>();

            for (var curPriority : JobPriority.values()) {
                workList.addAll(timesMap.get(curPriority));
            }
        } else {
            workList = timesMap.get(jobPriority);
        }

        if (workList != null) {
            var longArray = new long[workList.size()];

            if (longArray.length > 0) {
                var medianIndex = longArray.length / 2;

                for (var i = 0; i < workList.size(); i++) {
                    longArray[i] = workList.get(i).get();
                }

                Arrays.sort(longArray);

                // calculate average of left and right values wrt. the median
                // position for even size lists
                if (longArray.length % 2 == 0) {
                    result = (longArray[medianIndex - 1] + longArray[medianIndex]) * 0.5;
                } else {
                    result = longArray[medianIndex];
                }
            }
        }

        return Math.round(result);
    }
}
