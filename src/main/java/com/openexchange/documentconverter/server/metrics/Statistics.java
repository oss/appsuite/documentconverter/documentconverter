package com.openexchange.documentconverter.server.metrics;

import com.openexchange.documentconverter.server.*;
import com.openexchange.documentconverter.server.error.JobError;
import com.openexchange.documentconverter.server.error.JobErrorEx;
import com.openexchange.documentconverter.server.job.*;
import com.openexchange.documentconverter.server.logging.LogData;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Component
public class Statistics {

    @Autowired
    private MetricsRegistry metricsRegistry;

    private static final String DC_STATISTICS_TIMER_NAME = "DC Statistics timer";
    // set period of reset task to 5min, which equals to the munin standard period
    private static final long RESET_TIMER_PERIOD_MILLISECONDS = 5 * 60 * 1000;
    // set ahead time to periodically retrieve results this time
    // span before the next external and periodic retrieval
    private static final long RESET_TIMER_AHEAD_TIME = 30 * 1000;

    private final AtomicInteger peakTotalQueueCount = new AtomicInteger();
    private final AtomicInteger curTotalQueueCount = new AtomicInteger();
    private final AtomicInteger peakAsyncQueueCount = new AtomicInteger();
    private final AtomicInteger curAsyncQueueCount = new AtomicInteger();
    private final AtomicLong jobTimesTotalResult = new AtomicLong();
    private final Map<JobPriority, ArrayList<AtomicLong>> queueTimes = new EnumMap<>(JobPriority.class);
    private final Map<JobPriority, ArrayList<AtomicLong>> conversionTimes = new EnumMap<>(JobPriority.class);
    private final Map<JobPriority, ArrayList<AtomicLong>> jobTimes = new EnumMap<>(JobPriority.class);
    private final Map<JobPriority, AtomicInteger> peakQueueCounts = new EnumMap<>(JobPriority.class);
    private final Map<JobPriority, AtomicInteger> curQueueCounts = new EnumMap<>(JobPriority.class);
    private final Map<JobPriority, AtomicLong> jobTimesResult = new EnumMap<>(JobPriority.class);
    private final Timer resetTimer = new Timer(DC_STATISTICS_TIMER_NAME, true);
    private final AtomicLong jobsProcessed = new AtomicLong();
    private final AtomicLong jobErrorsTimeout = new AtomicLong();
    private final AtomicLong jobErrorsTotal = new AtomicLong();
    private final AtomicInteger peakTotalQueueCountResult = new AtomicInteger();
    private final Map<JobPriority, AtomicInteger> peakQueueCountsResult = new EnumMap<>(JobPriority.class);
    private final AtomicInteger peakAsyncQueueCountResult = new AtomicInteger();
    private final AtomicLong queueTimesTotalResult = new AtomicLong();
    private final Map<JobPriority, AtomicLong> queueTimesResult = new EnumMap<>(JobPriority.class);
    private final AtomicLong cacheHits = new AtomicLong();
    private final AtomicLong errorCacheHits = new AtomicLong();
    private final Map<BackendType, AtomicLong> conversionCounts = new EnumMap<>(BackendType.class);
    private final AtomicLong multipleConversionRunCount = new AtomicLong();
    private final Map<JobError, AtomicLong> conversionResult = new EnumMap<>(JobError.class);
    private final AtomicLong conversionTimesTotalResult = new AtomicLong();
    private final Map<JobPriority, AtomicLong> conversionTimesResult = new EnumMap<>(JobPriority.class);
    private TimerTask resetTask = null;
    private final AtomicInteger pendingStatefulJobCount = new AtomicInteger();
    private final AtomicLong asyncJobProcessedCount = new AtomicLong();
    private final AtomicLong asyncJobDroppedCount = new AtomicLong();
    private final AtomicLong asyncServerExecutorOmitCounter = new AtomicLong(0);
    private final AtomicInteger scheduledJobCountInQueue = new AtomicInteger(0);
    private final AtomicLong localCachePersistentSize = new AtomicLong(0);

    @PostConstruct
    public void init() {

        // initialize all periodic members
        for (var jobPriority : JobPriority.values()) {
            curQueueCounts.put(jobPriority, new AtomicInteger());
            peakQueueCounts.put(jobPriority, new AtomicInteger());
            peakQueueCountsResult.put(jobPriority, new AtomicInteger());

            queueTimes.put(jobPriority, new ArrayList<>());
            queueTimesResult.put(jobPriority, new AtomicLong());

            conversionTimes.put(jobPriority, new ArrayList<>());
            conversionTimesResult.put(jobPriority, new AtomicLong());

            jobTimes.put(jobPriority, new ArrayList<>());
            jobTimesResult.put(jobPriority, new AtomicLong());
        }

        // initialize
        for (var jobError : JobError.values()) {
            conversionResult.put(jobError, new AtomicLong());
        }

        for (var backendType : BackendType.values()) {
            conversionCounts.put(backendType, new AtomicLong());
        }

        registerMetrics();
    }

    public void registerMetrics() {
        metricsRegistry.addMetric("jobs.processed.total", this, (v) -> getJobsProcessed(), TagsType.JOBS, "The total number of jobs that have been processed.");
        metricsRegistry.addMetric("jobs.processed.error.timeout.total", this, (v) -> getJobErrorsTimeout(), TagsType.JOBS, "The total number of jobs, that returned with a timeout error.");
        metricsRegistry.addMetric("jobs.processed.error.total", this, (v) -> getJobErrorsTotal(), TagsType.JOBS, "The total number of jobs, that returned with an error.");

        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(JobPriority.BACKGROUND), TagsType.PRIO_BACKGROUND, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(JobPriority.LOW), TagsType.PRIO_LOW, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(JobPriority.MEDIUM), TagsType.PRIO_MEDIUM, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(JobPriority.HIGH), TagsType.PRIO_HIGH, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(JobPriority.INSTANT), TagsType.PRIO_INSTANT, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time", this, (v) -> getMedianJobTimeMillis(null), TagsType.PRIO_ALL, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");

        metricsRegistry.addMetric("jobs.stateful.pending.count", this, (v) -> getPendingStatefulJobCount(), TagsType.JOBS, "The number of stateful job conversions for that no endConvert has been called up to now.");
        metricsRegistry.addMetric("jobs.scheduled.total", this, (v) -> getScheduledJobCountInQueue(), TagsType.JOBS, "The current number of all scheduled, synchronous jobs within the queue, waiting to be processed.");

        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(JobPriority.BACKGROUND), TagsType.PRIO_BACKGROUND, "The peak number of jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(JobPriority.LOW), TagsType.PRIO_LOW);
        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(JobPriority.MEDIUM), TagsType.PRIO_MEDIUM);
        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(JobPriority.HIGH), TagsType.PRIO_HIGH);
        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(JobPriority.INSTANT), TagsType.PRIO_INSTANT);
        metricsRegistry.addMetric("jobs.peak.count", this, (v) -> getPeakJobCountInQueue(null), TagsType.PRIO_ALL);

        metricsRegistry.addMetric("jobs.asyncqueue.scheduled.total", this, (v) -> getAsyncJobCountScheduled(), TagsType.JOBS_ASYNC, "The total number of asynchronous jobs that are currently scheduled.");
        metricsRegistry.addMetric("jobs.asyncqueue.processed.total", this, (v) -> getAsyncJobProcessedCount(), TagsType.JOBS_ASYNC, "The total number of asynchronous jobs that have been processed.");
        metricsRegistry.addMetric("jobs.asyncqueue.dropped.total", this, (v) -> getAsyncJobCountDropped(), TagsType.JOBS_ASYNC, "The total number of asynchronous jobs that have been dropped.");
        metricsRegistry.addMetric("jobs.asyncqueue.peak.total", this, (v) -> getPeakAsyncJobCount(), TagsType.QUEUE_ASYNC, "The peak number of asynchronous jobs that have been scheduled, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");

        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(JobPriority.BACKGROUND), TagsType.PRIO_BACKGROUND, BaseUnit.MILLISECONDS, "The median time in milliseconds, a job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(JobPriority.LOW), TagsType.PRIO_LOW, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(JobPriority.MEDIUM), TagsType.PRIO_MEDIUM, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(JobPriority.HIGH), TagsType.PRIO_HIGH, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(JobPriority.INSTANT), TagsType.PRIO_INSTANT, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("jobs.median.time.waiting", this, (v) -> getMedianQueueTimeMillis(null), TagsType.PRIO_ALL, BaseUnit.MILLISECONDS);

        metricsRegistry.addMetric("cache.free.volume.size", this, (v) -> getLocalCacheFreeVolumeSize(), TagsType.CACHE, "The current free size in bytes of the volume, the cache root directory is located on. The size is calculated on a volume block size basis to reflect the correct size of the free volume.");
        metricsRegistry.addMetric("cache.hit.ratio", this, (v) -> getCacheHitRatio(), TagsType.CACHE,"The age of the oldest entry within the cache in seconds.");
        metricsRegistry.addMetric("errorcache.hit.ratio", this, (v) -> getErrorCacheHitRatio(), TagsType.CACHE,"The ratio of job errors that are retrieved from the errror cache against the total number of job errors as simple quotient.");

        metricsRegistry.addMetric("conversions.readerengine.and.pdftool.total", this, (v) -> getConversionCount(null), TagsType.CONVERSIONS,"The total number of all physical ReaderEngine and PDFTool conversions.");
        metricsRegistry.addMetric("conversions.readerengine.total", this, (v) -> getConversionCount(BackendType.READERENGINE), TagsType.CONVERSIONS,"The total number of all physical ReaderEngine conversions.");
        metricsRegistry.addMetric("conversions.pdftool.total", this, (v) -> getConversionCount(BackendType.PDFTOOL), TagsType.CONVERSIONS,"The total number of all physical PDFTool conversions.");
        metricsRegistry.addMetric("conversions.needed.new.readerengine.total", this, (v) -> getConversionCountWithMultipleRuns(), TagsType.CONVERSIONS,"The total number of physical ReaderEngine conversions, that needed a second, new ReaderEngine instance.");

        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsGeneral(), TagsType.ERROR_GENERAL,"The total number of physical ReaderEngine conversions, that resulted in an error.");
        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsTimeout(), TagsType.ERROR_TIMEOUT);
        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsDisposed(), TagsType.ERROR_DISPOSED);
        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsPassword(), TagsType.ERROR_PASSWORD);
        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsNoContent(), TagsType.ERROR_NO_CONTENT);
        metricsRegistry.addMetric("conversions.readerengine.error.total", this, (v) -> getConversionErrorsOutOfMemory(), TagsType.ERROR_OUT_OF_MEMORY);
        metricsRegistry.addMetric("conversions.pdftool.error.total", this, (v) -> getConversionErrorsPDFTool(), TagsType.CONVERSIONS);

        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(JobPriority.BACKGROUND), TagsType.PRIO_BACKGROUND, BaseUnit.MILLISECONDS, "The median time in milliseconds, a physical ReaderEngine conversion with lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes).");
        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(JobPriority.LOW), TagsType.PRIO_LOW, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(JobPriority.MEDIUM), TagsType.PRIO_MEDIUM, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(JobPriority.HIGH), TagsType.PRIO_HIGH, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(JobPriority.INSTANT), TagsType.PRIO_INSTANT, BaseUnit.MILLISECONDS);
        metricsRegistry.addMetric("conversions.readerengine.median.time", this, (v) -> getMedianConversionTimeMillis(null), TagsType.PRIO_ALL, BaseUnit.MILLISECONDS);

    }

    public synchronized void statusChanged(String jobId, Object data) {
        if (data instanceof JobEntry changedJobEntry) {
            var changedJobStatus = changedJobEntry.getJobStatus();
            var changedJobPriority = changedJobEntry.getJobPriority();
            var curQueueCount = 0;

            if (changedJobStatus == JobStatus.SCHEDULED) {
                if (!changedJobEntry.isMultipleSchedule()) {
                    // entry has just been added to queue
                    curTotalQueueCount.incrementAndGet();
                    curQueueCount = curQueueCounts.get(changedJobPriority).incrementAndGet();
                }
            } else if ((JobStatus.FINISHED == changedJobStatus) || (JobStatus.ERROR == changedJobStatus)) {
                var jobErrorEx = (JobStatus.FINISHED == changedJobStatus) ? new JobErrorEx() : changedJobEntry.getJobErrorEx();

                // increment general counters
                incrementProcessedJobCount(jobErrorEx, changedJobEntry.isCacheHit() || changedJobEntry.isErrorCacheHit());

                // update queue and conversion times
                queueTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getQueueTimeMillis()));
                conversionTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getConversionTimeMillis()));
                jobTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getJobTimeMillis()));
            } else if (JobStatus.REMOVED == changedJobStatus) {
                // entry has just been removed from queue
                if (curTotalQueueCount.get() > 0) {
                    curTotalQueueCount.decrementAndGet();
                }

                var curAtomicInteger = curQueueCounts.get(changedJobPriority);

                if (curAtomicInteger.get() > 0) {
                    curQueueCount = curAtomicInteger.decrementAndGet();
                }
            }

            // adjust appropriate queue counts
            if (curTotalQueueCount.get() > peakTotalQueueCount.get()) {
                peakTotalQueueCount.set(curTotalQueueCount.get());
            }

            var curPeakQueueCount = peakQueueCounts.get(changedJobPriority);

            if (curQueueCount > curPeakQueueCount.get()) {
                curPeakQueueCount.set(curQueueCount);
            }
        }
    }

    public long incrementAndGetAsyncServerExecutorOmitCounter() {
        return asyncServerExecutorOmitCounter.incrementAndGet();
    }

    public LogData[] getAsyncServerExcutorLogData() {
        return new LogData[] {
                new LogData("async_jobs_remaining", Long.toString(getAsyncJobCountScheduled())),
                new LogData("async_jobs_ran", Long.toString(getAsyncJobProcessedCount())),
                new LogData("async_job_omitted", asyncServerExecutorOmitCounter.toString())
        };
    }

    public synchronized void incrementProcessedJobCount(JobErrorEx jobErrorEx, boolean incrementCacheCounts) {
        jobsProcessed.incrementAndGet();

        if (jobErrorEx.hasNoError()) {
            if (incrementCacheCounts) {
                cacheHits.incrementAndGet();
            }
        } else {
            jobErrorsTotal.incrementAndGet();

            if (incrementCacheCounts) {
                errorCacheHits.incrementAndGet();

                if (JobError.TIMEOUT == jobErrorEx.getJobError()) {
                    jobErrorsTimeout.incrementAndGet();
                }
            }
        }
    }

    public void incrementConversionCount(@NonNull BackendType backendType) {
        conversionCounts.get(backendType).incrementAndGet();
    }

    public void incrementConversionWithMultipleRunsCount() {
        multipleConversionRunCount.incrementAndGet();
    }

    public void incrementConversionErrorCount(JobError jobError) {
        conversionResult.get(jobError).incrementAndGet();
    }

    public synchronized void incrementAsyncQueueCount() {
        var asyncQueueCount = curAsyncQueueCount.incrementAndGet();

        if (asyncQueueCount > peakAsyncQueueCount.get()) {
            peakAsyncQueueCount.set(asyncQueueCount);
        }
    }

    public synchronized void decrementAsyncQueueCount() {
        if (curAsyncQueueCount.get() > 0) {
            curAsyncQueueCount.decrementAndGet();
        }
    }

    public void incrementStatefulJobCount() {
        pendingStatefulJobCount.incrementAndGet();
    }

    public void decrementStatefulJobCount() {
        pendingStatefulJobCount.decrementAndGet();
    }

    public void terminate() {
        resetTimer.cancel();
    }

    public long getJobsProcessed() {
        return jobsProcessed.get();
    }

    public long getJobErrorsTimeout() {
        return jobErrorsTimeout.get();
    }

    public long getJobErrorsTotal() {
        return jobErrorsTotal.get();
    }

    public synchronized long getMedianJobTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return (jobPriority == null) ? jobTimesTotalResult.get() : jobTimesResult.get(jobPriority).get();
    }

    public long getPendingStatefulJobCount() {
        return pendingStatefulJobCount.get();
    }

    public synchronized int getPeakJobCountInQueue(JobPriority jobPriority) {
        var peakCount = 0;

        initPeriodicResetTimer();

        if (jobPriority == null) {
            peakCount = peakTotalQueueCountResult.get();
        } else {
            peakCount = peakQueueCountsResult.get(jobPriority).get();
        }

        return peakCount;
    }

    public int getAsyncJobCountScheduled() {
        return curAsyncQueueCount.get();
    }

    public void incrementAsyncJobProcessedCount() {
        asyncJobProcessedCount.incrementAndGet();
    }
    public long getAsyncJobProcessedCount() {
        return asyncJobProcessedCount.get();
    }

    public void incrementAsyncJobDroppedCount() {
        asyncJobDroppedCount.incrementAndGet();
    }
    public long getAsyncJobCountDropped() {
        return asyncJobDroppedCount.get();
    }

    /**
     * @return The current number of jobs in queue
     */
    public synchronized int getPeakAsyncJobCount() {
        initPeriodicResetTimer();
        return peakAsyncQueueCountResult.get();
    }

    public synchronized long getMedianQueueTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return (null == jobPriority) ?
                queueTimesTotalResult.get() :
                queueTimesResult.get(jobPriority).get();
    }

    public double getCacheHitRatio() {
        var jobsProcessedCount = jobsProcessed.get();
        if (jobsProcessedCount > 0) {
            return cacheHits.get() / (double) jobsProcessedCount;
        }
        return 1.0;
    }

    public long getErrorCacheHits() {
        return jobsProcessed.get() > 0 ? errorCacheHits.get() : 0;
    }

    public double getErrorCacheHitRatio() {
        var jobErrorsTotalCount = jobErrorsTotal.get();
        if (jobErrorsTotalCount > 0) {
            return errorCacheHits.get() / (double) jobErrorsTotalCount;
        }
        return 0.0;
    }

    public long getConversionCount(BackendType backendType) {
        return (null == backendType) ?
                conversionCounts.get(BackendType.READERENGINE).get() + conversionCounts.get(BackendType.PDFTOOL).get() :
                conversionCounts.get(backendType).get();
    }

    public long getConversionCountWithMultipleRuns() {
        return multipleConversionRunCount.get();
    }

    public long getConversionErrorsGeneral() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.GENERAL).get();
    }
    public long getConversionErrorsTimeout() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.TIMEOUT).get();
    }

    public long getConversionErrorsDisposed() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.DISPOSED).get();
    }

    public long getConversionErrorsPassword() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.PASSWORD).get();
    }

    public long getConversionErrorsOutOfMemory() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.OUT_OF_MEMORY).get();
    }

    public long getConversionErrorsPDFTool() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.PDFTOOL).get();
    }

    public long getConversionErrorsNoContent() {
        // TODO NEXT use object for metrics and remove class or only one method conversionResult(JobError)
        return conversionResult.get(JobError.NO_CONTENT).get();
    }

    public long getCacheHits() {
        return jobsProcessed.get() > 0 ? cacheHits.get() : 0;
    }

    public synchronized long getMedianConversionTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return jobPriority == null ?
                conversionTimesTotalResult.get() :
                conversionTimesResult.get(jobPriority).get();
    }

    private void initPeriodicResetTimer() {
        if (resetTask == null) {
            // the first values are retrieved => call reset period to retrieve
            // latest results
            resetMeasurementPeriod();

            // the periodically called reset task
            resetTask = new TimerTask() {

                /*
                 * (non-Javadoc)
                 * @see com.openexchange.documentconverter.impl.Statistics.ResetTask #run()
                 */
                @Override
                public void run() {
                    resetMeasurementPeriod();
                }
            };

            // start the timer to periodically retrieve further result sets
            // a short amount of time before the next results will be requested
            // with a fixed period from external tools, e.g. 300s with munin
            resetTimer.scheduleAtFixedRate(
                    resetTask,
                    RESET_TIMER_PERIOD_MILLISECONDS - RESET_TIMER_AHEAD_TIME,
                    RESET_TIMER_PERIOD_MILLISECONDS);
        }
    }

    public synchronized void resetMeasurementPeriod() {
        // standard queue
        peakTotalQueueCountResult.set(peakTotalQueueCount.get());
        peakTotalQueueCount.set(curTotalQueueCount.get());

        // async queue
        peakAsyncQueueCountResult.set(peakAsyncQueueCount.get());
        peakAsyncQueueCount.set(curAsyncQueueCount.get());

        // median total times
        queueTimesTotalResult.set(MetricsUtils.getMedianTimeMillis(queueTimes, null));
        conversionTimesTotalResult.set(MetricsUtils.getMedianTimeMillis(conversionTimes, null));
        jobTimesTotalResult.set(MetricsUtils.getMedianTimeMillis(jobTimes, null));

        for (var jobPriority : JobPriority.values()) {
            // save PeakJobCount results and reset working map
            peakQueueCountsResult.get(jobPriority).set(peakQueueCounts.get(jobPriority).get());
            peakQueueCounts.get(jobPriority).set(curQueueCounts.get(jobPriority).get());

            // save QueueTimes results and reset working Map
            queueTimesResult.put(jobPriority, new AtomicLong(MetricsUtils.getMedianTimeMillis(queueTimes, jobPriority)));
            queueTimes.get(jobPriority).clear();

            // save ExecutionTimes results and reset working Map
            conversionTimesResult.put(jobPriority, new AtomicLong(MetricsUtils.getMedianTimeMillis(conversionTimes, jobPriority)));
            conversionTimes.get(jobPriority).clear();

            // save JobTimes results and reset working Map
            jobTimesResult.put(jobPriority, new AtomicLong(MetricsUtils.getMedianTimeMillis(jobTimes, jobPriority)));
            jobTimes.get(jobPriority).clear();
        }
    }

    public int getScheduledJobCountInQueue() {
        return scheduledJobCountInQueue.get();
    }

    public int incrementScheduledJobCountInQueue() {
        return scheduledJobCountInQueue.incrementAndGet();
    }

    public int decrementScheduledJobCountInQueue() {
        return scheduledJobCountInQueue.decrementAndGet();
    }

    public long getLocalCachePersistentSize() {
        return localCachePersistentSize.get();
    }

    public void addLocalCachePersistentSize(long value) {
        localCachePersistentSize.addAndGet(value);
    }

    public void subtractLocalCachePersistentSize(long value) {
        localCachePersistentSize.addAndGet(-value);
    }

    private final AtomicLong localCacheFreeVolumeSize = new AtomicLong(0);

    public long getLocalCacheFreeVolumeSize() {
        return localCacheFreeVolumeSize.get();
    }

    public void setLocalCacheFreeVolumeSize(long value) {
        localCacheFreeVolumeSize.set(value);
    }

    public void addLocalCacheFreeVolumeSize(long value) {
        localCacheFreeVolumeSize.addAndGet(value);
    }

    public void subtractLocalCacheFreeVolumeSize(long value) {
        localCacheFreeVolumeSize.addAndGet(-value);
    }
}
