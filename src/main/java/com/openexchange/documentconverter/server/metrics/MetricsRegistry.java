package com.openexchange.documentconverter.server.metrics;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.function.ToDoubleFunction;

@Component
public class MetricsRegistry {

    public static final String NAME_PREFIX = "documentconverter.";

    public <T> void addMetric(@NonNull String name, T obj, @NonNull ToDoubleFunction<T> function, Tags tags, String baseUnit, String description) {
        var gaugeBuilder = Gauge.builder(NAME_PREFIX + name, obj, function);
        if (StringUtils.isNotEmpty(description)) {
            gaugeBuilder.description(description);
        }
        if (tags != null) {
            gaugeBuilder.tags(tags);
        }
        if (baseUnit != null) {
            gaugeBuilder.baseUnit(baseUnit);
        }
        gaugeBuilder.register(Metrics.globalRegistry);
    }

    public <T> void addMetric(@NonNull String name, T obj, @NonNull ToDoubleFunction<T> function, TagsType tagType, String description) {
        addMetric(name, obj, function, tagType.getTags(), null, description);
    }

    public <T> void addMetric(@NonNull String name, T obj, @NonNull ToDoubleFunction<T> function, TagsType tagType) {
        addMetric(name, obj, function, tagType.getTags(), null, null);
    }

    public <T> void addMetric(@NonNull String name, T obj, @NonNull ToDoubleFunction<T> function, TagsType tagType, BaseUnit baseUnit) {
        addMetric(name, obj, function, tagType.getTags(), baseUnit.getUnit(), null);
    }

    public <T> void addMetric(@NonNull String name, T obj, @NonNull ToDoubleFunction<T> function, TagsType tagType, @NonNull BaseUnit baseUnit, String description) {
        addMetric(name, obj, function, tagType.getTags(), baseUnit.getUnit(), description);
    }
}
