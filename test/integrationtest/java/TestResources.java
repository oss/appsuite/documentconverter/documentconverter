/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

public class TestResources {

    private static final String XLSX_RESOURCE_NAME = "test.xlsx";
    private static final String PPTX_RESOURCE_NAME = "test.pptx";
    private static final String ODT_RESOURCE_NAME = "test.odt";
    private static final String DOCX_RESOURCE_NAME = "test.docx";
    private static final String PDF_RESOURCE_NAME = "test.pdf";

    public enum DocType {
        XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
        DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
        PPTX("application/vnd.openxmlformats-officedocument.presentationml.presentation"),
        ODT("application/vnd.oasis.opendocument.text"),
        PDF("application/pdf");

        private final String mimeType;

        DocType(String mimeType) {
            this.mimeType = mimeType;
        }

        public String getMimeType() {
            return mimeType;
        }
    }

    public record DocFileData(File file, DocType docType, String mimeType) {}

    public static DocFileData getTestDocumentFile(DocType docType) throws URISyntaxException {
        File file;

        switch (docType) {
            case XLSX -> file = getResourceFile(XLSX_RESOURCE_NAME);
            case DOCX -> file = getResourceFile(DOCX_RESOURCE_NAME);
            case PPTX -> file = getResourceFile(PPTX_RESOURCE_NAME);
            case ODT -> file = getResourceFile(ODT_RESOURCE_NAME);
            case PDF -> file = getResourceFile(PDF_RESOURCE_NAME);
            default -> throw new IllegalArgumentException("Unsupported document type: " + docType);
        }

        return new DocFileData(file, docType, docType.getMimeType());
    }

    private static File getResourceFile(String resourceName) throws URISyntaxException {
        var resourceFilePath = Path.of(ClassLoader.getSystemResource(resourceName).toURI());
        return resourceFilePath.toFile();
    }

}
