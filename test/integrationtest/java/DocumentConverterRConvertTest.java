/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class DocumentConverterRConvertTest extends DocumentConverterMain {

    private final Tika tika = new Tika();
    private final Base64.Decoder decoder = Base64.getDecoder();

    @BeforeEach
    public void setUp() {
        waitForServerConnection();
    }

    public static Stream<Arguments> docTypes() {
        return Stream.of(Arguments.of(TestResources.DocType.DOCX),
                         Arguments.of(TestResources.DocType.ODT),
                         Arguments.of(TestResources.DocType.PPTX),
                         Arguments.of(TestResources.DocType.XLSX));
    }

    // process (action=rconvert, method=convert, jobtype=pdf, jobid=n/a, priority=instant, locale=en_US, cachehash=n/a, remotecachehash=cb68e37d9325850d04447c0d33f733bb5cdd699e6a33fc113fee9eb0ec8ccbee-en_US-pdf#1-99999, filtershortname=n/a, inputtype=n/a, inputurl=file:///Drive/My files/Documents/unnamed (1).docx, pixelwidth=n/a, pixelheight=n/a, async=false, mimetype=n/a, pagerange=n/a, pagenumber=n/a, shapenumber=n/a, ziparchive=n/a, infofilename=unnamed (1).docx, featuresid=n/a, nocache=false, async=false, imageresolution=n/a, imagescaletype=n/a, remoteurl=http://localhost:8009/documentconverterws, url=n/a, returntype=json, autoscale=false, hidechanges=false, hidecomments=false, userrequest=true, shapereplacements=n/a)
    @DisplayName("rconvert(method=convert, jobtype=pdf, inputtype=n/a, returntype=json, async=false)")
    @ParameterizedTest
    @MethodSource("docTypes")
    public void testRConvert_ConvertToPdfAndJsonResponse(TestResources.DocType docType) throws URISyntaxException, IOException {
        var docFileData = TestResources.getTestDocumentFile(docType);
        var response =
            given().
                spec(getRestRequestSpecification()).
                multiPart(new MultiPartSpecBuilder(docFileData.file()).
                        fileName(docFileData.file().getName()).
                        controlName("sourcefile").
                        mimeType(docType.getMimeType())
                    .build()).
                formParam("action", "rconvert").
                formParam("method", "convert").
                formParam("jobtype", "pdf").
                formParam("priority", "instant").
                formParam("locale", "en-US").
                formParam("infofilename", docFileData.file().getName()).
                formParam("autoscale", false).
                formParam("hidechanges", false).
                formParam("hidecomments", false).
                formParam("userrequest", false).
            when().
                post("/documentconverterws").
            then().
                contentType(ContentType.JSON).
                assertThat().statusCode(200).
            and().
                extract().response();

        assertNotNull(response);
        var jsonBodyData = new JSONObject(response.getBody().asString());
        var errorcode = jsonBodyData.optInt("errorcode");
        var dataUrl = jsonBodyData.optString("result");

        assertEquals(0, errorcode);
        assertNotNull(dataUrl);
        var base64Data = extractBase64FromDataUrl(dataUrl);
        var pdfDataStream = new ByteArrayInputStream(decoder.decode(base64Data));
        var metaData = new Metadata();

        try (TikaInputStream stream = TikaInputStream. get(pdfDataStream)) {
            var mediaType = tika.getDetector().detect(stream, metaData);
            assertEquals("application/pdf", mediaType.toString());
        }
    }

    // process (action=rconvert, method=queryavailability, jobtype=pdf, jobid=n/a, priority=background, locale=en_US, cachehash=n/a, remotecachehash=815c5b48cb971ed3ed3d7ff3a5eb0cc8be218f8b03092de97b7cdea55937509a-en_US-pdf#1-99999, filtershortname=n/a, inputtype=n/a, inputurl=file:///Drive/My files/Documents/unnamed.docx, pixelwidth=n/a, pixelheight=n/a, async=false, mimetype=n/a, pagerange=n/a, pagenumber=n/a, shapenumber=n/a, ziparchive=n/a, infofilename=unnamed.docx, featuresid=n/a, nocache=false, async=false, imageresolution=n/a, imagescaletype=n/a, remoteurl=http://localhost:8009/documentconverterws, url=n/a, returntype=json, autoscale=false, hidechanges=false, hidecomments=false, userrequest=false, shapereplacements=n/a)
    @DisplayName("rconvert(method=queryavailability, jobtype=pdf, inputtype=n/a, returntype=json, async=false)")
    @ParameterizedTest
    @MethodSource("docTypes")
    public void testRConvert_QueryAvailabilityWithoutRemoteCacheHashToPdfAndJsonResponseResultsInError(TestResources.DocType docType) throws URISyntaxException, IOException {
        var docFileData = TestResources.getTestDocumentFile(docType);
        var response =
            given().
                spec(getRestRequestSpecification()).
                multiPart(new MultiPartSpecBuilder(docFileData.file()).
                        fileName(docFileData.file().getName()).
                        controlName("sourcefile").
                        mimeType(docType.getMimeType())
                    .build()).
                formParam("action", "rconvert").
                formParam("method", "queryavailability").
                formParam("jobtype", "pdf").
                formParam("priority", "background").
                formParam("locale", "en-US").
                formParam("infofilename", docFileData.file().getName()).
                formParam("autoscale", false).
                formParam("hidechanges", false).
                formParam("hidecomments", false).
                formParam("userrequest", false).
            when().
                post("/documentconverterws").
            then().
                contentType(ContentType.JSON).
                assertThat().statusCode(200).
            and().
                extract().response();

        assertNotNull(response);

        var jsonBodyData = new JSONObject(response.getBody().asString());
        var errorcode = jsonBodyData.optInt("errorcode");

        assertEquals(1, errorcode); // JobError.GENERAL
    }

    // process (action=rconvert, method=queryavailability, jobtype=pdf, jobid=n/a, priority=background, locale=en_US, cachehash=n/a, remotecachehash=815c5b48cb971ed3ed3d7ff3a5eb0cc8be218f8b03092de97b7cdea55937509a-en_US-pdf#1-99999, filtershortname=n/a, inputtype=n/a, inputurl=file:///Drive/My files/Documents/unnamed.docx, pixelwidth=n/a, pixelheight=n/a, async=false, mimetype=n/a, pagerange=n/a, pagenumber=n/a, shapenumber=n/a, ziparchive=n/a, infofilename=unnamed.docx, featuresid=n/a, nocache=false, async=false, imageresolution=n/a, imagescaletype=n/a, remoteurl=http://localhost:8009/documentconverterws, url=n/a, returntype=json, autoscale=false, hidechanges=false, hidecomments=false, userrequest=false, shapereplacements=n/a)
    @DisplayName("rconvert(method=queryavailability, jobtype=pdf, inputtype=n/a, returntype=json, async=false)")
    @ParameterizedTest
    @MethodSource("docTypes")
    public void testRConvert_QueryAvailabilityWithRemoteCacheHashToPdfAndJsonResponse(TestResources.DocType docType) throws URISyntaxException, IOException {
        var docFileData = TestResources.getTestDocumentFile(docType);
        var remoteCacheHash = generateRemoteCacheHash(docFileData.file(), null, "pdf", "1-99999");

        var convertResponse =
            given().
                spec(getRestRequestSpecification()).
                multiPart(new MultiPartSpecBuilder(docFileData.file()).
                    fileName(docFileData.file().getName()).
                    controlName("sourcefile").
                    mimeType(docType.getMimeType())
                    .build()).
                formParam("action", "rconvert").
                formParam("method", "convert").
                formParam("jobtype", "pdf").
                formParam("priority", "instant").
                formParam("locale", "en-US").
                formParam("remotecachehash", remoteCacheHash).
                formParam("infofilename", docFileData.file().getName()).
                formParam("autoscale", false).
                formParam("hidechanges", false).
                formParam("hidecomments", false).
                formParam("userrequest", false).
            when().
                post("/documentconverterws").
            then().
                contentType(ContentType.JSON).
                assertThat().statusCode(200).
            and().
                extract().response();

        assertNotNull(convertResponse);
        var jsonBodyData = new JSONObject(convertResponse.getBody().asString());
        var errorcode = jsonBodyData.optInt("errorcode");
        var dataUrl = jsonBodyData.optString("result");

        assertEquals(0, errorcode);
        assertNotNull(dataUrl);

        var queryAvailresponse =
            given().
                spec(getRestRequestSpecification()).
                multiPart("action", "rconvert"). // workaround setting contentType to "application/form-data" errors with encoding issues
                formParam("method", "queryavailability").
                formParam("jobtype", "pdf").
                formParam("priority", "background").
                formParam("locale", "en-US").
                formParam("remotecachehash", remoteCacheHash).
                formParam("infofilename", docFileData.file().getName()).
                formParam("autoscale", false).
                formParam("hidechanges", false).
                formParam("hidecomments", false).
                formParam("userrequest", false).
            when().
                post("/documentconverterws").
            then().
                contentType(ContentType.JSON).
                assertThat().statusCode(200).
            and().
                extract().response();

        assertNotNull(queryAvailresponse);

        jsonBodyData = new JSONObject(queryAvailresponse.getBody().asString());
        errorcode = jsonBodyData.optInt("errorcode");

        assertEquals(0, errorcode);
    }

    private String extractBase64FromDataUrl(String dataUrl) {
        return dataUrl.split(",")[1];
    }

    private String generateRemoteCacheHash(File docFile, String locale, String ext, String pageRange) {
        var hashBuilder = new StringBuilder(256);

        try {
            byte[] sha256Checksum;

            try (var inputStream = new FileInputStream(docFile)) {
                sha256Checksum = generateSha256Checksum(inputStream);
            }

            hashBuilder.append(Hex.encodeHexString(sha256Checksum)).append('-');

        } catch (final Exception e) {
            fail("Failed to generate SHA-256 checksum for file: " + docFile.getAbsolutePath(), e);
        }

        // 'en*' is considered to be the default locale; add all other locales to the input file
        // hash in order to be able to perform locale based conversions
        if (StringUtils.isNotEmpty(locale)) {
            hashBuilder.append(locale).append('-');
        }

        if (StringUtils.isNotEmpty(ext)) {
            hashBuilder.append(ext);
        }

        if (StringUtils.isNotEmpty(pageRange)) {
            hashBuilder.append('#').append(pageRange);
        }

        return hashBuilder.toString();
    }

    private static byte[] generateSha256Checksum(InputStream inputStream) throws IOException, IllegalArgumentException {
        var BUFFER_SIZE = 8192;

        try {
            var buffer = new byte[BUFFER_SIZE];
            var digest = MessageDigest.getInstance("SHA-256");

            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                digest.update(buffer, 0, bytesRead);
            }

            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("SHA-256 hashing algorithm is not available", e);
        }
    }

}
