/*
 *
 *  * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 *  * @license AGPL-3.0
 *  *
 *  * This code is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU Affero General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU Affero General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU Affero General Public License
 *  * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *  *
 *  * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *  *
 *
 */

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.SpecificationQuerier;
import logging.RequestLogger;
import logging.ResponseLogger;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static io.restassured.RestAssured.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DocumentConverterMain {

    public static final String PATH_DC_PREFIX = "/documentconverterws";
    public static final String PATH_STATUS = PATH_DC_PREFIX + "/status";
    private final static long MAX_INITIAL_CONNECTION_TIMEOUT_MILLIS = 15000;

    private static String SERVER_PROTOCOL = "http";
    private static String SERVER_HOST = "localhost";
    private static int SERVER_PORT = 8080;

    // Map to store the environment variables from the properties file.
    private static Map<String, String> properties;
    // Read the environment variable from a properties file if the properties file path is set via system env <code>PROPERTIES_FILE</code>
    static {
        var propertiesFilePath = System.getenv("PROPERTIES_FILE");
        if (StringUtils.isNotEmpty(propertiesFilePath)) {
            var propertiesFile = new Properties();
            try {
                propertiesFile.load(new FileInputStream(propertiesFilePath));
                properties = new HashMap<>();
                var keys = propertiesFile.keys();
                while (keys.hasMoreElements()) {
                    var key = keys.nextElement().toString();
                    // Change the property name format to the same as the system env name format. com.x.y To COM_X_Y
                    var keyEvnFormat = key.toUpperCase().replace(".", "_");
                    properties.put(keyEvnFormat, propertiesFile.getProperty(key));
                }
            } catch (IOException e) {
                System.out.println("Warning: Could not load properties file: " + propertiesFilePath + ": " + e.getMessage());
            }
        }
    }

    @BeforeAll
    public void setUp() {
        // setup
    }

    @AfterAll
    public void cleanUp() {
        // cleanup
    }

    public DocumentConverterMain() {
        RestAssured.filters(new RequestLogger(), new ResponseLogger());

        final String serverProtocolFromEnv = getEnv("SERVER_PROTOCOL");
        final String serverHostFromEnv = getEnv("SERVER_HOST");
        final String serverPortFromEnv = getEnv("SERVER_PORT");

        // overwrite server protocol if given
        if (StringUtils.isNotBlank(serverProtocolFromEnv) && ("http".equals(serverProtocolFromEnv))) {
            SERVER_PROTOCOL = serverProtocolFromEnv;
        }

        // overwrite server host if given
        if (StringUtils.isNotBlank(serverHostFromEnv)) {
            SERVER_HOST = serverHostFromEnv;
        }

        // overwrite server port if given
        if (StringUtils.isNotBlank(serverPortFromEnv) && StringUtils.isNumeric(serverPortFromEnv)) {
            try {
                SERVER_PORT = Integer.parseUnsignedInt(serverPortFromEnv);

                if (SERVER_PORT < 1) {
                    throw new NumberFormatException("'SERVER' env var given must be greater than 0");
                }
            } catch (NumberFormatException e) {
                Assertions.fail("Given SERVER_PORT env var is not a valid Integer value");
            }
        }

        System.out.println("Server configuration:");
        System.out.println("SERVER_PROTOCOL=" + SERVER_PROTOCOL);
        System.out.println("SERVER_HOST=" + SERVER_HOST);
        System.out.println("SERVER_PORT=" + SERVER_PORT);
    }

    public void waitForServerConnection() {
        final long connectionCheckEndTimeMillis = System.currentTimeMillis() + MAX_INITIAL_CONNECTION_TIMEOUT_MILLIS;

        var requestSpec = getRestRequestSpecification();
        do {
            //noinspection OverlyBroadCatchBlock
            try {
                if (given().spec(requestSpec).get(PATH_STATUS).statusCode() == 200) {
                    return;
                }
            } catch (Exception e) {
                // wait
            }
        } while (System.currentTimeMillis() < connectionCheckEndTimeMillis);

        fail("Not able to get initial connection to: " +
            SERVER_PROTOCOL + "://" +
            SERVER_HOST + ":" +
            SERVER_PORT);
    }

    protected RequestSpecification getRestRequestSpecification() {
        var restRequestSpec = new RequestSpecBuilder()
            .setBaseUri(SERVER_PROTOCOL + "://" + SERVER_HOST)
            .setPort(SERVER_PORT)
            .build();

        dumpRequestSpecification("RestAssured setup for Documentconverter requests:", restRequestSpec);
        return restRequestSpec;
    }

    private void dumpRequestSpecification(String headline, RequestSpecification requestSpecification) {
        var queryRequest = SpecificationQuerier.query(requestSpecification);
        System.out.println();
        System.out.println(headline);
        System.out.println("==================================================");
        System.out.println("RestAssured.baseURI=" + queryRequest.getBaseUri());
        System.out.println("RestAssured.port=" + queryRequest.getPort());
        System.out.println("RestAssured.proxy=" + queryRequest.getProxySpecification());
        System.out.println();
    }

    /**
     * Get the environment variable via properties file or if not set via system environment variables.
     * @param varName Name of the environment variable
     * @return the value of the environment variable or <code>null</code> if not found.
     */
    private static String getEnv(String varName) {
        String env = null;
        // Try to get the value via properties file
        if (properties != null) {
            env = properties.get(varName);
        }
        // If properties file is not set or the variable was not found in properties file try to get it via system environment variable
        if (env == null) {
            env = System.getenv(varName);
        }
        return env;
    }

}
