/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package logging;

import java.util.ArrayList;
import java.util.List;

public class LoggerUtil {

    private static final List<String> methodLogBlackList = new ArrayList<>();

    private LoggerUtil() {
        // utility class
    }

    public static boolean logRequest() {
        return logTestname(true);
    }

    public static boolean logResponse() {
        return logTestname(false);
    }

    private static boolean logTestname(boolean request) {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        String className = "";
        String methodName = null;
        int lineNumber = -1;
        for (var stackElement : stacktrace) {
            className = stackElement.getClassName();
            if (!className.contains(".")) {
                methodName = stackElement.getMethodName();
                lineNumber = stackElement.getLineNumber();
                break;
            }
        }

        if (methodLogBlackList.contains(methodName.toLowerCase())) {
            return false;
        }

        System.out.println();

        if (methodName != null) {
            System.out.println((request ? "REQUEST:" : "RESPONSE:") + "\t" + className + " "+ methodName + " " + lineNumber);
        }

        return true;
    }
}
